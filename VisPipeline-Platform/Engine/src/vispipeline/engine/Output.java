/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.engine;

import java.awt.Color;


/**
 *
 * @author Rafael
 */
public abstract class Output {

    public abstract void clear();

    public abstract void append(String msg, Color color, boolean bold,
            boolean italic, boolean showtime);

    public static Output instance = new ConsoleOutput();

}
class ConsoleOutput extends Output {

    public void clear() {
        
    }

    public void append(String msg, Color color, boolean bold,
            boolean italic, boolean showtime) {
        System.out.println(msg);
    }

}