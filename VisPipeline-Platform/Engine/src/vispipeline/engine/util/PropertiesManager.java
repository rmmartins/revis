/*
 * Copyright (C) 2011  VICG/LabES, ICMC-USP, Brazil
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package vispipeline.engine.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.Lookup;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class PropertiesManager {

    /** Creates a new instance of PropertiesManager */
    private PropertiesManager(String filename, Class clas) throws IOException {
        Logger.getLogger(toString()).info("new(" + filename + ")...");
        this.filename = filename;

        ClassLoader systemCL = Lookup.getDefault().lookup(ClassLoader.class);

        if (systemCL.getResource(filename) != null) {
//            System.out.println("Loaded '" + filename + "' successfully!");
//            try {
//                File file = new File(systemCL.getResource(filename).toURI());
//
//                if (file.exists()) {
//                    this.properties = new Properties();
//                    FileInputStream fis = new FileInputStream(file);
//                    this.properties.load(fis);
//                    fis.close();
//                } else {
//                    throw new FileNotFoundException("File \'" + filename + "\' does not exist.");
//                }
//            } catch (URISyntaxException ex) {
//                Logger.getLogger(PropertiesManager.class.getName()).log(Level.SEVERE, null, ex);
//            }
            properties = new Properties();
            properties.load(systemCL.getResourceAsStream(filename));
        } else {
//            System.out.println("Couldn't load resource '" + filename + "'.");
            this.properties = new Properties();
        }
    }

    // TODO tirar esse "clas"
    public static PropertiesManager getInstance(String filename, Class clas) throws IOException {
        Logger.getLogger("PropertiesManager").info("getInstance");
        return new PropertiesManager(filename, clas);
    }

    public String getProperty(String id) {
        if (properties == null) {
            return "";
        } else {
            if (properties.containsKey(id)) {
                return properties.getProperty(id);
            } else {
                return "";
            }
        }
    }

    public void setProperty(String id, String value) {
        if (properties == null) {
            properties = new Properties();
        }

        properties.setProperty(id, value);

        try {
            if (getClass().getClassLoader().getResource(filename) != null) {
                File file = new File(getClass().getClassLoader().getResource(filename).toURI());
                FileOutputStream out = new FileOutputStream(file);
                properties.store(out, "Recording the properties...");
                out.flush();
                out.close();
            }
        } catch (URISyntaxException ex) {
            Logger.getLogger(PropertiesManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(PropertiesManager.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private Properties properties;
    private String filename;
}
