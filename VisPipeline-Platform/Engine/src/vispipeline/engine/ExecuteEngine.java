/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.engine;

import org.openide.util.Exceptions;
import vispipeline.engine.pipeline.*;
import java.awt.Color;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import vispipeline.basics.interfaces.AbstractParametersView;
import vispipeline.engine.component.ComponentProxy;
import vispipeline.engine.component.method.InputMethod;
import vispipeline.engine.component.method.MultipleInputMethod;
import vispipeline.engine.component.method.OutputMethod;
import vispipeline.engine.component.method.UniqueInputMethod;
import vispipeline.engine.component.parameter.InputParameter;
import vispipeline.engine.component.parameter.MultipleInputParameter;
import vispipeline.engine.component.parameter.UniqueInputParameter;
import vispipeline.engine.component.parameter.OutputParameter;
import vispipeline.engine.view.WizardDialog;
import vispipeline.engine.view.MessageDialog;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class ExecuteEngine {

    // this should be on the Pipeline class..
//    public static String path = "";

    public ExecuteEngine(Pipeline pipeline) {
        try {
            Properties props = new Properties();
            props.setProperty("java.util.logging.FileHandler.formatter", "java.util.logging.SimpleFormatter");
            props.setProperty("java.util.logging.FileHandler.pattern", "vispipeline.log");
            FileWriter fw = new FileWriter("vispipeline.properties");
            props.store(fw, "");
            fw.close();
            //LogManager.getLogManager().readConfiguration(new FileInputStream("vispipeline.properties"));
            //Logger.getLogger("").addHandler(new FileHandler());
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        } catch (SecurityException ex) {
            Exceptions.printStackTrace(ex);
        }
        this.pipeline = pipeline;
//        path = this.pipeline.getFilename();
        this.threadpoolsize = 5;
    }

    public void start(boolean wizard) throws IOException {
        executor = Executors.newFixedThreadPool(threadpoolsize);

        before();

        //reset the pipeline
        pipeline.reset();
        pipeline.setExecuteWizard(wizard);

        //find the components which does not have input parameters
        ArrayList<ComponentProxy> noInputComponents = getNoInputComponents();

        for (int i = 0; i < noInputComponents.size(); i++) {
            //and execute it
            Executor ex = new Executor(noInputComponents.get(i));
            ex.execute();
        }

        after();
    }

    protected void before() {
        // log?
    }

    protected void after() {
        // log?
    }

    public void stop() {
        if (executor != null && !executor.isTerminated()) {
            executor.shutdown();
            executor.shutdownNow();
        }
    }

    protected ArrayList<ComponentProxy> getNoInputComponents() {
        ArrayList<ComponentProxy> noninput = new ArrayList<ComponentProxy>();

        ArrayList<ComponentProxy> components = pipeline.getComponents();
        for (int i = 0; i < components.size(); i++) {
            ArrayList<InputMethod> inmeth = components.get(i).getInput();
            boolean empty = true;

            for (int j = 0; j < inmeth.size(); j++) {
                if (inmeth.get(j).getParameters().size() > 0) {
                    empty = false;
                    break;
                }
            }

            if (empty) {
                noninput.add(components.get(i));
            }
        }

        return noninput;
    }

    public int getThreadPoolSize() {
        return threadpoolsize;
    }

    public void setThreadPoolSize(int threadpoolsize) {
        this.threadpoolsize = threadpoolsize;
    }

    public class Executor implements Runnable {

        public Executor(ComponentProxy comp) {
            this.proxy = comp;
        }

        @Override
        public void run() {            
            try {
                before(proxy);

                proxy.setState(ComponentProxy.State.EXECUTING);
//                Logger.getLogger(proxy.getClass().getName()).info(
//                        "Iniciando execução.");

                ArrayList<InputMethod> inputs = proxy.getInput();
//                Logger.getLogger(proxy.getClass().getName()).info(
//                        "Inputs = " + inputs.size());

                int result = WizardDialog.SUCESS;

                if (pipeline.isExecuteWizard() || proxy.isWizard()) {
//                     Logger.getLogger(proxy.getClass().getName()).info(
//                        "Chamando Wizard.");
                     
                    AbstractParametersView parameditor =
                            proxy.getParametersEditor();

                    if (parameditor != null) {
                        parameditor.reset();

                        WizardDialog dialog = WizardDialog.getInstance(null,
                                parameditor);
                        result = dialog.display();
                    }
                }

                //get all input methods
                inputs = proxy.getInput();
//                Logger.getLogger(proxy.getClass().getName()).info(
//                        "Inputs = " + inputs.size());

                for (InputMethod in : inputs) {
//                     Logger.getLogger(proxy.getClass().getName()).info(
//                        "Processando Input = " + in.getMethod());
                    //call all input methods that are filled
                    if (in.isFilled()) {
//                        Logger.getLogger(proxy.getClass().getName()).info(
//                                in + " está preenchido.");
                        if (in instanceof UniqueInputMethod) {
                            //creating the parameters list
                            Object[] param = new Object[in.getParameters().size()];
                            for (int i = 0; i < in.getParameters().size(); i++) {
                                param[i] = ((UniqueInputParameter) in.getParameters().get(i)).getObject();
                            }

//                            Logger.getLogger(proxy.getClass().getName()).info(
//                                    "Chamando o método: " + in.getMethod() +
//                                    " no objeto: " + proxy.getComponentToExecute());
                            //calling the method
                            in.getMethod().invoke(proxy.getComponentToExecute(), param);
                        } else if (in instanceof MultipleInputMethod) {
                            //get the objects associated to the only parameter of
                            //the MultipleInputMethod
                            ArrayList<Object> objs =
                                    ((MultipleInputParameter) in.getParameters().get(0)).getObjects();

                            //for each object
                            for (Object o : objs) {
                                //call the method
                                in.getMethod().invoke(proxy.getComponentToExecute(), o);
                            }
                        }
                    }
                }
                proxy.resetInput();

                if (result == WizardDialog.SUCESS) {
//                    Logger.getLogger(proxy.getClass().getName()).info(
//                            "Wizard terminado com sucesso.");

                    md = MessageDialog.show(null,
                            "Executing: \"" + proxy.toString().trim() + "\" component...");

                    try {
                        long start = System.currentTimeMillis();
//                        Logger.getLogger(proxy.getClass().getName()).info(
//                                "Executando o objeto: " + proxy.getComponentToExecute());
//                        Logger.getLogger(proxy.getClass().getName()).info(
//                                "Start = " + start);
                        
                        proxy.getComponentToExecute().execute();
                        long finish = System.currentTimeMillis();
//                        Logger.getLogger(proxy.getClass().getName()).info(
//                                "Finish = " + start);

                        Output.instance.append("[EXECUTED]: " + proxy.toString() +
                                " - execution time " + ((finish - start) / 1000.0f) + "s",
                                Color.BLACK, false, false, true);

//                        Logger.getLogger(proxy.getClass().getName()).info(
//                                "Definindo output methods.");

                        //define that all output methods are available
                        ArrayList<OutputMethod> outputs = proxy.getOutput();
                        for (int i = 0; i < outputs.size(); i++) {
//                            Logger.getLogger(proxy.getClass().getName()).info(
//                                "OutputMethod: " + outputs.get(i));
                            available(outputs.get(i).getOutputParameter());
                        }

                        proxy.setState(ComponentProxy.State.EXECUTED);

                        after(proxy);
                    } catch (Exception ex) {
                        Output.instance.append("[EXCEPTION]: " + proxy.toString() 
                                + " - " + ex.toString() + " (check log for more "
                                + "details)", Color.RED, true, false, false);
                        
                        Logger.getLogger(proxy.getClass().getName()).log(
                                Level.SEVERE, ex.getMessage(), ex);
                        
//                        exception = ex;
                    }
                }                

            } catch (IllegalAccessException ex) {
                Logger.getLogger(ExecuteEngine.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalArgumentException ex) {
                Logger.getLogger(ExecuteEngine.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvocationTargetException ex) {
                Logger.getLogger(ExecuteEngine.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                if (md != null) {
                    md.close();
                }
            }
        }

        protected void before(ComponentProxy comp) {
            // log?
        }

        protected void after(ComponentProxy comp) {
            if (comp.howToCite().trim().length() > 0) {
                Output.instance.append("[" + comp.toString() + "] >> " +
                        comp.howToCite(), Color.BLUE, true, false, false);
            }
        }

        protected void available(OutputParameter output) throws InvocationTargetException,
                IllegalArgumentException, IllegalAccessException, IOException {
//            Logger.getLogger(proxy.getClass().getName()).info(
//                                "OutputParameter: " + output);
            //get the value of this output
            Method method = output.getParentMethod().getMethod();
            Object result = method.invoke(output.getParentMethod().getParentComponent().getComponentToExecute());
//            Logger.getLogger(proxy.getClass().getName()).info(
//                                "Result: " + result);

            //pass this value to all input parameters that are interested in
            for (int i = 0; i < output.getInputParameters().size(); i++) {
                InputParameter inparam = output.getInputParameters().get(i);
//                Logger.getLogger(proxy.getClass().getName()).info(
//                                "Passando para inparam: " + inparam);

                if (inparam instanceof UniqueInputParameter) {
                    ((UniqueInputParameter) inparam).setObject(result);
                } else if (inparam instanceof MultipleInputParameter) {
                    ((MultipleInputParameter) inparam).addObject(result);
                }

                //call the input method associated to it
                ComponentProxy cproxy = inparam.getParentMethod().getParentComponent();
//                Logger.getLogger(proxy.getClass().getName()).info(
//                                "Proxy associada: " + cproxy);

                //check if the component can be executed, if it is true execute it
                if (canExecute(cproxy)) {
//                    Logger.getLogger(proxy.getClass().getName()).info(
//                        cproxy + " pode ser executada.");
                    Executor ex = new Executor(cproxy);
                    ex.execute();
                }
            }
        }

        protected boolean canExecute(ComponentProxy comp) {
//            Logger.getLogger(proxy.getClass().getName()).info(
//                        "canExecute: " + comp);
            ArrayList<InputMethod> inputs = comp.getInput();

            for (InputMethod in : inputs) {
//                Logger.getLogger(proxy.getClass().getName()).info(
//                        "InputMethod: " + in.getMethod());
                ArrayList<InputParameter> parameters = in.getParameters();

                for (InputParameter param : parameters) {
//                    Logger.getLogger(proxy.getClass().getName()).info(
//                        "InputParameter: " + param);
                    if (param instanceof UniqueInputParameter) {
                        if (((UniqueInputParameter) param).getOutputParameter() != null &&
                                !in.isFilled()) {
//                            Logger.getLogger(proxy.getClass().getName()).info(
//                                "false Unique: " + param);
                            return false;
                        }
                    } else if (param instanceof MultipleInputParameter) {
                        if (((MultipleInputParameter) param).getOutputParameters().size() > 0 &&
                                !in.isFilled()) {
//                            Logger.getLogger(proxy.getClass().getName()).info(
//                                "false Multiple: " + param);
                            return false;
                        }
                    }
                }
            }
//            Logger.getLogger(proxy.getClass().getName()).info(
//                        "true!");
            return true;
        }

        public void execute() throws IOException {
            if (!executor.isShutdown()) {
                executor.execute(this);
            }
            
//            if (exception != null) {                
//                throw exception;
//            }
        }

        private IOException exception;
        private MessageDialog md;
        protected ComponentProxy proxy;
    }

    protected Pipeline pipeline;
    protected ExecutorService executor;
    protected int threadpoolsize;
}
