/*
 * Copyright (C) 2011  VICG/LabES, ICMC-USP, Brazil
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package vispipeline.engine.actions;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import vispipeline.engine.Output;
import vispipeline.engine.ExecuteEngine;
import vispipeline.engine.pipeline.Pipeline;
import vispipeline.engine.util.OpenDialog;
import vispipeline.engine.util.PIPFilter;
import vispipeline.engine.util.PropertiesManager;
import vispipeline.engine.util.VisPipelineConstants;

public final class RunPipelineAction implements ActionListener {

    public void actionPerformed(ActionEvent e) {
        try {
            PropertiesManager spm = PropertiesManager.getInstance(
                    VisPipelineConstants.PROPFILENAME, getClass());
            int result = OpenDialog.showOpenDialog(spm, new PIPFilter(), null);
            if (result == JFileChooser.APPROVE_OPTION) {
//                String filename = OpenDialog.getFilename();
//                Pipeline pipeline = Pipeline.load(filename);
//
//                new ExecuteEngine(pipeline).start(true);
//
//                Output.instance.clear();
//                Output.instance.append("[OPENED]: " + filename,
//                        Color.BLACK, false, false, true);
            }
        } catch (IOException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }
}
