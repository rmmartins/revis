/*
 * Copyright (C) 2011  VICG/LabES, ICMC-USP, Brazil
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package vispipeline.engine.component;

import java.awt.datatransfer.Transferable;
import javax.swing.JComponent;
import javax.swing.JTree;
import javax.swing.TransferHandler;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 * Transfer handler of components from the components tree.
 * @author Fernando Vieira Paulovich
 */
public class ComponentTransferHandler extends TransferHandler {

    @Override
    protected Transferable createTransferable(JComponent c) {
        if (c instanceof JTree) {
            JTree tree = (JTree) c;
            Object lastPathComponent = ((DefaultMutableTreeNode) tree.getSelectionPath().
                    getLastPathComponent()).getUserObject();

            if (lastPathComponent instanceof ComponentTransfer) {
                return (ComponentTransfer) lastPathComponent;
            }
        }

        return null;
    }

    @Override
    public int getSourceActions(JComponent c) {
        return COPY;
    }

    public static final long serialVersionUID = 1L;
}
