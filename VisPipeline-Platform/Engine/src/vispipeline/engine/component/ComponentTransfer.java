/*
 * Copyright (C) 2011  VICG/LabES, ICMC-USP, Brazil
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package vispipeline.engine.component;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import vispipeline.basics.annotations.VisComponent;
import vispipeline.basics.interfaces.AbstractComponent;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class ComponentTransfer implements Serializable, Transferable {

    private ComponentTransfer(Class compproxyclass, String compname) {
        this.compproxyclass = compproxyclass;
        this.compname = compname;
    }

    public Object getTransferData(DataFlavor flavor) throws
            UnsupportedFlavorException, IOException {
        if (flavor.equals(compflavor)) {
            return this;
        } else {
            throw new UnsupportedFlavorException(flavor);
        }
    }

    public DataFlavor[] getTransferDataFlavors() {
        return new DataFlavor[]{compflavor};
    }

    public boolean isDataFlavorSupported(DataFlavor flavor) {
        return flavor.equals(compflavor);
    }

    public ComponentProxy getComponentProxy() throws IOException {
        try {
            Object instance = compproxyclass.newInstance();
            int type = ((VisComponent)compproxyclass.getAnnotation(
                    VisComponent.class)).type();
            if (type == VisComponent.BASE) {
                return new BaseComponentProxy((AbstractComponent) instance);
            }
            //else if (type == VisComponent.SUPER) {
                // To do
              //  return new ComponentProxy((AbstractComponent) instance);
            //}
            else {
                return new ComponentProxy((AbstractComponent) instance);
            }
        } catch (InstantiationException ex) {
            Logger.getLogger(ComponentTransfer.class.getName()).log(
                    Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(ComponentTransfer.class.getName()).log(
                    Level.SEVERE, null, ex);
        }

        return null;
    }

    public static ComponentTransfer getInstance(Class compproxyclass,
            String compname) {
        Class[] interfaces = compproxyclass.getInterfaces();

        for (int i = 0; i < interfaces.length; i++) {
            if (interfaces[i] == AbstractComponent.class) {
                return new ComponentTransfer(compproxyclass, compname);
            }
        }

        return null;
    }

    @Override
    public String toString() {
        return compname;
    }

    public static final long serialVersionUID = 1L;
    public static final DataFlavor compflavor = new DataFlavor(
            ComponentProxy.class, "vizcomponent");
    private Class compproxyclass;
    private String compname;
}
