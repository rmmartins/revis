/*
 * Copyright (C) 2011  VICG/LabES, ICMC-USP, Brazil
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package vispipeline.engine.component;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.JComboBox;
import org.openide.util.Lookup;
import vispipeline.basics.annotations.VisComponent;
import vispipeline.basics.interfaces.AbstractComponent;
import vispipeline.basics.interfaces.AbstractParametersView;
import vispipeline.engine.component.ComponentProxy.State;
import vispipeline.engine.component.parameter.Parameter;

/**
 *
 * @author Rafael
 */
public class BaseComponentProxy extends ComponentProxy {

    List<AbstractComponent> compList;

    public BaseComponentProxy(AbstractComponent baseComponent)
            throws IOException {
        super(baseComponent);
        base = baseComponent;
        compList = new ArrayList();
        // Looking for all components that extend the base one
        Class clas = baseComponent.getClass();
        Collection<? extends AbstractComponent> components =
                Lookup.getDefault().lookupAll(AbstractComponent.class);        
        for (AbstractComponent ac : components) {
            if (clas.isInstance(ac) && !clas.equals(ac.getClass())) {
                compList.add(ac);
            }
        }
        // dangerous
        if (compList.size() > 0)
            active = (AbstractComponent)compList.get(0);
        else
            active = baseComponent;
        
    }

    @Override
    public void draw(Graphics2D g2) {
        //if the name of the component is larger than the its width, increase it
        FontMetrics metrics = g2.getFontMetrics();
        width = Math.max(width, metrics.stringWidth(toString()) + Parameter.SPACE * 2);

        //draw the component
        g2.setColor(new Color(0.8f, 1.0f, 0.8f));
        g2.fillRoundRect(position.x, position.y, width, height, Parameter.SIZE, Parameter.SIZE);

        if (selected) {
            g2.setStroke(new BasicStroke(3.0f));
            g2.setColor(Color.RED);
            g2.drawRoundRect(position.x, position.y, width, height, Parameter.SIZE, Parameter.SIZE);
            g2.setStroke(new BasicStroke(1.0f));
        } else {
            if (state == State.EXECUTED) {
                g2.setStroke(new BasicStroke(3.0f));
                g2.setColor(Color.BLUE);
                g2.drawRoundRect(position.x, position.y, width, height, Parameter.SIZE, Parameter.SIZE);
                g2.setStroke(new BasicStroke(1.0f));
            } else if (state == State.EXECUTING) {
                g2.setStroke(new BasicStroke(3.0f));
                g2.setColor(Color.YELLOW);
                g2.drawRoundRect(position.x, position.y, width, height, Parameter.SIZE, Parameter.SIZE);
                g2.setStroke(new BasicStroke(1.0f));
            } else {
                g2.setColor(Color.BLACK);
                g2.drawRoundRect(position.x, position.y, width, height, Parameter.SIZE, Parameter.SIZE);
            }
        }

        //draw the component name
        g2.setColor(Color.BLACK);
        g2.drawString(toString(), position.x + Parameter.SPACE,
                position.y + (Parameter.SIZE * 2.5f));

        //draw the inputs
        if (getInput() != null) {
            for (int i = getInput().size() - 1; i >= 0; i--) {
                getInput().get(i).draw(g2);
            }
        }

        //draw the outputs
        if (getOutput() != null) {
            for (int i = getOutput().size() - 1; i >= 0; i--) {
                getOutput().get(i).draw(g2);
            }
        }
    }

    @Override
    public AbstractComponent getComponentToExecute() {
        return active;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        return new BaseCompParamView(this);
    }

    @Override
    public String toString() {
        return active.getClass().getAnnotation(VisComponent.class).name() +
                " [" + pipeline.getComponents().indexOf(this) + "]";
    }
    
    public void setActive(AbstractComponent comp) {
//        if (compList.contains(comp)) {
//            active = comp;
//        }
        int exchange = -1;
        for (AbstractComponent comp2 : compList) {
            if (comp2.equals(comp)) {
                active = comp;
                return;
            }
            if (comp2.getClass().equals(comp.getClass())) {
                active = comp;
                exchange = compList.indexOf(comp2);
                break;
            }
        }
        if (exchange > -1) {
            compList.add(exchange, comp);
        }
    }

    public AbstractComponent getBase() {
        return base;
    }

    public void setBase(AbstractComponent base) {
        this.base = base;
    }

    AbstractComponent active;
    AbstractComponent base;
    JComboBox combo;

}
