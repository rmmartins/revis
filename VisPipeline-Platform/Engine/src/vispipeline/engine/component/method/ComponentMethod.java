/*
 * Copyright (C) 2011  VICG/LabES, ICMC-USP, Brazil
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package vispipeline.engine.component.method;

import vispipeline.engine.component.parameter.Parameter;
import vispipeline.engine.component.*;
import java.awt.Graphics2D;
import java.awt.Point;
import java.io.Serializable;
import java.lang.reflect.Method;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public abstract class ComponentMethod implements Serializable {

    public ComponentMethod(ComponentProxy comp, Method method) {
        this.comp = comp;
        this.method = method;
        this.methodname = method.toString();
        this.position = new Point();
    }

    public abstract void draw(Graphics2D g2);

    public int getSize() {
        return this.size;
    }

    public Method getMethod() {
        if (method == null) {
            Method[] methods = comp.getComponentToExecute().getClass().getMethods();

            for (int i = 0; i < methods.length; i++) {
                if (methods[i].toString().equals(methodname)) {
                    method = methods[i];
                    break;
                }
            }
        }

        return method;
    }

    public abstract Parameter isInside(Point pos);

    public ComponentProxy getParentComponent() {
        return comp;
    }

    public int getId() {
        return comp.getOutput().indexOf(this);
    }

    public abstract void setPosition(Point position);

    public static final long serialVersionUID = 1L;
    protected int size;
    protected Point position;
    protected ComponentProxy comp;
    private String methodname;
    private transient Method method;
}
