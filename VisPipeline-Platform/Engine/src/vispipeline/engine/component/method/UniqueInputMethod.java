/*
 * Copyright (C) 2011  VICG/LabES, ICMC-USP, Brazil
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package vispipeline.engine.component.method;

import java.awt.Color;
import java.awt.Graphics2D;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import vispipeline.engine.component.ComponentProxy;
import vispipeline.engine.component.parameter.Parameter;
import vispipeline.engine.component.parameter.UniqueInputParameter;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class UniqueInputMethod extends InputMethod {

    public UniqueInputMethod(ComponentProxy comp, Method method) throws IOException {
        super(comp, method);

        if (method.getParameterTypes().length > 0) {
            //getting the method parameters
            Class[] paramTypes = method.getParameterTypes();
            Annotation[][] annotations = method.getParameterAnnotations();

            for (int i = 0; i < paramTypes.length; i++) {
                String name = "";

                //getting the parameters name, if it is provided by an annotation
                for (int j = 0; j < annotations[i].length; j++) {
                    if (annotations[i][j] instanceof vispipeline.basics.annotations.Param) {
                        name = ((vispipeline.basics.annotations.Param) annotations[i][j]).name();
                        break;
                    }
                }

                this.parameters.add(new UniqueInputParameter(this, paramTypes[i], name));
            }

            //define the size of the component
            this.size = parameters.size() * Parameter.SIZE + (parameters.size() + 1) * Parameter.SPACE;
        } else {
            throw new IOException(comp.toString() + ": an unique input method " +
                    "should have at least one input parameter.");
        }
    }

    @Override
    public void draw(Graphics2D g2) {
        if (parameters.size() > 0) {
            g2.setColor(Color.LIGHT_GRAY);
            g2.fillRoundRect(position.x, position.y, size,
                    Parameter.SIZE + (Parameter.SIZE / 2), Parameter.SIZE, Parameter.SIZE);

            g2.setColor(Color.BLACK);
            g2.drawRoundRect(position.x, position.y, size,
                    Parameter.SIZE + (Parameter.SIZE / 2), Parameter.SIZE, Parameter.SIZE);

            for (int i = parameters.size() - 1; i >= 0; i--) {
                parameters.get(i).draw(g2);
            }
        }
    }

}
