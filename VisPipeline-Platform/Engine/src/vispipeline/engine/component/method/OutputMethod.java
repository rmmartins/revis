/*
 * Copyright (C) 2011  VICG/LabES, ICMC-USP, Brazil
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package vispipeline.engine.component.method;

import java.awt.Color;
import java.awt.Graphics2D;
import vispipeline.engine.component.parameter.Parameter;
import vispipeline.engine.component.*;
import java.awt.Point;
import java.io.IOException;
import java.lang.reflect.Method;
import vispipeline.basics.annotations.Return;
import vispipeline.engine.component.parameter.OutputParameter;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class OutputMethod extends ComponentMethod {

    public OutputMethod(ComponentProxy comp, Method method) throws IOException {
        super(comp, method);

        if (method.getParameterTypes().length == 0) {
            Class type = method.getReturnType();
            String name = "";
            if (method.getAnnotation(Return.class) != null) {
                name = method.getAnnotation(Return.class).name();
            }
            parameter = new OutputParameter(this, type, name);

            //define the size of the component
            this.size = Parameter.SIZE + 2 * Parameter.SPACE;
        } else {
            throw new IOException(comp.toString() + ": an output method cannot " +
                    "have parameters.");
        }
    }

    @Override
    public void draw(Graphics2D g2) {
        g2.setColor(Color.LIGHT_GRAY);
        g2.fillRoundRect(position.x, position.y, size,
                Parameter.SIZE + (Parameter.SIZE / 2), Parameter.SIZE, Parameter.SIZE);

        g2.setColor(Color.BLACK);
        g2.drawRoundRect(position.x, position.y, size,
                Parameter.SIZE + (Parameter.SIZE / 2), Parameter.SIZE, Parameter.SIZE);

        parameter.draw(g2);
    }

    @Override
    public Parameter isInside(Point pos) {
        if (parameter.isInside(pos)) {
            return parameter;
        }

        return null;
    }

    @Override
    public void setPosition(Point position) {
        if (position != null) {
            this.position = position;

            //define the position of each parameter
            parameter.setPosition(new Point(Parameter.SPACE + position.x,
                    position.y + Parameter.SIZE));
        }
    }

    public OutputParameter getOutputParameter() {
        return parameter;
    }

    private OutputParameter parameter;
}
