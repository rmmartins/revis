/*
 * Copyright (C) 2011  VICG/LabES, ICMC-USP, Brazil
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package vispipeline.engine.component.method;

import vispipeline.engine.component.parameter.Parameter;
import vispipeline.engine.component.*;
import java.awt.Point;
import java.lang.reflect.Method;
import java.util.ArrayList;
import vispipeline.engine.component.parameter.InputParameter;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public abstract class InputMethod extends ComponentMethod {

    public InputMethod(ComponentProxy comp, Method method) {
        super(comp, method);

        this.parameters = new ArrayList<InputParameter>();
    }

    @Override
    public Parameter isInside(Point pos) {
        for (int i = 0; i < parameters.size(); i++) {
            if (parameters.get(i).isInside(pos)) {
                return parameters.get(i);
            }
        }

        return null;
    }

    @Override
    public void setPosition(Point position) {
        if (position != null) {
            this.position = position;

            //define the position of each parameter
            for (int i = 0; i < parameters.size(); i++) {
                int x = ((i + 1) * Parameter.SPACE + i * Parameter.SIZE) + position.x;
                int y = position.y - Parameter.SIZE / 2;
                parameters.get(i).setPosition(new Point(x, y));
            }
        }
    }

    public ArrayList<InputParameter> getParameters() {
        return parameters;
    }

    public void reset() {
        for (InputParameter ip : parameters) {
            ip.reset();
        }
    }

    public boolean isFilled() {
        for (InputParameter param : parameters) {
            if (!param.isFilled()) {
                return false;
            }
        }

        return true;
    }

    protected ArrayList<InputParameter> parameters;
}
