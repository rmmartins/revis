/*
 * Copyright (C) 2011  VICG/LabES, ICMC-USP, Brazil
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package vispipeline.engine.component.parameter;

import java.util.ArrayList;
import vispipeline.engine.component.method.MultipleInputMethod;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class MultipleInputParameter extends InputParameter {

    public MultipleInputParameter(MultipleInputMethod method, Class type, String name) {
        super(method, type, name);

        this.outputs = new ArrayList<OutputParameter>();
    }

    public ArrayList<Object> getObjects() {
        if (objects == null) {
            objects = new ArrayList<Object>();
        }

        return objects;
    }

    public void addObject(Object object) {
        if (objects == null) {
            objects = new ArrayList<Object>();
        }

        objects.add(object);
    }

    public boolean isFilled() {
        return (objects != null && objects.size() == outputs.size());
    }

    public void reset() {
        objects = new ArrayList<Object>();
    }

    public ArrayList<OutputParameter> getOutputParameters() {       
        return outputs;
    }

    public void addOutputParameter(OutputParameter output) {
        outputs.add(output);
    }

    public void removeOutputParameter(OutputParameter output) {
        outputs.remove(output);
    }

    private transient ArrayList<Object> objects;
    private ArrayList<OutputParameter> outputs;
}
