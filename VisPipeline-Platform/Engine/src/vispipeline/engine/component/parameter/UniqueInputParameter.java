/*
 * Copyright (C) 2011  VICG/LabES, ICMC-USP, Brazil
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package vispipeline.engine.component.parameter;

import vispipeline.engine.component.method.UniqueInputMethod;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class UniqueInputParameter extends InputParameter {

    public UniqueInputParameter(UniqueInputMethod method, Class type, String name) {
        super(method, type, name);
    }

    public boolean isFilled() {
        return (object != null);
    }

    public void reset() {
        object = null;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public OutputParameter getOutputParameter() {
        return output;
    }

    public void setOutputParameter(OutputParameter output) {
        this.output = output;
    }

    private transient Object object;
    private OutputParameter output;
}
