/*
 * Copyright (C) 2011  VICG/LabES, ICMC-USP, Brazil
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package vispipeline.engine.pipeline;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import java.awt.Point;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.Lookup;
import vispipeline.basics.annotations.Parameter;
import vispipeline.basics.interfaces.AbstractComponent;
import vispipeline.engine.component.BaseComponentProxy;
import vispipeline.engine.component.ComponentProxy;
import vispipeline.engine.component.method.InputMethod;
import vispipeline.engine.component.method.OutputMethod;
import vispipeline.engine.component.parameter.InputParameter;
import vispipeline.engine.component.parameter.OutputParameter;

/**
 *
 * @author Rafael
 */
public class PipelineConverter implements Converter {

    public boolean canConvert(Class type) {
        return type.equals(Pipeline.class);
    }

    public void marshal(Object o, HierarchicalStreamWriter writer, MarshallingContext mc) {
        // pipeline
        Pipeline pipeline = (Pipeline) o;
        // wizard
//        writer.addAttribute("wizard", String.valueOf(pipeline.isExecuteWizard()));
        // components
        for (ComponentProxy proxy : pipeline.getComponents()) {
            AbstractComponent component = proxy.getComponentToExecute();
            writer.startNode("component");
            // id
            int id = proxy.getId();
            writer.addAttribute("id", String.valueOf(id));
            // class
            Class clas = component.getClass();
            writer.addAttribute("class", clas.getName());            
            // fields (@Parameters)
            for (Field field : clas.getFields()) {
//                logger.info("field: " + field.getName());
//                logger.info("annotations: " + field.getAnnotations().length);
                if (field.getAnnotation(Parameter.class) != null) {
//                    logger.info("parameter found");
                    writer.startNode(field.getName());
                    String type = field.getType().getName();
                    writer.addAttribute("type", type);
                    try {
//                        writer.setValue(String.valueOf(field.get(component)));
                        Object value = field.get(component);
                        if (value != null) {
                            mc.convertAnother(value);
                        }
//                        logger.info("field: " + field.getName() + ", " + value);
                    } catch (IllegalAccessException e) {
                        writer.setValue("exception - check logs");
//                        logger.log(Level.SEVERE, e.getMessage(), e);
                    }
                    writer.endNode(); // field
                }
            }
            writer.endNode(); // component
        }
        // proxies
        for (ComponentProxy proxy : pipeline.getComponents()) {
            // proxy
            writer.startNode("proxy");
            // id
            writer.addAttribute("id", String.valueOf(proxy.getId()));
            // position
            writer.addAttribute("x", String.valueOf(proxy.getPosition().x));
            writer.addAttribute("y", String.valueOf(proxy.getPosition().y));
            // wizard
            boolean wizard = proxy.isWizard();
            writer.addAttribute("wizard", String.valueOf(wizard));
            // has base component?
            if (proxy instanceof BaseComponentProxy) {
                String base = ((BaseComponentProxy)proxy).getBase().getClass().getName();
                writer.addAttribute("base", String.valueOf(base));
            } else {
                writer.addAttribute("base", "");
            }
            // links
            if (proxy.getOutput().size() > 0) {
                for (OutputMethod om : proxy.getOutput()) {
                    ArrayList<InputParameter> links =
                            om.getOutputParameter().getInputParameters();
                    if (links.size() > 0) {                        
                        for (InputParameter link : links) {
                            writer.startNode("link");
                            // source parameter name
                            writer.addAttribute("srcName", om.getOutputParameter().getName());
                            // destination proxy id
                            int destProxy = link.getParentMethod().getParentComponent().getId();
                            writer.addAttribute("destProxy", String.valueOf(destProxy));
                            // destination parameter name
                            String destName = link.getName();
                            writer.addAttribute("destName", destName);
                            writer.endNode(); // link
                        }
                    }                    
                }                
            }
            writer.endNode(); // proxy
        }        
    }

    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext uc) {
        // pipeline
        Pipeline pipeline = new Pipeline();
        // wizard
//        pipeline.setExecuteWizard(Boolean.valueOf(reader.getAttribute("wizard")));
        // --- component map
        Map<Integer, AbstractComponent> components =
                new TreeMap<Integer, AbstractComponent>();
        // --- proxy map
        Map<Integer, ComponentProxy> proxies =
                new TreeMap<Integer, ComponentProxy>();
        // --- link map
        ArrayList<Link> links = new ArrayList<Link>();
        while (reader.hasMoreChildren()) {
            reader.moveDown();
//            logger.info("nodeName: " + reader.getNodeName());
            // component
            if (reader.getNodeName().equals("component")) {
                // id
                int id = Integer.valueOf(reader.getAttribute("id"));
                // class
                String className = reader.getAttribute("class");
//                logger.info("className: " + className);
                try {
                    Class compClass = load(className);
                    Object instance = compClass.newInstance();
//                    logger.info("class loaded and new instance created");
                    // fields (@Parameters)
//                    logger.info("reading parameters...");
                    while (reader.hasMoreChildren()) {
                        reader.moveDown();
                        String fieldName = reader.getNodeName();
//                        String fieldValue = reader.getValue();
                        String fieldType = reader.getAttribute("type");
                        Class fieldClass = load(fieldType);
                        Object fieldValue = uc.convertAnother(pipeline, fieldClass);
//                        logger.info(fieldName + ", " + fieldValue);
                        compClass.getField(fieldName).set(instance, fieldValue);
                        reader.moveUp();
                    }
                    components.put(id, (AbstractComponent) instance);
//                    logger.info("component registered on map: " + id);
                } catch (Exception e) {
                    logger.log(Level.SEVERE, e.getMessage(), e);
                }
            }
            // proxy
            if (reader.getNodeName().equals("proxy")) {
                try {
                    // id
                    int id = Integer.valueOf(reader.getAttribute("id"));                    
                    AbstractComponent component = components.get(id);
                    ComponentProxy proxy;
                    // is base? complicates a little bit
                    String base = reader.getAttribute("base");
                    if (base != null && !base.equals("")) {
//                        JOptionPane.showMessageDialog(null, base);
                        Class baseClass = load(base);
                        Object baseInst = baseClass.newInstance();
                        proxy = new BaseComponentProxy((AbstractComponent)baseInst);
                        ((BaseComponentProxy)proxy).setActive(component);
                    } else {
                        proxy = new ComponentProxy(component);
                    }
                    proxies.put(id, proxy);
//                    logger.info("proxy created and registered on map: " + id);
                    // position
                    int x = Integer.valueOf(reader.getAttribute("x"));
                    int y = Integer.valueOf(reader.getAttribute("y"));
                    proxy.setPosition(new Point(x, y));
                    // wizard
                    boolean wizard = Boolean.valueOf(reader.getAttribute("wizard"));
                    proxy.setWizard(wizard);
                    // links
                    while (reader.hasMoreChildren()) {
                        reader.moveDown();
                        Link link = new Link();
                        link.srcProxy = id;
                        // source parameter name
                        link.srcName = reader.getAttribute("srcName");
                        // destination proxy id
                        link.destProxy = Integer.valueOf(reader.getAttribute("destProxy"));
                        // destination parameter name
                        link.destName = reader.getAttribute("destName");
                        links.add(link);
//                        logger.info("registered link: " + link);
                        reader.moveUp();
                    }
                } catch (Exception e) {
                    logger.log(Level.SEVERE, e.getMessage(), e);
                }
            }
            reader.moveUp();
        }
        // adding proxies in the original order
        for (int id : proxies.keySet()) {
            pipeline.addComponent(proxies.get(id));
//            logger.info("proxy " + id + " added");
        }
        // adding links
        for (Link link : links) {
//            logger.info("processing link: " + link);
            ComponentProxy srcProxy = proxies.get(link.srcProxy);
            String srcName = link.srcName;
            OutputParameter output = null;
            for (OutputMethod om : srcProxy.getOutput()) {
                if (om.getOutputParameter().getName().equals(srcName)) {
                    output = om.getOutputParameter();
//                    logger.info("found output parameter");
                    break;
                }
            }
            if (output != null) {
                ComponentProxy destProxy = proxies.get(link.destProxy);                
                for (InputMethod im : destProxy.getInput()) {
                    boolean found = false;
                    for (InputParameter ip : im.getParameters()) {
                        if (ip.getName().equals(link.destName)) {
                            output.addInputParameter(ip);
                            found = true;
//                            logger.info("found input parameter");
                            break;
                        }
                    }
                    if (found) {
                        break;
                    }
                }
            }
//            logger.info("link " + link + " added");
        }
        return pipeline;
    }

    private Class load(String type) throws ClassNotFoundException {
        ClassLoader classLoader = Lookup.getDefault().lookup(ClassLoader.class);
        if (type.equals("int")) {
            return Integer.TYPE;
        }
        if (type.equals("long")) {
            return Long.TYPE;
        }
        if (type.equals("float")) {
            return Float.TYPE;
        }
        if (type.equals("double")) {
            return Double.TYPE;
        }
        if (type.equals("boolean")) {
            return Boolean.TYPE;
        }
        if (type.equals("char")) {
            return Character.TYPE;
        }
        if (type.equals("byte")) {
            return Byte.TYPE;
        }
        if (type.equals("void")) {
            return Void.TYPE;
        }
        if (type.equals("short")) {
            return Short.TYPE;
        }
        return classLoader.loadClass(type);
    }

    private Logger logger = Logger.getLogger(getClass().getSimpleName());

}


class Link {
    int srcProxy;
    String srcName;
    int destProxy;
    String destName;

    @Override
    public String toString() {
        return srcProxy + ": " + srcName + " >> " + destProxy + ": " + destName;
    }

}