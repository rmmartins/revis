/*
 * Copyright (C) 2011  VICG/LabES, ICMC-USP, Brazil
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package vispipeline.engine.pipeline;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import org.jgrapht.DirectedGraph;
import org.jgrapht.alg.CycleDetector;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.openide.filesystems.FileObject;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;
import vispipeline.engine.component.ComponentProxy;
import vispipeline.engine.component.method.InputMethod;
import vispipeline.engine.component.method.OutputMethod;
import vispipeline.engine.component.method.UniqueInputMethod;
import vispipeline.engine.component.parameter.InputParameter;
import vispipeline.engine.component.parameter.MultipleInputParameter;
import vispipeline.engine.component.parameter.UniqueInputParameter;
import vispipeline.engine.component.parameter.OutputParameter;
import vispipeline.engine.component.parameter.Parameter;

/**
 *
 * @author Fernando Veira Paulovich
 */
public class Pipeline implements Serializable {

    public Pipeline() {
        this.components = new ArrayList<ComponentProxy>();
    }

    public void draw(Graphics2D g2) {
        g2.setFont(new Font("Sans Serif", Font.PLAIN, 10));

        for (int i = components.size() - 1; i >= 0; i--) {
            components.get(i).draw(g2);
        }
    }

    public void addComponent(ComponentProxy compproxy) {
        if (compproxy != null) {
            components.add(compproxy);
            compproxy.setPipeline(this);
            // so one can get components from the lookup
            ic.add(compproxy.getComponentToExecute());
        }
    }

    public boolean removeComponent(ComponentProxy compproxy) {
        if (compproxy != null) {
            compproxy.setPipeline(null);
        }

        return components.remove(compproxy);
    }

    public ComponentProxy isInsideComponent(Point pos) {
        for (int i = 0; i < components.size(); i++) {
            if (components.get(i).isInside(pos)) {
                return components.get(i);
            }
        }

        return null;
    }

    public Parameter isInsideParameter(Point pos) {
        Parameter param = null;

        for (int i = 0; i < components.size(); i++) {
            for (int j = 0; j < components.get(i).getInput().size(); j++) {
                param = components.get(i).getInput().get(j).isInside(pos);

                if (param != null) {
                    return param;
                }
            }

            for (int j = 0; j < components.get(i).getOutput().size(); j++) {
                param = components.get(i).getOutput().get(j).isInside(pos);

                if (param != null) {
                    return param;
                }
            }
        }

        return param;
    }

    public boolean canBeLinked(Parameter param1, Parameter param2) {
        InputParameter input = (InputParameter) ((param1 instanceof UniqueInputParameter)
                ? param1 : (param2 instanceof UniqueInputParameter)
                ? param2 : (param1 instanceof MultipleInputParameter)
                ? param1 : (param2 instanceof MultipleInputParameter) ? param2 : null);
        Parameter output = (OutputParameter) ((param1 instanceof OutputParameter)
                ? param1 : (param2 instanceof OutputParameter) ? param2 : null);

        //the parameters functions should be different, one should be
        //InputParameter and the other one OutputParameter
        if (input == null || output == null) {
//            System.out.println("debug: canBeLinked 1");
            return false;
        }

//        //the parameters types should be the same
//        if (param1.getType() != param2.getType()) {
//            System.out.println("debug: canBeLinked 2");
//            return false;
//        }
        
        if (!input.getType().isAssignableFrom(output.getType())) {
//            System.out.println("debug: canBeLinked 2");
            return false;
        }

        //the param2 and param1 of the same ComponentProxy cannot be linked
        if (param1.getParentMethod().getParentComponent() ==
                param2.getParentMethod().getParentComponent()) {
//            System.out.println("debug: canBeLinked 3");
            return false;
        }

        //one ouput can only be linked one time to a multiple input
        if (input instanceof MultipleInputParameter) {
            if (((MultipleInputParameter) input).getOutputParameters().contains(output)) {
//                System.out.println("debug: canBeLinked 4");
                return false;
            }
        }

        //an unique input can only be linked one time
        if (input instanceof UniqueInputParameter) {
            if (((UniqueInputParameter) input).getOutputParameter() != null) {
//                System.out.println("debug: canBeLinked 5");
                return false;
            }
        }

        //only one UniqueInputMethod can be called in one ComponentProxy at once
        if (input instanceof UniqueInputParameter) {
            ComponentProxy comp = input.getParentMethod().getParentComponent();
            ArrayList<InputMethod> inputs = comp.getInput();

            for (InputMethod in : inputs) {
                if (in instanceof UniqueInputMethod && in != input.getParentMethod()) {
                    if (((UniqueInputParameter) input).getOutputParameter() != null) {
//                        System.out.println("debug: canBeLinked 6");
                        return false;
                    }
                }
            }
        }

        //getting all links between input and output parameters (the first
        //element is the input and the second the output)
        ArrayList<Parameter[]> links = new ArrayList<Parameter[]>();

        for (int i = 0; i < components.size(); i++) {
            ArrayList<OutputMethod> outputs = components.get(i).getOutput();

            for (int j = 0; j < outputs.size(); j++) {
                ArrayList<InputParameter> inparam = outputs.get(j).
                        getOutputParameter().getInputParameters();

                //create the links, pairs of InputParamater and OutputParameter
                for (int n = 0; n < inparam.size(); n++) {
                    Parameter[] link = new Parameter[2];
                    link[0] = inparam.get(n);
                    link[1] = outputs.get(j).getOutputParameter();
                    links.add(link);
                }
            }
        }

        //we should avoid cyclic calling
        DirectedGraph<ComponentProxy, DefaultEdge> g =
                new DefaultDirectedGraph<ComponentProxy, DefaultEdge>(DefaultEdge.class);
        for (int i = 0; i < components.size(); i++) {
            g.addVertex(components.get(i));
        }

        for (int i = 0; i < links.size(); i++) {
            Parameter[] par = links.get(i);

            if (par[0] != null && par[1] != null) {
                g.addEdge(par[1].getParentMethod().getParentComponent(),
                        par[0].getParentMethod().getParentComponent());
            }
        }

        g.addEdge(output.getParentMethod().getParentComponent(),
                input.getParentMethod().getParentComponent());

        CycleDetector<ComponentProxy, DefaultEdge> detector =
                new CycleDetector<ComponentProxy, DefaultEdge>(g);
        if (detector.detectCycles()) {
//            System.out.println("debug: canBeLinked 7");
            return false;
        }

        return true;
    }

    public void save(String filename) throws IOException {
        reset();        

//        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(filename));
//        out.writeObject(this);
//        out.flush();
//        out.close();
        
//        saveParams();

        FileWriter fileWriter = new FileWriter(filename);
        XStream xstream = new XStream(new DomDriver());
        xstream.registerConverter(new PipelineConverter());        
        xstream.alias("pipeline", Pipeline.class);
        String xml = xstream.toXML(this);
        fileWriter.write(xml);
        fileWriter.flush();
        fileWriter.close();        
    }

//    public void saveParams() throws IOException {
//        if (filename != null) {
//            XStream xstream = new XStream();
//            String foldername = filename + "-params";
//            File folder = new File(foldername);
//            if (!folder.exists()) {
//                folder.mkdir();
//            }
//            if (folder.exists()) {
//                for (ComponentProxy proxy : components) {
//                    int id = components.indexOf(proxy);
//                    String xml = xstream.toXML(proxy.getComponentToExecute());
//                    File file = new File(foldername + "/" + id + ".xml");
//                    FileWriter writer = new FileWriter(file);
//                    writer.write(xml);
//                    writer.flush();
//                    writer.close();
//                }
//            }
//            else {
//                JOptionPane.showMessageDialog(null, "!folder.exists");
//            }
//        }
//        else {
//            JOptionPane.showMessageDialog(null, "filename = null");
//        }
//    }

    public static Pipeline load(FileObject file) throws IOException {
        Pipeline pipeline = null;

//        try {
//            ObjectInputStream in = new ObjectInputStream(new FileInputStream(filename));
//            pipeline = (Pipeline) in.readObject();
//            in.close();
//        } catch (FileNotFoundException e) {
//            throw new IOException("Error reading the file: " + e.getMessage());
//        } catch (IOException e) {
//            throw new IOException("Error reading the file: " + e.getMessage());
//        } catch (ClassNotFoundException e) {
//            throw new IOException("Error reading the file. Class not found: " + e.getMessage());
//        }

//        pipeline.loadParams();

        FileReader fileReader = new FileReader(file.getPath());
        XStream xstream = new XStream(new DomDriver());
        xstream.registerConverter(new PipelineConverter());        
        xstream.alias("pipeline", Pipeline.class);
        pipeline = (Pipeline) xstream.fromXML(fileReader);
        fileReader.close();

//        FileReader fileReader = new FileReader(filename);
//        pipeline = (Pipeline)new XStream().fromXML(fileReader);
//        fileReader.close();

        pipeline.file = file;

        return pipeline;
    }

//    public void loadParams() throws IOException {
//        if (filename != null) {
//            XStream xstream = new XStream();
//            String foldername = filename + "-params";
//            File folder = new File(foldername);
//            if (folder.exists()) {
//                for (ComponentProxy proxy : components) {
//                    int id = components.indexOf(proxy);
//                    xstream.fromXML(
//                            new FileReader(foldername + "/" + id + ".xml"),
//                            proxy.getComponentToExecute());
//                }
//            }
//            else {
//                System.out.println("loadParams: !folder.exists()");
//            }
//        }
//        else {
//            System.out.println("loadParams: filename = null");
//        }
//    }

    public ArrayList<ComponentProxy> getComponents() {
        return components;
    }

    public void deleteSelectedComponents() {
        //removing the components
        for (int i = components.size() - 1; i >= 0; i--) {
            if (components.get(i).isSelected()) {
                ComponentProxy removed = components.remove(i);
                removed.setPipeline(null);

                //removing the output links
                ArrayList<OutputMethod> output = removed.getOutput();
                for (int j = 0; j < output.size(); j++) {
                    OutputParameter out = output.get(j).getOutputParameter();
                    out.removeAllInputParameter();
                }

                //removing the input links
                ArrayList<InputMethod> input = removed.getInput();
                for (int j = 0; j < input.size(); j++) {
                    InputMethod inmeth = input.get(j);

                    for (int k = 0; k < inmeth.getParameters().size(); k++) {
                        InputParameter in = (InputParameter) inmeth.getParameters().get(k);

                        if (in instanceof UniqueInputParameter) {
                            if (((UniqueInputParameter) in).getOutputParameter() != null) {
                                ((UniqueInputParameter) in).getOutputParameter().removeInputParameter(in);
                            }
                        } else if (in instanceof MultipleInputParameter) {
                            ArrayList<OutputParameter> outparams = ((MultipleInputParameter) in).getOutputParameters();

                            for (int m = 0; m < outparams.size(); m++) {
                                OutputParameter op = outparams.get(m);
                                op.removeInputParameter(in);
                            }
                        }
                    }
                }
            }
        }
    }

    public void reset() {
        for (int i = 0; i < components.size(); i++) {
            components.get(i).reset();
        }
    }

    public boolean isExecuteWizard() {
        return executewizard;
    }

    public void setExecuteWizard(boolean executewizard) {
        this.executewizard = executewizard;
    }

    public String getFullPath() {
        return file.getPath();
    }

    public String getBasePath() {
        return file.getParent().getPath();
    }

    public Lookup getLookup() {
        if (lkp == null) {
            // a dynamic lookup            
            lkp = new AbstractLookup(ic);
        }
        return lkp;
    }

    public static final long serialVersionUID = 1L;
    private ArrayList<ComponentProxy> components;
    protected boolean executewizard;
    private FileObject file;
    private Lookup lkp;
    private InstanceContent ic = new InstanceContent();
}
