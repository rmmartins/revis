/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vispipeline.engine;

import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.CookieAction;
import vispipeline.engine.pipfiletype.PIPDataObject;

public final class RunPipelineAction extends CookieAction {

    protected void performAction(Node[] activatedNodes) {
        
    }

    protected int mode() {
        return CookieAction.MODE_EXACTLY_ONE;
    }

    public String getName() {
        return NbBundle.getMessage(RunPipelineAction.class, "CTL_RunPipelineAction");
    }

    protected Class[] cookieClasses() {
        return new Class[]{PIPDataObject.class};
    }

    @Override
    protected void initialize() {
        super.initialize();
        // see org.openide.util.actions.SystemAction.iconResource() Javadoc for more details
        putValue("noIconInMenu", Boolean.TRUE);
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    @Override
    protected boolean asynchronous() {
        return false;
    }
}

