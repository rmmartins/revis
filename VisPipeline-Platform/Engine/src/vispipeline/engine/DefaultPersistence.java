/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.engine;

import com.thoughtworks.xstream.XStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import org.openide.util.lookup.ServiceProvider;
import vispipeline.basics.interfaces.Persistence;

/**
 *
 * @author Rafael
 */

@ServiceProvider(service=Persistence.class, position=0)
public final class DefaultPersistence extends Persistence {

    @Override
    public Object load(File file) throws IOException {
        FileReader reader = new FileReader(file);
        Object obj = new XStream().fromXML(reader);
        reader.close();
        return obj;
    }

    @Override
    public void save(Object object, File file) throws IOException {
        FileWriter writer = new FileWriter(file);
        new XStream().toXML(object, writer);
        writer.flush();
        writer.close();
    }

}
