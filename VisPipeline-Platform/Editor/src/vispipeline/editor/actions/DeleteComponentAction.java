/*
 * Copyright (C) 2011  VICG/LabES, ICMC-USP, Brazil
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package vispipeline.editor.actions;

import java.io.IOException;
import javax.swing.JOptionPane;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.CookieAction;
import vispipeline.editor.view.PipelineDrawTopComponent.ComponentNode;

public final class DeleteComponentAction extends CookieAction {

    protected void performAction(Node[] activatedNodes) {
        ComponentNode node = (ComponentNode)activatedNodes[0];
        try {
            node.destroy();
        }
        catch (IOException e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    protected int mode() {
        return CookieAction.MODE_EXACTLY_ONE;
    }

    public String getName() {
        return NbBundle.getMessage(DeleteComponentAction.class,
                "CTL_DeleteComponentAction");
    }

    protected Class[] cookieClasses() {
        return new Class[]{ComponentNode.class};
    }

    @Override
    protected String iconResource() {
        return "vispipeline/editor/actions/delete.png";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    @Override
    protected boolean asynchronous() {
        return false;
    }
}

