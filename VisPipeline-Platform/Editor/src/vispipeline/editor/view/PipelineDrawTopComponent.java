/*
 * Copyright (C) 2011  VICG/LabES, ICMC-USP, Brazil
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package vispipeline.editor.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseMotionListener;
import java.beans.BeanInfo;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ActionMap;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;
//import org.openide.util.ImageUtilities;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.cookies.SaveCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.SaveAsCapable;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.actions.SystemAction;
import vispipeline.basics.interfaces.AbstractParametersView;
import vispipeline.editor.ExecuteEngineGUI;
import vispipeline.editor.actions.DeleteComponentAction;
import vispipeline.engine.Output;
import vispipeline.engine.component.ComponentDropTargetListener;
import vispipeline.engine.component.ComponentProxy;
import vispipeline.engine.component.parameter.InputParameter;
import vispipeline.engine.component.parameter.MultipleInputParameter;
import vispipeline.engine.component.parameter.OutputParameter;
import vispipeline.engine.component.parameter.Parameter;
import vispipeline.engine.component.parameter.ParameterLink;
import vispipeline.engine.component.parameter.UniqueInputParameter;
import vispipeline.engine.ExecuteEngine;
import vispipeline.engine.VisPipelineOptions;
import vispipeline.engine.pipeline.Pipeline;
import vispipeline.engine.pipfiletype.PIPDataObject;

/**
 * Top component which displays something.
 */
@ConvertAsProperties(dtd = "-//vispipeline.editor.view//PipelineDraw//EN",
autostore = false)
public final class PipelineDrawTopComponent extends TopComponent {

    private static PipelineDrawTopComponent instance;
    /** path to the icon used by the component and its open action */
//    static final String ICON_PATH = "SET/PATH/TO/ICON/HERE";
    private static final String PREFERRED_ID = "PipelineDrawTopComponent";

    public PipelineDrawTopComponent() {
        initComponents();
        setName(NbBundle.getMessage(PipelineDrawTopComponent.class, "CTL_PipelineDrawTopComponent"));
        
        setToolTipText(NbBundle.getMessage(PipelineDrawTopComponent.class, "HINT_PipelineDrawTopComponent"));
//        setIcon(ImageUtilities.loadImage(ICON_PATH, true));

        //create a new pipeline when starting the tool
        drawPanel = new PipelineDrawPanel();
        drawPanel.setPipeline(new Pipeline());
        add(drawPanel, BorderLayout.CENTER);

        setActivatedNodes(new Node[]{pipelineNode = new PipelineNode()});

        //resizing the draw panel
//            drawPanel.setSize(drawPanel.getSize().width, screenSize.height);
//            drawPanel.setPreferredSize(drawPanel.getSize());

        ActionMap map = getActionMap();
        map.put("delete", SystemAction.get(DeleteComponentAction.class));
//        associateLookup(ExplorerUtils.createLookup(explorerManager, getActionMap()));
        
    }

    public PipelineDrawTopComponent(PIPDataObject pipData) {
        this();

        try {
            projectFolder = pipData.getFolder().getPrimaryFile().getPath();
            drawPanel.setPipeline(pipData.getPipeline());
            setName(pipData.getPrimaryFile().getNameExt());
            setIcon(pipData.getNodeDelegate().getIcon(
                    BeanInfo.ICON_COLOR_16x16));
        } catch (IOException e) {
            DialogDisplayer.getDefault().notify(
                    new NotifyDescriptor.Message(e));
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jToolBar1 = new javax.swing.JToolBar();
        stopButton = new javax.swing.JButton();
        executeButton = new javax.swing.JButton();
        wizardButton = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();

        setLayout(new java.awt.BorderLayout());

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);

        stopButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vispipeline/editor/view/control_stop.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(stopButton, org.openide.util.NbBundle.getMessage(PipelineDrawTopComponent.class, "PipelineDrawTopComponent.stopButton.text")); // NOI18N
        stopButton.setFocusable(false);
        stopButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stopButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(stopButton);

        executeButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vispipeline/editor/view/control_fastforward.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(executeButton, org.openide.util.NbBundle.getMessage(PipelineDrawTopComponent.class, "PipelineDrawTopComponent.executeButton.text")); // NOI18N
        executeButton.setFocusable(false);
        executeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                executeButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(executeButton);

        wizardButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vispipeline/editor/view/control_play.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(wizardButton, org.openide.util.NbBundle.getMessage(PipelineDrawTopComponent.class, "PipelineDrawTopComponent.wizardButton.text")); // NOI18N
        wizardButton.setFocusable(false);
        wizardButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                wizardButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(wizardButton);

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vispipeline/editor/view/cross.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(jButton1, org.openide.util.NbBundle.getMessage(PipelineDrawTopComponent.class, "PipelineDrawTopComponent.jButton1.text")); // NOI18N
        jButton1.setFocusable(false);
        jButton1.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jButton1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton1);

        add(jToolBar1, java.awt.BorderLayout.PAGE_START);
    }// </editor-fold>//GEN-END:initComponents

    private void stopButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stopButtonActionPerformed
        if (exengine != null) {
            exengine.stop();
        }
    }//GEN-LAST:event_stopButtonActionPerformed

    private void executeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_executeButtonActionPerformed
               
        Pipeline pipeline = drawPanel.getPipeline();

        if (pipeline != null) {
            try {
                if (exengine != null) {
                    exengine.stop();
                }

                exengine = new ExecuteEngineGUI(drawPanel, pipeline);
                exengine.setThreadPoolSize(VisPipelineOptions.getInstance(null)
                        .getNumberSimutaneousThreads());
                exengine.start(false);
            } catch (IOException ex) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null,
                        ex);
            }
        }
    }//GEN-LAST:event_executeButtonActionPerformed

    private void wizardButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_wizardButtonActionPerformed
        Pipeline pipeline = drawPanel.getPipeline();

        if (pipeline != null) {
            try {
                if (exengine != null) {
                    exengine.stop();
                }

                exengine = new ExecuteEngineGUI(drawPanel, pipeline);
                exengine.setThreadPoolSize(VisPipelineOptions.getInstance(null)
                        .getNumberSimutaneousThreads());
                exengine.start(true);
            } catch (IOException ex) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null,
                        ex);
            }
        }
    }//GEN-LAST:event_wizardButtonActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        drawPanel.getPipeline().deleteSelectedComponents();
        drawPanel.repaint();
    }//GEN-LAST:event_jButton1ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton executeButton;
    private javax.swing.JButton jButton1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JButton stopButton;
    private javax.swing.JButton wizardButton;
    // End of variables declaration//GEN-END:variables
    /**
     * Gets default instance. Do not use directly: reserved for *.settings files only,
     * i.e. deserialization routines; otherwise you could get a non-deserialized instance.
     * To obtain the singleton instance, use {@link #findInstance}.
     */
    public static synchronized PipelineDrawTopComponent getDefault() {
        if (instance == null) {
            instance = new PipelineDrawTopComponent();
        }
        return instance;
    }

    /**
     * Obtain the PipelineDrawTopComponent instance. Never call {@link #getDefault} directly!
     */
    public static synchronized PipelineDrawTopComponent findInstance() {
        TopComponent win = WindowManager.getDefault().findTopComponent(PREFERRED_ID);
        if (win == null) {
            Logger.getLogger(PipelineDrawTopComponent.class.getName()).warning(
                    "Cannot find " + PREFERRED_ID + " component. It will not be located properly in the window system.");
            return getDefault();
        }
        if (win instanceof PipelineDrawTopComponent) {
            return (PipelineDrawTopComponent) win;
        }
        Logger.getLogger(PipelineDrawTopComponent.class.getName()).warning(
                "There seem to be multiple components with the '" + PREFERRED_ID +
                "' ID. That is a potential source of errors and unexpected behavior.");
        return getDefault();
    }

    @Override
    public int getPersistenceType() {
        return TopComponent.PERSISTENCE_ALWAYS;
    }

    @Override
    public void componentOpened() {
        // TODO add custom code on component opening
    }

    @Override
    public void componentClosed() {
        // TODO add custom code on component closing
    }

    void writeProperties(java.util.Properties p) {
        // better to version settings since initial version as advocated at
        // http://wiki.apidesign.org/wiki/PropertyFiles
        p.setProperty("version", "1.0");
        // TODO store your settings
    }

    Object readProperties(java.util.Properties p) {
        PipelineDrawTopComponent singleton = PipelineDrawTopComponent.getDefault();
        singleton.readPropertiesImpl(p);
        return singleton;
    }

    private void readPropertiesImpl(java.util.Properties p) {
        String version = p.getProperty("version");
        // TODO read your settings according to their version
    }

    @Override
    protected String preferredID() {
        return PREFERRED_ID;
    }

    public class PipelineDrawPanel extends JPanel {

        public PipelineDrawPanel() {
//            this.visfrm = visfrm;

            setBackground(Color.WHITE);

            //adding the mouse listeners
            MouseListener listener = new MouseListener();
            this.addMouseMotionListener(listener);
            this.addMouseListener(listener);
            
            ActionMap map = getActionMap();
            map.put("delete", SystemAction.get(DeleteComponentAction.class));
        }

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D g2 = (Graphics2D) g;

            if (pipeline != null) {
                g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                pipeline.draw(g2);

                if (tmplink != null) {
                    tmplink.draw(g2);
                }
            }

            if (sourceret != null && targetret != null) {
                int x = Math.min(sourceret.x, targetret.x);
                int width = Math.abs(sourceret.x - targetret.x);

                int y = Math.min(sourceret.y, targetret.y);
                int height = Math.abs(sourceret.y - targetret.y);

                g2.setColor(Color.YELLOW);
                g2.drawRect(x, y, width, height);
                g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 0.45f));
                g2.fillRect(x, y, width, height);
                g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 1.0f));
            }
        }

        public void setPipeline(Pipeline pipeline) {
            this.pipeline = pipeline;

            //create the drop control
            setDropTarget(new DropTarget(this, DnDConstants.ACTION_COPY,
                    new ComponentDropTargetListener(this, pipeline), true));

            repaint();
        }

        public Pipeline getPipeline() {
            return pipeline;
        }

//        public VisPipeline getVisFramework() {
//            return visfrm;
//        }
        class MouseListener extends MouseAdapter implements MouseMotionListener {

            @Override
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                super.mouseMoved(evt);

                if (pipeline != null) {
                    if (param == null) {
                        param = pipeline.isInsideParameter(evt.getPoint());

                        if (param != null) {
                            param.setSelected(true);
                            repaint();
                        } else {
                            param = null;
                        }

                    } else {
                        Parameter param_aux = pipeline.isInsideParameter(evt.getPoint());

                        if (param != param_aux) {
                            param.setSelected(false);
                            param = param_aux;

                            if (param != null) {
                                param.setSelected(true);
                            }

                            repaint();
                        }
                    }
                }
            }

            @Override
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                super.mouseDragged(evt);

                if (pipeline != null) {
                    if (proxy != null) {
                        //change the component position
                        Point newpos = new Point(evt.getPoint());
                        newpos.x = newpos.x - proxy.getWidth() / 2;
                        newpos.y = newpos.y - proxy.getHeight() / 2;
                        proxy.setPosition(newpos);

                        //resize the panel if it is necessary
                        Dimension size = PipelineDrawPanel.this.getSize();
                        Dimension prefsize = PipelineDrawPanel.this.getPreferredSize();

                        int maxwidth = Math.max((int) size.getWidth(), (int) prefsize.getWidth());
                        int maxheight = Math.max((int) size.getHeight(), (int) prefsize.getHeight());

                        int reswidth = Math.max(maxwidth, newpos.x + (proxy.getWidth() * 2));
                        int resheight = Math.max(maxheight, newpos.y + (proxy.getHeight() * 2));

                        PipelineDrawPanel.this.setSize(new Dimension(reswidth, resheight));
                        PipelineDrawPanel.this.setPreferredSize(new Dimension(reswidth, resheight));

                        repaint();
                    } else if (tmplink != null) {
                        tmplink.setFreePosition(evt.getPoint());

                        if (param == null) {                            
                            param = pipeline.isInsideParameter(evt.getPoint());
//                            System.out.println("debug: " + param + ", " + tmplink.getParameter());
                            if (pipeline.canBeLinked(param, tmplink.getParameter())) {
//                                System.out.println("debug: can be linked");
                                param.setSelected(true);
                            } else {
//                                System.out.println("debug: can't be linked");
                                param = null;
                            }
                        } else {                           
                            Parameter param_aux = pipeline.isInsideParameter(evt.getPoint());

                            if (param != param_aux) {
                                param.setSelected(false);
                                param = param_aux;

                                if (param != null) {
                                    param.setSelected(true);
                                }
                            }
                        }

                        repaint();
                    } else if (sourceret != null) {
                        //get the second point to draw
                        targetret = evt.getPoint();
                        repaint();
                    }
                }
            }

            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                super.mouseClicked(evt);

                if (evt.getClickCount() == 1) {
                    boolean shouldrepaint = false;

                    //clean all selected components
                    for (ComponentProxy c : pipeline.getComponents()) {
                        if (c.isSelected()) {
                            c.setSelected(false);
                            shouldrepaint = true;
                        }
                    }
                    setActivatedNodes(new Node[]{new PipelineNode()});                    

                    proxy = pipeline.isInsideComponent(evt.getPoint());

                    //if a component is found, select it
                    if (proxy != null) {
                        proxy.setSelected(true);
                        setActivatedNodes(new Node[]{new ComponentNode(proxy)});
                        shouldrepaint = true;
                    }

                    //only repaint if it is necessary
                    if (shouldrepaint) {
                        repaint();
                    }
                } else if (evt.getClickCount() == 2) {
                    proxy = pipeline.isInsideComponent(evt.getPoint());

                    //if two clicks are executed, call the parameters editor of
                    //the component
                    if (proxy != null) {
                        AbstractParametersView parameditor = proxy.getParametersEditor();

                        if (parameditor != null) {
                            ParametersEditorDialog.getInstance(null, 
                                    parameditor, proxy).display();
                        }
                    }
                }

//                System.out.println("Activated Nodes:");
                for (Node n : getActivatedNodes()) {
//                    System.out.println(n.toString() + "," + n.canDestroy());
                }

            }

            @Override
            public void mousePressed(java.awt.event.MouseEvent evt) {
                super.mousePressed(evt);

                if (pipeline != null) {
                    //get the component the mouse is inside
                    proxy = pipeline.isInsideComponent(evt.getPoint());

                    //if the mouse is not inside a component
                    if (proxy == null) {
                        //get the parameter the mouse is inside
                        param = pipeline.isInsideParameter(evt.getPoint());

                        //if the component is inside a parameter
                        if (param != null) {
                            //if it is a input parameter
                            if (param instanceof UniqueInputParameter) {
                                OutputParameter output = ((UniqueInputParameter) param).getOutputParameter();

                                //if it has a link to an output, remove it
                                if (output != null) {
                                    output.removeInputParameter((UniqueInputParameter) param);
                                    tmplink = new ParameterLink(output);
                                } else {
                                    //otherwise create the link
                                    tmplink = new ParameterLink(param);
                                }
                            } else if (param instanceof MultipleInputParameter) {
                                ArrayList<OutputParameter> outs = ((MultipleInputParameter) param).getOutputParameters();

                                if (outs.size() > 0) {
                                    outs.get(0).removeInputParameter((InputParameter) param);
                                    tmplink = new ParameterLink(outs.get(0));
                                } else {
                                    //otherwise create the link
                                    tmplink = new ParameterLink(param);
                                }
                            } else {
                                //otherwise create the link
                                tmplink = new ParameterLink(param);
                            }
                        } else {
                            //get the first point to draw the selection rectangle
                            sourceret = evt.getPoint();
                        }
                    }
                }
            }

            @Override
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                super.mouseReleased(evt);

                if (pipeline != null) {
                    proxy = null;

                    if (tmplink != null) {
                        Parameter target = pipeline.isInsideParameter(evt.getPoint());

                        if (pipeline.canBeLinked(target, tmplink.getParameter())) {
                            target.setSelected(false);

                            Parameter input = (target instanceof InputParameter) ? target : tmplink.getParameter();
                            Parameter output = (target instanceof OutputParameter) ? target : tmplink.getParameter();
                            ((OutputParameter) output).addInputParameter((InputParameter) input);
                        }

                        tmplink = null;

                        repaint();
                    } else if (sourceret != null && targetret != null) {
                        //get the selected components
                        for (int i = 0; i < pipeline.getComponents().size(); i++) {
                            ComponentProxy component = pipeline.getComponents().get(i);

                            if (component.isInside(sourceret, targetret)) {
                                component.setSelected(true);
                            } else {
                                component.setSelected(false);
                            }
                        }

                        sourceret = null;
                        targetret = null;

                        repaint();
                    }
                }
            }
            private ComponentProxy proxy;
            private Parameter param;
        }
        private ParameterLink tmplink; //used to draw the temp link between parameters
        private Point sourceret; //used to select points with the retangle
        private Point targetret; //used to select points with the retangle
        private Pipeline pipeline;
//        private VisPipeline visfrm;
    }


    private class PipelineNode extends AbstractNode {

        SaveCookieImpl impl;

        public PipelineNode() {
            super(Children.LEAF);
            impl = new SaveCookieImpl();
            getCookieSet().assign(SaveCookie.class, impl);
            getCookieSet().assign(SaveAsCapable.class, impl);
        }

        private class SaveCookieImpl implements SaveCookie, SaveAsCapable {

            public void save() throws IOException {
                Pipeline pipeline = drawPanel.getPipeline();
                if (pipeline != null) {
                    String filename = pipeline.getFullPath();
                    pipeline.save(filename);
                    Output.instance.append("[SAVED]: " + filename,
                            Color.BLACK, false, false, true);
                }
            }

            public void saveAs(FileObject folder, String name)
                    throws IOException {                
                // Primeiro ele gera o diálogo automático, depois vem pra cá
                Pipeline pipeline = drawPanel.getPipeline();
                if (pipeline != null) {
                    String filename = folder.getPath() + "/" + name;
                    pipeline.save(filename);
                    Output.instance.append("[SAVED]: " + filename,
                            Color.BLACK, false, false, true);
                }
            }
        }
    }

    public class ComponentNode extends AbstractNode {

        private ComponentProxy cp;

        public ComponentNode(ComponentProxy cp) {
            super(Children.LEAF);
            this.cp = cp;
        }

        @Override
        public boolean canDestroy() {
            return true;
        }

        @Override
        public void destroy() throws IOException {
            super.destroy();
            Pipeline pipeline = drawPanel.getPipeline();
            if (pipeline != null) {
                pipeline.deleteSelectedComponents();
                drawPanel.repaint();
            }
            setActivatedNodes(new Node[]{new PipelineNode()});
        }
    }

    public String getProjectFolder() {
        return projectFolder;
    }
    
    private PipelineDrawPanel drawPanel;
    private PipelineNode pipelineNode;
    private ExecuteEngine exengine;
    private String projectFolder;
}
