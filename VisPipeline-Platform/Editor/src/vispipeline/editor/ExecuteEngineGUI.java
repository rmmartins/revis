/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.editor;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.logging.Logger;
import vispipeline.basics.interfaces.Persistence;
import vispipeline.editor.view.PipelineDrawTopComponent.PipelineDrawPanel;
import vispipeline.engine.component.ComponentProxy;
import vispipeline.engine.ExecuteEngine;
import vispipeline.engine.ExecuteEngine.Executor;
import vispipeline.engine.component.parameter.InputParameter;
import vispipeline.engine.component.parameter.MultipleInputParameter;
import vispipeline.engine.component.parameter.OutputParameter;
import vispipeline.engine.component.parameter.UniqueInputParameter;
import vispipeline.engine.pipeline.Pipeline;

/**
 *
 * @author Rafael
 */
public class ExecuteEngineGUI extends ExecuteEngine {

    public ExecuteEngineGUI(PipelineDrawPanel drawpanel, Pipeline pipeline) {
        super(pipeline);
        this.drawpanel = drawpanel;
    }

    @Override
    protected void before() {
        super.before();
        if (drawpanel != null) {
                drawpanel.repaint();
            }
    }

    @Override
    public void start(boolean wizard) throws IOException {
        executor = Executors.newFixedThreadPool(threadpoolsize);

        before();

        //reset the pipeline
        pipeline.reset();
        pipeline.setExecuteWizard(wizard);

        //find the components which does not have input parameters
        ArrayList<ComponentProxy> noInputComponents = getNoInputComponents();

        for (int i = 0; i < noInputComponents.size(); i++) {
            //and execute it
            Executor ex = new ExecutorGUI(noInputComponents.get(i));
            ex.execute();
        }

        after();
    }

    public class ExecutorGUI extends Executor {

        public ExecutorGUI(ComponentProxy comp) {
            super(comp);
        }

        @Override
        protected void before(ComponentProxy comp) {
            super.before(comp);
            if (drawpanel != null) {
                drawpanel.repaint();
            }
        }

        @Override
        protected void after(ComponentProxy comp) {
            super.after(comp);
            if (drawpanel != null) {
                drawpanel.repaint();
            }
        }

        @Override
        protected void available(OutputParameter output) throws
                InvocationTargetException, IllegalArgumentException,
                IllegalAccessException, IOException {
            Method method = output.getParentMethod().getMethod();
            Object result = method.invoke(output.getParentMethod()
                    .getParentComponent().getComponentToExecute());

//            Logger.getLogger("available").info(output.);

            int compId = proxy.getId();
            int methId = output.getParentMethod().getId();
            
            // Setting up the persistence path
//            String fullPath = pipeline.getFilename();
//            String[] pathParts = fullPath.split("/");
//            String name = pathParts[pathParts.length-1];
//            String path = fullPath.substring(0, fullPath.length() -
//                    name.length());
//            String simpleName = name.substring(0, name.length() - 4);
//
//            String persistPath = path + "/" + simpleName + "_data";
//            File persistFolder = new File(persistPath);
//            if (!persistFolder.exists() || !persistFolder.isDirectory()) {
//                persistFolder.mkdir();
//            }
//            if (persistFolder.exists() && persistFolder.isDirectory()) {
//                File file = new File(persistFolder, simpleName + "_" + compId +
//                        "_" + methId);
//                // Save result
//                Persistence p = Persistence.getFor(result.getClass());
//                if (p != null) {
//                    p.save(result, file);
//                }
//            }
//            else {
//                Logger.getLogger(proxy.getClass().getName()).info(
//                        "Error on trying to persist the result.");
//            }

            //pass this value to all input parameters that are interested in
            for (int i = 0; i < output.getInputParameters().size(); i++) {
                InputParameter inparam = output.getInputParameters().get(i);
//                Logger.getLogger(proxy.getClass().getName()).info(
//                                "Passando para inparam: " + inparam);

                if (inparam instanceof UniqueInputParameter) {
                    ((UniqueInputParameter) inparam).setObject(result);
                } else if (inparam instanceof MultipleInputParameter) {
                    ((MultipleInputParameter) inparam).addObject(result);
                }

                //call the input method associated to it
                ComponentProxy cproxy = inparam.getParentMethod().getParentComponent();
                Logger.getLogger(proxy.getClass().getName()).info(
                                "Proxy associada: " + cproxy);

                //check if the component can be executed, if it is true execute it
                if (canExecute(cproxy)) {
                    Logger.getLogger(proxy.getClass().getName()).info(
                        cproxy + " pode ser executada.");
                    Executor ex = new ExecutorGUI(cproxy);
                    ex.execute();
                }
            }
        }

    }

    private PipelineDrawPanel drawpanel;
    private String path;
    
}
