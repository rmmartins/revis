This module holds the visual configuration of the platform, e.g. Menus, buttons,
windows, etc. These are defined in the "vispipeline/layer.xml" file. 
You can not only add new features, but also change what is defined in other 
layer files from other modules; the easier way to do that is to explore the 
layers inside NetBeans by going to:

+- Important Files
   +- XML Layer
      +- <this layer in context>
      
All configurations from all layer files that affect the platform are available
there. The parts that were already modified in any way are in bold.

Some interesting links:

http://wiki.netbeans.org/DevFaqModulesLayerFile
http://wiki.netbeans.org/TaT_HackingNetBeansXMLLayerPartOne
http://wiki.netbeans.org/TaT_HackingNetBeansXMLLayerPartTwo
http://netbeans.dzone.com/tips/play-with-xml-layer