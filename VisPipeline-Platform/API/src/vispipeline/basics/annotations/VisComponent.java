/*
 * Copyright (C) 2011  VICG/LabES, ICMC-USP, Brazil
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 */
package vispipeline.basics.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Metadata to define a vispipeline component.
 * @author Fernando Vieira Paulovich, Rafael Martins
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface VisComponent {

    /**
     * The hierarchy of the component as it appears on the component list.
     * Consists on a string for each level, from the highest to the lowest,
     * separated by a dot. <br/>
     * Example: <code>Example.Input</code>     
     * 
     * @return The hierarchy as a String.
     */
    String hierarchy();
    
    /**
     * The name of the component, as it appears on the visual editor.
     * 
     * @return The name as a String.
     */
    String name();
    
    /**
     * A small description of the component.
     * 
     * @return The description as a String.
     */
    String description() default "";

    /**
     * Instructions on how to cite the component when using it as part of a 
     * scientifical work.
     * 
     * @return The instructions as a String.
     */
    String howtocite() default "";

    /**
     * The type of the component: COMMON (1) or BASE (2). A "base" component is 
     * a placeholder that can be switched in runtime between its children 
     * components. If not specified will default to common.
     * 
     * @return The type as an Integer.
     */
    int type() default COMMON;

    /**
     * Used in <code>type()</code> to define a common component (default).
     */
    public static final int COMMON = 1;
    /**
     * Used in <code>type()</code> to define a base component.
     */
    public static final int BASE = 2;
    
}
