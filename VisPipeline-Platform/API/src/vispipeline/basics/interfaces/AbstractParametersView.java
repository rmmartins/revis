/*
 * Copyright (C) 2011  VICG/LabES, ICMC-USP, Brazil
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 */
package vispipeline.basics.interfaces;

import java.io.IOException;
import javax.swing.JPanel;

/**
 * Base class for Swing panels that show a form for the user to fill in external
 * parameters needed by the component.
 * @author Fernando Vieira Paulovich
 */
public abstract class AbstractParametersView extends JPanel {

    /**
     * This method runs when the user confirms the parameters.
     * @throws IOException
     */
    public abstract void finished() throws IOException;

    /**
     * This method runs everytime the form must be cleaned up.
     */
    public abstract void reset();

}
