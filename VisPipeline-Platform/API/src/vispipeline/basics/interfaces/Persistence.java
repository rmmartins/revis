/*
 * Copyright (C) 2011  VICG/LabES, ICMC-USP, Brazil
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 */
package vispipeline.basics.interfaces;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import org.openide.util.Lookup;

/**
 * Base class to implement different persistence algorithms for the components.
 * @param <E> 
 * @author Rafael
 */
public abstract class Persistence<E> {

    /**
     * Returns the persistence manager registered for a certain class.
     * @param The class for which the persistence is needed.
     * @return A persistence manager for the specified class.
     */
    public static Persistence getFor(Class clas) {
        Collection<? extends Persistence> pers =
                Lookup.getDefault().lookupAll(Persistence.class);
        for (Persistence p : pers) {
            Type t = p.getClass().getGenericSuperclass();
            if (t instanceof ParameterizedType) {
                Type par = ((ParameterizedType)t).getActualTypeArguments()[0];
                if (par instanceof Class) {
                    if (((Class)par).getName().equals(clas.getName())) {
                        return p;
                    }
                }
            }
        }
//        return Lookup.getDefault().lookup(Persistence.class);
        return null;
    }

    /**
     * Saves an object using the persistence method implemented.
     * @param object - The object (component instance) to be saved.
     * @param file - The file where the object will be saved.
     * @throws IOException
     */
    public abstract void save(E object, File file) throws IOException;

    /**
     * Loads an object using the implemented persistence method.
     * @param file - The file from which the object will be loaded.
     * @return The loaded object.
     * @throws IOException
     */
    public abstract E load(File file) throws IOException;

}
