/*
 * Copyright (C) 2011  VICG/LabES, ICMC-USP, Brazil
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package vispipeline.examples;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */



import javax.swing.JOptionPane;
import org.openide.util.lookup.ServiceProvider;
import vispipeline.basics.annotations.VisComponent;
import vispipeline.basics.interfaces.AbstractComponent;
import vispipeline.basics.interfaces.AbstractParametersView;

/**
 *
 * @author Fernando Vieira Paulovich
 */
@VisComponent(hierarchy = "Example.Input",
name = "Input",
description = "This is an input component.")
@ServiceProvider(service=AbstractComponent.class)
public class InputComponent implements AbstractComponent {

    public void execute() {
        String result = JOptionPane.showInputDialog("input");
        input = Integer.parseInt(result);
    }

    public Integer output() {
        return input;
    }

    public AbstractParametersView getParametersEditor() {
        return null;
    }

    public void reset() {
    }

    public static final long serialVersionUID = 1L;
    private transient int input;
}
