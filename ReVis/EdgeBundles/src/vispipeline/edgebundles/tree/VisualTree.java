package vispipeline.edgebundles.tree;

import java.util.ArrayList;

import vispipeline.edgebundles.io.DataReader;
import vispipeline.visualization.model.AbstractInstance;
import vispipeline.visualization.model.AbstractModel;
import vispipeline.visualization.model.Scalar;

/**
 * <p>Title: Visual Tree</p>
 *
 * <p>Description: A class which stores the tree and its adjacency relations
 * in a rich data structure which enables fast queries</p>
 * It reads the hiearchical and adjacency relations from a data reader, and
 * create the corresponding data structure upon contruction
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class VisualTree extends AbstractModel {
    private TreeNode[] nodes;

    /**
     * the roots of the tree
     * @return TreeNode
     */
    public TreeNode getRoot() {
        return nodes[0];
    }

    /**
     * the ith node in the tree (the nodes are partially ordered via
     * an order which respects the hierarchy
     * @return int
     */

    public TreeNode getNode(int i) {
        return nodes[i];
    }

    /**
     * the number of tree nodes
     * @return int
     */

    public int getNodesCount() {
        return nodes.length;
    }

    /**
     * constructing the tree from a data reader
     * @param dr DataReader
     */
//    public VisualTree() {
//
//    }

    public void fill(DataReader dr) {
        int count = dr.getNodesCount();
    	nodes = new TreeNode[count];
        ArrayList[] children = new ArrayList[count];
        nodes[0] = new TreeNode(this); // root
        children[0] = new ArrayList();
        for (int i = 1; i < count; i++) {
            int prnt = dr.getParent(i);
            nodes[i] = new TreeNode(nodes[prnt], dr.getCaption(i), i);
            children[prnt].add(nodes[i]);
            children[i] = new ArrayList();
        }
        for (int i = 0; i < count; i++) {
            nodes[i].children =(TreeNode[]) children[i].toArray(new TreeNode[0]);
            int[] adjacentNodes = dr.getAdjacentNodes(i);
            nodes[i].adjacentNodes = new TreeNode[adjacentNodes.length];
            for (int j = 0; j < adjacentNodes.length; j++)
                nodes[i].adjacentNodes[j] = nodes[adjacentNodes[j]];
        }
        setChanged();
    }

    @Override
    public void removeInstances(ArrayList<AbstractInstance> reminst) {
//        super.removeInstances(reminst);
    }

    @Override
    public void removeSelectedInstances() {
//        super.removeSelectedInstances();
    }

}
