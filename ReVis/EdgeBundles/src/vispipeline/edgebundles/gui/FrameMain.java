package vispipeline.edgebundles.gui;

import java.awt.*;
import java.awt.event.*;
import java.io.IOException;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import vispipeline.edgebundles.io.*;
import vispipeline.edgebundles.tree.TreeNode;
import vispipeline.edgebundles.tree.VisualTree;
import vispipeline.edgebundles.visualisation.DataVisualiser;
import vispipeline.edgebundles.visualisation.link.BSplineLinkVisualiser;
import vispipeline.edgebundles.visualisation.tree.TreeVisualiser;

/**
 * <p>Title: Frame Main</p>
 *
 * <p>Description: The main window of the application
 * It offers a comfortable GUI for loading the data, choosing the tree layout,
 * the colors, the bundling strength and the alphablending. It also enables
 * panning and zooming in the viewing area.
 * </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class FrameMain extends JFrame {
    JPanel contentPane;
    BorderLayout borderLayout1 = new BorderLayout();
    JMenuBar menuBarMain = new JMenuBar();
    JToolBar jToolBar = new JToolBar();
    JLabel statusBar = new JLabel();
    JMenu jMenuFile = new JMenu();
    JMenuItem jMenuFileExit = new JMenuItem();
    JMenuItem jMenuItemOpen = new JMenuItem();
    JMenuItem jMenuItemRandomData = new JMenuItem();
    JMenu jMenuTreeLayout = new JMenu();
    JMenuItem[] jMenuItemTreeVis = new JMenuItem[TreeVisualiser.
                                   getVisualisersCount()];
    JMenu jMenuView = new JMenu();
    JMenuItem jMenuItemZoomIn = new JMenuItem();
    JMenuItem jMenuItemZoomOut = new JMenuItem();
    JMenuItem jMenuItemColors = new JMenuItem();
    JMenu jMenuHelp = new JMenu();
    JMenuItem jMenuItemAbout = new JMenuItem();
    JSlider sliderAlpha = new JSlider(0, 90);
    JSlider sliderBeta = new JSlider(0, 100);

    JButton btnZoomIn = new JButton("+");
    JButton btnZoomOut = new JButton("-");
    JLabel btnSrcColor = new JLabel(" ");
    JLabel btnDstColor = new JLabel(" ");

    Color srcColor = Color.RED, dstColor = Color.GREEN;

    BSplineLinkVisualiser linkVis = new BSplineLinkVisualiser(Color.RED,
            Color.GREEN);
    DataVisualiser dataVisualiser = new DataVisualiser(TreeVisualiser.
            getVisualiser(0), linkVis, 0.5, 0.5);
    DataReader dr;
    JPanel drawPanel = new JPanel() {
        public void paint(Graphics g) {
            super.paint(g);
            dataVisualiser.visualise((Graphics2D) g, getWidth(), getHeight());
        }
    };

    JScrollPane scrollPane = new JScrollPane(drawPanel);

    public FrameMain() {
        try {            
            jbInit();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    private void jbInit() throws Exception {
        contentPane = (JPanel) getContentPane();
        contentPane.setLayout(borderLayout1);
        setSize(new Dimension(400, 300));
        setTitle("Edge Bundles");
        statusBar.setText(" ");
        jMenuFile.setText("File");
        jMenuFileExit.setText("Exit");
        jMenuItemOpen.setText("Open");
        jMenuTreeLayout.setText("Tree Layout");
        jMenuView.setText("View");
        jMenuItemZoomOut.setText("Zomm Out");
        jMenuItemZoomIn.setText("Zoom In");
        jMenuItemColors.setText("Colors...");
        jMenuHelp.setText("Help");
        jMenuItemAbout.setText("About...");
        jMenuFileExit.addActionListener(new
                                        FrameMain_jMenuFileExit_ActionAdapter(this));
        jMenuItemOpen.addActionListener(new
                                        FrameMain_jMenuItemOpen_actionAdapter(this));
        
        drawPanel.addMouseMotionListener(new MyMouseMotionListener());

        for (int i = 0; i < jMenuItemTreeVis.length; i++) {
            final int index = i;
            jMenuItemTreeVis[i] = new JMenuItem(TreeVisualiser.getVisualiser(i).
                                                toString());
            jMenuItemTreeVis[i].addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    dataVisualiser.setTreeVisualiser(TreeVisualiser.
                            getVisualiser(index));
                    drawPanel.repaint();
                }
            });
            jMenuTreeLayout.add(jMenuItemTreeVis[i]);

        }
        jMenuItemAbout.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(FrameMain.this,
                                              "By Bilal Alsallakh, 2007\nbilal.sal@gmail.com",
                                              "About", JOptionPane.OK_OPTION);
            }
        });
        jMenuItemZoomOut.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                FrameMain.this.zoomOut();
            }
        });
        jMenuItemZoomIn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                FrameMain.this.zoomIn();
            }
        });
        jMenuItemColors.addActionListener(new
                                          FrameMain_jMenuItemColors_actionAdapter(this));
        sliderAlpha.addChangeListener(new FrameMain_sliderAlpha_changeAdapter(this));
        sliderBeta.addChangeListener(new FrameMain_sliderBeta_changeAdapter(this));
        jLabelBeta.setText("Beta:");
        jLabelAlpha.setText("Alpha:");
        scrollPane.addMouseListener(new FrameMain_scrollPane_mouseAdapter(this));
        btnZoomIn.setPreferredSize(new Dimension(21, 21));
        btnZoomOut.setPreferredSize(new Dimension(21, 21));
        btnSrcColor.setPreferredSize(new Dimension(21, 21));
        btnSrcColor.addMouseListener(new FrameMain_btnSrcColor_mouseAdapter(this));
        btnDstColor.setPreferredSize(new Dimension(21, 21));
        btnDstColor.addMouseListener(new FrameMain_btnDstColor_mouseAdapter(this));
        btnSrcColor.setOpaque(true);
        btnDstColor.setOpaque(true);
        btnSrcColor.setBackground(srcColor);
        btnDstColor.setBackground(dstColor);
        btnZoomIn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                FrameMain.this.zoomIn();
            }
        });
        btnZoomOut.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                FrameMain.this.zoomOut();
            }
        });
        jMenuItemRandomData.setText("Random Data");
        jMenuItemRandomData.addActionListener(new
                                              FrameMain_jMenuItemRandomData_actionAdapter(this));
        menuBarMain.add(jMenuFile);
        menuBarMain.add(jMenuTreeLayout);
        menuBarMain.add(jMenuView);
        menuBarMain.add(jMenuHelp);
        jMenuFile.add(jMenuItemOpen);
        jMenuFile.add(jMenuItemRandomData);
        jMenuFile.add(jMenuFileExit);
        jMenuView.add(jMenuItemZoomIn);
        jMenuView.add(jMenuItemZoomOut);
        jMenuView.add(jMenuItemColors);
        jMenuHelp.add(jMenuItemAbout);
        setJMenuBar(menuBarMain);
        jToolBar.add(jLabelAlpha);
        jToolBar.add(sliderAlpha);
        jToolBar.add(jLabelBeta);
        jToolBar.add(sliderBeta);
        jToolBar.add(btnZoomIn);
        jToolBar.add(btnZoomOut);
        jToolBar.add(btnSrcColor);
        jToolBar.add(btnDstColor);
        contentPane.add(jToolBar, BorderLayout.NORTH);
        contentPane.add(scrollPane, BorderLayout.CENTER);
        contentPane.add(statusBar, BorderLayout.SOUTH);
    }

    void jMenuFileExit_actionPerformed(ActionEvent actionEvent) {
        System.exit(0);
    }

    public void openDataFile(ActionEvent e) {
        JFileChooser jfc = new JFileChooser("./samples");
        if (jfc.showDialog(this, "Open") == JFileChooser.APPROVE_OPTION) {
            try {
                dataVisualiser.setData(new HEBFileReader(jfc.getSelectedFile()));
                drawPanel.repaint();
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(this, "Can't Open File");
            }
        }
    }

    void sliderBeta_stateChanged(ChangeEvent e) {
        dataVisualiser.setBeta(sliderBeta.getValue() / 100.0);
        drawPanel.repaint();
    }

    public void sliderAlpha_stateChanged(ChangeEvent e) {
        dataVisualiser.setMaxAlpha(sliderAlpha.getValue() / 100.0);
        drawPanel.repaint();
    }

    int zoomFactor = 0;
    Dimension dim = new Dimension();
    JLabel jLabelBeta = new JLabel();
    JLabel jLabelAlpha = new JLabel();

    public void zoomOut() {
        if (zoomFactor > 0) {
            zoomFactor--;
        }
        adjustZoom();
    }

    public void zoomIn() {
        zoomFactor++;
        adjustZoom();
    }

    private void adjustZoom() {
        int w = scrollPane.getWidth() - 2;
        int h = scrollPane.getHeight() - 2;
        double scale = Math.pow(1.3, zoomFactor);
        dim.width = (int) (w * scale);
        dim.height = (int) (h * scale);
        drawPanel.setPreferredSize(dim);
        drawPanel.revalidate();
    }

    public void jMenuItemColors_actionPerformed(ActionEvent e) {
        Color c1 = JColorChooser.showDialog(this, "Choose source color",
                                            srcColor);
        if (c1 != null) {
            Color c2 = JColorChooser.showDialog(this,
                                                "Choose destination color",
                                                dstColor);
            if (c2 != null) {
                linkVis.setStartAndEndColor(c1, c2);
            }
        }
        drawPanel.repaint();
    }

    Point pressedPoint;

    public void scrollPane_mousePressed(MouseEvent e) {
        scrollPane.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
        pressedPoint = e.getPoint();
    }

    public void scrollPane_mouseReleased(MouseEvent e) {
        scrollPane.setCursor(Cursor.getDefaultCursor());
        JViewport VP = scrollPane.getViewport();
        Point p = VP.getViewPosition();
        int newX = p.x + pressedPoint.x - e.getPoint().x;
        int newY = p.y + pressedPoint.y - e.getPoint().y;
        newX = Math.min(Math.max(newX, 0), drawPanel.getWidth() - VP.getWidth());
        newY = Math.min(Math.max(newY, 0), drawPanel.getHeight() - VP.getHeight());
        p.move(newX, newY);
        VP.setViewPosition(p);
//        System.out.println(" " + e.getPoint());
    }

    public void btnSrcColor_mouseClicked(MouseEvent e) {
        Color c1 = JColorChooser.showDialog(this, "Choose source color",
                                            srcColor);
        if (c1 != null) {
            srcColor = c1;
            btnSrcColor.setBackground(c1);
            linkVis.setStartAndEndColor(srcColor, dstColor);
        }
        drawPanel.repaint();
    }

    public void btnDstColor_mouseClicked(MouseEvent e) {
        Color c1 = JColorChooser.showDialog(this, "Choose destination color",
                                            dstColor);
        if (c1 != null) {
            dstColor = c1;
            btnDstColor.setBackground(c1);
            linkVis.setStartAndEndColor(srcColor, dstColor);
        }
        drawPanel.repaint();
    }

    public void jMenuItemRandomData_actionPerformed(ActionEvent e) {
        int n = Integer.parseInt(JOptionPane.showInputDialog(
                this, "Input the number of nodes", "40"));
        int m = Integer.parseInt(JOptionPane.showInputDialog(
                this, "Input the appx. number of leaves", "40"));
        dataVisualiser.setData(new RandomDataGenerator(n, m));
        drawPanel.repaint();
    }

    public DataVisualiser getDataVisualiser() {
        return dataVisualiser;
    }

    public JPanel getDrawPanel() {
        return drawPanel;
    }

    class MyMouseMotionListener extends MouseMotionAdapter {

        public MyMouseMotionListener() {
            ToolTipManager.sharedInstance().setDismissDelay(Integer.MAX_VALUE);
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            JPanel panel = ((JPanel) e.getSource());
            VisualTree vtree = (VisualTree) dataVisualiser.getModel();
            double dist = Double.MAX_VALUE;
            TreeNode showNode = null;
            for (int i = 0; i < vtree.getNodesCount(); i++) {
                TreeNode node = vtree.getNode(i);
//                Double dist2 = node.getCenter().distance(e.getPoint());
//                if (dist2 < dist) {
//                    dist = dist2;
//                    showNode = node;
//                }
                if (node.getArea().contains(e.getPoint())) {
                    showNode = node;
                }
            }
            if (showNode != null) {
                panel.setToolTipText(showNode.toString());
            }
            super.mouseMoved(e);
        }
    }
}


class FrameMain_jMenuItemRandomData_actionAdapter implements ActionListener {
    private FrameMain adaptee;
    FrameMain_jMenuItemRandomData_actionAdapter(FrameMain adaptee) {
        this.adaptee = adaptee;
    }

    public void actionPerformed(ActionEvent e) {
        adaptee.jMenuItemRandomData_actionPerformed(e);
    }
}


class FrameMain_btnSrcColor_mouseAdapter extends MouseAdapter {
    private FrameMain adaptee;
    FrameMain_btnSrcColor_mouseAdapter(FrameMain adaptee) {
        this.adaptee = adaptee;
    }

    public void mouseClicked(MouseEvent e) {
        adaptee.btnSrcColor_mouseClicked(e);
    }
}


class FrameMain_btnDstColor_mouseAdapter extends MouseAdapter {
    private FrameMain adaptee;
    FrameMain_btnDstColor_mouseAdapter(FrameMain adaptee) {
        this.adaptee = adaptee;
    }

    public void mouseClicked(MouseEvent e) {
        adaptee.btnDstColor_mouseClicked(e);
    }
}


class FrameMain_scrollPane_mouseAdapter extends MouseAdapter {
    private FrameMain adaptee;
    FrameMain_scrollPane_mouseAdapter(FrameMain adaptee) {
        this.adaptee = adaptee;
    }

    public void mousePressed(MouseEvent e) {
        adaptee.scrollPane_mousePressed(e);
    }

    public void mouseReleased(MouseEvent e) {
        adaptee.scrollPane_mouseReleased(e);
    }
}


class FrameMain_sliderAlpha_changeAdapter implements ChangeListener {
    private FrameMain adaptee;
    FrameMain_sliderAlpha_changeAdapter(FrameMain adaptee) {
        this.adaptee = adaptee;
    }

    public void stateChanged(ChangeEvent e) {
        adaptee.sliderAlpha_stateChanged(e);
    }
}


class FrameMain_jMenuItemColors_actionAdapter implements ActionListener {
    private FrameMain adaptee;
    FrameMain_jMenuItemColors_actionAdapter(FrameMain adaptee) {
        this.adaptee = adaptee;
    }

    public void actionPerformed(ActionEvent e) {
        adaptee.jMenuItemColors_actionPerformed(e);
    }
}


class FrameMain_jMenuItemOpen_actionAdapter implements ActionListener {
    private FrameMain adaptee;
    FrameMain_jMenuItemOpen_actionAdapter(FrameMain adaptee) {
        this.adaptee = adaptee;
    }

    public void actionPerformed(ActionEvent e) {
        adaptee.openDataFile(e);
    }
}


class FrameMain_jMenuFileExit_ActionAdapter implements ActionListener {
    FrameMain adaptee;

    FrameMain_jMenuFileExit_ActionAdapter(FrameMain adaptee) {
        this.adaptee = adaptee;
    }

    public void actionPerformed(ActionEvent actionEvent) {
        adaptee.jMenuFileExit_actionPerformed(actionEvent);
    }
}


class FrameMain_sliderBeta_changeAdapter implements javax.swing.event.
        ChangeListener {
    FrameMain adaptee;

    FrameMain_sliderBeta_changeAdapter(FrameMain adaptee) {
        this.adaptee = adaptee;
    }

    public void stateChanged(ChangeEvent e) {
        adaptee.sliderBeta_stateChanged(e);
    }

}

