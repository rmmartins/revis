package vispipeline.edgebundles.visualisation;

import java.awt.*;
import java.awt.image.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <p>Title: Minimum Composite</p>
 *
 * <p>Description: composing two colors intended to be viewed upon each other</p>
 * The rule used is the Minimum rule, which computes the minimum value for each
 * color component between the two colors, further the result is alpha-blended
 * with a white pixel.
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: TU Wien</p>
 *
 * @author Bilal Alsallakh
 * @version 1.0
 */
public class MINComposite implements CompositeContext, Composite {
    public final static MINComposite DEFAULT = new MINComposite();
    private double alpha;
    public void dispose() {
    }

    /**
     * composing two colors intended to be viewed upon each other
     * @param p_srcIn Raster
     * @param p_dstIn Raster
     * @param p_dstOut WritableRaster
     */
    public void compose(Raster p_srcIn, Raster p_dstIn,
                        WritableRaster p_dstOut) {
        // Walk the entire destination
        for (int x = 0; x < p_dstOut.getWidth(); x++) {
            for (int y = 0; y < p_dstOut.getHeight(); y++) {
//                try {
                    //   Get the source pixels
                int[] src = new int[4];

                try {
                    p_srcIn.getPixel(x, y, src);
                    int[] dst = new int[4];
                    p_dstIn.getPixel(x, y, dst);
                    // Create a logaction for the result
                    int[] result = new int[4];
                    result[0] = Math.min(src[0], dst[0]);
                    result[1] = Math.min(src[1], dst[1]);
                    result[2] = Math.min(src[2], dst[2]);
                    result[0] = (int) (alpha * 255 + (1 - alpha) * result[0]);
                    result[1] = (int) (alpha * 255 + (1 - alpha) * result[1]);
                    result[2] = (int) (alpha * 255 + (1 - alpha) * result[2]);
                    p_dstOut.setPixel(x, y, result);
                } catch (ArrayIndexOutOfBoundsException e) {
//                    Logger.getLogger(this.getClass().getName()).info(
//                            "x=" + x + ",y=" + y);
                    return;
                }
            }
        }
    }

    public CompositeContext createContext(ColorModel srcColorModel,
                                          ColorModel dstColorModel,
                                          RenderingHints hints) {
        return this;
    }

    /**
     * setAlpha: sets the current alpha blending value
     *
     * @param alpha double
     */
    public void setAlpha(double alpha) {
        this.alpha = alpha;
    }
}

