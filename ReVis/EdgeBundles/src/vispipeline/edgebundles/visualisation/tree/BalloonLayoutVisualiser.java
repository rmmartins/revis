package vispipeline.edgebundles.visualisation.tree;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import vispipeline.edgebundles.tree.VisualTree;
import vispipeline.edgebundles.tree.TreeNode;

/**
 * <p>Title: BalloonLayoutVisualiser</p>
 *
 * <p>Description: visualises a tree using the Balloon layout.
 * This layout visualse a node as circle, and its children as circles
 * (balloons) inside this circle.
 *
 * </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author Bilal Alsallakh
 * @version 1.0
 */
public class BalloonLayoutVisualiser extends TreeVisualiser {


    public String toString() {
        return "Balloon Layout";
    }

    Graphics g;
    double scalingFactor;

    private double[] nodeR, nodeGap, nodeScale, nodeAlpha;

    public void visualise(VisualTree tree, Graphics2D g, int width, int height) {
        this.g = g;
        nodeR = new double[tree.getNodesCount()];
        nodeGap = new double[tree.getNodesCount()];
        nodeScale = new double[tree.getNodesCount()];
        nodeAlpha = new double[tree.getNodesCount()];
        double dim = Math.min(height, width);
        layoutTree(tree.getRoot());
        drawTree(tree.getRoot(), 0, width / 2, height / 2, dim / 2,
                 Integer.MAX_VALUE);
    }

    public void drawTree(TreeNode node, double theta, int x, int y, double r,
                         int prevBallR) {
        node.setCenter(x, y);
        g.setColor(Color.BLUE);
        int ballR = Math.min((int) (r - 5), prevBallR - 13);
        ballR = Math.max(ballR, 3);
        int childrenCount = node.getChildrenCount();
        if (childrenCount > 0) {
            g.setColor(Color.LIGHT_GRAY);
            g.drawArc((int) (x - r), (int) (y - r),
                      (int) (2 * r), (int) (2 * r), 0, 360);
            double gap = 2 * nodeGap[node.getId()] / (childrenCount);
            double scale = this.nodeScale[node.getId()];
            double childTheta = theta + Math.PI - gap / 2;
            double prevAlpha = 0;
            double maxChildR = 0;
            for (int i = 0; i < childrenCount; i++) {
                TreeNode child = node.getChild(i);
                double childAlpha = (nodeAlpha[child.getId()] *= scale);
                double sin = Math.sin(childAlpha);
                double childR = r * sin / (1 + sin);
                childTheta += childAlpha + prevAlpha + gap;
                if (maxChildR < childR) {
                    maxChildR = childR;
                }
                prevAlpha = childAlpha;
                int childX = x + (int) ((r - childR) * Math.cos(childTheta)),
                             childY = y +
                                      (int) ((r - childR) * Math.sin(childTheta));
//        g.setColor(Color.DARK_GRAY);
//        g.drawLine(x, y, childX, childY);
                drawTree(child, childTheta, childX, childY, childR,
                         (int) (r - 2 * maxChildR));
            }
            ballR = Math.min((int) (r - 2 * maxChildR) - 3, prevBallR - 10);
            ballR = Math.max(ballR, 7);
        }
        g.setColor(Color.BLACK);
        g.fillArc(x - ballR, y - ballR, 2 * ballR, 2 * ballR, 0, 360);
    }

    int layoutTree(TreeNode node) {
        if (node.getChildrenCount() == 0) {
            return 1;
        } else {
            int maxR = 0;
            int circum = 0;
            for (int i = 0; i < node.getChildrenCount(); i++) {
                TreeNode child = node.getChild(i);
                int childR = layoutTree(child);
                nodeR[child.getId()] = childR;
                circum += childR;
                if (childR > maxR) {
                    maxR = childR;
                }
            }
            int r = 2 * maxR + 1;
            double alphasum = 0;
            for (int i = 0; i < node.getChildrenCount(); i++) {
                TreeNode child = node.getChild(i);
                double childR = nodeR[child.getId()];
                nodeAlpha[child.getId()] = Math.asin(childR / (r - childR));
                alphasum += nodeAlpha[child.getId()];
            }
            if (alphasum < Math.PI) {
                nodeScale[node.getId()] = 1;
                nodeGap[node.getId()] = (Math.PI - alphasum);
            } else {
                nodeScale[node.getId()] = Math.PI / alphasum;
                nodeGap[node.getId()] = 0;
            }
            node.setCenter(r, (int) circum);
            return r;
        }
    }
}
