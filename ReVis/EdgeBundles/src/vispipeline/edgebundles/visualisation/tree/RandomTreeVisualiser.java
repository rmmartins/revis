package vispipeline.edgebundles.visualisation.tree;

import java.awt.*;

import vispipeline.edgebundles.tree.TreeNode;
import vispipeline.edgebundles.tree.VisualTree;

/**
 * <p>Title: Random Layout Visualisation</p>
 *
 * <p>Description: This layout places the tree nodes randomly in the viewing
 * space! It was originally created as a first layout for testing the other
 * parts of the program, and then kept since it can produce art-like images:)
 * </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author Bilal Alsallakh
 * @version 1.0
 */
public class RandomTreeVisualiser extends TreeVisualiser {


    public String toString() {
        return "Random Layout";
    }

    int width, height;
    Graphics g;
    public void visualise(VisualTree tree, Graphics2D g,  int width, int height) {
        this.width = width;
        this.height = height;
        this.g = g;
        drawTree(tree.getRoot());
    }

    void drawTree(TreeNode node) {
        int x = (int)(Math.random() * width), y = (int)(Math.random() * height);
        node.setCenter(x, y);
        g.setColor(Color.RED);
        g.fillArc(x, y, 3, 3, 0, 360);
        g.setColor(Color.BLACK);
        for (int i = 0; i < node.getChildrenCount(); i++) {
            TreeNode child = node.getChild(i);
            drawTree(child);
            Point childLocation = child.getCenter();
            g.drawLine(x, y, childLocation.x, childLocation.y);
        }
    }

}
