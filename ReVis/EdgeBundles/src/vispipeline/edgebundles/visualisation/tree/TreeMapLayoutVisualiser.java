package vispipeline.edgebundles.visualisation.tree;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import vispipeline.edgebundles.tree.TreeNode;
import vispipeline.edgebundles.tree.VisualTree;

/**
 * <p>Title: TreeMapLayoutVisualiser</p>
 *
 * <p>Description: This class layouts a tree as a treemap as explained in.
 * http://www.cs.umd.edu/hcil/treemap-history/java_algorithms/LayoutApplet.html
 * Each node is repsented as a box, and subnodes are represened as boxes
 * contained in that box. The division of the box alternate from vertical to
 * horizontal each time the level is increased.
 * </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author Bilal Alsallakh
 * @version 1.0
 */


public class TreeMapLayoutVisualiser extends TreeVisualiser {

    public String toString() {
        return "TreeMap Layout";
    }

    Graphics g;
    public void visualise(VisualTree tree, Graphics2D g, int width, int height) {
        this.g = g;
        g.setColor(Color.DARK_GRAY);
        layout(tree.getRoot());
        drawTree(tree.getRoot(), 1, 1, width - 1, height - 1, true);
    }

    public void drawTree(TreeNode node, int x, int y, int width, int height,
                         boolean hor) {

        g.drawRect(x, y, width, height);
        int nodeSize = node.getCenter().x;
        int childX = x + 3, childY = y + 3;
        int childWidth = width - 6;
        int childHeight = height - 6;
        for (int i = 0; i < node.getChildrenCount(); i++) {
            TreeNode child = node.getChild(i);
            int childSize = child.getCenter().x;
            if (hor) {
                childWidth = (childSize *
                              (width - 3 * (node.getChildrenCount() + 1))) /
                             nodeSize;
            } else {
                childHeight = (childSize *
                               (height - 3 * (node.getChildrenCount() + 1))) /
                              nodeSize;
            }
            drawTree(child, childX, childY, childWidth, childHeight, !hor);
            if (hor) {
                childX += childWidth + 3;
            } else {
                childY += childHeight + 3;
            }
        }
        node.setCenter(x + width / 2, y + height / 2);
    }

    public int layout(TreeNode node) {
        if (node.getChildrenCount() == 0) {
            node.setCenter(1, 0);
            return 1;
        } else {
            int sum = 0;
            for (int i = 0; i < node.getChildrenCount(); i++) {
                sum += layout(node.getChild(i));
            }
            node.setCenter(sum, 0);
            return sum;
        }
    }
}
