package vispipeline.edgebundles.visualisation.tree;

import java.awt.*;

import vispipeline.edgebundles.tree.TreeNode;
import vispipeline.edgebundles.tree.VisualTree;


/**
 * <p>Title: Rooted Layout Visualiser</p>
 *
 * <p>Description: This class visualise a tree using the classical rooted tree
 * layout. The root is drawn at the top of the viewing area, and children are
 * drawn below it, and so on. Spacing bewteen children is packed so that the
 * leave nodes are equally distributed. The parent of a set of children nodes
 * is placed in the middle of their horizontal range.
 * </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author Bilal Alsallakh
 * @version 1.0
 */
public class RootedLayoutVisualiser extends TreeVisualiser {

    public String toString() {
        return "Rooted Tree Layout";
    }

    public void visualise(VisualTree tree, Graphics2D g, int width, int height) {
        currCol = maxLevel = 0;
        layoutTree(tree.getRoot(), 0);
        int xStep = width / (currCol - 1);
        int yStep = height / (maxLevel + 1);
        for (int i = 0; i < tree.getNodesCount(); i++) {
            TreeNode node = tree.getNode(i);
            Point childLocation = node.getCenter();
            childLocation.setLocation(childLocation.x * xStep + xStep / 2,
                                      childLocation.y * yStep + yStep / 2);
            g.setColor(Color.BLACK);
            g.fillArc(childLocation.x - 3, childLocation.y - 3, 6, 6, 0, 360);
            TreeNode parentNode = node.getParent();
            if (parentNode != null) {
                Point parentLocation = parentNode.getCenter();
                g.setColor(Color.DARK_GRAY);
                g.drawLine(childLocation.x, childLocation.y,
                           parentLocation.x, parentLocation.y);
            }
        }
    }

    int currCol, maxLevel;

    void layoutTree(TreeNode node, int level) {
        if (level > maxLevel) {
            maxLevel = level;
        }
        if (node.getChildrenCount() == 0) {
            node.setCenter(currCol, level);
            currCol += 2;
        } else {
            int avg = 0;
            for (int i = 0; i < node.getChildrenCount(); i++) {
                TreeNode child = node.getChild(i);
                layoutTree(child, level + 1);
                avg += child.getCenter().x;
            }
            node.setCenter(avg / node.getChildrenCount(), level);
        }
    }

}
