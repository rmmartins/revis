package vispipeline.edgebundles.visualisation.tree;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;

import vispipeline.edgebundles.tree.*;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.geom.Arc2D;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;

/**
 * <p>Title: InvertedRadialLayoutVisualiser</p>
 *
 * <p>Description: This layout is similar to the radial layout, but it reverses
 * the placement of the nodes, so that the root is in the most outer level
 * and the leaves are in the most inner level. It offers the advantage that an
 * inner circle is vacated offering good place to visualise adjacency relations,
 * and a desirable separation between hierachical and adjacency relations.
 *
 * Note 1: the logical (output) locations of the nodes are preserved as in the
 * radial layout, only the visualisation is inverted.
 * This enables thee links to follow the virtual (non-inverted) heirarchy
 * in the vacated inner place.
 * Note 2: this layout shoulbe be used only for trees where the adjacency
 * relations takes place only between leaves. Otherwise, a non-desirable
 * visalisation may occur, making a link originate from a node-free location...
 * </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author Bilal Alsallakh
 * @version 1.0
 */
public class InvertedRadialLayoutVisualiser extends TreeVisualiser {

    public String toString() {
        return "Inverted Radial Layout";
    }
    private int outerRStep, innerRStep, innerR;
    private double alphaStep;
    Graphics2D g;
    int centerX, centerY;

    @Override
    public void visualise(VisualTree tree, Graphics2D g, int width, int height) {
        g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
//        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
//        g.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
        g.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
        g.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);

        currAngle = maxLevel = 0;
        this.centerX = width / 2;
        this.centerY = height / 2;
        this.g = g;
        layoutTree(tree.getRoot(), 0);
        int dim = Math.min(height, width);
        innerR = 4 * dim / 10;
        innerRStep = innerR / maxLevel;
        outerRStep = (dim / 2 - innerR) / maxLevel;
        alphaStep = 360d / currAngle;
        for (int i = 0; i < tree.getRoot().getChildrenCount(); i++) {
            drawNode(tree.getRoot().getChild(i), 1);
        }
    }
    int currAngle, maxLevel;

    void drawNode(TreeNode node, int level) {
        g.setStroke(new BasicStroke());
        Point p = node.getCenter();
        int amin = (int) (p.x * alphaStep);
        int amax = (int) (p.y * alphaStep);
        double aminRadian = amin * Math.PI / 180,
                amaxRadian = amax * Math.PI / 180;
        int x;
        int y;
        double cosAMid = Math.cos((aminRadian + amaxRadian) / 2);
        double sinAMid = Math.sin((aminRadian + amaxRadian) / 2);

        if (node.getChildrenCount() == 0) {
            int ro = innerR + (maxLevel - level + 1) * outerRStep
                    + (level == 1 ? -3 : 3);
            x = (int) (innerR * cosAMid) + centerX;
            y = (int) (innerR * sinAMid) + centerY;
            int x1 = (int) (ro * cosAMid) + centerX;
            int y1 = (int) (ro * sinAMid) + centerY;

            g.setColor(Color.BLACK);
            g.drawLine(x, y, x1, y1);

            g.setPaintMode();
            g.setColor(getColorOf(node));
//            Logger.getLogger(this.getClass().getName()).info(
//                    "node="+node.toString()+",color="+getColorOf(node));
            g.fillOval(x - (diam / 2), y - (diam / 2), (diam / 2) * 2, (diam / 2) * 2);
            g.setColor(Color.BLACK);
            g.drawOval(x - (diam / 2), y - (diam / 2), (diam / 2) * 2, (diam / 2) * 2);

            if (node.isSelected()) {
                g.setColor(Color.DARK_GRAY);
                ((Graphics2D) g).setStroke(new BasicStroke(4.0f));
                int sdiam = (diam / 2) + 1;
                g.drawOval(x - sdiam, y - sdiam, sdiam * 2, sdiam * 2);
                ((Graphics2D) g).setStroke(new BasicStroke());
            }
            // area
            Area area = new Area(new Ellipse2D.Double(x - (diam / 2),
                    y - (diam / 2), (diam / 2) * 2, (diam / 2) * 2));
            node.setArea(area);
        } else {
            x = (int) (level * innerRStep * cosAMid) + centerX;
            y = (int) (level * innerRStep * sinAMid) + centerY;
            for (int i = 0; i < node.getChildrenCount(); i++) {
                drawNode(node.getChild(i), level + 1);
            }
            double cos1 = Math.cos(aminRadian), cos2 = Math.cos(amaxRadian),
                    sin1 = Math.sin(aminRadian), sin2 = Math.sin(amaxRadian);
            int ri = innerR + (maxLevel - level) * outerRStep + 3;
            int ro = innerR + (maxLevel - level + 1) * outerRStep - 3;
            int x1 = (int) (ri * cos1) + centerX;
            int y1 = (int) (ri * sin1) + centerY;
            int x2 = (int) (ri * cos2) + centerX;
            int y2 = (int) (ri * sin2) + centerY;
            int x3 = (int) (ro * cos1) + centerX;
            int y3 = (int) (ro * sin1) + centerY;
            int x4 = (int) (ro * cos2) + centerX;
            int y4 = (int) (ro * sin2) + centerY;
            g.setColor(Color.BLACK);
            g.drawArc(centerX - ri, centerY - ri, 2 * ri, 2 * ri, -amin,
                    amin - amax);
            g.drawArc(centerX - ro, centerY - ro, 2 * ro, 2 * ro, -amin,
                    amin - amax);
            g.drawLine(x1, y1, x3, y3);
            g.drawLine(x2, y2, x4, y4);
            // area
            Arc2D smallArc = new Arc2D.Double(centerX - ri, centerY - ri, 2 * ri,
                    2 * ri, -amin, amin - amax, Arc2D.PIE);
            Arc2D bigArc = new Arc2D.Double(centerX - ro, centerY - ro, 2 * ro,
                    2 * ro, -amin, amin - amax, Arc2D.PIE);
            Area area = new Area(bigArc);
            area.subtract(new Area(smallArc));            
            node.setArea(area);
        }
        p.setLocation(x, y);
    }

    void layoutTree(TreeNode node, int level) {
        if (level > maxLevel) {
            maxLevel = level;
        }
        if (node.getChildrenCount() == 0) {
            node.setCenter(currAngle, currAngle + 1);
            currAngle += 2;
        } else {
            TreeNode child = node.getChild(0);
            layoutTree(child, level + 1);
            int minAngle = child.getCenter().x;
            for (int i = 1; i < node.getChildrenCount(); i++) {
                child = node.getChild(i);
                layoutTree(child, level + 1);
            }
            int maxAngle = child.getCenter().y;
            node.setCenter(minAngle, maxAngle);
        }
    }
}
