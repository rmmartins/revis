package vispipeline.edgebundles.visualisation.tree;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.logging.Logger;
import vispipeline.edgebundles.tree.TreeNode;

import vispipeline.edgebundles.tree.VisualTree;
import vispipeline.visualization.color.ColorScale;
import vispipeline.visualization.model.AbstractInstance;

/**
 * <p>Title: Tree Visualiser</p>
 *
 * <p>Description: Abstract Class and a factory for all tree visualizers</p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author Bilal Alsallakh
 * @version 1.0
 */
public abstract class TreeVisualiser {

    /**
     * visualise
     *
     * @param tree VisualTree: The tree to visualise
     * @param g Graphics: port to a canvas with drawing primitives
     * @param width: The width (in pixels) of the drawing area
     * @param height: The height (in pixels) of the drawing area
     *
     * The visualisation is performed for all layouts in two major steps,
     * first the nodes are assigned relative coordinates, then the actual
     * placement and size for each node is calculated, based on the available
     * drawing space. The RandomLayout is an exception since it assigns
     * random coordinates for the nodes inside the available space.
     */

    public abstract void visualise(VisualTree tree, Graphics2D g,
                                   int width, int height);

    // list of tree visualisers
    private static TreeVisualiser[] treeVis = {                               
                               new InvertedRadialLayoutVisualiser(),
                               new RadialLayoutVisualiser(),
                               new RootedLayoutVisualiser(),
                               new InvertedRootedLayoutVisualiser(),
                               new BalloonLayoutVisualiser(),
                               new TreeMapLayoutVisualiser(),
                               new RandomTreeVisualiser()
    };

    /**
     * getVisualisersCount
     *
     * returns the number of available visualisers
     */

    public static int getVisualisersCount() {
        return treeVis.length;
    }

    /**
     * getVisualiser
     *
     * returns the ith visualiser in the visualisers list.
     * @param i: the index of the visualiser
     */

    public static TreeVisualiser getVisualiser(int i) {
        return treeVis[i];
    }

    protected Color getColorOf(TreeNode node) {
        float value = node.getNormalizedScalarValue(
                node.getModel().getSelectedScalar());
//        Logger.getLogger(this.getClass().getName()).info(
//                    "node="+node.toString()+",scalarvalue="+value+",color="+scale.getColor(value));
        return scale.getColor(value);
    }

    public void setColorScale(ColorScale scale) {
        this.scale = scale;
    }

    public ColorScale getColorScale() {
        return scale;
    }

    public int getDiam() {
        return diam;
    }

    protected int diam = 10; // instance draw diameter
    protected ColorScale scale;

}
