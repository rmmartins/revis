package vispipeline.edgebundles.visualisation.tree;


import vispipeline.edgebundles.tree.*;
import java.awt.Point;
import java.awt.Color;
import java.awt.Graphics2D;

/**
 * <p>Title: Radial Layout Visualiser</p>
 *
 * <p>Description: This visualiser palces the root node in the center of
 * the viewing area, then distribute its child nodes radially around it.
 * Each child node occupies a sector proportional to the number of leave nodes
 * it has. Then further children are drawn on a circle around the first-level
 * children, where each child is drawn in the sector of its parent node.
 * </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author Bilal Alsallakh
 * @version 1.0
 */
public class RadialLayoutVisualiser extends TreeVisualiser {

    public String toString() {
        return "Radial Layout";
    }

    public void visualise(VisualTree tree, Graphics2D g,  int width, int height) {
        currAngle =  maxLevel = 0;
        layoutTree(tree.getRoot(), 0);
        int dim = Math.min(height, width);
        int rStep = (dim - 5) / 2 / maxLevel;
        double alphaStep = (2 * Math.PI) / currAngle;
        g.setColor(Color.LIGHT_GRAY);
        for (int l = 0; l < maxLevel; l++)
            g.drawArc(width / 2 - (l + 1) * rStep, height / 2 - (l + 1) * rStep, (l + 1) * rStep * 2, (l + 1) * rStep * 2, 0, 360);
        for (int i = 0; i < tree.getNodesCount(); i++) {
            TreeNode node = tree.getNode(i);
            Point childLocation = node.getCenter();
            int r = childLocation.y * rStep;
            double a = childLocation.x * alphaStep;
            childLocation.setLocation(r * Math.cos(a) + width / 2, r * Math.sin(a) + height / 2);
            g.setColor(Color.BLACK);
            g.fillArc(childLocation.x - 3, childLocation.y - 3, 6, 6, 0, 360);
//            g.drawString(node.toString(), childLocation.x - 6, childLocation.y - 6);
            TreeNode parentNode = node.getParent();
            if (parentNode != null) {
                Point parentLocation = parentNode.getCenter();
                g.setColor(Color.DARK_GRAY);
                g.drawLine(childLocation.x, childLocation.y,
                           parentLocation.x, parentLocation.y);
            }
        }

    }

    int currAngle, maxLevel;

    void layoutTree(TreeNode node, int level) {
        if (level > maxLevel)
            maxLevel = level;
        if (node.getChildrenCount() == 0) {
            node.setCenter(currAngle, level);
            currAngle += 2;
        }
        else {
            int avg = 0;
            for (int i = 0; i < node.getChildrenCount(); i++) {
                TreeNode child = node.getChild(i);
                layoutTree(child, level + 1);
                avg += child.getCenter().x;
            }
            node.setCenter(avg / node.getChildrenCount(), level);
        }
    }

}
