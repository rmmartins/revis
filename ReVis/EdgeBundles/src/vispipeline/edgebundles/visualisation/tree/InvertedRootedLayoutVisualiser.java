package vispipeline.edgebundles.visualisation.tree;

import java.awt.*;

import vispipeline.edgebundles.tree.TreeNode;
import vispipeline.edgebundles.tree.VisualTree;

/**
 * <p>Title: Inverted Rooted Layout Visualiser </p>
 *
 * <p>Description: This layout is identical to the rooted layout, but the
 * nodes are logically (as opposed to visually), assigned inverted locations
 * (about the vertical axes which passes through the last level).
 * These locations will guide the links representing adjacency relations, which
 * means in end effect, a desired sepratation between hierarchical and
 * adjancency relations (for the purpose of clarity).
 *
 * Note 1: the logical (output) locations of the nodes are NOT identical to the
 * displayed locations.
 *
 * Note 2: this layout shoulbe be used only for trees where the adjacency
 * relations takes place only between leaves. Otherwise, a non-desirable
 * visalisation may occur, making a link originate from a node-free location...
 * </p>
 *
 * </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author Bilal Alsallakh
 * @version 1.0
 */
public class InvertedRootedLayoutVisualiser extends TreeVisualiser {

    public String toString() {
        return "Inv. Rooted Tree Layout";
    }

    public void visualise(VisualTree tree, Graphics2D g, int width, int height) {
        currCol = maxLevel = 0;
        layoutTree(tree.getRoot(), 0);
        int xStep = width / (currCol - 1);
        int treeHeight = height / 2;
        int yStep = treeHeight / maxLevel;
        for (int i = 0; i < tree.getNodesCount(); i++) {
            TreeNode node = tree.getNode(i);
            Point childLocation = node.getCenter();
            childLocation.setLocation(childLocation.x * xStep + xStep / 2,
                                      childLocation.y * yStep + yStep / 2);            
            TreeNode parentNode = node.getParent();
            if (parentNode != null) {
                Point parentLocation = parentNode.getCenter();
                g.setColor(Color.LIGHT_GRAY);
                g.drawLine(childLocation.x, childLocation.y,
                           parentLocation.x, parentLocation.y);
                if (node.getChildrenCount() == 0) {
                    g.drawLine(childLocation.x, childLocation.y,
                               childLocation.x, treeHeight + yStep / 2);
                    childLocation.y = treeHeight + yStep / 2;
                }
            }
            g.setColor(Color.BLACK);
            g.fillArc(childLocation.x - 3, childLocation.y - 3, 6, 6, 0, 360);
        }
        for (int i = 0; i < tree.getNodesCount(); i++) {
            TreeNode node = tree.getNode(i);
            Point childLocation = node.getCenter();
            childLocation.y = 2 * treeHeight + yStep - childLocation.y;
        }
    }

    int currCol, maxLevel;

    void layoutTree(TreeNode node, int level) {
        if (level > maxLevel) {
            maxLevel = level;
        }
        if (node.getChildrenCount() == 0) {
            node.setCenter(currCol, level);
            currCol += 2;
        } else {
            int avg = 0;
            for (int i = 0; i < node.getChildrenCount(); i++) {
                TreeNode child = node.getChild(i);
                layoutTree(child, level + 1);
                avg += child.getCenter().x;
            }
            node.setCenter(avg / node.getChildrenCount(), level);
        }
    }

}
