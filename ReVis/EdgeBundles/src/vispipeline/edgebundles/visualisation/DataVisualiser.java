package vispipeline.edgebundles.visualisation;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.geom.Area;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.JComboBox;

import vispipeline.edgebundles.io.DataReader;
import vispipeline.edgebundles.tree.TreeNode;
import vispipeline.edgebundles.tree.VisualTree;
import vispipeline.edgebundles.visualisation.link.BSplineLinkVisualiser;
import vispipeline.edgebundles.visualisation.link.Link;
import vispipeline.edgebundles.visualisation.tree.TreeVisualiser;
import vispipeline.visualization.model.AbstractInstance;
import vispipeline.visualization.view.ModelViewer;

/**
 * <p>Title: Data Visualiser
 * </p>
 *
 * <p>Description: This class visualises the data (hierarchical + adjacency
 * relations), using adjustable tree layout layout, and a link visualiser.
 * The bundling degree as well as the alpha blending can be conrolled.
 * </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class DataVisualiser extends ModelViewer {

//    private VisualTree visualTree;
    protected BSplineLinkVisualiser linkVis;
    protected TreeVisualiser treeVis;
    protected double maxAlpha, beta;

    public DataVisualiser() {
        setMaxAlpha(0.8);
        setBeta(0.5);
        setTreeVisualiser(TreeVisualiser.getVisualiser(0));
        setLinkVisualiser(new BSplineLinkVisualiser(
                Color.CYAN, Color.BLUE));        
    }

    public DataVisualiser(TreeVisualiser treeVis, BSplineLinkVisualiser linkVis,
                          double maxAlpha, double beta) {
        setMaxAlpha(maxAlpha);
        setBeta(beta);
        setTreeVisualiser(treeVis);
        setLinkVisualiser(linkVis);
    }

    protected BasicStroke thickStroke = new BasicStroke(2);
    protected Stroke thinStroke = new BasicStroke(0);

    protected MINComposite minComposite = MINComposite.DEFAULT;

    /**
     * visualisees the current dataset (if any) on the given Graphics object
     *
     * @param g Graphics2D, port to the drawing area
     * @param width int dimensions of the drawing area
     * @param height int dimensions of the drawing area
     */
    public void visualise(Graphics2D g, int width, int height) {
        g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        g.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
        g.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
        g.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);

        g.setColor(Color.WHITE);
        g.fillRect(0, 0, width, height);
        g.drawRect(0, 0, width - 1, height - 1);

        VisualTree visualTree = (VisualTree)model;
        if (visualTree != null) {
            treeVis.visualise(visualTree, g, width, height);
            int linksCount = 0;
            for (int i = 0; i < visualTree.getNodesCount(); i++) {
                linksCount += visualTree.getNode(i).getAdjacentNodesCount();
            }
            if (linksCount == 0) {
                return;
            }
            // computing the links (the bundled control polygons and their
            // length
            Link[] links = new Link[linksCount];
            int ind = 0;
            for (int i = 0; i < visualTree.getNodesCount(); i++) {
                TreeNode srcNode = visualTree.getNode(i);
                for (int j = 0; j < srcNode.getAdjacentNodesCount(); j++, ind++) {
                    ArrayList treePath = getPath(srcNode,
                                                 srcNode.getAdjacentNode(j));
                    links[ind] = new Link(treePath, beta, srcNode,
                                                 srcNode.getAdjacentNode(j));
                }
            }
            // sorting the links
            Arrays.sort(links, new java.util.Comparator() {
                public int compare(Object o1, Object o2) {
                    return ((Link) o1).length > ((Link) o2).length ? 1 : -1;
                }
                public boolean equals(Object obj) {
                    return false;
                }
            });
            // viewing the links starting with the longest, and assinging the
            // alpha blending value for each link
            double alphaStep;
            g.setColor(Color.WHITE);
            g.fillRect(0, 0, width, height);
            g.drawRect(0, 0, width - 1, height - 1);
            g.setComposite(minComposite);
//            g.setColor(Color.GRAY);
            g.setStroke(thickStroke);
            alphaStep = maxAlpha /
                        (links[linksCount - 1].length - links[0].length);
            for (int i = links.length - 1; i >= 0; i--) {
                double alpha = alphaStep * (links[i].length - links[0].length);
                minComposite.setAlpha(alpha);
                linkVis.drawLink(g, links[i]);
            }
            minComposite.setAlpha(0);
            g.setStroke(thinStroke);
            treeVis.visualise(visualTree, g, width, height);
        }
    }


    /**
     * setBeta
     *
     * @param beta double, the value for bundling strength (0 for no bundling
     * i.e. straight line links, 1 for full bundling i.e. b-spline for the
     * exact tree path, between ]0,1[ for partial bundling
     *
     */
    public void setBeta(double beta) {
        this.beta = beta;
    }

    /**
     * getBeta
     *
     * returns the current value of beta, the bundling strength
     */
    public double getBeta() {
        return beta;
    }

    /**
     * getMaxAlpha
     *
     * returns the maximum value of alpha blending, which will be assigned
     * to the longest link (and zero-blending to the shortest).
     */
    public double getMaxAlpha() {
        return maxAlpha;
    }

    /**
     * setMaxAlpha
     *
     * sets the maximum value of alpha blending, which will be assigned
     * to the longest link (and zero-blending to the shortest).
     *
     * @param maxAlpha the desired maximum value for alpha blending (should be
     * in [0, 1]).
     */
    public void setMaxAlpha(double maxAlpha) {
        this.maxAlpha = maxAlpha;
    }

    // this method returns the treepath between two given nodes
    ArrayList node1Parents = new ArrayList();
    ArrayList node2Parents = new ArrayList();
    protected ArrayList getPath(TreeNode srcNode, TreeNode destNode) {
        TreeNode node = srcNode;
        node1Parents.clear();
        while (node != null) {
            node1Parents.add(node);
            node = node.getParent();
        }
        node = destNode;
        node2Parents.clear();
        while (node != null) {
            node2Parents.add(node);
            node = node.getParent();
        }
        int n1 = node1Parents.size() - 1, n2 = node2Parents.size() - 1;
        while (n1 >= 0 && n2 >= 0 &&
               node1Parents.get(n1).equals(node2Parents.get(n2))) {
            node1Parents.remove(n1);
            n1--;
            n2--;
        }
        if ((n1 == 0 && n2 == 0) || (n2 < 0) || (n1 < 0)) {
            n2++;
        }
        for (; n2 >= 0; n2--) {
            node1Parents.add(node2Parents.get(n2));
        }
        return node1Parents;
    }

    /**
     * setTreeVisualiser, sets the tree visualisation algorithm to use
     *
     * @param treeVis, the tree visualsier to use
     */
    public void setTreeVisualiser(TreeVisualiser treeVis) {
        this.treeVis = treeVis;
        repaint();
    }

    /**
     * setLinkVisualiser, sets the link visualisation algorithm to use
     * Currently there is only one implemented visualiser, the
     * BSpline based one.
     * The link visualiser is assumed to handle the start and end color
     * for the link, and enable the user to assign them.
     * @param linkVis, the link visualsier to use
     */

    public void setLinkVisualiser(BSplineLinkVisualiser linkVis) {
        this.linkVis = linkVis;
    }


    /**
     * setData, sets the dataset to visualise
     * @param dataReader, the datareader which provides the hierachical and
     * the adjacnaey relations to visualise. A more elaborate data structure
     * is created from this data reader, which offers fast answers to many
     * queries.
     */
    public void setData(DataReader dataReader) {
        VisualTree visualTree = (VisualTree) model;
        if (visualTree == null)
            visualTree = new VisualTree();
        visualTree.fill(dataReader);

    }

//    public VisualTree getVisualTree() {
//        return visualTree;
//    }
//
//    public void setVisualTree(VisualTree tree) {
//        visualTree = tree;
//    }

    public TreeVisualiser getTreeVisualizer() {
        return treeVis;
    }

    // ModelViewer

    protected AbstractInstance getInstanceByPosition(Point p, boolean leafOnly) {
        for (AbstractInstance inst : model.getInstances()) {
            TreeNode node = (TreeNode) inst;
            if (leafOnly && node.getChildrenCount() > 0) {
                continue;
            }
            Area area = node.getArea();
            if (area != null && area.contains(p)) {
                return node;
            }
        }
        return null;
    }

    @Override
    public AbstractInstance getInstanceByPosition(Point p) {
        return getInstanceByPosition(p, true);
    }

    @Override
    public ArrayList<AbstractInstance> getInstancesByPosition(Polygon p) {
        VisualTree tree = (VisualTree) model;
        ArrayList<AbstractInstance> ret = new ArrayList<AbstractInstance>();
        for (int i = 0; i < tree.getNodesCount(); i++) {
            TreeNode node = tree.getNode(i);
            if (p.contains(node.getCenter()) && node.getChildrenCount() == 0) {
                ret.add(node);
            }
        }
        return ret;
    }

    @Override
    public ArrayList<AbstractInstance> getInstancesByPosition(Rectangle r) {
        VisualTree tree = (VisualTree) model;
        ArrayList<AbstractInstance> ret = new ArrayList<AbstractInstance>();
        for (int i = 0; i < tree.getNodesCount(); i++) {
            TreeNode node = tree.getNode(i);
            if (r.contains(node.getCenter()) && node.getChildrenCount() == 0) {
                ret.add(node);
            }
        }
        return ret;
    }

    @Override
    public void zoom(float rate) {
        
    }

    @Override
    public void draw(Graphics2D g) {
//        Logger.getLogger(getClass().getName()).info("draw " + getWidth() + " " + getHeight());
        visualise(g, getWidth(), getHeight());
    }

    @Override
    public String toString() {
        return treeVis.toString();
    }

    // Actions:

    class SelectTreeVisAction extends AbstractAction {

        public SelectTreeVisAction() {
            TreeVisualiser[] treeVis =
                    new TreeVisualiser[TreeVisualiser.getVisualisersCount()];
            for (int i = 0; i < TreeVisualiser.getVisualisersCount(); i++) {
                treeVis[i] = TreeVisualiser.getVisualiser(i);
            }
            JComboBox jcb = new JComboBox(treeVis);
            putValue(ModelViewer.COMPONENT, jcb);
        }

        public void actionPerformed(ActionEvent e) {
            if (e.getSource() instanceof JComboBox) {
                TreeVisualiser treeVis =
                        (TreeVisualiser) ((JComboBox) e.getSource()).getSelectedItem();
                setTreeVisualiser(treeVis);
            }
        }
    }

}

