package vispipeline.edgebundles.visualisation.link;

import java.util.ArrayList;
import vispipeline.edgebundles.tree.*;
import java.awt.Point;

/**
 * <p>Title: Link</p>
 *
 * <p>Description: The link objects stores the control points of the control
 * polygon of a link and pre compute the length of the control polygon.
 * The actual points and their colors of the link, will be computed in the
 * Link Visualiser.
 * </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: TU Wien</p>
 *
 * @author not attributable
 * @version 1.0
 */
public class Link {

    public final float length;

    public Point[] points;

    /**
     * Link
     *
     * @param path: the tree path which the link is based upon
     * @param beta: the degree of bundling (0: Link is a straigtline, 1: link
     * is identical to the path input parameter, and from ]0, 1[ is a compormise
     * between these two exteremeties.
     */

    public Link(ArrayList path, double beta, TreeNode src, TreeNode dest) {
        this.path = path;
        this.src = src;
        this.dest = dest;
        int n = path.size() - 1;
        points = new Point[path.size()];
        points[0] = ((TreeNode)path.get(0)).getCenter();
        points[n] = ((TreeNode)path.get(n)).getCenter();
        float sum = 0;
        for (int i = 1; i <= n; i++) {
            Point p = ((TreeNode)path.get(i)).getCenter();
            points[i] = new Point(
                    (int)(beta * p.x + (1 - beta)*(points[0].x +
                    (points[n].x - points[0].x) * i / n)),
                    (int)(beta * p.y + (1 - beta)*(points[0].y +
                    (points[n].y - points[0].y) * i / n)));
            sum += points[i].distance(points[i - 1]);
        }
        length = sum;
    }
    
    public ArrayList getPath() {
        return path;
    }

    public TreeNode getDest() {
        return dest;
    }

    public TreeNode getSrc() {
        return src;
    }
    
    private ArrayList path;
    private TreeNode src;
    private TreeNode dest;
    
}
