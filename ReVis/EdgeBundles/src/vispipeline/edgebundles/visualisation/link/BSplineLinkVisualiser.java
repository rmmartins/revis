package vispipeline.edgebundles.visualisation.link;

import java.awt.*;
import java.util.logging.Logger;


/**
 * BSplineLinkVisualiser
 *
 * Visualises a link as a BSpline curve based on the link as a control polygon
 * The curve is drawn using gradiant colors from the given start and end colors.
 */

public class BSplineLinkVisualiser {

    private int splineRes = 10;

    private Color startColor, endColor;

    private Color[] colorGradient = new Color[25];

    private int currColor;

    /**
     * drawLink
     *
     * @param lnk: The control polygon representing the link to draw
     * @param g: a port to the drawing area with prmitive methods
     *
     * This method draws the given links on the given Graphics object.
     * A colored cubic and uniform B-Spline is drawn based on the given control
     * polygon. The color changes gradually from start to end color
     */

    public void drawLink(Graphics2D g, Link lnk) {
        float len = lnk.length;
        int n = lnk.points.length - 1;
        Point[] p = new Point[4];
        for (int i = 0; i < p.length - 1; i++) {
            p[i] = lnk.points[0];
        }
        Point q1 = new Point(p[0]), q2 = new Point();
        float lenSum = 0;
        int j = 0;
        currColor = 0;
        for (int i = 1; i < n + 3; i++) {
            if (i <= n) {
                j = i;
            }
            p[3] = lnk.points[j];
            lenSum +=
                    (p[1].distance(p[2]) + p[0].distance(p[1]) +
                     p[2].distance(p[3])) / 3;
            int colorLim = (int) (((colorGradient.length - 1) * lenSum) / len);
            for (int t = 1; t < splineRes; t++) {
                getSplinePoint(p, ((float) t) / splineRes, q2);

                try {
                    g.drawLine(q1.x, q1.y, q2.x, q2.y);
                } catch (ArrayIndexOutOfBoundsException e) {
//                    Logger.getLogger(this.getClass().getName()).info(
//                            "q1.x=" + q1.x + ",q1.y=" + q1.y + ",q2.x=" + q2.x + ",q2.y=" + q2.y);
                    throw e;
                }

                int colorInd = currColor +
                               (t * (colorLim - currColor) / (splineRes - 1));
                g.setColor(colorGradient[colorInd]);
                q1.setLocation(q2);
            }
            currColor = colorLim;
            getSplinePoint(p, 1, q2);
            g.drawLine(q1.x, q1.y, q2.x, q2.y);
            q1.setLocation(q2);
            p[0] = p[1];
            p[1] = p[2];
            p[2] = p[3];
        }
    }

    private double[] a = new double[4];
    private void getSplinePoint(Point[] p, double t, Point pout) {
        double t2 = t * t;
        double t3 = t2 * t;
        a[0] = -1 * t3 + 3 * t2 - 3 * t + 1;
        a[1] = +3 * t3 - 6 * t2 + 4;
        a[2] = -3 * t3 + 3 * t2 + 3 * t + 1;
        a[3] = +1 * t3;
        double x = 0, y = 0;
        for (int i = 0; i < a.length; i++) {
            x += a[i] * p[i].x;
            y += a[i] * p[i].y;
        }
        pout.setLocation(x / 6, y / 6);
    }

    /**
     * getStartColor
     *
     * returns the start color.
     */

    public Color getStartColor() {
        return startColor;
    }

    /**
     * getEndColor
     *
     * returns the end color
     */

    public Color getEndColor() {
        return endColor;
    }

    /**
     * setStadtAndEndColor
     *
     * @param startColor: the start color, the color at the beginning (source)
     * of the link
     * @param endColor: the color at the end (destination) of the link
     *
     * sets the start and the end colors, and precompute the spectrum between
     * them.
     */

    public void setStartAndEndColor(Color startColor, Color endColor) {
        this.endColor = endColor;
        this.startColor = startColor;
        int n = colorGradient.length - 1;
        for (int i = 0; i < colorGradient.length; i++) {
            colorGradient[i] = new Color(
                    startColor.getRed() + i *
                    (endColor.getRed() - startColor.getRed()) / n,
                    startColor.getGreen() + i *
                    (endColor.getGreen() - startColor.getGreen()) / n,
                    startColor.getBlue() + i *
                    (endColor.getBlue() - startColor.getBlue()) / n
                               );
        }
    }


    /**
     * BSplineLinkVisualiser
     *
     * @param startColor: the start color, the color at the beginning (source)
     * of the link
     * @param endColor: the color at the end (destination) of the link
     *
     * constructs the visualiser objects, and assigns the start and the end
     * colors.
     */

    public BSplineLinkVisualiser(Color startColor,
                                 Color endColor) {
        setStartAndEndColor(startColor, endColor);
    }
}
