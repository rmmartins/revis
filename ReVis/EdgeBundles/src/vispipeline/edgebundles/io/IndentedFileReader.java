package vispipeline.edgebundles.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.util.ArrayList;

/**
 * <p>Title: NOT YET TESTED, Indented File Reader</p>
 *
 * <p>Description: , a class to read another file format,
 * which represents the hierarchy by indented lines
 * </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class IndentedFileReader implements DataReader {

    BufferedReader fileReader;
    boolean directed;
    ArrayList parentNodes = new ArrayList();
    int[][] adjacencyList;
    public IndentedFileReader(File file) throws IOException {
        fileReader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
        directed = Boolean.valueOf(fileReader.readLine()).booleanValue();
        String line = fileReader.readLine();

        line = fileReader.readLine();
        while (!line.equals("")) {
            line = fileReader.readLine();
            int indent = line.lastIndexOf(' ');
            int parent = 0;
            parentNodes.add(new Integer(parent));
        }
        adjacencyList = new int[getNodesCount()][];
        String[] tokens;
        line = fileReader.readLine();
        while (line != null) {
            tokens = line.split("[: ]+");
            int node = Integer.parseInt(tokens[0]) - 1;
            adjacencyList[node] = new int[tokens.length - 1];
            for (int i = 0; i < adjacencyList[node].length; i++) {
                adjacencyList[node][i] = Integer.parseInt(tokens[i + 1]) - 1;
            }
            line = fileReader.readLine();
        }
        for (int i = 0; i < adjacencyList.length; i++) {
            if (adjacencyList[i] == null)
                adjacencyList[i] = new int[0];
        }
    }

    public int getNodesCount() {
        return parentNodes.size();
    }

    public int[] getAdjacentNodes(int i) {
        return adjacencyList[i];
    }

    public int getParent(int node) {
        return ((Integer)parentNodes.get(node)).intValue();
    }

    public String getCaption(int node) {
        return String.valueOf(node);
    }

}
