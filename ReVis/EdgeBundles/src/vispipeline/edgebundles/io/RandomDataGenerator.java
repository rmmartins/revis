package vispipeline.edgebundles.io;

import java.util.Arrays;
import java.util.ArrayList;

/**
 * <p>Title: Random Data Generator</p>
 *
 * <p>Description: A data provider which create random hierarchical and adjacency
 * relations for the purpose of testing
 * displaying adjacency relations in hierarchy
 * </p>
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: TU Wien</p>
 *
 * @author Bilal Alsallakh
 * @version 1.0
 */
public class RandomDataGenerator implements DataReader {
    int[] parentNodes;
   int[][] adjacencyList;

    public RandomDataGenerator(int n, int linksNum) {
        parentNodes = new int[n];
        parentNodes[0] = -1;
        boolean[] innerNode = new boolean[n];
        for (int i = 1; i < n; i++) {
            int ind = (int) (2 * i * Math.random() / 3);
            parentNodes[i] = ind;
            innerNode[ind] = true;
        }
        ArrayList leaves = new ArrayList();
        for (int i = 1; i < n; i++)
            if (!innerNode[i])
                leaves.add(Integer.valueOf(i));
        ArrayList[] links = new ArrayList[n];
        for (int j = 0; j < n; j++) {
            links[j] = new ArrayList();
        }
        int i = 0;
        while (i < linksNum) {
            int src = (int)(Math.random() * (leaves.size() - 1));
            int dst = src +1 + (int)(Math.random() * (leaves.size() - src - 1));
            src = ((Integer)leaves.get(src)).intValue();
            dst = ((Integer)leaves.get(dst)).intValue();
            boolean newLink = true;
            for (int j = 0; j < links[src].size(); j++) {
                if (links[src].get(j).equals(Integer.valueOf(dst)))
                    newLink = false;
            }
            if (newLink) {
                links[src].add(Integer.valueOf(dst));
                i++;
            }
        }
        adjacencyList = new int[n][];
        for (i = 0; i < links.length; i++) {
            adjacencyList[i] = new int[links[i].size()];
            for (int j = 0; j < adjacencyList[i].length; j++) {
                adjacencyList[i][j] = ((Integer)(links[i].get(j))).intValue();
            }
        }
    }

    public int getNodesCount() {
        return parentNodes.length;
    }

    public int[] getAdjacentNodes(int i) {
        return adjacencyList[i];
    }

    public int getParent(int node) {
        return parentNodes[node];
    }

    public String getCaption(int node) {
        return String.valueOf(node);
    }
}
