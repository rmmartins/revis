package vispipeline.edgebundles.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * <p>Title: A HEB File reader</p>
 *
 * <p>Description: An implementaton of the data reader interface, which
 * reads the simplest format to describe a tree, a file which define a parent
 * node for each node (0 for no parent) by writing its index in the nodes list,
 * and define also the indices of the adjacent nodes for a given node.
 * Example:
 * 0 1 1 2 2 3 3 4 4 5 5 6 6 7 7
 * 10: 14, 15
 * 12: 11, 13
 *
 * This format is very simplistic
 * </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author Rafael M. Martins
 * @version 1.0
 */
public class HEBFileReader implements DataReader {
    BufferedReader fileReader;
    int[] parentNodes;
    int[][] adjacencyList;
    String[] captions;
    
    public HEBFileReader(File file) throws IOException {
        fileReader = new BufferedReader(new FileReader(file));
        fileReader.readLine();
        String[] tokens = fileReader.readLine().split(" +");
        parentNodes = new int[tokens.length];
        for (int i = 0; i < parentNodes.length; i++)
            parentNodes[i] = Integer.parseInt(tokens[i]) - 1;

        String[] titles = fileReader.readLine().split(" +");
        captions = new String[titles.length];
        for (int i = 0; i < captions.length; i++)
            captions[i] = titles[i];

        adjacencyList = new int[getNodesCount()][];
        String line = fileReader.readLine();
        while (line != null) {
            tokens = line.split("[: ]+");
            int node = Integer.parseInt(tokens[0]) - 1;
            adjacencyList[node] = new int[tokens.length - 1];
            for (int i = 0; i < adjacencyList[node].length; i++) {
                adjacencyList[node][i] = Integer.parseInt(tokens[i + 1]) - 1;
            }
            line = fileReader.readLine();
        }
        for (int i = 0; i < adjacencyList.length; i++) {
            if (adjacencyList[i] == null)
                adjacencyList[i] = new int[0];
        }
    }

    public int getNodesCount() {
        return parentNodes.length;
    }

    public int[] getAdjacentNodes(int i) {
        return adjacencyList[i];
    }

    public int getParent(int node) {
        return parentNodes[node];
    }

    public String getCaption(int node) {
        return captions[node];
    }

}
