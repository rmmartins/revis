/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.sysreview.project;

import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectInformation;
import org.netbeans.spi.project.ActionProvider;
import org.netbeans.spi.project.CopyOperationImplementation;
import org.netbeans.spi.project.DeleteOperationImplementation;
import org.netbeans.spi.project.ProjectState;
import org.netbeans.spi.project.ui.support.DefaultProjectOperations;
import org.openide.filesystems.FileObject;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;
import org.openide.windows.TopComponent;
import vispipeline.sysreview.project.actions.StartReviewAction;

/**
 *
 * @author Rafael
 */
public class SysReviewProject implements Project {

    public static SysReviewProject currentProject;

    public static ArrayList<TopComponent> tcs = new ArrayList<TopComponent>();

    private final FileObject projectDir;
    private final ProjectState state;
    private Lookup lkp;
    private InstanceContent ic;

    public SysReviewProject(FileObject projectDir, ProjectState state) {
        this.projectDir = projectDir;
        this.state = state;        
    }

    @Override
    public FileObject getProjectDirectory() {
        return projectDir;
    }

//    FileObject getTextFolder(boolean create) {
//        FileObject result =
//                projectDir.getFileObject(PipelineProjectFactory.PIP_FILE);
//        if (result == null && create) {
//            try {
//                result = projectDir.createFolder(PipelineProjectFactory.PIP_FILE);
//            } catch (IOException ioe) {
//                Exceptions.printStackTrace(ioe);
//            }
//        }
//        return result;
//    }

    //The project type's capabilities are registered in the project's lookup:
    @Override
    public Lookup getLookup() {
        if (lkp == null) {
            // equivalent, but this lookup is dynamic
            ic = new InstanceContent();
            ic.add(state);
            ic.add(new ActionProviderImpl());
            ic.add(new SysReviewDeleteOperation());
            ic.add(new SysReviewCopyOperation(this));
            ic.add(new Info());
            ic.add(new SysReviewProjectLogicalView(this));            
            lkp = new AbstractLookup(ic);
//            lkp = Lookups.fixed(new Object[]{
//                //allow outside code to mark the project as needing saving
//                state,
//                //Provides standard actions like Build and Clean
//                new ActionProviderImpl(),
//                new SysReviewDeleteOperation(),
//                new SysReviewCopyOperation(this),
//                //Project information implementation
//                new Info(),
//                 //Logical view of project implementation
//                new SysReviewProjectLogicalView(this),
//            });
        }
        return lkp;
    }

    public void addToLookup(Object obj) {
        ic.add(obj);
    }

    public void finishStage(int stage) {
        Properties revProps = new Properties();
        String filePath = getProjectDirectory().getPath()
                    + "/config/review.properties";
        try {
            revProps.load(new FileReader(filePath));
            revProps.setProperty("stage" + stage + ".complete", "true");
            revProps.store(new FileWriter(filePath), null);
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "problem while saving stage;" +
                    " please edit the 'config/review.properties' file.");
        }

    }

    private final class ActionProviderImpl implements ActionProvider {

        private String[] supported = new String[]{
            ActionProvider.COMMAND_DELETE,
            ActionProvider.COMMAND_COPY,
            ActionProvider.COMMAND_RUN
        };

        @Override
        public String[] getSupportedActions() {
            return supported;
        }

        @Override
        public void invokeAction(String string, Lookup lookup)
                throws IllegalArgumentException {
            if (string.equalsIgnoreCase(ActionProvider.COMMAND_DELETE)) {
                DefaultProjectOperations.performDefaultDeleteOperation(
                        SysReviewProject.this);
            }
            if (string.equalsIgnoreCase(ActionProvider.COMMAND_COPY)) {
                DefaultProjectOperations.performDefaultCopyOperation(
                        SysReviewProject.this);
            }
            if (string.equalsIgnoreCase(ActionProvider.COMMAND_RUN)) {
                StartReviewAction action = 
                        new StartReviewAction(SysReviewProject.this);
                action.startReview();
            }
        }

        @Override
        public boolean isActionEnabled(String command, Lookup lookup)
                throws IllegalArgumentException {
            if ((command.equals(ActionProvider.COMMAND_DELETE))) {
                return true;
            } else if ((command.equals(ActionProvider.COMMAND_COPY))) {
                return true;
            } else if ((command.equals(ActionProvider.COMMAND_RUN))) {
                return true;
            } else {
                throw new IllegalArgumentException(command);
            }
        }
    }

    private final class SysReviewDeleteOperation implements
            DeleteOperationImplementation {

        public void notifyDeleting() throws IOException {
        }

        public void notifyDeleted() throws IOException {
        }

        public List<FileObject> getMetadataFiles() {
            List<FileObject> dataFiles = new ArrayList<FileObject>();
            return dataFiles;
        }

        public List<FileObject> getDataFiles() {
            List<FileObject> dataFiles = new ArrayList<FileObject>();
            return dataFiles;
        }
    }

    private final class SysReviewCopyOperation implements
            CopyOperationImplementation {

        private final SysReviewProject project;
        private final FileObject projectDir;

        public SysReviewCopyOperation(SysReviewProject project) {
            this.project = project;
            this.projectDir = project.getProjectDirectory();
        }

        public List<FileObject> getMetadataFiles() {
            return Collections.EMPTY_LIST;
        }

        public List<FileObject> getDataFiles() {
            return Collections.EMPTY_LIST;
        }

        public void notifyCopying() throws IOException {
        }

        public void notifyCopied(Project arg0, File arg1, String arg2)
                throws IOException {
        }
    }

    private final class Info implements ProjectInformation {

        @Override
        public Icon getIcon() {
            return new ImageIcon(ImageUtilities.loadImage(
                    "vispipeline/sysreview/project/table_edit.png"));
        }

        @Override
        public String getName() {
            return getProjectDirectory().getName();
        }

        @Override
        public String getDisplayName() {
            return getName();
        }

        @Override
        public void addPropertyChangeListener(PropertyChangeListener pcl) {
            //do nothing, won't change
        }

        @Override
        public void removePropertyChangeListener(PropertyChangeListener pcl) {
            //do nothing, won't change
        }

        @Override
        public Project getProject() {
            return SysReviewProject.this;
        }
    }
}

