/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.sysreview.project;

import java.io.IOException;
import org.netbeans.api.project.Project;
import org.netbeans.spi.project.ProjectFactory;
import org.netbeans.spi.project.ProjectState;
import org.openide.filesystems.FileObject;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author Rafael
 */
@ServiceProvider(service=ProjectFactory.class)
public class SysReviewProjectFactory implements ProjectFactory {

    public static final String PIP_FILE = "review.pip";
    public static final String PROPS_FILE = "config/review.properties";

    //Specifies when a project is a project, i.e.,
    //if the file "pipeline.pip" is present:
    @Override
    public boolean isProject(FileObject dir) {
        if (dir.getFileObject(PIP_FILE) != null && dir.getFileObject(PROPS_FILE) != null) {
            return true;
        }
        return false;
    }

    //Specifies when the project will be opened, i.e.,
    //if the project exists:
    @Override
    public Project loadProject(FileObject dir, ProjectState state)
            throws IOException {
        return isProject(dir) ? new SysReviewProject(dir, state) : null;
    }

    @Override
    public void saveProject(Project project) throws IOException,
            ClassCastException {
        FileObject projectRoot = project.getProjectDirectory();
        if (projectRoot.getFileObject(PIP_FILE) == null) {
            throw new IOException("Project dir " + projectRoot.getPath() +
                    " deleted," +
                    " cannot save project");
        }
        //Force creation of the texts dir if it was deleted:
//        ((DemoProject) project).getTextFolder(true);
    }

}
