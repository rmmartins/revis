/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vispipeline.sysreview.project.actions;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Properties;
import javax.swing.JOptionPane;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import vispipeline.basics.interfaces.AbstractComponent;
import vispipeline.engine.ExecuteEngine;
import vispipeline.engine.VisPipelineOptions;
import vispipeline.engine.component.ComponentProxy;
import vispipeline.engine.pipeline.Pipeline;
import vispipeline.socialnet.reader.GraphDataComp;
import vispipeline.socialnet.graphdata.BibGraphData;
import vispipeline.socialnet.graphdata.BibGraphDataPersistence;
import vispipeline.socialnet.graphdata.GraphData;
import vispipeline.socialnet.graphdata.GraphDataFactory;
import vispipeline.sysreview.graph.RevisGraphViewComp;
import vispipeline.sysreview.network.RevisNetworkViewComp;
import vispipeline.sysreview.project.SysReviewProject;
import vispipeline.sysreview.sysrev.SysReviewFrameComp;

public final class StartReviewAction {

    private final SysReviewProject project;

    public StartReviewAction(SysReviewProject context) {
        this.project = context;
    }

    public void startReview() {
        SysReviewProject.currentProject = project;
        Properties revProps = new Properties();
        try {
            revProps.load(project.getProjectDirectory().getFileObject(
                    "config/review.properties").getInputStream());
            stage = 1;
            while (revProps.get("stage" + stage + ".complete").equals("true")) {
                stage++;
                if (stage == 5) {
                    JOptionPane.showMessageDialog(null, "The review selection is over.");
                    return;
                }
            }
            
            input = project.getProjectDirectory().getFileObject(
                    "input/papers.bib");
            String inputStageExt = "stage" + stage + "." + input.getExt();
            FileObject inputStage = project.getProjectDirectory().getFileObject(
                    "input/" + input.getName() + "." + inputStageExt);
            if (inputStage == null || !inputStage.isValid()) {
                if (stage > 1) {
                    input = project.getProjectDirectory().getFileObject(
                        "input/papers.stage" + (stage - 1) + ".bib");
                    input.copy(input.getParent(), "papers.stage" + stage, "bib");
                }
                else {
                    // the first time it has to load the BIB and save again
                    // because the saved BIB is not identical to the original
                    // if this is not done, then the next savings will result
                    // in different projections
                    String url = input.getPath();
                    GraphData gdata = GraphDataFactory.getInstance(url);
                    // remove "cited only"
//                    ArrayList<ArrayList<String>> toRemove =
//                            new ArrayList<ArrayList<String>>();
//                    for (ArrayList<String> item : gdata.getMatrix()) {
//                        if (item.get(0).startsWith("C[")) {
//                            toRemove.add(item);
//                        }
//                    }
//                    for (ArrayList<String> item : gdata.getRelationshipMatrix()) {
//                        if (item.get(0).startsWith("C[") || item.get(1).startsWith("C[")) {
//                            toRemove.add(item);
//                        }
//                    }
//                    for (ArrayList<String> item : toRemove) {
//                        gdata.getMatrix().remove(item);
//                        gdata.getRelationshipMatrix().remove(item);
//                    }
                    // save it back into "stage1"
                    String toSavePath = project.getProjectDirectory().getPath() +
                            "/input/" + input.getName() + "." + inputStageExt;
                    File toSave = new File(toSavePath);
                    try {
                        new BibGraphDataPersistence().save((BibGraphData) gdata, toSave);
                    } catch (IOException ex) {
                        Exceptions.printStackTrace(ex);
                        JOptionPane.showMessageDialog(null, ex.toString());
                    }
                }                
                inputStage = project.getProjectDirectory().getFileObject(
                        "input/papers.stage" + stage + ".bib");
            }
            String title = "";
            String criteria = "";
            switch (stage) {
                case 1:
                    title = "Stage 1: Preselection";
                    criteria = "config/selection.properties";
                    break;
                case 2:
                    title = "Stage 2: Selection";
                    criteria = "config/selection.properties";
                    break;
                case 3:
                    title = "Stage 3: Quality Evaluation";
                    criteria = "config/quality.properties";
                    break;
                case 4:
                    title = "Stage 4: Validation (Expert)";
                    criteria = "config/selection.properties";
                    break;
            }
            FileObject criteriaFile = project.getProjectDirectory()
                    .getFileObject(criteria);
            pipeline = Pipeline.load(project.getProjectDirectory()
                    .getFileObject("review.pip"));
            // setting up the components for the current stage
//            for (ComponentProxy cp : pipeline.getComponents()) {
//                AbstractComponent comp = cp.getComponentToExecute();
//                if (comp instanceof GraphDataComp) {
//                    ((CorpusComp)comp).url = inputStage.getPath();
//                }
//                if (comp instanceof SysReviewFrameComp) {
//                    ((SysReviewFrameComp)comp).criteriaFile =
//                            criteriaFile.getPath();
//                    ((SysReviewFrameComp)comp).stage = stage;
//                }
//                if (comp instanceof GraphViewFrameComp) {
//                    ((GraphViewFrameComp)comp).title = title;
//                }
//                if (comp instanceof VisualTreeComp) {
//                    ((VisualTreeComp)comp).stage = stage;
//                }
//            }
            Lookup lkp = pipeline.getLookup();

            // gambiarra
            if (revProps.get("projection.showWizard").equals("true")) {
                int proj_id = 17;
                ComponentProxy proj_proxy = pipeline.getComponents().get(proj_id);
                proj_proxy.setWizard(true);
            }
            
            Collection<? extends GraphDataComp> gdcomps =
                    lkp.lookupAll(GraphDataComp.class);
            for (GraphDataComp gdc : gdcomps) {
                gdc.url = inputStage.getPath();
            }

            SysReviewFrameComp srfcomp = lkp.lookup(SysReviewFrameComp.class);
            srfcomp.criteriaFile = criteriaFile.getPath();
            srfcomp.stage = stage;

            Collection<? extends RevisGraphViewComp> gvfcomps =
                    lkp.lookupAll(RevisGraphViewComp.class);
            for (RevisGraphViewComp gvfc : gvfcomps) {
                gvfc.title = title;
                gvfc.stage = stage;
            }

            Collection<? extends RevisNetworkViewComp> rnvcomps =
                    lkp.lookupAll(RevisNetworkViewComp.class);
            for (RevisNetworkViewComp rnvc : rnvcomps) {
//                rnvc.title = title;
                rnvc.stage = stage;
            }

//            WindowManager.getDefault().getMainWindow().addWindowListener(
//                    new WindowAdapter() {
//
//                @Override
//                public void windowClosing(WindowEvent e) {
//                    try {
//                        SwingUtilities.invokeAndWait(new Runnable() {
//
//                            public void run() {
//                                new StopReviewAction().performAction(null);
//                            }
//                        });
//                    } catch (InterruptedException ex) {
//                        //...
//                    } catch (InvocationTargetException ex2) {
//                        //...
//                    }
//                    JOptionPane.showMessageDialog(null, "Closed.");
//                    super.windowClosing(e);
//                }
//
//            });
            
            // go!
            if (pipeline != null) {
                ExecuteEngine exengine = new ExecuteEngine(pipeline);
                exengine.setThreadPoolSize(VisPipelineOptions.getInstance(null)
                        .getNumberSimutaneousThreads());
                exengine.start(false);
            }
            
            // read final state
            for (ComponentProxy cp : pipeline.getComponents()) {
                AbstractComponent comp = cp.getComponentToExecute();
                if (comp instanceof SysReviewFrameComp) {
                    ((SysReviewFrameComp)comp).criteriaFile =
                            criteriaFile.getPath();
                }
            }
        } catch (IOException e) {
            Exceptions.printStackTrace(e);
        }
    }

    private Pipeline pipeline;
    private int stage;
    private FileObject input;

}
