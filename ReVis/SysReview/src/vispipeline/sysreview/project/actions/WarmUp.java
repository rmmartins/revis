package vispipeline.sysreview.project.actions;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

public class WarmUp implements Runnable {

    @Override
    public void run() {
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                
            }
        });
    }

    class Listener extends WindowAdapter {

        @Override
        public void windowClosing(WindowEvent event) {
            for (TopComponent tc : TopComponent.getRegistry().getOpened()) {
                if (tc.getActivatedNodes()[0].getCookie(SysReviewCookie.class) != null) {
                    tc.close();
                }           
            }
            JOptionPane.showMessageDialog(null, "closing");
//            super.windowClosing(event);
        }

        @Override
        public void windowClosed(WindowEvent e) {
            JOptionPane.showMessageDialog(null, "closed");
            super.windowClosed(e);
        }


            
    }

}