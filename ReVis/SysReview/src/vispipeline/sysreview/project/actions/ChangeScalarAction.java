/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vispipeline.sysreview.project.actions;

import java.awt.Component;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JOptionPane;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.Utilities;
import org.openide.util.actions.Presenter;
import vispipeline.visualization.view.ModelViewerTopComponent.ScalarCookie;

public final class ChangeScalarAction extends AbstractAction implements Presenter.Toolbar, LookupListener {

    private final Lookup lookup;
    private Lookup.Result<? extends ScalarCookie> res;
    private ChangeScalarPanel comp;

    public ChangeScalarAction() {
        this(Utilities.actionsGlobalContext());        
    }

    private ChangeScalarAction(Lookup lookup) {
        this.lookup = lookup;
        res = lookup.lookupResult(ScalarCookie.class);
        res.addLookupListener(this);
        this.comp = new ChangeScalarPanel();
//        Icon icon = ImageUtilities.image2Icon(
//                ImageUtilities.loadImage("com/foo/icon.gif"));
//        putValue(SMALL_ICON, icon);
        //set the initial enabled state
        setEnabled(false);
    }

    public void actionPerformed(ActionEvent e) {
//        JOptionPane.showMessageDialog(null, "Action!");
    }

    @Override
    public Component getToolbarPresenter() {
        return comp;
    }

//    @Override
//    public synchronized void addPropertyChangeListener(PropertyChangeListener l) {
//        boolean startListening = getPropertyChangeListeners().length == 0;
//        super.addPropertyChangeListener(l);
//        if (startListening) {
//            res = lookup.lookupResult(ScalarCookie.class);
//            res.addLookupListener(this);
//        }
//    }

//    @Override
//    public synchronized void removePropertyChangeListener(PropertyChangeListener l) {
//        super.removePropertyChangeListener(l);
//        if (getPropertyChangeListeners().length == 0) {
//            res.removeLookupListener(this);
//            res = null;
//        }
//    }

    public void resultChanged(LookupEvent ev) {        
        if (!res.allItems().isEmpty()) {
            setEnabled(true);
            comp.updateCookie(res.allInstances().iterator().next());
//            JOptionPane.showMessageDialog(null, "resultChanged " + sc.getScalars().length);
        } else {
            comp.updateCookie(null);
            setEnabled(false);            
        }
    }

    @Override
    public void setEnabled(boolean newValue) {
        super.setEnabled(newValue);
        comp.setEnabled(newValue);        
    }

}
