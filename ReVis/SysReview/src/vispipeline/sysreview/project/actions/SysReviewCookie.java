/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.sysreview.project.actions;

import java.io.File;
import java.io.IOException;
import org.openide.cookies.SaveCookie;
import vispipeline.socialnet.graphdata.BibGraphData;
import vispipeline.socialnet.graphdata.BibGraphDataPersistence;

/**
 *
 * @author Rafael
 */
public class SysReviewCookie implements SaveCookie {

    private BibGraphData gdata;

    public SysReviewCookie(BibGraphData gdata) {
        this.gdata = gdata;
    }

    public void save() throws IOException {
//        JOptionPane.showMessageDialog(null, gdata.getMatrix().size() + "," +
//                gdata.getUrl());
        BibGraphDataPersistence pers = new BibGraphDataPersistence();        
        pers.save(this.gdata, new File(gdata.getUrl()));        
//        JOptionPane.showMessageDialog(null, gdata.getUrl());
//        JasperBibDS ds = new JasperBibDS(gdata);
//        ds.save(new File(gdata.getUrl() + ".xml"));        
    }

}
