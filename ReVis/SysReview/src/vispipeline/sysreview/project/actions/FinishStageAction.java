/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.sysreview.project.actions;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;
import javax.swing.JOptionPane;
import org.openide.awt.ActionID;
import org.openide.awt.ActionRegistration;
import org.openide.filesystems.FileObject;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.actions.CookieAction;
import vispipeline.sysreview.project.SysReviewProject;

/**
 *
 * @author Rafael
 */
public class FinishStageAction extends CookieAction {

    @Override
    protected Class<?>[] cookieClasses() {
        return new Class<?>[]{SysReviewCookie.class};
    }

    @Override
    public String getName() {
        return "Finish Current Stage";
    }

    @Override
    protected int mode() {
        return CookieAction.MODE_EXACTLY_ONE;
    }

    @Override
    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    @Override
    protected void performAction(Node[] activatedNodes) {
        // stop the current review, ask for saving and maybe cancel the whole
        // operation.
        StopReviewAction stopAction = StopReviewAction.findObject(
                StopReviewAction.class);
        stopAction.performAction(activatedNodes);
        if (stopAction.canceled) {
            return;
        }
        // start the next level
        SysReviewProject project = SysReviewProject.currentProject;
        String path = project.getProjectDirectory().getPath();
        Properties props = new Properties();
        try {            
            props.load(new FileReader(path + "/config/review.properties"));
            int stage = 1;
            while (stage < 5) {
                String name = "stage" + stage + ".complete";
                if (props.getProperty(name).equals("false")) {
                    props.setProperty(name, "true");
                    props.store(new FileWriter(path + "/config/review.properties"), "");
                    break;
                }
                stage++;
            }
            // delete possible input file left over from previous operations
            stage++;
            FileObject nextInputFile = project.getProjectDirectory()
                    .getFileObject("input/papers.stage" + stage + ".bib");
            if (nextInputFile != null) {
                nextInputFile.delete();
            }
            // start the next stage
            new StartReviewAction(project).startReview();
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "An error occurred.");
            e.printStackTrace();
        }

    }

    @Override
    protected String iconResource() {
        //Replace org/nvarun/tat with your path/to/icon
        return "vispipeline/sysreview/project/actions/StepOverOperation24.gif";
    }
    //see attachments to download icon24_CreateConditionallyEnabledActionFromScratch.png

}
