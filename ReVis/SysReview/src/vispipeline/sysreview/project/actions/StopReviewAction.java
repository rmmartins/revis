/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.sysreview.project.actions;

import java.io.PrintWriter;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.actions.CookieAction;
import org.openide.windows.TopComponent;
import vispipeline.sysreview.project.SysReviewProject;

/**
 *
 * @author Rafael
 */
public class StopReviewAction extends CookieAction {

    boolean canceled = false;

    @Override
    protected Class<?>[] cookieClasses() {
        return new Class<?>[]{SysReviewCookie.class};
    }

    @Override
    public String getName() {
        return "Stop Review";
    }

    @Override
    protected int mode() {
        return CookieAction.MODE_EXACTLY_ONE;
    }

    @Override
    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    @Override
    protected void performAction(Node[] activatedNodes) {
        try {
            final Node node = activatedNodes[0];
            final PrintWriter pw = new PrintWriter("errors.txt");
            SwingUtilities.invokeAndWait(new Runnable() {

                public void run() {
//                    for (TopComponent tc : TopComponent.getRegistry().getOpened()) {
//                        if (tc.getActivatedNodes().length > 0) {
//                            Node node = tc.getActivatedNodes()[0];
//                            if (node != null) {
//                                SysReviewCookie src = node.getCookie(SysReviewCookie.class);
//                                if (src != null) {
//                                    try {
//                                        src.save();
//                                        tc.close();
//                                    } catch (Exception e) {
//                                    }
//                                }
//                            }
//                        }
//                    }
                    try {
                        int answer = JOptionPane.showConfirmDialog(null,
                                "Do you want to save the review state?");
                        canceled = false;
                        if (answer == JOptionPane.YES_OPTION) {
                            node.getCookie(SysReviewCookie.class).save();                            
                        }
                        if (answer == JOptionPane.CANCEL_OPTION) {
                            canceled = true;
                            return;
                        }
                        for (TopComponent tc : SysReviewProject.tcs) {
                            tc.close();
                        }
                        SysReviewProject.tcs.clear();
                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, "Error while closing windows.");
                        e.printStackTrace(pw);
                    }
                }
            });
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            e.printStackTrace();
        }
    }

    @Override
    protected String iconResource() {
        //Replace org/nvarun/tat with your path/to/icon
        return "vispipeline/sysreview/project/actions/Kill24.gif";
    }
    //see attachments to download icon24_CreateConditionallyEnabledActionFromScratch.png

}
