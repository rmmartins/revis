/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.sysreview.network;

import graph.model.Connectivity;
import graph.model.GraphModel;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import textprocessing.corpus.Corpus;
import vispipeline.socialnet.view.GraphViewTC;
import vispipeline.socialnet.view.NetworkModelViewer;
import vispipeline.sysreview.sysrev.RedGreenScale;
import vispipeline.sysreview.sysrev.RevisQualityScale;
import vispipeline.visualization.model.AbstractModel;
import vispipeline.visualization.model.Scalar;
import vispipeline.visualization.view.ModelViewer;

/**
 *
 * @author Rafael
 */
public class RevisNetworkTC extends GraphViewTC implements Observer {

   public RevisNetworkTC() {        
        // turn edges on/off
        edgesButton = new JToggleButton(
                new ImageIcon(getClass().getResource("chart_line_add.png")));
        edgesButton.setToolTipText("Enable/Disable Edges");
        edgesButton.setPreferredSize(new Dimension(25, 25));
        northPanel.add(edgesButton);
        edgesButton.setEnabled(false);

        edgesButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                if (edgesButton.isSelected()) {
                    edgesButton.setIcon(
                            new ImageIcon(getClass().getResource(
                            "chart_line_delete.png")));
                    graph.setSelectedConnectivity(
                            graph.getConnectivities().get(1));
                } else {
                    edgesButton.setIcon(
                            new ImageIcon(getClass().getResource(
                            "chart_line_add.png")));
                    graph.setSelectedConnectivity(null);
                }
            }
        });
        // play/stop
        runForceButton = new JToggleButton(
                new ImageIcon(getClass().getResource("control_play.png")));
        runForceButton.setToolTipText("Start/Stop Force");
        runForceButton.setPreferredSize(new Dimension(25, 25));
        northPanel.add(runForceButton);
        runForceButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                if (view != null) {
                    boolean start = ((RevisNetworkView)view).runForce();
                    if (!start) {
                        //this.expandVerticesToggleButton.setSelected(false);
                        runForceButton.setIcon(new javax.swing.ImageIcon(getClass().
                                getResource("control_stop.png")));
                        runForceButton.setToolTipText("Stop Force Directed Layout");
                    } else {
                        runForceButton.setIcon(new javax.swing.ImageIcon(getClass().
                                getResource("control_play.png")));
                        runForceButton.setToolTipText("Run Force Directed Layout");
                    }
                }
            }
        });
    }

    public void setEdgesOn(boolean on) {
        if (on && !edgesButton.isSelected())
            edgesButton.doClick();
        if (!on && edgesButton.isSelected())
            edgesButton.doClick();
    }

    @Override
    public void updateScalars(Scalar scalar) {                
        GraphModel graphModel = (GraphModel) view.getModel();
        if (scalar.getName().equals("Sys. Review")) {
            graphModel.changeColorScale(new RedGreenScale());
        } else {
            graphModel.changeColorScale(new RevisQualityScale());
        }
        super.updateScalars(scalar);
        repaint();
    }

    @Override
    public void setModelViewer(ModelViewer view) {
        super.setModelViewer(view);
        view.getModel().addObserver(this);
        // edges
        final GraphModel model = (GraphModel) view.getModel();
        graph = model;
        for (final Connectivity c : model.getConnectivities()) {
            if (!c.getName().equals("...")) {
                final JCheckBoxMenuItem menuItem =
                        new JCheckBoxMenuItem(c.getName());
//                edgesMenu.add(menuItem);
                menuItem.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        if (menuItem.isSelected())
                            model.setSelectedConnectivity(c);
                        else
                            model.setSelectedConnectivity(null);                        
                    }
                });
            }
        }
        updateScalars(view.getModel().getSelectedScalar());
        edgesButton.setEnabled(true);        
        ((NetworkModelViewer)view).fitSize(getWidth(), getHeight());
    }

    @Override
    public void update(Observable o, Object arg) {
        AbstractModel model = (AbstractModel) o;
//        logger.info("update");
//        if (model.getScalars().size() != scalarCombo.getModel().getSize()) {
//            logger.info("sizes are different");
//            // update scalars
//            Scalar selected = model.getSelectedScalar();
//            Scalar[] scalars = model.getScalars().toArray(new Scalar[0]);
//            scalarCombo.setModel(new DefaultComboBoxModel(scalars));
//            scalarCombo.setSelectedItem(selected);
//        }
        updateScalars();
    }

    public void setCorpus(Corpus corpus) {
        this.corpus = corpus;
    }
    
    protected JTextField searchTextField;
    protected Corpus corpus;
    protected JMenu edgesMenu;    
    private JToggleButton edgesButton;
    private JToggleButton runForceButton;
    private GraphModel graph;

    Logger logger = Logger.getLogger(getClass().getSimpleName());

}
