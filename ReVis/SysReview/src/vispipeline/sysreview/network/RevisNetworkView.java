/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.sysreview.network;

import graph.forcelayout.ForceData;
import graph.forcelayout.ForceDirectLayout;
import graph.model.GraphInstance;
import graph.model.GraphModel;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.LinkedHashMap;
import java.util.Observable;
import vispipeline.socialnet.view.NetworkModelViewer;
import vispipeline.sysreview.graph.RevisGraphModel;
import vispipeline.sysreview.sysrev.RedGreenScale;
import vispipeline.sysreview.sysrev.RevisQualityScale;
import vispipeline.visualization.color.ColorScale;
import vispipeline.visualization.color.ColorTable;
import vispipeline.visualization.color.PseudoRainbowScale;
import vispipeline.visualization.model.AbstractInstance;
import vispipeline.visualization.model.AbstractModel;
import vispipeline.visualization.model.Scalar;

/**
 *
 * @author Rafael
 */
public class RevisNetworkView extends NetworkModelViewer {

    int ALPHA = 100;

    @Override
    public void setModel(AbstractModel model) {
        super.setModel(model);
        instance_views = new LinkedHashMap<AbstractInstance, InstanceView>();
        // gambiarra; incluir primeiro os estudos da revisão, depois os
        // "cited-only"
        for (AbstractInstance instance : model.getInstances()) {
            if (!instance.toString().startsWith("C[")) {
                GraphInstance g_inst = (GraphInstance) instance;
                instance_views.put(instance, new InstanceView(g_inst));
            }
        }
        for (AbstractInstance instance : model.getInstances()) {
            if (instance.toString().startsWith("C[")) {
                GraphInstance g_inst = (GraphInstance) instance;
                InstanceView iv = new InstanceView(g_inst);
                iv.setAlpha(ALPHA);
                instance_views.put(instance, iv);
            }
        }
        // adjust to viewer size, since model comes with initial positions        
//        fitSize(getWidth(), getHeight());
    }

    @Override
    public void update(Observable o, Object arg) {        
        // set color scale according to scalar
        ColorScale newScale = new PseudoRainbowScale();
        Scalar scalar = model.getSelectedScalar();
        if (scalar.getName().equals("Sys. Review")) {
            newScale = new RedGreenScale();
        }
        if (scalar.getName().equals("Quality")) {
            newScale = new RevisQualityScale();
        }
//        try {
//            newScale.setMinMax(scalar.getMin(), scalar.getMax());
//        } catch (IOException ex) {
//            Exceptions.printStackTrace(ex);
//        }
        ((GraphModel)model).changeColorScale(newScale);
        // update the display panel
        ColorTable colortable = new ColorTable();
        colortable.setColorScale(newScale);
        colorscale.setColorTable(colortable);
//        colorscale.setPreferredSize(new Dimension(180, 12));
        // this updates the colors again
        model.setSelectedScalar(model.getSelectedScalar());

        // update positions
        for (AbstractInstance ai : model.getInstances()) {
            GraphInstance gi = (GraphInstance) ai;
            InstanceView iv = instance_views.get(ai);
            iv.setCenter(new Point((int)gi.getX(), (int)gi.getY()));
        }

        super.update(o, arg);

        repaint();
    }

    @Override
    public void draw(Graphics2D g) {
        // TODO gambiarra
        if (model instanceof RevisGraphModel) {
            RevisGraphModel graph = (RevisGraphModel) model;
            if (model.getSelectedScalar().getName().equals("Quality")) {
                for (AbstractInstance ai : model.getInstances()) {
                    GraphInstance gi = (GraphInstance) ai;
                    if (graph.isExcluded(ai)) {
                        gi.setColor(Color.red);
                    }
                    if (gi.toString().startsWith("C[")) {
                        gi.setColor(Color.lightGray);
                    }
                }
            }
        }
        super.draw(g);
    }
    
    public boolean runForce() {
        if (model != null) {
            GraphModel graph = (GraphModel) model;
            if (start) {
//                this.cleanTopics();

                for (AbstractInstance ai : graph.getInstances()) {
                    GraphInstance v = (GraphInstance) ai;
                    if (v.fdata == null) {
                        v.fdata = new ForceData();
                    }
                }

                force = new ForceDirectLayout(graph, this);
                force.start(graph.getSelectedConnectivity());
                this.start = false;
            } else {
                this.force.stop();
                this.start = true;
//                JOptionPane.showMessageDialog(null, getWidth() + "," + getHeight());
                fitSize(getWidth(), getHeight());
            }

            graph.setChanged();
            graph.notifyObservers();
        }

//        this.setGraphChanged(true);

        return this.start;
    }

    private boolean start = true;
    private ForceDirectLayout force;

}
