/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.sysreview.edgebundles;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import vispipeline.edgebundles.io.DataReader;
import vispipeline.edgebundles.tree.TreeNode;
import vispipeline.edgebundles.tree.VisualTree;
import vispipeline.socialnet.graphdata.GraphData;

/**
 *
 * @author Rafael
 */
public class RevisVisualTree extends VisualTree {

    public RevisVisualTree(GraphData gdata) {
        this.gdata = gdata;
    }

    @Override
    public void fill(DataReader dr) {
        super.fill(dr);
        // temporary index to facilitate search
        Map<String, TreeNode> index = new HashMap<String, TreeNode>();
        for (int i = 0; i < getNodesCount(); i++) {
            TreeNode node = getNode(i);
            index.put(node.toString(), node);
        }
        // fill the 'excluded' set
        if (gdata != null) {
            for (ArrayList<String> row : gdata.getMatrix()) {
                String id = row.get(0);
                TreeNode node = index.get(id);
                String history = row.get(7);
                if (history.startsWith("E")) {
                    excluded.add(node);
                }
            }
        }
    }

    public boolean isExcluded(TreeNode node) {
        return excluded.contains(node);
    }

    protected GraphData gdata;
    protected Set<TreeNode> excluded = new HashSet<TreeNode>();

}
