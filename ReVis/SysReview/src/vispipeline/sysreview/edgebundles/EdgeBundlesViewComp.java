/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.sysreview.edgebundles;

import java.io.IOException;
import java.util.ArrayList;
import org.openide.util.lookup.ServiceProvider;
import vispipeline.basics.annotations.Param;
import vispipeline.basics.annotations.VisComponent;
import vispipeline.basics.interfaces.AbstractComponent;
import vispipeline.basics.interfaces.AbstractParametersView;
import vispipeline.edgebundles.tree.VisualTree;
import vispipeline.edgebundles.visualisation.DataVisualiser;
import vispipeline.visualization.coordination.AbstractCoordinator;
import vispipeline.visualization.view.ModelViewerFrame;

/**
 *
 * @author Rafael
 */
@VisComponent(hierarchy = "Systematic Review.EdgeBundles",
name = "EdgeBundles View",
description = "View from the EdgeBundles software.")
@ServiceProvider(service=AbstractComponent.class)
public class EdgeBundlesViewComp implements AbstractComponent {

    public void execute() throws IOException {
//        FrameMain frame = new FrameMain();
        ModelViewerFrame frame = new ModelViewerFrame();

//        DataVisualiser dataVisualiser = frame.getDataVisualiser();
        DataVisualiser dataVisualiser = new DataVisualiser();
        dataVisualiser.setModel(visualtree);
        frame.setModelViewer(dataVisualiser);

//        frame.getDrawPanel().revalidate();
//        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        if (coordinators != null) {
            for (int i = 0; i < coordinators.size(); i++) {
                frame.addCoordinator(coordinators.get(i));
            }
        }

        frame.setSize(750, 600);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public AbstractParametersView getParametersEditor() {
        return null;
    }

    public void reset() {
        visualtree = null;        
    }
    
    public void input(@Param(name = "VisualTree") VisualTree vtree) {
        this.visualtree = vtree;
    }
    
    public void attach(@Param(name = "Coordinator") AbstractCoordinator coordinator) {
        if (coordinators == null) {
            coordinators = new ArrayList<AbstractCoordinator>();
        }

        if (coordinator != null) {
            coordinators.add(coordinator);
        }
    }


    protected transient VisualTree visualtree;    
    protected transient ArrayList<AbstractCoordinator> coordinators;

}
