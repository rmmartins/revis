package vispipeline.sysreview.edgebundles;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import vispipeline.edgebundles.io.DataReader;

/**
 * <p>Title: A HEB String reader</p>
 *
 * <p>Description: An implementaton of the data reader interface, which
 * reads the simplest format to describe a tree, a file which define a parent
 * node for each node (0 for no parent) by writing its index in the nodes list,
 * and define also the indices of the adjacent nodes for a given node.
 * Example:
 * 0 1 1 2 2 3 3 4 4 5 5 6 6 7 7
 * 10: 14, 15
 * 12: 11, 13
 *
 * This format is very simplistic
 * </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author Rafael M. Martins
 * @version 1.0
 */
public class HEBStringReader implements DataReader {
    BufferedReader reader;
    int[] parentNodes;
    int[][] adjacencyList;
    String[] captions;

    public HEBStringReader(String string) throws IOException {
        reader = new BufferedReader(new StringReader(string));
        reader.readLine();
        String[] tokens = reader.readLine().split(" +");
        parentNodes = new int[tokens.length];
        for (int i = 0; i < parentNodes.length; i++)
            parentNodes[i] = Integer.parseInt(tokens[i]) - 1;        

        adjacencyList = new int[getNodesCount()][];
        String line = reader.readLine();
        while (!line.equals("<titles>")) {
            tokens = line.split("[: ]+");
            int node = Integer.parseInt(tokens[0]) - 1;
            adjacencyList[node] = new int[tokens.length - 1];
            for (int i = 0; i < adjacencyList[node].length; i++) {
                adjacencyList[node][i] = Integer.parseInt(tokens[i + 1]) - 1;
            }
            line = reader.readLine();
        }
        for (int i = 0; i < adjacencyList.length; i++) {
            if (adjacencyList[i] == null)
                adjacencyList[i] = new int[0];
        }

        captions = new String[parentNodes.length];
        for (int i = 0; i < captions.length; i++)
            captions[i] = reader.readLine();
        
        reader.close();
    }

    public int getNodesCount() {
        return parentNodes.length;
    }

    public int[] getAdjacentNodes(int i) {
        return adjacencyList[i];
    }

    public int getParent(int node) {
        return parentNodes[node];
    }

    public String getCaption(int node) {
        return captions[node];
    }
}
