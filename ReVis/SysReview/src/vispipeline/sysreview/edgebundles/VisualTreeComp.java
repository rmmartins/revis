/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.sysreview.edgebundles;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import org.openide.util.lookup.ServiceProvider;
import vispipeline.basics.annotations.Param;
import vispipeline.basics.annotations.Return;
import vispipeline.basics.annotations.VisComponent;
import vispipeline.basics.interfaces.AbstractComponent;
import vispipeline.basics.interfaces.AbstractParametersView;
import vispipeline.edgebundles.tree.TreeNode;
import vispipeline.edgebundles.tree.VisualTree;
import vispipeline.hpex.view.draw.ClusterElement;
import vispipeline.hpex.view.draw.ClusterHierarchy;
import vispipeline.hpex.view.draw.DataInstanceElement;
import vispipeline.hpex.view.draw.GraphicalElement;
import vispipeline.socialnet.graphdata.GraphData;

/**
 *
 * @author Rafael
 */
@VisComponent(hierarchy = "Systematic Review.EdgeBundles",
name = "Visual Tree Builder",
description = "Builds a Visual Tree with Sys. Review information.")
@ServiceProvider(service=AbstractComponent.class)
public class VisualTreeComp implements AbstractComponent {

    public void execute() throws IOException {
        auxMap = new HashMap<String, Integer>();
        StringWriter stringWriter = new StringWriter();
        input = new PrintWriter(stringWriter);
        parents = new ArrayList<Integer>();
        captions = new ArrayList<String>();
        nodeCount = 0;

        // Hierarquia
        walkHierarchy(hierarchy.getRoot(), nodeCount++);
        input.println();
        for (int parent : parents) {
            input.print(parent + " ");
        }
        input.println();

        // Conexões
        links = new HashMap<Integer, List<Integer>>();
        for (ArrayList<String> t : gdata.getRelationshipMatrix()) {
            int node = findNode(t.get(0));
            if (node != -1) {
                if (links.get(node) == null) {
                    links.put(node, new ArrayList<Integer>());
                }
                List<Integer> nlinks = links.get(node);
                if (!t.get(1).startsWith("C")) {
                    int ref = findNode(t.get(1));
                    if (ref != -1) {
                        nlinks.add(ref);
                    }
                    else {
                        System.err.println("Ref. not found: " + t.get(0));
                    }
                }
            }
            else {
                System.err.println("Node not found: " + t.get(0));
            }
        }
        for (int node : links.keySet()) {
            List<Integer> nlinks = links.get(node);
            if (nlinks != null && nlinks.size() > 0) {
                input.print(node + ": ");
                for (int ref : nlinks) {
                    input.print(ref + " ");
                }
                input.println();
            }
        }

        // Titulos
        input.println("<titles>");
        for (String caption : captions) {
            input.println(caption);
        }
        input.println();

        input.flush();
        input.close();

        String content = stringWriter.toString();

//        System.out.println(content);

        HEBStringReader reader = new HEBStringReader(content);

//        visualtree = new VisualTree();
        visualtree = new RevisVisualTree(gdata);
        visualtree.fill(reader);        

        // Correcting IDs
//        logger.info("Correcting IDs...");
        for (int id : gdata.getIds()) {
            String label = gdata.getLabel(1, id);
//            logger.info("id: " + id + ", label: " + label);
            for (int i = 0; i < visualtree.getNodesCount(); i++) {
                TreeNode node = visualtree.getNode(i);
                if (node.toString().equals(label)) {                    
                    node.setId(id);
//                    logger.info("found node and corrected ID!");
                    break;
                }
            }
        }

        // save topics into BIB
//        System.out.println("saveTopics");
        // there are two roots for some reason
        auxMap = new HashMap<String, Integer>();
        for (int i = 0; i < gdata.getMatrix().size(); i++) {
            ArrayList<String> item = gdata.getMatrix().get(i);
            auxMap.put(item.get(0), i);
        }
//        saveTopics(visualtree.getRoot(), "");

        // verificando
//        System.out.println("Verificando...");
//        for (ArrayList<String> item : gdata.getMatrix()) {
//            System.out.println(item.get(0) + ", " + item.get(11));
//        }

    }

//    private void saveTopics(TreeNode node, String topic) {
//        if (node.getChildrenCount() > 0) {
//            for (int i = 0; i < node.getChildrenCount(); i++) {
//                TreeNode child = node.getChild(i);
//                if (!node.equals(visualtree.getRoot())) {
//                    saveTopics(child, topic + " / " + node.toString());
//                } else {
//                    saveTopics(child, "");
//                }
//
//            }
//        } else {
//            int index = auxMap.get(node.toString());
//            gdata.getMatrix().get(index).set(11, topic);
//        }
//    }


    private void walkHierarchy(ClusterElement el, int parent) {
        int id = nodeCount++;
        input.print(id + " ");
        parents.add(parent);
        captions.add(el.getTitle());        
        auxMap.put(el.getTitle(), id);

        if (el.getChildren().size() > 0) {
            for (GraphicalElement ge : el.getChildren()) {                
                if (!(ge instanceof DataInstanceElement)) {
                    walkHierarchy((ClusterElement) ge, id);
                }
                else {
                    int id2 = nodeCount++;
                    input.print(id2 + " ");
                    parents.add(id);
                    auxMap.put(ge.getTitle(), id2);
                    captions.add(ge.getTitle());
                }
            }
        }
    }

    private int findNode(String name) {
        Set<String> nodes = auxMap.keySet();
        for (String n : nodes) {
            if (n.trim().equals(name.trim())) {
                return auxMap.get(n);
            }
        }
        return -1;
    }

    public AbstractParametersView getParametersEditor() {
        return null;
    }

    public void input(@Param(name = "GraphData") GraphData gdata,
            @Param(name = "ClusterHierarchy") ClusterHierarchy hierarchy) {
        this.gdata = gdata;
        this.hierarchy = hierarchy;
    }

    @Return(name = "VisualTree") public RevisVisualTree outputTree() {
        return visualtree;
    }

    public void reset() {
        gdata = null;
        hierarchy = null;
        visualtree = null;        
    }

    protected transient GraphData gdata;
    protected transient ClusterHierarchy hierarchy;
    protected transient RevisVisualTree visualtree;

    private Map<String, Integer> auxMap;
    private PrintWriter input;
    private List<Integer> parents;
    private List<String> captions;
    
    private int nodeCount;
    private Map<Integer, List<Integer>> links;

    private Logger logger = Logger.getLogger(getClass().getSimpleName());
}
