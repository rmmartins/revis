package vispipeline.sysreview.edgebundles;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Arc2D;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Logger;

import vispipeline.edgebundles.tree.TreeNode;
import vispipeline.edgebundles.tree.VisualTree;
import vispipeline.edgebundles.visualisation.DataVisualiser;
import vispipeline.edgebundles.visualisation.link.Link;
import vispipeline.edgebundles.visualisation.tree.TreeVisualiser;
import vispipeline.sysreview.sysrev.RedGreenScale;
import vispipeline.visualization.color.ColorScale;
import vispipeline.visualization.color.ColorScalePanel;
import vispipeline.visualization.color.ColorTable;
import vispipeline.visualization.model.AbstractInstance;

public class RevisDataVisualiser extends DataVisualiser {

    public RevisDataVisualiser() {
        colorscale = new ColorScalePanel(this);
        colorscale.setPreferredSize(new Dimension(180, 12));
        add(colorscale);
    }

    @Override
    public void setTreeVisualiser(TreeVisualiser treeVis) {
        super.setTreeVisualiser(treeVis);
        colorscale = new ColorScalePanel(this);
//        treeVis.setColorScale(new RedGreenScale());
        setColorScale(new RedGreenScale());        
        ColorTable ct = new ColorTable();
        ct.setColorScale(treeVis.getColorScale());
        colorscale.setColorTable(ct);       
        addMouseMotionListener(new MyMouseAdapter());
    }

    public void setColorScale(ColorScale colorScale) {
//        treeVis.setColorScale(colorScale);
        scale = colorScale;
        ColorTable ct = new ColorTable();
        ct.setColorScale(colorScale);
        colorscale.setColorTable(ct);
    }

    @Override
    public void visualise(Graphics2D g, int width, int height) {
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, width, height);
        g.drawRect(0, 0, width - 1, height - 1);

        VisualTree visualTree = (VisualTree)model;
        if (visualTree != null) {
            visualise(visualTree, g, width, height);
            g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
//        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
//        g.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
            g.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
            g.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
            int linksCount = 0;
            for (int i = 0; i < visualTree.getNodesCount(); i++) {
                linksCount += visualTree.getNode(i).getAdjacentNodesCount();
            }
            if (linksCount == 0) {
                return;
            }
            // computing the links (the bundled control polygons and their
            // length
            Link[] links = new Link[linksCount];
            int ind = 0;

            // link map, to check the dest. node further down the code
//            Map<Link, ArrayList> linkMap = new HashMap<Link, ArrayList>();

            for (int i = 0; i < visualTree.getNodesCount(); i++) {
                TreeNode srcNode = visualTree.getNode(i);
                for (int j = 0; j < srcNode.getAdjacentNodesCount(); j++, ind++) {
                    ArrayList treePath = getPath(srcNode,
                                                 srcNode.getAdjacentNode(j));
                    links[ind] = new Link(treePath, beta, srcNode,
                                                 srcNode.getAdjacentNode(j));
                    //
//                    linkMap.put(links[ind], treePath);
                    //
                }
            }
            // sorting the links
            Arrays.sort(links, new java.util.Comparator() {
                public int compare(Object o1, Object o2) {
//                    if (((Link)o1).getDest().isSelected()) {
//                        return 1;
//                    }
                    return ((Link) o1).length > ((Link) o2).length ? 1 : -1;
                }
//                public boolean equals(Object obj) {
//                    return false;
//                }
            });
            // viewing the links starting with the longest, and assinging the
            // alpha blending value for each link
            double alphaStep;
            g.setColor(Color.WHITE);
            g.fillRect(0, 0, width, height);
            g.drawRect(0, 0, width - 1, height - 1);
            g.setComposite(minComposite);
            g.setColor(Color.GRAY);
            g.setStroke(thickStroke);
            // got this from outside the loop to here
            alphaStep = maxAlpha
                    / (links[linksCount - 1].length - links[0].length);
            for (int i = links.length - 1; i >= 0; i--) {
                double alpha = alphaStep * (links[i].length - links[0].length);
                // if the dest. node is selected, draw normally;
                // else, draw with half alpha
//                ArrayList path = links[i].getPath();
                TreeNode dest = links[i].getDest();
//                System.out.println("link.length = " + links[i].length);
//                System.out.println("link.dest = " + dest.toString());
                if (dest.isSelected()) {                    
                    alpha /= 50;
                }
                //                
                minComposite.setAlpha(alpha);
                linkVis.drawLink(g, links[i]);
            }
            minComposite.setAlpha(0);
            g.setStroke(thinStroke);

            visualise(visualTree, g, width, height);
        }
    }

    public void visualise(VisualTree tree, Graphics2D g, int width, int height) {
        currAngle = maxLevel = 0;
        this.centerX = width / 2;
        this.centerY = height / 2;
        this.g = g;
        layoutTree(tree.getRoot(), 0);
        int dim = Math.min(height, width);
        innerR = 4 * dim / 10;
        innerRStep = innerR / maxLevel;
        outerRStep = (dim / 2 - innerR) / maxLevel;
        alphaStep = 360d / currAngle;
        for (int i = 0; i < tree.getRoot().getChildrenCount(); i++) {
            drawNode(tree.getRoot().getChild(i), 1);
        }
    }

    void drawNode(TreeNode node, int level) {
        g.setStroke(new BasicStroke());
        Point p = node.getCenter();
        int amin = (int) (p.x * alphaStep);
        int amax = (int) (p.y * alphaStep);
        double aminRadian = amin * Math.PI / 180,
                amaxRadian = amax * Math.PI / 180;
        int x;
        int y;
        double cosAMid = Math.cos((aminRadian + amaxRadian) / 2);
        double sinAMid = Math.sin((aminRadian + amaxRadian) / 2);

        if (node.getChildrenCount() == 0) {
            int ro = innerR + (maxLevel - level + 1) * outerRStep
                    + (level == 1 ? -3 : 3);
            x = (int) (innerR * cosAMid) + centerX;
            y = (int) (innerR * sinAMid) + centerY;
            int x1 = (int) (ro * cosAMid) + centerX;
            int y1 = (int) (ro * sinAMid) + centerY;

            g.setColor(Color.BLACK);
            g.drawLine(x, y, x1, y1);

            g.setPaintMode();
            g.setColor(getColorOf(node));
//            Logger.getLogger(this.getClass().getName()).info(
//                    "node="+node.toString()+",color="+getColorOf(node));
            g.fillOval(x - (diam / 2), y - (diam / 2), (diam / 2) * 2, (diam / 2) * 2);
            g.setColor(Color.BLACK);
            g.drawOval(x - (diam / 2), y - (diam / 2), (diam / 2) * 2, (diam / 2) * 2);

            if (node.isSelected()) {
                g.setColor(Color.DARK_GRAY);
                ((Graphics2D) g).setStroke(new BasicStroke(4.0f));
                int sdiam = (diam / 2) + 1;
                g.drawOval(x - sdiam, y - sdiam, sdiam * 2, sdiam * 2);
                ((Graphics2D) g).setStroke(new BasicStroke());
            }
            // area
            Area area = new Area(new Ellipse2D.Double(x - (diam / 2),
                    y - (diam / 2), (diam / 2) * 2, (diam / 2) * 2));
            node.setArea(area);
        } else {
            x = (int) (level * innerRStep * cosAMid) + centerX;
            y = (int) (level * innerRStep * sinAMid) + centerY;
            for (int i = 0; i < node.getChildrenCount(); i++) {
                drawNode(node.getChild(i), level + 1);
            }
            double cos1 = Math.cos(aminRadian), cos2 = Math.cos(amaxRadian),
                    sin1 = Math.sin(aminRadian), sin2 = Math.sin(amaxRadian);
            int ri = innerR + (maxLevel - level) * outerRStep + 3;
            int ro = innerR + (maxLevel - level + 1) * outerRStep - 3;
            int x1 = (int) (ri * cos1) + centerX;
            int y1 = (int) (ri * sin1) + centerY;
            int x2 = (int) (ri * cos2) + centerX;
            int y2 = (int) (ri * sin2) + centerY;
            int x3 = (int) (ro * cos1) + centerX;
            int y3 = (int) (ro * sin1) + centerY;
            int x4 = (int) (ro * cos2) + centerX;
            int y4 = (int) (ro * sin2) + centerY;
            g.setColor(Color.BLACK);
            g.drawArc(centerX - ri, centerY - ri, 2 * ri, 2 * ri, -amin,
                    amin - amax);
            g.drawArc(centerX - ro, centerY - ro, 2 * ro, 2 * ro, -amin,
                    amin - amax);
            g.drawLine(x1, y1, x3, y3);
            g.drawLine(x2, y2, x4, y4);
            // area
            Arc2D smallArc = new Arc2D.Double(centerX - ri, centerY - ri, 2 * ri,
                    2 * ri, -amin, amin - amax, Arc2D.PIE);
            Arc2D bigArc = new Arc2D.Double(centerX - ro, centerY - ro, 2 * ro,
                    2 * ro, -amin, amin - amax, Arc2D.PIE);
            Area area = new Area(bigArc);
            area.subtract(new Area(smallArc));
            node.setArea(area);
        }
        p.setLocation(x, y);
    }

    void layoutTree(TreeNode node, int level) {
        if (level > maxLevel) {
            maxLevel = level;
        }
        if (node.getChildrenCount() == 0) {
            node.setCenter(currAngle, currAngle + 1);
            currAngle += 2;
        } else {
            TreeNode child = node.getChild(0);
            layoutTree(child, level + 1);
            int minAngle = child.getCenter().x;
            for (int i = 1; i < node.getChildrenCount(); i++) {
                child = node.getChild(i);
                layoutTree(child, level + 1);
            }
            int maxAngle = child.getCenter().y;
            node.setCenter(minAngle, maxAngle);
        }
    }

    protected Color getColorOf(TreeNode node) {
        if (model instanceof RevisVisualTree) {
            RevisVisualTree tree = (RevisVisualTree) model;
            if (model.getSelectedScalar().getName().equals("Quality")) {
                if (tree.isExcluded(node)) {
                    return Color.red;
                }
            }
        }
        float value = node.getNormalizedScalarValue(
                node.getModel().getSelectedScalar());        
//        Logger.getLogger(this.getClass().getName()).info(
//                    "node="+node.toString()+",scalarvalue="+value+",color="+scale.getColor(value));
        return scale.getColor(value);
    }

    class MyMouseAdapter extends MouseAdapter {

        @Override
        public void mouseMoved(MouseEvent e) {
            if (model != null) {
                AbstractInstance instance =
                        getInstanceByPosition(e.getPoint(), false);

                if (instance != null) {
                    //Show the instance label
                    label = instance.toString();

                    if (label.trim().length() > 0) {
                        if (label.length() > 100) {
                            label = label.substring(0, 96) + "...";
                        }

                        labelpos = e.getPoint();
                        repaint();
                    }
                } else {
                    //Clear the label
                    label = null;
                    labelpos = null;
                    repaint();
                }
            }
        }


    }
    private int outerRStep, innerRStep, innerR;
    private double alphaStep;
    Graphics2D g;
    int centerX, centerY;
    int currAngle, maxLevel;
    protected int diam = 10; // instance draw diameter
    protected ColorScale scale;

    Logger logger = Logger.getLogger(getClass().getName());
    
}

