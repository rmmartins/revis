/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.sysreview.edgebundles;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import org.openide.util.lookup.ServiceProvider;
import vispipeline.basics.interfaces.Persistence;
import vispipeline.edgebundles.tree.TreeNode;
import vispipeline.edgebundles.tree.VisualTree;

/**
 *
 * @author Rafael
 */
@ServiceProvider(service=Persistence.class)
public class VisualTreePersistence extends Persistence<VisualTree> {

    public VisualTree load(File file) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");

    }

    public void save(VisualTree tree, File file) throws IOException {
        FileWriter writer = new FileWriter(file);
        for (int i = 1; i < tree.getNodesCount(); i++) {
            writer.write(i + " ");
        }
        writer.write("\n");
        for (int i = 1; i < tree.getNodesCount(); i++) {
            writer.write(tree.getNode(i).getParent().getId() + " ");
        }
        writer.write("\n");
        for (int i = 1; i < tree.getNodesCount(); i++) {
            writer.write(tree.getNode(i).toString() + " ");
        }
        writer.write("\n");
        for (int i = 1; i < tree.getNodesCount(); i++) {
            TreeNode node = tree.getNode(i);
            writer.write(node.getId() + ": ");
            for (int j = 0; j < node.getAdjacentNodesCount(); j++) {
                writer.write(node.getAdjacentNode(j).getId() + " ");
            }
            writer.write("\n");
        }
        writer.write("\n");
        writer.flush();
        writer.close();
    }

}
