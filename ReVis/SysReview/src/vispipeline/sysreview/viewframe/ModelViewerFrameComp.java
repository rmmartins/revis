/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.sysreview.viewframe;

import java.io.File;
import vispipeline.visualization.view.ModelViewerFrame;
import java.io.IOException;
import org.openide.util.lookup.ServiceProvider;
import vispipeline.basics.annotations.VisComponent;
import vispipeline.basics.interfaces.AbstractComponent;
import vispipeline.basics.interfaces.AbstractParametersView;
import vispipeline.edgebundles.io.HEBFileReader;
import vispipeline.edgebundles.tree.VisualTree;
import vispipeline.edgebundles.visualisation.DataVisualiser;

/**
 *
 * @author Rafael
 */
@VisComponent(hierarchy = "Systematic Review",
name = "Empty Test Frame",
description = "...")
@ServiceProvider(service=AbstractComponent.class)
public class ModelViewerFrameComp implements AbstractComponent {

    public void execute() throws IOException {
        VisualTree vt = new VisualTree();
        vt.fill(new HEBFileReader(new File(
                "C:/Users/Rafael/Desktop/data.txt")));
//        System.out.println(vt.getNodesCount());

        DataVisualiser dv = new DataVisualiser();
        dv.setModel(vt);
        
        ModelViewerFrame frame = new ModelViewerFrame();
        frame.setModelViewer(dv);
        frame.setSize(600, 600);
        frame.setModal(true);
        frame.setVisible(true);
    }

    public AbstractParametersView getParametersEditor() {
        return null;
    }

    public void reset() {
        
    }
    
//    public void input(@Param(name = "VisualTree") VisualTree vtree) {
//        this.visualtree = vtree;
//    }
    
//    public void attach(@Param(name = "Coordinator") AbstractCoordinator coordinator) {
//        if (coordinators == null) {
//            coordinators = new ArrayList<AbstractCoordinator>();
//        }
//
//        if (coordinator != null) {
//            coordinators.add(coordinator);
//        }
//    }

    public Object output() {
        return new Object();
    }

}
