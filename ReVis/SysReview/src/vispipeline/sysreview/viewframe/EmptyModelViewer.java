/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.sysreview.viewframe;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import vispipeline.visualization.model.AbstractInstance;
import vispipeline.visualization.view.ModelViewer;

/**
 *
 * @author Rafael
 */
public class EmptyModelViewer extends ModelViewer {

    public EmptyModelViewer() {
        setSize(600, 600);
    }

    @Override
    public AbstractInstance getInstanceByPosition(Point p) {
        return null;
    }

    @Override
    public ArrayList<AbstractInstance> getInstancesByPosition(Polygon p) {
        return new ArrayList<AbstractInstance>();
    }

    @Override
    public ArrayList<AbstractInstance> getInstancesByPosition(Rectangle r) {
        return new ArrayList<AbstractInstance>();
    }

    @Override
    public void zoom(float rate) {
        
    }

    @Override
    public void draw(Graphics2D g) {
        
    }

    @Override
    public void cleanSelectedInstances() {
        JOptionPane.showMessageDialog(this, "cleanSelectedInstances");
    }

    @Override
    public void removeSelectedInstances() {
        JOptionPane.showMessageDialog(this, "removeSelectedInstances");
    }

    @Override
    public void zoomIn() {
        JOptionPane.showMessageDialog(this, "zoomIn");
    }

    @Override
    public void zoomOut() {
        JOptionPane.showMessageDialog(this, "zoomOut");
    }

}
