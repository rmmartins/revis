/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.sysreview.hipp;

import vispipeline.hpex.projection.ExpandCluster;
import vispipeline.hpex.projection.Parameters;
import vispipeline.hpex.util.Cluster;
import vispipeline.hpex.view.draw.ClusterElement;
import vispipeline.hpex.view.draw.ClusterHierarchy;
import vispipeline.hpex.view.draw.DataInstanceElement;
import vispipeline.hpex.view.draw.GraphicalElement;
import java.io.IOException;
import java.util.Stack;
import org.openide.util.lookup.ServiceProvider;
import textprocessing.corpus.Corpus;
import vispipeline.basics.annotations.Param;
import vispipeline.basics.annotations.Return;
import vispipeline.basics.annotations.VisComponent;
import vispipeline.basics.interfaces.AbstractComponent;
import vispipeline.basics.interfaces.AbstractParametersView;
import vispipeline.matrix.AbstractMatrix;
import vispipeline.matrix.AbstractVector;
import vispipeline.matrix.sparse.SparseVector;

/**
 *
 * @author Rafael
 */

@VisComponent(hierarchy = "Systematic Review.HPEx",
name = "Cluster Hierarchy Builder",
description = "Builds a Cluster Hierarchy out of a Corpus.")
@ServiceProvider(service=AbstractComponent.class)
public class HierarchyBuilderComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        //creating the projection
//        System.out.println("checkpoint 1");
//
//        System.out.println("spm1: " + spm1.getAttributes());
//        System.out.println("spm2: " + spm2.getAttributes());

        //creating the hierarchy
        Cluster cluster = new Cluster();
        for (int i = 0; i < spm1.getRowCount(); i++) {
            AbstractVector abv1 = spm1.getRow(i);
            SparseVector spv1 = new SparseVector(abv1.toArray(), abv1.getId(),
                    abv1.getKlass());
            AbstractVector abv2 = spm2.getRow(i);
            SparseVector spv2 = new SparseVector(abv2.toArray(), abv2.getId(),
                    abv2.getKlass());
            DataInstanceElement el = new DataInstanceElement(spv1, spv2);
            el.setTitle(corpus.getLabel(1, i));
            cluster.addInstance(el);
        }

//        System.out.println("checkpoint 2");

        // Bizarro; só precisa do Parameters por causa do corpus!
        Parameters par = new Parameters();
        par.corpus = corpus;

        hierarchy = new ClusterHierarchy(cluster, par);
        hierarchy.reset();
        hierarchy.setCorpus(par.corpus);

        hierarchy.setAttributes(spm2.getAttributes());

//        System.out.println("checkpoint 3");

        //expanding the root cluster
        expand(hierarchy, hierarchy.getRoot());

//        System.out.println("checkpoint 4");

//        printHierarchy();

//        return hierarchy;
        //finishing the projection
//                    finished();
        
    }

    private static void expand(ClusterHierarchy ch, ClusterElement el) {
        if (el.getChildrenProjectionState()
                == ClusterElement.ChildrenProjectionState.NOT_PROJECTED) {
            ExpandCluster exp = new ExpandCluster();
            try {
//                System.out.println(el);
                exp.expand(ch, el);
            } catch (AssertionError e) {
                e.printStackTrace();
//                System.out.println(e.getClass() + " em " + el);
            } catch (IOException e) {
//                System.out.println(e.getClass() + " em " + el);
            }
            el.setChildrenProjectionState(
                    ClusterElement.ChildrenProjectionState.PROJECTED);
        }

//        System.out.println(el.getChildren().size());
        if (el.getChildren().size() > 0) {
            for (GraphicalElement ge : el.getChildren()) {
                if (ge instanceof DataInstanceElement) {
//                    System.out.println(ge);
                } else {
                    expand(ch, (ClusterElement) ge);
                }
            }
        }
    }

    public void printHierarchy() {
        Stack stack = new Stack();
        stack.push(hierarchy.getRoot());
        int count = 0;
        while (!stack.isEmpty()) {
            ClusterElement el = (ClusterElement)stack.pop();
//            System.out.println(el.getTitle());
            if (el.getChildren().size() > 0) {                
                for (GraphicalElement ge : el.getChildren()) {
                    if (ge instanceof DataInstanceElement) {
//                        System.out.println(el.getTitle());
                        count++;
                    } else {
                        stack.push(ge);
                    }
                }
            }
        }
//        System.out.println("hierarchy leaf count = " + count);
    }

    @Override
    public void reset() {
        spm1 = spm2 = null;
        corpus = null;
        hierarchy = null;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        return null;
    }

    public void input(@Param(name = "Matrix1") AbstractMatrix matrix1,
            @Param(name = "Matrix2") AbstractMatrix matrix2) {
        this.spm1 = matrix1;
        this.spm2 = matrix2;
    }

    public void input(@Param(name = "Corpus") Corpus corpus) {
        this.corpus = corpus;
    }

    @Return(name = "ClusterHierarchy") public ClusterHierarchy output() {
        return hierarchy;
    }

    transient AbstractMatrix spm1, spm2;
    transient Corpus corpus;
    transient ClusterHierarchy hierarchy;

}
