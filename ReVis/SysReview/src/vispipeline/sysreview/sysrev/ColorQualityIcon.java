/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.sysreview.sysrev;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import javax.swing.Icon;

/**
 *
 * @author Rafael
 */
public class ColorQualityIcon implements Icon {

    public ColorQualityIcon(Color color) {
        this.color = color;
    }

    public int getIconHeight() {
        return 16;
    }

    public int getIconWidth() {
        return 16;
    }

    public void paintIcon(Component c, Graphics g, int x, int y) {
        ((Graphics2D)g).setRenderingHint(
                RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        g.setColor(color);
        g.fillOval(8, 8, 8, 8);
    }

    private Color color = Color.BLACK;
    
}
