/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.sysreview.sysrev;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import vispipeline.visualization.coordination.IdentityCoordinator;
import vispipeline.visualization.model.AbstractInstance;
import vispipeline.visualization.model.AbstractModel;
import vispipeline.visualization.model.Scalar;

/**
 *
 * @author Rafael
 */
public class SysRevCoordinator extends IdentityCoordinator {

    @Override
    public void coordinate(ArrayList<AbstractInstance> selins, Object parameter) {
        super.coordinate(selins, parameter);
        if (selins.size() > 0) {
            // index for speed
            Map<Integer, AbstractInstance> index =
                    new HashMap<Integer, AbstractInstance>();
            for (AbstractInstance ins : selins) {
                index.put(ins.getId(), ins);
            }

            for (AbstractModel model : models) {                
                if (model != selins.get(0).getModel()) {
                    // get the scalar
                    Scalar sysRevScalar = null;
                    for (Scalar sc : model.getScalars()) {
                        if (sc.getName().equals("Sys. Review")) {
                            sysRevScalar = sc;
                        }
                    }
                    if (sysRevScalar == null) {
                        return;
                    }
                    // synchronize values of selected instances
                    for (AbstractInstance inst : model.getInstances()) {
                        if (index.keySet().contains(inst.getId())) {
//                            logger.info("model: " + model);
//                            logger.info("found match for instance: " + inst.getId());
                            float value = index.get(inst.getId())
                                    .getScalarValue(sysRevScalar);
                            inst.setScalarValue(sysRevScalar, value);
//                            logger.info("set value: " + value);
                        }
                    }
                    // this is necessary to change all instances colors on
                    // ProjectionModel (and who knows what other models)
//                    model.setSelectedScalar(sysRevScalar);
                }
            }
        }
    }

//    private Logger logger = Logger.getLogger(getClass().getSimpleName());

}
