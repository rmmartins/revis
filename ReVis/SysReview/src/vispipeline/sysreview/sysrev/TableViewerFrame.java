/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.sysreview.sysrev;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import vispipeline.visualization.color.ColorScale;
import vispipeline.visualization.model.AbstractInstance;
import vispipeline.visualization.model.AbstractModel;
import vispipeline.visualization.model.Scalar;
import vispipeline.visualization.view.ModelViewer;
import vispipeline.visualization.view.ModelViewerTopComponent;

/**
 *
 * @author Rafael
 */
public class TableViewerFrame extends ModelViewerTopComponent implements Observer {

    public TableViewerFrame() {
        super();
        disableSaveImage();
    }

    @Override
    public void setModelViewer(ModelViewer view) {
        super.setModelViewer(view);
        view.getModel().addObserver(this);
        setDisplayName("Table View");
    }

    @Override
    public void updateScalars(Scalar scalar) {
        TableViewer tview = (TableViewer) view;
        if (scalar.getName().equals("Sys. Review")) {
            tview.changeColorScale(new RedGreenScale());
        } else {
            tview.changeColorScale(new RevisQualityScale());
        }
        super.updateScalars(scalar);        
        repaint();
    }

    public void update(Observable o, Object arg) {
        if (o instanceof AbstractModel) {
            AbstractModel model = (AbstractModel) o;
//            selectionUpdate = false;
            ArrayList<AbstractInstance> sel = view.getModel().getSelectedInstances();
//            selectionUpdate = true;
            dummyNode.fire(true);
            Scalar scalar = model.getSelectedScalar();
            TableViewer tview = (TableViewer) view;
            ColorScale newScale;
            if (scalar.getName().equals("Sys. Review")) {
                newScale = new RedGreenScale();
            } else {
                newScale = new RevisQualityScale();
            }
            tview.changeColorScale(newScale);
            repaint();
        }
    }
}
