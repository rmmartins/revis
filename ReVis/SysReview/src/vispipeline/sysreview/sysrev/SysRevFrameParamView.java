/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * ZipCorpusParamView.java
 *
 * Created on 27/05/2009, 09:41:46
 */
package vispipeline.sysreview.sysrev;

import java.io.File;
import java.io.IOException;
import javax.swing.JFileChooser;
import vispipeline.basics.interfaces.AbstractParametersView;
import vispipeline.sysreview.project.SysReviewProject;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class SysRevFrameParamView extends AbstractParametersView {

    /** Creates new form ZipCorpusParamView */
    public SysRevFrameParamView(SysReviewFrameComp comp) {
        initComponents();
        this.comp = comp;

        reset();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        criteriaTextField = new javax.swing.JTextField();
        criteriaButton = new javax.swing.JButton();
        criteriaLabel = new javax.swing.JLabel();

        setBorder(javax.swing.BorderFactory.createTitledBorder("Sys. Review Frame"));
        setLayout(new java.awt.GridBagLayout());

        criteriaTextField.setColumns(35);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        add(criteriaTextField, gridBagConstraints);

        criteriaButton.setText("Search...");
        criteriaButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                criteriaButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        add(criteriaButton, gridBagConstraints);

        criteriaLabel.setText("Criteria file:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        add(criteriaLabel, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void criteriaButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_criteriaButtonActionPerformed
//        try {
            String folder = ".";
//            TopComponent top = TopComponent.getRegistry().getActivated();
//            if (top instanceof PipelineDrawTopComponent) {
//                folder = ((PipelineDrawTopComponent)top).getProjectFolder();
//            }
            SysReviewProject.currentProject.getProjectDirectory().getPath();
            JFileChooser jfc = new JFileChooser(folder);
            jfc.setFileFilter(new CritFilter());
            jfc.setDialogTitle("Choose the criteria file:");
            int result = jfc.showOpenDialog(this);
            if (result == JFileChooser.APPROVE_OPTION) {
                File file = jfc.getSelectedFile();
                criteriaTextField.setText(file.getAbsolutePath());
            }
//        } catch (IOException ex) {
//            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
//        }
}//GEN-LAST:event_criteriaButtonActionPerformed

    @Override
    public void reset() {
        criteriaTextField.setText(comp.criteriaFile);
    }

    @Override
    public void finished() throws IOException {
        if (criteriaTextField.getText().trim().length() > 0) {
            comp.criteriaFile = criteriaTextField.getText();
        } else {
            throw new IOException("A corpus file name must be provided.");
        }
    }

    private SysReviewFrameComp comp;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton criteriaButton;
    private javax.swing.JLabel criteriaLabel;
    private javax.swing.JTextField criteriaTextField;
    // End of variables declaration//GEN-END:variables
}
