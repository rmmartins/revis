/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.sysreview.sysrev;

import com.sun.pdfview.PDFViewer;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Observable;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;
import vispipeline.edgebundles.tree.TreeNode;
import vispipeline.edgebundles.tree.VisualTree;
import vispipeline.socialnet.graphdata.GraphData;
import vispipeline.sysreview.edgebundles.RevisVisualTree;
import vispipeline.sysreview.project.SysReviewProject;
import vispipeline.visualization.color.ColorScale;
import vispipeline.visualization.model.AbstractInstance;
import vispipeline.visualization.model.AbstractModel;
import vispipeline.visualization.model.Scalar;
import vispipeline.visualization.view.ModelViewer;

/**
 *
 * @author Rafael
 */
public class TableViewer extends ModelViewer {

    public static final String SYSREV_SCALAR_NAME = "Sys. Review";
    public static final String QUALITY_SCALAR_NAME = "Quality";
    
    private JTable table;
    private GraphData gdata;
    private String[] columnNames;
    protected JPopupMenu popupMenu;
    protected Properties critProps;
    private JPanel commentsPanel;
    private JEditorPane commentsTextPane;
    private JDialog commentsDialog;
    private JCheckBoxMenuItem conflictItem;
    private JMenu qualityMenu;
    private ArrayList<String> dataItem;
    private int stage = 1;

    protected String[] ex_criteria = new String[0];
    protected String[] ex_criteria_desc = new String[0];
    protected String[] in_criteria = new String[0];
    protected String[] in_criteria_desc = new String[0];
    protected String[] q_criteria = new String[0];
    protected Float[] q_criteria_value = new Float[0];

    Map<String, Integer> modelIds = new HashMap<String, Integer>();
//    protected JComboBox scalarCombo = new JComboBox();
    private ColorScale colorScale = new RedGreenScale();
    private boolean selectionUpdate = true; // avoid infinite loops on coordination
    private Map<Object, Boolean> conflictMap = new HashMap<Object, Boolean>();

    public TableViewer(Properties critProps, int stage) {
        super();
        this.critProps = critProps;
        this.stage = stage;

        setLayout(new BorderLayout());

        columnNames = new String[]{"ID", "Cit."};
        table = new JTable(new Object[][]{}, columnNames);
        table.setDefaultRenderer(Object.class, new MyCellRenderer());
        table.setFillsViewportHeight(true);
        
        table.addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                if (gdata == null) {
                    return;
                }
                int row = table.rowAtPoint(e.getPoint());
                if (row >= 0) {
                    for (ArrayList<String> item : gdata.getMatrix()) {
                        if (item.get(0).equals(table.getValueAt(row, 0).toString())) {
                            table.setToolTipText(item.get(1));
                        }
                    }
                }
            }
        });

        JScrollPane scroll = new JScrollPane(table);
        scroll.setBorder(BorderFactory.createLoweredBevelBorder());
        scroll.setPreferredSize(new Dimension(150, 0));
        setLayout(new BorderLayout());
        add(scroll);

        readCritFile();

        popupMenu = new JPopupMenu("Sys. Review"); // name doesn't show (?)
        // comments button
        JMenuItem commentItem = new JMenuItem("Comments", new ImageIcon(
                getClass().getResource("comments.png")));
        commentItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                showComments();
            }
        });
        popupMenu.add(commentItem);
        // conflict button
        conflictItem = new JCheckBoxMenuItem("Conflict");
        conflictItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setConflict(conflictItem.isSelected());
            }
        });
        popupMenu.add(conflictItem);
        popupMenu.addSeparator();
        if (stage <= 2) {
            buildSelectionMenu();
        }
        if (stage == 3) {
            buildQualityMenu();
        }
        if (stage == 4) {
            buildSelectionMenu();
            buildQualityMenu();
        }


//        JPanel northPanel = new JPanel();
//        northPanel.add(new JLabel("Scalar:"));
//        northPanel.add(scalarCombo, BorderLayout.NORTH);
//        add(northPanel, BorderLayout.NORTH);
//        // react to scalar changes - change color scale
//        scalarCombo.addActionListener(new ActionListener() {
//
//            public void actionPerformed(ActionEvent e) {
//                Scalar scalar = (Scalar) scalarCombo.getSelectedItem();
//                ColorScale newScale;
//                if (scalar.getName().equals("Sys. Review")) {
//                    newScale = new RedGreenScale();
//                } else {
//                    newScale = new PseudoRainbowScale();
//                }
//                colorScale = newScale;
//                repaint();
//            }
//        });
    }

    public void setGraphData(GraphData gdata) {
        this.gdata = gdata;
        for (ArrayList<String> item : gdata.getMatrix()) {
            boolean conflict = (!item.get(10).equals("no-conflict"));
            conflictMap.put(item.get(0), conflict);
        }
    }

    @Override
    public void setModel(AbstractModel model) {
        super.setModel(model);

        // Filling the names and the citations:
        if (model instanceof VisualTree) {
            VisualTree tree = (VisualTree) model;
            Map<TreeNode, Integer> cits = new HashMap<TreeNode, Integer>();
            for (int i = 0; i < tree.getNodesCount(); i++) {
                TreeNode tn = tree.getNode(i);
                if (tn.getChildrenCount() == 0) {
                    Integer num = cits.get(tn);
                    if (num == null) {
                        cits.put(tn, 0);
                    }
                    for (int j = 0; j < tn.getAdjacentNodesCount(); j++) {
                        num = cits.get(tn.getAdjacentNode(j));
                        if (num == null) {
                            cits.put(tn.getAdjacentNode(j), 1);
                        } else {
                            cits.put(tn.getAdjacentNode(j), num + 1);
                        }
                    }
                }
            }

            int size = cits.keySet().size();
            TableModel tableModel = new MyTableModel(columnNames, size);
            int row = 0;
            for (Entry<TreeNode, Integer> e : cits.entrySet()) {
                tableModel.setValueAt(e.getKey(), row, 0);
                tableModel.setValueAt(e.getValue(), row++, 1);
            }
            table.setModel(tableModel);
            // this map is used to connect each table row with its original id
            for (int i = 0; i < tree.getInstances().size(); i++) {
                AbstractInstance inst = tree.getInstances().get(i);
                modelIds.put(inst.toString(), i);
            }
        }
        
        table.setAutoCreateRowSorter(true);
        table.getSelectionModel().addListSelectionListener(
                new MyListSelectionListener());
        table.addMouseListener(new MyMouseAdapter());

//        ComboBoxModel cbm = new DefaultComboBoxModel(model.getScalars().toArray());
//        scalarCombo.setModel(cbm);
    }

    private void readCritFile() {
        if (stage == 3) {
            List<String> q_crits = new ArrayList<String>();
            List<Float> q_values = new ArrayList<Float>();
            for (Object key : critProps.keySet()) {
                q_crits.add(key.toString());
                q_values.add(Float.valueOf(critProps.getProperty(key.toString())));
            }
            q_criteria = q_crits.toArray(new String[0]);
            q_criteria_value = q_values.toArray(new Float[0]);
        } else {
            List<String> ex_crits = new ArrayList<String>();
            List<String> ex_crits_desc = new ArrayList<String>();
            List<String> in_crits = new ArrayList<String>();
            List<String> in_crits_desc = new ArrayList<String>();
            for (Object key : critProps.keySet()) {
                String name = key.toString().substring(2);
                String desc = critProps.getProperty(name);
//            logger.info("readCritFile: " + name + ", " + desc);
                if (key.toString().startsWith("i.")) {
                    in_crits.add(name);
                    in_crits_desc.add(desc);
                } else {
                    ex_crits.add(name);
                    ex_crits_desc.add(desc);
                }
            }
            ex_criteria = ex_crits.toArray(new String[0]);
            ex_criteria_desc = ex_crits_desc.toArray(new String[0]);
            in_criteria = in_crits.toArray(new String[0]);
            in_criteria_desc = in_crits_desc.toArray(new String[0]);
//        logger.info("criteria length: " + criteria.length);
        }

        // gambiarra por enquanto...
        if (stage == 4) {
            List<String> q_crits = new ArrayList<String>();
            List<Float> q_values = new ArrayList<Float>();
            SysReviewProject project = SysReviewProject.currentProject;
            FileObject qfile = project.getProjectDirectory().getFileObject(
                    "config/quality.properties");
            Properties qprops = new Properties();
            try {
                qprops.load(qfile.getInputStream());
                for (Object key : qprops.keySet()) {
                    q_crits.add(key.toString());
                    q_values.add(Float.valueOf(qprops.getProperty(key.toString())));
                }
                q_criteria = q_crits.toArray(new String[0]);
                q_criteria_value = q_values.toArray(new Float[0]);
            } catch (IOException e) {
                JOptionPane.showMessageDialog(null, "Error reading quality.properties file; " +
                        "please check the stack trace log.");
                e.printStackTrace();
            }
        }
    }

    private void showComments() {
        int row = table.getSelectedRow();
        TreeNode node = (TreeNode) table.getValueAt(row, 0);
        String id = node.toString();
        ArrayList<String> item = null;
        for (ArrayList<String> i : gdata.getMatrix()) {
            if (i.get(0).equals(id)) {
                item = i;
            }
        }
        dataItem = item;
        if (commentsPanel == null) {
            commentsPanel = new JPanel(new BorderLayout());
            commentsTextPane = new JEditorPane();
            commentsPanel.add(new JScrollPane(commentsTextPane));
        }
        commentsTextPane.setText(dataItem.get(9));
//        final JDialog d = new JDialog(this, "Comments: " + id, true);
        commentsDialog = new JDialog((JDialog)null, "Comments: " + id, true);
        JButton okButton = new JButton("Ok");
        okButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String comment = commentsTextPane.getText();
                if (dataItem != null) {
                    dataItem.set(9, comment);
                }
                commentsDialog.dispose();                
            }
        });
        JPanel southPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        southPanel.add(okButton);
        commentsPanel.add(southPanel, BorderLayout.SOUTH);
        commentsDialog.add(commentsPanel);
        commentsDialog.setSize(400, 280);
        commentsDialog.setLocationRelativeTo(null);
        commentsDialog.setVisible(true);
    }

    private void setConflict(boolean conflict) {
        int row = table.getSelectedRow();
        String id = table.getValueAt(row, 0).toString();
        conflictMap.put(id, conflict);

        TreeNode node = (TreeNode) table.getValueAt(row, 0);
        ArrayList<String> item = null;
        for (ArrayList<String> i : gdata.getMatrix()) {
            if (i.get(0).equals(id)) {
                item = i;
            }
        }
        dataItem = item;

        if (conflict) {
            dataItem.set(10, "yes");
        } else {
            dataItem.set(10, "no-conflict");
        }
    }

    class MyCellRenderer extends DefaultTableCellRenderer {

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
                boolean hasFocus, int row, int column) {
            Component cell = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

            Color bgColor, fgColor;

            try {
                int id = modelIds.get(value.toString());
//                Logger.getLogger(getClass().getName()).info(
//                        "id=" + id);

                AbstractInstance inst = getModel().getInstances().get(id);
                float svalue = inst.getNormalizedScalarValue(
                        getModel().getSelectedScalar());
//                Logger.getLogger(getClass().getName()).info(
//                        "svalue=" + svalue);
                bgColor = colorScale.getColor(svalue);
//                Logger.getLogger(getClass().getName()).info(
//                        "bgColor=" + bgColor);

                if (isSelected) {
                    bgColor = Color.BLACK;
//                    fgColor = Color.WHITE;
//            } else {
//                bgColor = Color.WHITE;
//                fgColor = Color.BLACK;
                }

                if (model.getSelectedScalar().getName().equals("Quality")) {
                    // TODO gambiarra
                    if (model instanceof RevisVisualTree) {
                        RevisVisualTree tree = (RevisVisualTree) model;
                        if (tree.isExcluded((TreeNode) value)) {
                            bgColor = Color.red;
                        }
                    }
                    if (isSelected) {
                        bgColor = Color.LIGHT_GRAY;
                    }
                }

                if (column > 0) {
                    bgColor = Color.white;
                    fgColor = Color.black;
                }

                // http://www.codeproject.com/KB/GDI-plus/IdealTextColor.aspx
                // modified the threshold to get more black than white
                int nThreshold = 186;
                int bgDelta = (int) ((bgColor.getRed() * 0.299)
                        + (bgColor.getGreen() * 0.587) + (bgColor.getBlue() * 0.114));
//                logger.info("bgColor = " + bgColor + ",bgDelta = " + bgDelta);
                fgColor = (255 - bgDelta < nThreshold) ? Color.BLACK : Color.WHITE;
            } catch (Exception e) {
                bgColor = Color.WHITE;
                fgColor = Color.BLACK;
            }            

            cell.setBackground(bgColor);
            cell.setForeground(fgColor);

            Boolean conflict = conflictMap.get(value.toString());
            if (conflict != null && conflict) {
                Font font = cell.getFont();
                cell.setFont(font.deriveFont(Font.BOLD));
            }

            return cell;
        }
    }

    private void buildSelectionMenu() {
//        logger.info(("buildSelectionMenu"));
        // gambiarra
        SysReviewProject proj = SysReviewProject.currentProject;
        File critFile = FileUtil.toFile(
                proj.getProjectDirectory().getFileObject(
                "/config/selection.properties"));
        try {
            critProps = new Properties();
            critProps.load(new FileReader(critFile));
        } catch(IOException e) {
            Logger.getLogger(getClass().getName()).severe(
                    "Error while loading 'selection.properties';" + e.getMessage());
        }
        readCritFile();
        // Inclusion
//        JMenu inclusionMenu = new JMenu("Inclusion");
        JMenu includeMenu = new JMenu("Include");
        for (int i = 0; i < in_criteria.length; i++) {
            String c = in_criteria[i];
            JMenuItem menuItem = new JMenuItem(c, new ImageIcon(
                    getClass().getResource("bullet_green.png")));
            final int index = i;
            menuItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    actionInclude(index);
                }
            });
            menuItem.setToolTipText(in_criteria_desc[i]);
            includeMenu.add(menuItem);
        }
        includeMenu.setIcon(new ImageIcon(
                getClass().getResource("accept.png")));
//        popupMenu.add(inclusionMenu);
        popupMenu.add(includeMenu);
//        JMenuItem includeAction = new JMenuItem("Include", new ImageIcon(
//                getClass().getResource("bullet_green.png")));
//        inclusionMenu.addActionListener(new ActionListener() {
        
//        inclusionMenu.add(includeAction);
        // Exclusion
        JMenu excludeMenu = new JMenu("Exclusion");
        for (int i = 0; i < ex_criteria.length; i++) {
            String c = ex_criteria[i];
            JMenuItem menuItem = new JMenuItem(c, new ImageIcon(
                    getClass().getResource("bullet_red.png")));
            final int index = i;
            menuItem.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    actionExclude(index);
                }
            });
            menuItem.setToolTipText(ex_criteria_desc[i]);
            excludeMenu.add(menuItem);
        }
        excludeMenu.setIcon(new ImageIcon(
                getClass().getResource("delete.png")));
        popupMenu.add(excludeMenu);
    }

    private void buildQualityMenu() {
//        logger.info(("buildQualityMenu"));
        // gambiarra
        SysReviewProject proj = SysReviewProject.currentProject;
        File critFile = FileUtil.toFile(
                proj.getProjectDirectory().getFileObject(
                "/config/quality.properties"));
        try {
            critProps = new Properties();
            critProps.load(new FileReader(critFile));
        } catch(IOException e) {
            Logger.getLogger(getClass().getName()).severe(
                    "Error while loading 'quality.properties';" + e.getMessage());
        }
        readCritFile();

        qualityMenu = new JMenu("Quality");
        qualityMenu.setIcon(new ImageIcon(getClass().getResource("medal_gold_3.png")));
        popupMenu.add(qualityMenu);

        // put criteria in ascending order
        final List criteriaList = Arrays.asList(q_criteria); // aux.
        Arrays.sort(q_criteria, new Comparator<String>() {
            public int compare(String o1, String o2) {
                float q1 = q_criteria_value[criteriaList.indexOf(o1)];
                float q2 = q_criteria_value[criteriaList.indexOf(o2)];
                return (int) (q1 - q2);
            }
        });
        Arrays.sort(q_criteria_value);

        RevisQualityScale scale = new RevisQualityScale();
        try {
                scale.setMinMax(q_criteria_value[0],
                        q_criteria_value[q_criteria_value.length - 1]);
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
            }
        for (int i = 0; i < q_criteria.length; i++) {
            final float quality = q_criteria_value[i];            
//            JOptionPane.showMessageDialog(null, quality);
            // porcentagem do critério i na escala
            Color color = scale.getColor(quality);
            JMenuItem menuItem = new JMenuItem(q_criteria[i],
                    new ColorQualityIcon(color));
            menuItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    actionSetQuality(quality);
                }
            });
            qualityMenu.add(menuItem);
        }
    }

    private void actionInclude(int cid) {
        int row = table.getSelectedRow();
        TreeNode node = (TreeNode) table.getValueAt(row, 0);
        // set 'history':
        for (ArrayList<String> item : gdata.getMatrix()) {
            if (item.get(0).equals(node.toString())) {
                item.set(7, "I" + stage + "C" + cid);
                break;
            }
        }
        // set scalar:
        Scalar sysRevScalar = null;
        for (Scalar s : getModel().getScalars()) {
            if (s.getName().equals(SYSREV_SCALAR_NAME)) {
                sysRevScalar = s;
                break;
            }
        }
        if (sysRevScalar != null) {
            node.setScalarValue(sysRevScalar, 1.0f);
        }
        repaint();

        // this is necessary for the coordination to kick in and update the
        // scalars on both models
        if (getCoordination() != null) {            
            getCoordination().selected(getModel().getSelectedInstances());
        }
    }

    private void actionExclude(int cid) {
        int row = table.getSelectedRow();
        TreeNode node = (TreeNode) table.getValueAt(row, 0);
        // set 'history':
        for (ArrayList<String> item : gdata.getMatrix()) {
            if (item.get(0).equals(node.toString())) {
                item.set(7, "E" + stage + "C" + cid);
                break;
            }
        }
        // set scalar:
        Scalar sysRevScalar = null;
        for (Scalar s : getModel().getScalars()) {
            if (s.getName().equals(SYSREV_SCALAR_NAME)) {
                sysRevScalar = s;
                break;
            }
        }
        if (sysRevScalar != null) {
            node.setScalarValue(sysRevScalar, -1.0f);
        }
        repaint();

        // this is necessary for the coordination to kick in and update the
        // scalars on both models
        if (getCoordination() != null) {
            getCoordination().selected(getModel().getSelectedInstances());
        }
    }

    private void actionSetQuality(float value) {
//        logger.info("actionSetQuality: " + value);
        int row = table.getSelectedRow();
        TreeNode node = (TreeNode) table.getValueAt(row, 0);
        // set 'quality':
        for (ArrayList<String> item : gdata.getMatrix()) {
            if (item.get(0).equals(node.toString())) {
                item.set(6, String.valueOf(value));
                break;
            }
        }
        // set scalar:
        Scalar qualityScalar = null;
        for (Scalar s : getModel().getScalars()) {
            if (s.getName().equals(QUALITY_SCALAR_NAME)) {
                qualityScalar = s;
                break;
            }
        }
        if (qualityScalar != null) {
            node.setScalarValue(qualityScalar, value);
        }
        repaint();

        // this is necessary for the coordination to kick in and update the
        // scalars on both models
        if (getCoordination() != null) {
            getCoordination().selected(getModel().getSelectedInstances());
        }
    }

//    private void actionSave() {
//        File toSave = new File(gdata.getUrl());
//        try {
//            new BibGraphDataPersistence().save((BibGraphData) gdata, toSave);
//        } catch (IOException ex) {
//            Exceptions.printStackTrace(ex);
//            JOptionPane.showMessageDialog(null, ex.toString());
//        }
//    }

    public void update(Observable o, Object arg) {
        if (o instanceof AbstractModel) {
            selectionUpdate = false;
            table.clearSelection();
            ArrayList<AbstractInstance> sel = getModel().getSelectedInstances();
            for (int row = 0; row < table.getRowCount(); row++) {
                if (sel.contains((AbstractInstance) table.getValueAt(row, 0))) {
                    table.addRowSelectionInterval(row, row);
                }
            }
            selectionUpdate = true;
        }
    }

    public void showAbstract(ArrayList<String> item) {
//        JDialog jd = new JDialog(this, item.get(1), true);
        JDialog jd = new JDialog((JDialog)null, item.get(1), true);
        jd.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        jd.getContentPane().setLayout(new BorderLayout());
        JEditorPane jtp = new JEditorPane();
        jtp.setContentType("text/html");
        jtp.setText("<b>" + item.get(1) + "</b><br><br>" + item.get(3) + "<br><br>" + item.get(4));
        jtp.setCaretPosition(0);
        jd.getContentPane().add(new JScrollPane(jtp));
        jd.setSize(500, 500);
        jd.setLocationRelativeTo(this);
        jd.setVisible(true);
    }

    public void showFullText(String id) {
        File file = new File(
                SysReviewProject.currentProject.getProjectDirectory().getPath()
                + "/input/pdf/" + id + ".pdf");
        PDFViewer pdfv = new PDFViewer(false);
        try {
            pdfv.openFile(file);
        } catch (FileNotFoundException e) {
            pdfv.dispose();
            JOptionPane.showMessageDialog(this, "PDF not found.");
        } catch (IOException e) {
            pdfv.dispose();
            JOptionPane.showMessageDialog(this, "Problem while trying to show PDF.");
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    class MyTableModel extends DefaultTableModel {

        public MyTableModel(String[] s, int i) {
            super(s, i);
        }

        @Override
        public boolean isCellEditable(int row, int column) {
            return false;
        }
    }

    @Override
    public AbstractInstance getInstanceByPosition(Point p) {
        AbstractInstance ai = null;
        int row = table.rowAtPoint(p);
        if (row > -1) {
            ai = (AbstractInstance) table.getValueAt(row, 0);
        }
        return ai;
    }

    @Override
    public ArrayList<AbstractInstance> getInstancesByPosition(Polygon p) {
        return null;
    }

    @Override
    public ArrayList<AbstractInstance> getInstancesByPosition(Rectangle r) {
        return null;
    }

    @Override
    public void zoom(float rate) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void draw(Graphics2D g) {
        
    }

    @Override
    public String toString() {
        return "Primary Studies";
    }

    private void showPopup(Point p) {
        int row = table.getSelectedRow();
        String id = table.getValueAt(row, 0).toString();
        Boolean conflict = conflictMap.get(id);
        if (conflict != null) {
            conflictItem.setSelected(conflict);
        }
        if (qualityMenu != null) {
            // if the item is excluded, the quality menu is disabled
            for (ArrayList<String> item : gdata.getMatrix()) {
                if (item.get(0).equals(id)) {
                    if (item.get(7).startsWith("E")) {
                        qualityMenu.setEnabled(false);
                    } else {
                        qualityMenu.setEnabled(true);
                    }
                }
            }
        }
        popupMenu.show(this, p.x, p.y);
    }

    public void changeColorScale(ColorScale scale) {
        if (scale != null) {
            colorScale = scale;
        }
        repaint();
    }

    class MyListSelectionListener implements ListSelectionListener {

        public void valueChanged(ListSelectionEvent e) {
//            view.cleanSelectedInstances();
            ArrayList<AbstractInstance> sel = new ArrayList<AbstractInstance>();
            for (int row : table.getSelectedRows()) {
                TreeNode id = (TreeNode) table.getValueAt(row, 0);
                sel.add(id);
            }

            if (!e.getValueIsAdjusting() && selectionUpdate && getCoordination() != null) {
                getCoordination().selected(sel);
            }
        }
    }

    class MyMouseAdapter extends MouseAdapter {

        @Override
        public void mouseClicked(MouseEvent e) {
            if (e.getButton() == e.BUTTON3) {
                int r = table.rowAtPoint(e.getPoint());
                table.setRowSelectionInterval(r, r);
                showPopup(e.getPoint());
            }
            if (e.getClickCount() == 2) {
                int row = table.getSelectionModel().getLeadSelectionIndex();
                String id = table.getValueAt(row, 0).toString();
                for (ArrayList<String> item : gdata.getMatrix()) {
                    if (item.get(0).equals(id)) {
                        if (stage == 1) {
                            showAbstract(item);
                        } else {
                            showFullText(item.get(0));
                        }
                    }
                }

            }
        }
    }

    Logger logger = Logger.getLogger(getClass().getName());

}
