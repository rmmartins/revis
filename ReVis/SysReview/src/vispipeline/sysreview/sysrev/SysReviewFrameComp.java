/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.sysreview.sysrev;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import org.openide.filesystems.FileObject;
import org.openide.nodes.Node;
import org.openide.util.lookup.ServiceProvider;
import org.openide.windows.Mode;
import org.openide.windows.WindowManager;
import vispipeline.basics.annotations.Param;
import vispipeline.basics.annotations.Parameter;
import vispipeline.basics.annotations.VisComponent;
import vispipeline.basics.interfaces.AbstractComponent;
import vispipeline.basics.interfaces.AbstractParametersView;
import vispipeline.edgebundles.tree.TreeNode;
import vispipeline.edgebundles.visualisation.DataVisualiser;
import vispipeline.socialnet.graphdata.BibGraphData;
import vispipeline.socialnet.graphdata.GraphData;
import vispipeline.sysreview.edgebundles.EdgeBundlesViewComp;
import vispipeline.sysreview.edgebundles.RevisDataVisualiser;
import vispipeline.sysreview.project.SysReviewProject;
import vispipeline.sysreview.project.actions.SysReviewCookie;
import vispipeline.sysreview.scalars.RevisQualityScalar;
import vispipeline.sysreview.scalars.RevisScalar;
import vispipeline.visualization.model.Scalar;

/**
 *
 * @author Rafael
 */
@VisComponent(hierarchy = "Systematic Review",
name = "Sys. Review Frame",
description = "...")
@ServiceProvider(service=AbstractComponent.class)
public class SysReviewFrameComp extends EdgeBundlesViewComp 
        implements AbstractComponent {

    @Override
    public void execute() throws IOException {        
//        frame = new SysReviewFrame(critprops, stage);

        // history scalar
        Scalar sysRevScalar = visualtree.addScalar(
                RevisScalar.SCALAR_NAME);
        visualtree.setSelectedScalar(sysRevScalar);
        sysRevScalar.store(-1.0f);
        sysRevScalar.store(1.0f);        
        Map<String, Float> tmpSysRevScalar = new HashMap<String, Float>();        
        for (ArrayList<String> item : gdata.getMatrix()) {
            String id = item.get(0);            
            String history = item.get(7);           
            if (history.startsWith("E1") && stage > 1) {
                tmpSysRevScalar.put(id, -0.2f);
            } else if (history.startsWith("E")) {
                tmpSysRevScalar.put(id, -1.0f);
            } else if (history.startsWith("I1") && stage > 1) {
                tmpSysRevScalar.put(id, 0.2f);
            } else if (history.startsWith("I")) {
                tmpSysRevScalar.put(id, 1.0f);
            } else {
                tmpSysRevScalar.put(id, 0.0f);
            }            
        }
        for (int i = 0; i < visualtree.getNodesCount(); i++) {
            TreeNode treenode = visualtree.getNode(i);
            String id = treenode.toString();
            Float srvalue = tmpSysRevScalar.get(id);            
            if (srvalue != null) {
                treenode.setScalarValue(sysRevScalar, srvalue);                
            }
            else {
                treenode.setScalarValue(sysRevScalar, 0.0f);                
            }
        }
        
        // quality scalar
        if (stage > 2) {
            Scalar qualityScalar = visualtree.addScalar(
                    RevisQualityScalar.SCALAR_NAME);
            visualtree.setSelectedScalar(qualityScalar);
//            qualityScalar.store(0.0f);
//            qualityScalar.store(10.0f);

            // para saber o mínimo e o máximo do escalar, preciso ler o arquivo
            // de propriedades de qualidade
            SysReviewProject project = SysReviewProject.currentProject;
            FileObject qfile = project.getProjectDirectory().getFileObject(
                    "config/quality.properties");
            Properties qprops = new Properties();
            qprops.load(qfile.getInputStream());
            float min = 0, max = 0;
            for (Object value : qprops.values()) {
                try {
                    Float fv = Float.parseFloat(value.toString());
                    if (fv > max) {
                        max = fv;
                    }
                    if (fv < min) {
                        min = fv;
                    }
                } catch(NumberFormatException e) {
                    //...
                }
            }
            qualityScalar.store(min);
            qualityScalar.store(max);

            Map<String, Float> tmpQualityScalar = new HashMap<String, Float>();
            for (ArrayList<String> item : gdata.getMatrix()) {
                String id = item.get(0);
                String quality = item.get(6);
//                String history = item.get(7);
//                if (history.startsWith("E")) {
//                    tmpQualityScalar.put(id, -1.0f);
//                } else
                    if (quality.equals("no-quality")) {
                    tmpQualityScalar.put(id, 0.0f);
                } else {
                    tmpQualityScalar.put(id, Float.parseFloat(quality));
                }
            }
            for (int i = 0; i < visualtree.getNodesCount(); i++) {
                TreeNode treenode = visualtree.getNode(i);
                String id = treenode.toString();
                Float qlvalue = tmpQualityScalar.get(id);
                if (qlvalue != null) {
                    treenode.setScalarValue(qualityScalar, qlvalue);
//                    qualityScalar.store(qlvalue);
                } else {
                    treenode.setScalarValue(qualityScalar, 0.0f);
//                    qualityScalar.store(0.0f);
                }
            }
        }

        final DataVisualiser dataVisualiser = new RevisDataVisualiser();
        dataVisualiser.setModel(visualtree);
//        frame.setModelViewer(dataVisualiser);

//        if (coordinators != null) {
//            for (int i = 0; i < coordinators.size(); i++) {
//                frame.addCoordinator(coordinators.get(i));
//            }
//        }

        // frame position
//        if (framePosition != null) {
//            frame.setLocation(framePosition);
//        }
//        else {
//            frame.setLocationRelativeTo(null);
//        }
        // frame size
//        if (frameSize != null) {
//            frame.setSize(frameSize);
//        } else {
//            frame.setSize(750, 600);
//        }
//        frame.addWindowListener(new MyWindowListener());
        
        // this makes the frame accessible for closing
//        SysReviewProject.currentProject.addToLookup(frame);

//        frame.setVisible(true);

        // TopComponent:
        try {
            SwingUtilities.invokeAndWait(new Runnable() {

                public void run() {
                    frame = new SysReviewFrame(stage, gdata);
                    frame.setModelViewer(dataVisualiser);
                    frame.gdata = gdata;                    
                    Mode m1 = WindowManager.getDefault().findMode("editor2");
                    m1.dockInto(frame);
                    SysReviewProject.currentProject.addToLookup(frame);
                    frame.setCookie(new SysReviewCookie((BibGraphData) gdata));

                    SysReviewProject.tcs.add(frame);

                    Node node = frame.getActivatedNodes()[0];
                    if (node != null) {
//                        JOptionPane.showMessageDialog(null, "node não nulo");
                        SysReviewCookie cookie = node.getCookie(SysReviewCookie.class);
                        if (cookie != null) {
//                            JOptionPane.showMessageDialog(null, "cookie não nulo");
                        } else {
//                            JOptionPane.showMessageDialog(null, "cookie nulo");
                        }
                    } else {
//                        JOptionPane.showMessageDialog(null, "node nulo");
                    }

                    frame.open();

                    Properties critprops = new Properties();
                    try {
                        critprops.load(new FileReader(criteriaFile));
                    } catch (IOException e) {
                        Logger.getLogger(getClass().getName()).severe(
                                "Not possible to load criteria file.");
                    }
                    TableViewer tv = new TableViewer(critprops, stage);
                    tv.setModel(visualtree);
                    tv.setGraphData(gdata);
                    TableViewerFrame tc = new TableViewerFrame();

                    SysReviewProject.tcs.add(tc);

                    tc.setDisplayName("display");
//                tc.setLayout(new BorderLayout());
//                tc.add(tv, BorderLayout.CENTER);
                    tc.setModelViewer(tv);
                    Mode m2 = WindowManager.getDefault().findMode("right");
                    m2.dockInto(tc);
                    tc.setCookie(new SysReviewCookie((BibGraphData) gdata));
                    tc.open();

                    if (coordinators != null) {
                        for (int i = 0; i < coordinators.size(); i++) {
                            frame.addCoordinator(coordinators.get(i));
                            tc.addCoordinator(coordinators.get(i));
                        }
                    }

                }
            });
        } catch (InterruptedException e) {
            JOptionPane.showMessageDialog(null, e.toString());
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            JOptionPane.showMessageDialog(null, "Exception; check stack trace.");
            e.printStackTrace();
        }
        
    }

    public void input(@Param(name = "GraphData") GraphData gdata) {
        this.gdata = gdata;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        return new SysRevFrameParamView(this);
    }

    @Override
    public void reset() {
        super.reset();
    }

    protected transient GraphData gdata;
    
    private SysReviewFrame frame = null;

    @Parameter public Dimension frameSize = null;
    @Parameter public Point framePosition = null;
    @Parameter public boolean saveFrameAtts = true;
    @Parameter public String criteriaFile = "";    
    @Parameter public int stage = 1;

    class MyWindowListener extends WindowAdapter {

        @Override
        public void windowClosing(WindowEvent e) {
//            if (saveFrameAtts) {
//                frameSize = frame.getSize();
//                framePosition = frame.getLocation();
//            }
//            frame.dispose();
        }

    }

}
