/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.sysreview.sysrev;

import java.io.IOException;
import org.openide.util.lookup.ServiceProvider;
import vispipeline.basics.annotations.VisComponent;
import vispipeline.basics.interfaces.AbstractComponent;
import vispipeline.visualization.coordination.IdentityCoordinatorComp;

/**
 *
 * @author Rafael
 */
@VisComponent(hierarchy = "Systematic Review",
name = "Sys. Review Coordination",
description = "Creates an identity coordination object which synchronizes the" +
        " Systematic Review scalar.")
@ServiceProvider(service=AbstractComponent.class)
public class SysRevCoordinatorComp extends IdentityCoordinatorComp implements
        AbstractComponent {

    @Override
    public void execute() throws IOException {
        coord = new SysRevCoordinator();
    }

}
