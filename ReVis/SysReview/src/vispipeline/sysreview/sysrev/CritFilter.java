/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.sysreview.sysrev;

import vispipeline.engine.util.AbstractFilter;

/**
 *
 * @author Rafael
 */
public class CritFilter extends AbstractFilter {

    @Override
    public String getFileExtension() {
        return "properties";
    }

    @Override
    public String getProperty() {
        return "";
    }

    @Override
    public String getDescription() {
        return "Criteria properties file";
    }

}
