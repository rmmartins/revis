/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vispipeline.sysreview.sysrev;

import java.awt.Color;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import vispipeline.visualization.color.ColorScale;

/**
 *
 * @author Rafael
 */
public class RedGreenScale extends ColorScale {

    public RedGreenScale() {
        colors = new Color[256];
        colors[0] = Color.RED;
        colors[128] = Color.LIGHT_GRAY;
        linearInterpolation(0, 128);
        colors[255] = Color.GREEN;
        linearInterpolation(128, 255);
    }    

//    @Override
//    public Color getColor(float value) {
//        if (value > 0.33f && value <= 0.66f) {
//            return colors[1];
//        }
//        if (value > 0.66f) {
//            return colors[2];
//        }
//        return colors[0];
//    }
    @Override
    public void setMinMax(float min, float max) throws IOException {
        // don't call it, please
    }

    @Override
    public void setReverse(boolean reverse) {
        // don't call it, please
    }
}
