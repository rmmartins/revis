/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vispipeline.sysreview.sysrev;

import com.sun.pdfview.PDFViewer;
import java.awt.BorderLayout;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;
import vispipeline.edgebundles.tree.TreeNode;
import vispipeline.edgebundles.tree.VisualTree;
import vispipeline.edgebundles.visualisation.DataVisualiser;
import vispipeline.socialnet.graphdata.GraphData;
import vispipeline.sysreview.edgebundles.RevisDataVisualiser;
import vispipeline.sysreview.project.SysReviewProject;
import vispipeline.visualization.color.ColorScale;
import vispipeline.visualization.model.AbstractInstance;
import vispipeline.visualization.model.AbstractModel;
import vispipeline.visualization.model.Scalar;
import vispipeline.visualization.view.ModelViewer;
import vispipeline.visualization.view.ModelViewerTopComponent;

/**
 *
 * @author Rafael
 */
public class SysReviewFrame extends ModelViewerTopComponent implements Observer {

    
    

    public SysReviewFrame(int stage, GraphData gdata) {
        super();
        this.stage = stage;
        this.gdata = gdata;

        // react to scalar changes - change color scale
//        scalarCombo.addActionListener(new ActionListener() {
//
//            public void actionPerformed(ActionEvent e) {
//                Scalar scalar = (Scalar) scalarCombo.getSelectedItem();
//                ColorScale newScale;
//                if (scalar.getName().equals("Sys. Review")) {
//                    newScale = new RedGreenScale();
//                } else {
//                    newScale = new PseudoRainbowScale();
//                }
//                SysReviewDataVisualiser srview = (SysReviewDataVisualiser) view;
//                if (srview != null) {
//                    srview.setColorScale(newScale);
//                }
//                repaint();
//            }
//        });

//        if (gdata instanceof BibGraphData) {
//            impl = new MySaveCookie((BibGraphData)gdata);
//        }

//        saveButton.addActionListener(new ActionListener() {
//            public void actionPerformed(ActionEvent e) {
//                actionSave();
//            }
//        });

//        JMenu sysRevMenu = new JMenu("ReVis");
//        JMenuItem finishItem = new JMenuItem("Finish Stage " + this.stage);
//        finishItem.addActionListener(new ActionListener() {
//
//            public void actionPerformed(ActionEvent e) {
//                SysReviewProject project = SysReviewProject.currentProject;
//                actionSave();
//                project.finishStage(SysReviewFrame.this.stage);
//                for (ModelViewerFrame frame : project.getLookup().lookupAll(
//                        ModelViewerFrame.class)) {
//                    frame.dispose();
//                }
//                StartReviewAction startAction = new StartReviewAction(project);
//                if (SysReviewFrame.this.stage < 4) {
//                    startAction.startReview();
//                }
//            }
//        });
//        sysRevMenu.add(finishItem);
//        menuBar.add(sysRevMenu);
    }

    @Override
    public void setModelViewer(ModelViewer view) {
        super.setModelViewer(view);
//        if (view.getModel() instanceof VisualTree) {
//            VisualTree tree = (VisualTree) view.getModel();
//        }
        view.addMouseListener(new MyMouseAdapter());
        view.addMouseListener(new DVMouseAdapter());

        // isso pode dar problema com loops infinitos..
        view.getModel().addObserver(this);

        if (view instanceof RevisDataVisualiser) {
            final RevisDataVisualiser srview =
                    (RevisDataVisualiser) view;
        }
        updateScalars(view.getModel().getSelectedScalar());
        setDisplayName("Edge Bundles");
    }

    @Override
    public void updateScalars(Scalar scalar) {
        RevisDataVisualiser srview = (RevisDataVisualiser) view;
        ColorScale newScale = null;
        if (scalar.getName().equals("Sys. Review")) {
            newScale = new RedGreenScale();
        } else if (scalar.getName().equals("quality")) {
            newScale = new RevisQualityScale();
        }        
        if (srview != null && newScale != null) {
            srview.setColorScale(newScale);
        }
        super.updateScalars(scalar);
        repaint();
    }

//    public void update(Observable o, Object arg) {
//        table.getSelectionModel().clearSelection();
//        for (AbstractInstance ai : view.getModel().getSelectedInstances()) {
//            for (int i = 0; i < table.getRowCount(); i++) {
//                if (ai.equals(table.getValueAt(i, 0))) {
//                    table.getSelectionModel().addSelectionInterval(i, i);
//                }
//            }
//        }
//    }
    class MyMouseAdapter extends MouseAdapter {

        @Override
        public void mouseClicked(MouseEvent e) {
            if (e.getClickCount() == 2) {
                if (e.getSource() instanceof DataVisualiser) {
                    TreeNode node = (TreeNode) view.getInstanceByPosition(
                            e.getPoint());
                    if (node != null) {
                        String id = node.toString();
                        for (ArrayList<String> item : gdata.getMatrix()) {
                            if (item.get(0).equals(id)) {
                                if (stage == 1) {
                                    showAbstract(item);
                                } else {
                                    showFullText(item.get(0));
                                }
                            }
                        }
                    }
                }
            }
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            super.mouseMoved(e);
        }

    }

    class DVMouseAdapter extends MouseAdapter {

        @Override
        public void mouseClicked(MouseEvent e) {
            Point p = e.getPoint();
            VisualTree tree = (VisualTree) view.getModel();
            TreeNode branch = null;
            for (int i = 0; i < tree.getNodesCount(); i++) {
                TreeNode node = tree.getNode(i);
                if (node.getChildrenCount() > 0 && node.getArea() != null) {
                    if (node.getArea().contains(p)) {
                        ArrayList sel = leafChildren(node);
                        view.getSelection().selected(sel);
                        view.getCoordination().selected(sel);
                    }
                }
            }
            if (branch != null) {
                JOptionPane.showMessageDialog(null, branch.toString());
            }
        }

        private ArrayList<AbstractInstance> leafChildren(TreeNode node) {
            ArrayList<AbstractInstance> ret = new ArrayList<AbstractInstance>();
            for (int i = 0; i < node.getChildrenCount(); i++) {
                if (node.getChild(i).getChildrenCount() > 0) {
                    ret.addAll(leafChildren(node.getChild(i)));
                } else {
                    ret.add(node.getChild(i));
                }
            }
            return ret;
        }

    }

    public void showAbstract(ArrayList<String> item) {
//        JDialog jd = new JDialog(this, item.get(1), true);
        JDialog jd = new JDialog((JDialog) null, item.get(1), true);
        jd.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        jd.getContentPane().setLayout(new BorderLayout());
        JEditorPane jtp = new JEditorPane();
        jtp.setContentType("text/html");
        jtp.setText("<b>" + item.get(1) + "</b><br><br>" + item.get(3) + "<br><br>" + item.get(4));
        jtp.setCaretPosition(0);
        jd.getContentPane().add(new JScrollPane(jtp));
        jd.setSize(500, 500);
        jd.setLocationRelativeTo(this);
        jd.setVisible(true);
    }

    public void showFullText(String id) {
        File file = new File(
                SysReviewProject.currentProject.getProjectDirectory().getPath()
                + "/input/pdf/" + id + ".pdf");
        PDFViewer pdfv = new PDFViewer(false);
        try {
            pdfv.openFile(file);
        } catch (FileNotFoundException e) {
            pdfv.dispose();
            JOptionPane.showMessageDialog(this, "PDF not found.");
        } catch (IOException e) {
            pdfv.dispose();
            JOptionPane.showMessageDialog(this, "Problem while trying to show PDF.");
            logger.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    public void update(Observable o, Object arg) {
        if (o instanceof AbstractModel) {
            AbstractModel model = (AbstractModel) o;
//            selectionUpdate = false;
            ArrayList<AbstractInstance> sel = view.getModel().getSelectedInstances();
//            selectionUpdate = true;
            dummyNode.fire(true);
            Scalar scalar = model.getSelectedScalar();
            ColorScale newScale;
            if (scalar.getName().equals("Sys. Review")) {
                newScale = new RedGreenScale();
            } else {
                newScale = new RevisQualityScale();
            }
            RevisDataVisualiser srview = (RevisDataVisualiser) view;
            if (srview != null) {
                srview.setColorScale(newScale);
            }
            repaint();
        }
    }

//    public class MySaveCookie implements SaveCookie {
//
//
//
//        public MySaveCookie(BibGraphData gdata) {
//            this.gdata = gdata;
//        }
//
//        public void save() throws IOException {
//            BibGraphDataPersistence pers = new BibGraphDataPersistence();
//            pers.save(this.gdata, new File(gdata.getUrl()));
//            dummyNode.fire(false);
//        }
//    }

//    private boolean selectionUpdate = true; // evitar loops infinitos    
    protected JPopupMenu popupMenu;
    protected GraphData gdata;
    int stage = 1;
    private Logger logger = Logger.getLogger(getClass().getName());
}

class MyTableModel extends DefaultTableModel {

    public MyTableModel(String[] s, int i) {
        super(s, i);
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }
}
