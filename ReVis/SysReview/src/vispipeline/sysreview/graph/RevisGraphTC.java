/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.sysreview.graph;

import graph.model.Connectivity;
import graph.model.GraphModel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import textprocessing.corpus.Corpus;
import textprocessing.topic.CovarianceTopic;
import textprocessing.topic.TextGraphModelViewer;
import textprocessing.topic.Topic;
import textprocessing.topic.TopicClusters;
import textprocessing.topic.TopicSelection;
import vispipeline.socialnet.view.GraphViewTC;
import vispipeline.sysreview.sysrev.RedGreenScale;
import vispipeline.sysreview.sysrev.RevisQualityScale;
import vispipeline.visualization.model.AbstractModel;
import vispipeline.visualization.model.Scalar;
import vispipeline.visualization.view.ModelViewer;
import vispipeline.visualization.view.selection.AbstractSelection;

/**
 *
 * @author Rafael
 */
public class RevisGraphTC extends GraphViewTC implements Observer {

    public RevisGraphTC() {
        this(true, true);
    }

    public RevisGraphTC(boolean search, boolean topics) {
        this.search = search;
        this.topics = topics;
//        moveInstancesToggleButton.setEnabled(false);
        // edges
//        edgesMenu = new JMenu("Edges");
//        menuBar.add(edgesMenu);

        if (search) {
            // search panel
            JPanel searchPanel = new JPanel(new BorderLayout());
//        searchPanel.add(new JLabel("Search:"), BorderLayout.WEST);
            searchTextField = new JTextField("Search...");
            searchTextField.setForeground(Color.gray);
            searchTextField.setPreferredSize(new Dimension(100, 20));
//        searchTextField.setMaximumSize(new Dimension(126,20));
//        searchTextField.setMinimumSize(new Dimension(126,20));
            searchPanel.add(searchTextField);
//        JButton searchButton = new JButton("OK");
//        searchPanel.add(searchButton, BorderLayout.EAST);
            ActionListener actionListener = new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    if (view != null) {
                        if (corpus != null) {
                            try {
//                            logger.info("actionPerformed, view & corpus != null");
//                            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                                String query = searchTextField.getText();
//                            logger.info("query: " + query);
                                Scalar s = createQueryScalar(query);
                                String name = "'" + query + "'";
                                for (Scalar sc : view.getModel().getScalars()) {
                                    if (sc.getName().equals(name)) {
                                        view.getModel().setSelectedScalar(sc);
                                        break;
                                    }
                                }
//                            logger.info("scalar = " + s.getName());
//                            gv.updateScalars(s);
                                view.getModel().setChanged();
                                view.getModel().notifyObservers();
                                updateScalars();
//                            this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                            } catch (IOException ex) {
                                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }
                }
            };
            searchTextField.addActionListener(actionListener);
//        searchButton.addActionListener(actionListener);
//        searchPanel.setPreferredSize(new Dimension(200, 20));
//        searchPanel.setMaximumSize(new Dimension(200, 20));
//        searchPanel.setMinimumSize(new Dimension(200, 20));
            searchTextField.addMouseListener(new MouseAdapter() {

                @Override
                public void mouseClicked(MouseEvent e) {
                    if (searchTextField.getText().equals("Search...")) {
                        searchTextField.setText("");
                    }
                    super.mouseClicked(e);
                }
            });
            northPanel.add(new JSeparator());
            northPanel.add(searchPanel);
        }

//        scalarCombo.addActionListener(new ActionListener() {
//
//            public void actionPerformed(ActionEvent e) {
//                Scalar scalar = (Scalar) scalarCombo.getSelectedItem();
//                GraphModel graphModel = (GraphModel) view.getModel();
//                if (scalar.getName().equals("Sys. Review")) {
//                    graphModel.changeColorScale(new RedGreenScale());
//                }
//                else {
//                    graphModel.changeColorScaleType(ColorScaleType.PSEUDO_RAINBOW_SCALE);
//                }
//                repaint();
//            }
//
//        });

//        toolButton.addActionListener(new ActionListener() {
//
//            public void actionPerformed(ActionEvent e) {
//                ProjectionViewOptions.getInstance(null).display(
//                        (ProjectionModelViewer)view);
//            }
//
//        });
        
        // turn edges on/off
        edgesButton = new JToggleButton(
                new ImageIcon(getClass().getResource("chart_line_add.png")));
        edgesButton.setToolTipText("Enable/Disable Edges");
        edgesButton.setPreferredSize(new Dimension(25, 25));
        northPanel.add(edgesButton);
        edgesButton.setEnabled(false);

        edgesButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                if (edgesButton.isSelected()) {
                    edgesButton.setIcon(
                            new ImageIcon(getClass().getResource(
                            "chart_line_delete.png")));
                    graph.setSelectedConnectivity(
                            graph.getConnectivities().get(1));
                } else {
                    edgesButton.setIcon(
                            new ImageIcon(getClass().getResource(
                            "chart_line_add.png")));
                    graph.setSelectedConnectivity(null);
                }
            }
        });

    }

    public void setEdgesOn(boolean on) {
        if (on && !edgesButton.isSelected())
            edgesButton.doClick();
        if (!on && edgesButton.isSelected())
            edgesButton.doClick();
    }

    @Override
    public void updateScalars(Scalar scalar) {                
        GraphModel graphModel = (GraphModel) view.getModel();
        if (scalar.getName().equals("Sys. Review")) {
            graphModel.changeColorScale(new RedGreenScale());
        } else {
            graphModel.changeColorScale(new RevisQualityScale());
        }
        super.updateScalars(scalar);
        repaint();
    }

    @Override
    public void setModelViewer(ModelViewer view) {
        super.setModelViewer(view);
        view.getModel().addObserver(this);
        // edges
        final GraphModel model = (GraphModel) view.getModel();
        graph = model;
        for (final Connectivity c : model.getConnectivities()) {
            if (!c.getName().equals("...")) {
                final JCheckBoxMenuItem menuItem =
                        new JCheckBoxMenuItem(c.getName());
//                edgesMenu.add(menuItem);
                menuItem.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        if (menuItem.isSelected())
                            model.setSelectedConnectivity(c);
                        else
                            model.setSelectedConnectivity(null);                        
                    }
                });
            }
        }
        // Text-specific functions
        if (view instanceof TextGraphModelViewer) {
            final TextGraphModelViewer tgview = (TextGraphModelViewer) view;

            if (topics) {
                // Topic-related features
                topicButton = addSelection(
                        new TopicSelection((TextGraphModelViewer) view,
                        new CovarianceTopic(model, corpus)));
                topicButton.addActionListener(new ActionListener() {

                    public void actionPerformed(ActionEvent e) {
                        List<Topic> topics = tgview.getTopics();
                        while (topics.size() > 1) {
                            topics.remove(1);
                        }
                    }
                });

                northPanel.add(new JSeparator());

                // botão topic clusters
                JButton topicClusterButton = new JButton(
                        new ImageIcon(getClass().getResource("comments.png")));
                topicClusterButton.setToolTipText("Create Topic Clusters");
                topicClusterButton.setPreferredSize(new Dimension(25, 25));
                northPanel.add(topicClusterButton);

                final TextGraphModelViewer textView = (TextGraphModelViewer) view;
                topicClusterButton.addActionListener(new ActionListener() {

                    public void actionPerformed(ActionEvent e) {
                        topicButton.doClick();
                        for (AbstractSelection sel : selections) {
                            if (sel instanceof TopicSelection) {
                                textView.setSelection(sel);
                            }
                        }
                        // gambiarra - não deveria estar aqui
                        textView.getTopics().clear();

                        TopicClusters tcl = new TopicClusters(textView, corpus);
                        try {
                            tcl.execute();
                        } catch (IOException e2) {
                            JOptionPane.showMessageDialog(null, "Error while "
                                    + "creating topic clusters.");
                        }

                        String name = "KMeans-" + tcl.getNrClusters();
                        for (Scalar sc : textView.getModel().getScalars()) {
                            if (sc.getName().equals(name)) {
                                textView.getModel().setSelectedScalar(sc);
                                break;
                            }
                        }
//                            logger.info("scalar = " + s.getName());
//                            gv.updateScalars(s);
                        textView.getModel().setChanged();
                        textView.getModel().notifyObservers();
                        updateScalars();

                        repaint();
                    }
                });

                // show/hide labels
                final JToggleButton showLabelsButton = new JToggleButton(
                        new ImageIcon(getClass().getResource("comment_delete.png")));
                showLabelsButton.setToolTipText("Show/Hide Labels");
                showLabelsButton.setPreferredSize(new Dimension(25, 25));
                showLabelsButton.setSelected(true);
                northPanel.add(showLabelsButton);

                showLabelsButton.addActionListener(new ActionListener() {

                    public void actionPerformed(ActionEvent e) {
                        if (showLabelsButton.isSelected()) {
                            showLabelsButton.setIcon(
                                    new ImageIcon(getClass().getResource(
                                    "comment_delete.png")));
                        } else {
                            showLabelsButton.setIcon(
                                    new ImageIcon(getClass().getResource(
                                    "comment_add.png")));
                        }
                        Topic.setShowLabels(showLabelsButton.isSelected());
                        repaint();
                    }
                });

                // highlight clusters
                final JToggleButton highlightClustersButton = new JToggleButton(
                        new ImageIcon(getClass().getResource("comment_edit.png")));
                highlightClustersButton.setToolTipText("Highlight Label Clusters");
                highlightClustersButton.setPreferredSize(new Dimension(25, 25));
                highlightClustersButton.setSelected(false);
                northPanel.add(highlightClustersButton);

                highlightClustersButton.addActionListener(new ActionListener() {

                    public void actionPerformed(ActionEvent e) {
                        Topic.setHighlightLabels(highlightClustersButton.isSelected());
                        repaint();
                    }
                });

            }

        }
        updateScalars(view.getModel().getSelectedScalar());
        edgesButton.setEnabled(true);
    }

    private Scalar createQueryScalar(String word) throws IOException {
        //Adding a new scalar
        String scalarName = "'" + word + "'";
        Scalar scalar = view.getModel().addScalar(scalarName);
        QuerySolver qS = new QuerySolver(this.corpus, view.getModel().getInstances());
        qS.createCdata(word, scalar);
//        for (AbstractInstance ai : view.getModel().getInstances()) {
//            logger.info(ai.getId() + ": " + ai.getScalarValue(scalar));
//        }
        return scalar;
    }

    public void update(Observable o, Object arg) {
        AbstractModel model = (AbstractModel) o;
//        logger.info("update");
//        if (model.getScalars().size() != scalarCombo.getModel().getSize()) {
//            logger.info("sizes are different");
//            // update scalars
//            Scalar selected = model.getSelectedScalar();
//            Scalar[] scalars = model.getScalars().toArray(new Scalar[0]);
//            scalarCombo.setModel(new DefaultComboBoxModel(scalars));
//            scalarCombo.setSelectedItem(selected);
//        }
        updateScalars();
    }

    public void setCorpus(Corpus corpus) {
        this.corpus = corpus;
    }
    
    protected JTextField searchTextField;
    protected Corpus corpus;
    protected JMenu edgesMenu;

    private JToggleButton topicButton;
    private JToggleButton edgesButton;

    private boolean search;
    private boolean topics;
    private GraphModel graph;

    Logger logger = Logger.getLogger(getClass().getSimpleName());

}
