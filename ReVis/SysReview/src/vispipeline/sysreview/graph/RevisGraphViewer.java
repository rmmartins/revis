/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.sysreview.graph;

import graph.model.GraphInstance;
import java.awt.Color;
import java.awt.Graphics2D;
import textprocessing.topic.TextGraphModelViewer;
import vispipeline.visualization.model.AbstractInstance;

/**
 *
 * @author Rafael
 */
public class RevisGraphViewer extends TextGraphModelViewer {

    @Override
    public void draw(Graphics2D g) {
        // TODO gambiarra
        if (model instanceof RevisGraphModel) {
            RevisGraphModel graph = (RevisGraphModel) model;
            if (model.getSelectedScalar().getName().equals("Quality")) {
                for (AbstractInstance ai : model.getInstances()) {
                    GraphInstance gi = (GraphInstance) ai;
                    if (graph.isExcluded(ai)) {
                        gi.setColor(Color.red);
                    }
                }
            }
        }
        super.draw(g);
    }

}
