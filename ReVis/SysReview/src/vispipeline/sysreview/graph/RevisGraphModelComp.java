/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.sysreview.graph;

import graph.model.Connectivity;
import graph.model.Edge;
import graph.model.GraphInstance;
import graph.model.GraphModelComp;
import java.io.IOException;
import java.util.ArrayList;
import org.openide.util.lookup.ServiceProvider;
import projection.util.ProjectionConstants;
import vispipeline.basics.annotations.Param;
import vispipeline.basics.annotations.Return;
import vispipeline.basics.annotations.VisComponent;
import vispipeline.basics.interfaces.AbstractComponent;
import vispipeline.matrix.AbstractVector;
import vispipeline.socialnet.graphdata.GraphData;

/**
 *
 * @author Rafael
 */
@VisComponent(hierarchy = "Systematic Review",
name = "Revis Graph Model",
description = "...")
@ServiceProvider(service=AbstractComponent.class)
public class RevisGraphModelComp extends GraphModelComp implements
        AbstractComponent {

    @Override
    public void execute() throws IOException {
        if (placement != null) {
            model = new RevisGraphModel(gdata);

            int nrows = placement.getRowCount();

            for (int i = 0; i < nrows; i++) {
                AbstractVector row = placement.getRow(i);
                GraphInstance pi = new GraphInstance(model, row.getId(),
                        row.getValue(0), row.getValue(1));
            }

            //adding the connectivities
            Connectivity dotsCon = new Connectivity(ProjectionConstants.DOTS,
                    new ArrayList<Edge>());
            model.addConnectivity(dotsCon);

            if (conns != null) {
                for (int i = 0; i < conns.size(); i++) {
                    model.addConnectivity(conns.get(i));
                }
            }
        } else {
            throw new IOException("A 2D position should be provided.");
        }
        for (int i = 0; i < placement.getRowCount(); i++) {
            GraphInstance gi = (GraphInstance) model.getInstances().get(i);
            gi.setLabel(gdata.getMatrix().get(i).get(0));
        }
        ((RevisGraphModel)model).updateExcluded();
    }

    public void input(@Param(name = "GraphData") GraphData gdata) {
        this.gdata = gdata;
    }

    @Return(name="Revis Model") public RevisGraphModel outputR() {
        return (RevisGraphModel) model;
    }

    protected transient GraphData gdata;

}
