/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.sysreview.graph;

import graph.view.GraphFrameComp;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Properties;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import org.openide.filesystems.FileObject;
import org.openide.nodes.Node;
import org.openide.util.lookup.ServiceProvider;
import org.openide.windows.Mode;
import org.openide.windows.WindowManager;
import textprocessing.corpus.Corpus;
import textprocessing.topic.Topic;
import vispipeline.basics.annotations.Param;
import vispipeline.basics.annotations.Parameter;
import vispipeline.basics.annotations.VisComponent;
import vispipeline.basics.interfaces.AbstractComponent;
import vispipeline.basics.interfaces.AbstractParametersView;
import vispipeline.socialnet.graphdata.BibGraphData;
import vispipeline.socialnet.graphdata.GraphData;
import vispipeline.sysreview.project.SysReviewProject;
import vispipeline.sysreview.project.actions.SysReviewCookie;
import vispipeline.sysreview.scalars.RevisQualityScalar;
import vispipeline.sysreview.scalars.RevisScalar;
import vispipeline.sysreview.scalars.RevisYearScalar;
import vispipeline.visualization.model.Scalar;


/**
 *
 * @author Rafael
 */
@VisComponent(hierarchy = "Systematic Review",
name = "Graph View Frame",
description = "Displays a graph model with enhancements.")
@ServiceProvider(service = AbstractComponent.class)
public class RevisGraphViewComp extends GraphFrameComp
        implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        if (model != null) {
            model.getScalars().clear();
            new RevisScalar((GraphData)corpus, stage).addInto(model);
            // quality scalar
            Scalar qualityScalar = new RevisQualityScalar(
                    (GraphData)corpus, stage).addInto(model);
            // year
            new RevisYearScalar((GraphData)corpus, stage).addInto(model);

            // para saber o mínimo e o máximo do escalar, preciso ler o arquivo
            // de propriedades de qualidade
            SysReviewProject project = SysReviewProject.currentProject;
            FileObject qfile = project.getProjectDirectory().getFileObject(
                    "config/quality.properties");
            Properties qprops = new Properties();
            qprops.load(qfile.getInputStream());
            float min = 0, max = 0;
            for (Object value : qprops.values()) {
                try {
                    Float fv = Float.parseFloat(value.toString());
                    if (fv > max) {
                        max = fv;
                    }
                    if (fv < min) {
                        min = fv;
                    }

                } catch(NumberFormatException e) {
                    //...
                }
            }
            if (qualityScalar != null) {
                qualityScalar.store(min);
                qualityScalar.store(max);
            }
//            logger.info("min, max: " + min + "," + max);
            // essa ordem tem que ser estritamente respeitada
            // não pergunte
            // não
//            frame = new GraphViewFrame();
//            ((GraphViewFrame)frame).setCorpus(corpus);
//            frame.setModelViewer(pview);

            // frame position
//            if (framePosition != null) {
//                frame.setLocation(framePosition);
//            }
//            // frame size
//            if (frameSize != null) {
//                frame.setSize(frameSize);
//            }
//            else {
//                frame.setSize(650, 650);
//            }
//            frame.addWindowListener(new MyWindowListener());

            // this makes it accessible for closing
//            SysReviewProject.currentProject.addToLookup(frame);

//            frame.setModal(false);
//            frame.setVisible(true);
//            frame.setTitle(title);

//            frame.addSelection(new RangeSelection(pview));
//            frame.addSelection(new CovarianceSelection(pview));
//            frame.addSelection(new DynamicColorSelection(pview));

//            Logger.getLogger(getClass().getName()).info(frame.getView().toString());
//
//            if (coordinators != null) {
//                for (int i = 0; i < coordinators.size(); i++) {
//                    frame.addCoordinator(coordinators.get(i));
//                }
//            }

            // TopComponent:
            try {
                SwingUtilities.invokeAndWait(new Runnable() {

                    public void run() {
                        final RevisGraphTC tc = new RevisGraphTC(search, topics);
                        SysReviewProject.tcs.add(tc);

                        Mode m1 = WindowManager.getDefault().findMode("editor");
                        m1.dockInto(tc);
                        tc.open();                        

                        // gambiarra; só assim eu consigo obter o tamanho do
                        // topcomponent com certeza.
                        tc.addComponentListener(new ComponentAdapter() {

                            @Override
                            public void componentShown(ComponentEvent e) {
                                final RevisGraphViewer pview = new RevisGraphViewer();
                                pview.addTopic(topic);
                                pview.setSize(tc.getWidth(), tc.getHeight());
                                pview.setModel(model);

                                tc.setCorpus(corpus);
                                tc.setModelViewer(pview);

                                if (coordinators != null) {
                                    for (int i = 0; i < coordinators.size(); i++) {
                                        tc.addCoordinator(coordinators.get(i));
                                    }
                                }

                                tc.validate();
//                                tc.repaint();
//                                tc.revalidate();

                                tc.removeComponentListener(this);
                            }
                        });                        
                        
                        SysReviewProject.currentProject.addToLookup(tc);
                        tc.setCookie(new SysReviewCookie((BibGraphData) corpus));
                        Node node = tc.getActivatedNodes()[0];
                        if (node != null) {
//                            JOptionPane.showMessageDialog(null, "node não nulo");
                            SysReviewCookie cookie = node.getCookie(SysReviewCookie.class);
                            if (cookie != null) {
//                                JOptionPane.showMessageDialog(null, "cookie não nulo");
                            } else {
//                                JOptionPane.showMessageDialog(null, "cookie nulo");
                            }
                        } else {
//                            JOptionPane.showMessageDialog(null, "node nulo");
                        }
                    }
                });
            } catch (InterruptedException e) {
                JOptionPane.showMessageDialog(null, e.toString());
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                JOptionPane.showMessageDialog(null, e.toString());
                e.printStackTrace();
            }

        } else {
            throw new IOException("A projection model should be provided.");
        }
    }

    public void attach(@Param(name = "Corpus") Corpus corpus) {
        this.corpus = corpus;
    }

    public void attach(@Param(name = "Topic") Topic topic) {
        this.topic = topic;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
          if (paramview == null) {
            paramview = new GraphFrameParamView(this);
        }

        return paramview;
    }

    protected Corpus corpus;
    protected Topic topic;

    @Parameter public int stage = 1;
    @Parameter public boolean search = true;
    @Parameter public boolean topics = true;

    Logger logger = Logger.getLogger(getClass().getName());

}
