/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.sysreview.graph;

import graph.model.GraphModel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import vispipeline.socialnet.graphdata.GraphData;
import vispipeline.visualization.model.AbstractInstance;

/**
 *
 * @author Rafael
 */
public class RevisGraphModel extends GraphModel {

    public RevisGraphModel(GraphData gdata) {
        this.gdata = gdata;
    }

    public void updateExcluded() {
        // temporary index to facilitate search
        Map<String, AbstractInstance> index = new HashMap<String, AbstractInstance>();
        for (AbstractInstance ai : getInstances()) {
            index.put(ai.toString(), ai);
        }
        // fill the 'excluded' set
        if (gdata != null) {
            for (ArrayList<String> row : gdata.getMatrix()) {
                String id = row.get(0);
                AbstractInstance ai = index.get(id);
                String history = row.get(7);
                if (history.startsWith("E")) {
                    excluded.add(ai);
                }
            }
        }
    }

    public boolean isExcluded(AbstractInstance ai) {
        return excluded.contains(ai);
    }

    protected GraphData gdata;
    protected Set<AbstractInstance> excluded = new HashSet<AbstractInstance>();

}
