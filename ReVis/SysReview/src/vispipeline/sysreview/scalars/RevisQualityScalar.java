/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.sysreview.scalars;

import java.util.ArrayList;
import java.util.logging.Logger;
import vispipeline.socialnet.graphdata.GraphData;
import vispipeline.visualization.model.AbstractInstance;
import vispipeline.visualization.model.AbstractModel;
import vispipeline.visualization.model.Scalar;

/**
 *
 * @author Rafael
 */
public class RevisQualityScalar {

    public static final String SCALAR_NAME = "Quality";
    
    public RevisQualityScalar(GraphData gdata, int stage) {
        this.gdata = gdata;
        this.stage = stage;
    }

    public Scalar addInto(AbstractModel model) {
        if (stage > 2) {
            Scalar qualityScalar = model.addScalar(SCALAR_NAME);
            model.setSelectedScalar(qualityScalar);
//            qualityScalar.store(0.0f);
//            qualityScalar.store(10.0f);
            for (int i = 0; i < gdata.getMatrix().size(); i++) {
                ArrayList<String> item = gdata.getMatrix().get(i);
                String quality = item.get(6);
                Float value = 0.0f;
                if (!quality.equals("no-quality")) {
                    value = Float.parseFloat(quality);
                }
                // TODO gambiarra
                if (value < 0) {
                    value = 0.0f;
                }
//                String history = item.get(7);
//                System.out.println(history);
                AbstractInstance inst = model.getInstances().get(i);
//                if (history.startsWith("E")) {
//                    inst.setScalarValue(qualityScalar, -1.0f);
//                    System.out.println("quality -1.0f " + inst.getId());
//                } else
                inst.setScalarValue(qualityScalar, value);
            }
            return qualityScalar;
        }
        return null;
    }

    protected GraphData gdata;
    protected int stage = 1;

    Logger logger = Logger.getLogger(getClass().getName());

}
