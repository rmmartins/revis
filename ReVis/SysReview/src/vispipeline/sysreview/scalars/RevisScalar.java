/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.sysreview.scalars;

import java.util.ArrayList;
import vispipeline.socialnet.graphdata.GraphData;
import vispipeline.sysreview.sysrev.SysReviewFrame;
import vispipeline.visualization.model.AbstractInstance;
import vispipeline.visualization.model.AbstractModel;
import vispipeline.visualization.model.Scalar;

/**
 *
 * @author Rafael
 */
public class RevisScalar {

    public static final String SCALAR_NAME = "Sys. Review";

    public RevisScalar(GraphData gdata, int stage) {
        this.gdata = gdata;
        this.stage = stage;
    }

    public void addInto(AbstractModel model) {
        Scalar revisScalar = model.addScalar(SCALAR_NAME);
        model.setSelectedScalar(revisScalar);
        revisScalar.store(-1.0f);
        revisScalar.store(1.0f);
        if (gdata != null) {
            for (int i = 0; i < gdata.getMatrix().size(); i++) {
                ArrayList<String> item = gdata.getMatrix().get(i);
                String history = item.get(7);
                AbstractInstance inst = model.getInstances().get(i);
                if (history.startsWith("E1") && stage > 1) {
                    inst.setScalarValue(revisScalar, -0.2f);
                } else if (history.startsWith("E")) {
                    inst.setScalarValue(revisScalar, -1.0f);
                } else if (history.startsWith("I1") && stage > 1) {
                    inst.setScalarValue(revisScalar, 0.2f);
                } else if (history.startsWith("I")) {
                    inst.setScalarValue(revisScalar, 1.0f);
                } else {
                    inst.setScalarValue(revisScalar, 0.0f);
                }
//                System.out.println("scalar:" + inst.getScalarValue(sysRevScalar));
            }
        } else {
            // todo mundo é zero
        }
        model.setSelectedScalar(revisScalar);
    }

    protected GraphData gdata;
    protected int stage = 1;

}
