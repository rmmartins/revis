/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package projection.coordination;

import java.io.IOException;
import org.openide.util.lookup.ServiceProvider;
import vispipeline.basics.annotations.VisComponent;
import vispipeline.basics.interfaces.AbstractComponent;
import vispipeline.basics.interfaces.AbstractParametersView;
import vispipeline.visualization.coordination.AbstractCoordinator;

/**
 *
 * @author Fernando Vieira Paulovich
 */
@VisComponent(hierarchy = "Coordination",
name = "Color Identity Coordination",
description = "Create an color identity coordination object.")
@ServiceProvider(service=AbstractComponent.class)
public class ColorIdentityCoordinatorComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        coord = new ColorIdentityCoordinator();
    }

    public AbstractCoordinator output() {
        return coord;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        return null;
    }

    @Override
    public void reset() {
        coord = null;
    }

    public static final long serialVersionUID = 1L;
    private transient ColorIdentityCoordinator coord;
}
