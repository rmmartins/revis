/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package projection.model;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import vispipeline.visualization.model.AbstractInstance;
import vispipeline.visualization.model.Scalar;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class ProjectionInstance extends AbstractInstance {

    public ProjectionInstance(ProjectionModel model, int id, float x, float y) {
        super(model, id);

        this.x = x;
        this.y = y;
        
        this.showlabel = false;
        this.color = Color.BLACK;
    }

    public ProjectionInstance(ProjectionModel model, int id) {
        this(model, id, 0.0f, 0.0f);
    }

    public void draw(BufferedImage image, boolean highquality) {
        Graphics2D g2 = (Graphics2D) image.getGraphics();

        if (highquality) {
            g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            g2.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
            g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
            g2.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
        }

//        float alpha = ((ProjectionModel) model).getAlpha();
//        if (selected) {
//            int rgbcolor = color.getRGB();
//            image.setRGB((int) x - 1, (int) y - 1, rgbcolor);
//            image.setRGB((int) x, (int) y - 1, rgbcolor);
//            image.setRGB((int) x + 1, (int) y - 1, rgbcolor);
//            image.setRGB((int) x - 1, (int) y, rgbcolor);
//            image.setRGB((int) x, (int) y, rgbcolor);
//            image.setRGB((int) x + 1, (int) y, rgbcolor);
//            image.setRGB((int) x - 1, (int) y + 1, rgbcolor);
//            image.setRGB((int) x, (int) y + 1, rgbcolor);
//            image.setRGB((int) x + 1, (int) y + 1, rgbcolor);
//
//            g2.setColor(Color.GRAY);
//            g2.drawRect((int) x - 2, (int) y - 2, 4, 4);
//            g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 1.0f));
//        } else {
//            int rgb = color.getRGB();
//            float alpha = ((ProjectionModel) model).getAlpha();
//            simulateAlpha(image, alpha, (int) x - 1, (int) y - 1, rgb);
//            simulateAlpha(image, alpha, (int) x, (int) y - 1, rgb);
//            simulateAlpha(image, alpha, (int) x + 1, (int) y - 1, rgb);
//            simulateAlpha(image, alpha, (int) x - 1, (int) y, rgb);
//            simulateAlpha(image, alpha, (int) x, (int) y, rgb);
//            simulateAlpha(image, alpha, (int) x + 1, (int) y, rgb);
//            simulateAlpha(image, alpha, (int) x - 1, (int) y + 1, rgb);
//            simulateAlpha(image, alpha, (int) x, (int) y + 1, rgb);
//            simulateAlpha(image, alpha, (int) x + 1, (int) y + 1, rgb);
//            g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 0.2f));
//        }

        g2.setColor(this.color);
        g2.fillOval(((int) this.x) - this.getRay(), ((int) this.y)
                - this.getRay(), this.getRay() * 2, this.getRay() * 2);

        g2.setColor(Color.BLACK);
        g2.drawOval(((int) this.x) - this.getRay(), ((int) this.y)
                - this.getRay(), this.getRay() * 2, this.getRay() * 2);

        //show the label associated to this instance
        if (showlabel) {
            java.awt.FontMetrics metrics = g2.getFontMetrics(g2.getFont());

            int width = metrics.stringWidth(toString().trim());
            int height = metrics.getAscent();

            g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 0.75f));
            g2.setPaint(java.awt.Color.WHITE);
            g2.fillRect(((int) x) + 3, ((int) y) - 1 - height, width + 4, height + 4);

            g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 1.0f));
            g2.setColor(java.awt.Color.DARK_GRAY);
            g2.drawRect(((int) x) + 3, ((int) y) - 1 - height, width + 4, height + 4);

            g2.drawString(toString().trim(), ((int) x) + 3, ((int) y));
        }
    }

    public boolean isInside(int x, int y) {
        return (Math.abs((this.x - x)) <= 1 && Math.abs((this.y - y)) <= 1);
    }

    public boolean isInside(Rectangle rect) {
        return ((x >= rect.x) && (x - rect.x < rect.width)) &&
                ((y >= rect.y) && (y - rect.y < rect.height));
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
        model.setChanged();
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
        model.setChanged();
    }

    public boolean isShowLabel() {
        return showlabel;
    }

    public void setShowLabel(boolean showlabel) {
        this.showlabel = showlabel;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
    
//    protected void simulateAlpha(BufferedImage image, float alpha, int x, int y, int rgb) {
//        //C = (alpha * (A-B)) + B
//        int oldrgb = image.getRGB(x, y);
//        int oldr = (oldrgb >> 16) & 0xFF;
//        int oldg = (oldrgb >> 8) & 0xFF;
//        int oldb = oldrgb & 0xFF;
//
//        int newr = (int) ((alpha * (((rgb >> 16) & 0xFF) - oldr)) + oldr);
//        int newg = (int) ((alpha * (((rgb >> 8) & 0xFF) - oldg)) + oldg);
//        int newb = (int) ((alpha * ((rgb & 0xFF) - oldb)) + oldb);
//
//        int newrgb = newb | (newg << 8) | (newr << 16);
//        image.setRGB(x, y, newrgb);
//    }

    public int getRay() {
        return (int) (rayBase + (this.rayFactor * rayBase));
    }

    @Override
    public void setScalarValue(Scalar scalar, float value) {
        super.setScalarValue(scalar, value);
        // update color (gambiarra)
        getModel().setSelectedScalar(getModel().getSelectedScalar());
    }

    private static int rayBase = 4; //The rayFactor of the vertex
    private float rayFactor = 0;  //The size of vertex ray (it must stay between 0.0 and 1.0)
    
    protected float x;
    protected float y;
    protected boolean showlabel;
    protected Color color;
}
