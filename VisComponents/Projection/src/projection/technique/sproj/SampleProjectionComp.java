/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package projection.technique.sproj;

import java.io.IOException;
import org.openide.util.lookup.ServiceProvider;
import projection.technique.ProjectionComp;
import projection.technique.sproj.SampleProjection.SampleType;
import vispipeline.basics.annotations.VisComponent;
import vispipeline.basics.interfaces.AbstractComponent;
import vispipeline.basics.interfaces.AbstractParametersView;
import vispipeline.distance.dissimilarity.AbstractDissimilarity;
import vispipeline.distance.dissimilarity.DissimilarityFactory;
import vispipeline.distance.dissimilarity.DissimilarityFactory.DissimilarityType;

/**
 *
 * @author Fernando Vieira Paulovich
 */
@VisComponent(hierarchy = "Projection.Technique",
name = "Sample Projection (SProj)",
description = "Project points from a multidimensional space to the plane " +
"preserving the distance relations.")
@ServiceProvider(service=AbstractComponent.class)
public class SampleProjectionComp extends ProjectionComp implements
        AbstractComponent {

    @Override
    public void execute() throws IOException {
        //projecting
        SampleProjection sproj = new SampleProjection();
        sproj.setFractionDelta(fracdelta);
        sproj.setNumberIteractions(nriterations);
        sproj.setSampleType(sampletype);

        if (matrix != null) { //using a matrix
            AbstractDissimilarity diss = DissimilarityFactory.getInstance(disstype);
            projection = sproj.project(matrix, diss);
        } else {
            throw new IOException("A points matrix should be provided.");
        }
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new SampleProjectionParamView(this);
        }

        return paramview;
    }

    @Override
    public void reset() {
        projection = null;
        matrix = null;
    }

    /**
     * @return the nriterations
     */
    public int getNumberIterations() {
        return nriterations;
    }

    /**
     * @param nriterations the nriterations to set
     */
    public void setNumberIterations(int nriterations) {
        this.nriterations = nriterations;
    }

    /**
     * @return the fracdelta
     */
    public float getFractionDelta() {
        return fracdelta;
    }

    /**
     * @param fracdelta the fracdelta to set
     */
    public void setFractionDelta(float fracdelta) {
        this.fracdelta = fracdelta;
    }

    /**
     * @return the disstype
     */
    public DissimilarityType getDissimilarityType() {
        return disstype;
    }

    /**
     * @param disstype the disstype to set
     */
    public void setDissimilarityType(DissimilarityType diss) {
        this.disstype = diss;
    }

    /**
     * @return the sampletype
     */
    public SampleType getSampleType() {
        return sampletype;
    }

    /**
     * @param sampletype the sampletype to set
     */
    public void setSampleType(SampleType sampletype) {
        this.sampletype = sampletype;
    }

    public static final long serialVersionUID = 1L;
    private DissimilarityType disstype = DissimilarityType.EUCLIDEAN;
    private SampleType sampletype = SampleType.CLUSTERING;
    private int nriterations = 50;
    private float fracdelta = 8.0f;
    private transient SampleProjectionParamView paramview;
    
}
