/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package projection.technique.sproj;

import vispipeline.datamining.clustering.BKmeans;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import lspsolver.Solver;
import projection.model.ProjectionModel;
import projection.model.ProjectionModelComp;
import projection.technique.Projection;
import projection.technique.idmap.IDMAPProjection;
import projection.technique.idmap.IDMAPProjection.InitializationType;
import projection.view.ProjectionViewComp;
import projection.stress.StressComp;
import projection.stress.StressFactory.StressType;
import vispipeline.distance.DistanceMatrix;
import vispipeline.distance.LightWeightDistanceMatrix;
import vispipeline.distance.dissimilarity.AbstractDissimilarity;
import vispipeline.distance.dissimilarity.DissimilarityFactory.DissimilarityType;
import vispipeline.distance.dissimilarity.Euclidean;
import vispipeline.matrix.AbstractMatrix;
import vispipeline.matrix.AbstractVector;
import vispipeline.matrix.dense.DenseMatrix;
import vispipeline.matrix.dense.DenseVector;
import vispipeline.matrix.reader.MatrixReaderComp;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class SampleProjection implements Projection {

    public enum SampleType {

        RANDOM, CLUSTERING, SEMI_CLUSTERING
    }

    @Override
    public AbstractMatrix project(AbstractMatrix matrix, AbstractDissimilarity diss) throws IOException {
        //define the sample
        int samplesize = (int) Math.sqrt(matrix.getRowCount());

        //create the sample matrix data
        AbstractMatrix sampledata = getSampleData(matrix, diss, samplesize);

        //project the sample using IDMAP
        IDMAPProjection idmap = new IDMAPProjection();
        idmap.setFractionDelta(fracdelta);
        idmap.setInitialization(InitializationType.FASTMAP);
        idmap.setNumberIterations(nriteractions);
        AbstractMatrix sampleproj = idmap.project(sampledata, diss);

        //finding A where (D'.A = P')
        int nrows = sampledata.getRowCount();
        int ncolumns = sampledata.getDimensions();

        Solver solver = new Solver(nrows, ncolumns);

        //creating matrix D'
        for (int i = 0; i < sampledata.getRowCount(); i++) {
            float[] array = sampledata.getRow(i).toArray();

            for (int j = 0; j < array.length; j++) {
//                solver.addToA(i, j, array[j] + (float) (Math.random() / 100));
                solver.addToA(i, j, array[j]);
            }
        }

        //creating P'
        for (int i = 0; i < sampledata.getRowCount(); i++) {
            float[] array = sampleproj.getRow(i).toArray();

            for (int j = 0; j < array.length; j++) {
                solver.addToB(i, j, array[j]);
            }
        }

        //solving to find A
        float[][] aux_A = new float[2][];
        aux_A[0] = new float[ncolumns];
        aux_A[1] = new float[ncolumns];

        float[] result = solver.solve();
        for (int i = 0; i < result.length; i += 2) {
            aux_A[0][i / 2] = result[i];
            aux_A[1][i / 2] = result[i + 1];
        }

        DenseMatrix A = new DenseMatrix();
        A.addRow(new DenseVector(aux_A[0]));
        A.addRow(new DenseVector(aux_A[1]));

        DenseMatrix projection = new DenseMatrix();

        //finally, calculating the projection (P = D.A)
        for (int i = 0; i < matrix.getRowCount(); i++) {
            AbstractVector row = matrix.getRow(i);

            float[] proj = new float[2];
            proj[0] = row.dot(A.getRow(0));
            proj[1] = row.dot(A.getRow(1));

            projection.addRow(new DenseVector(proj, row.getId(), row.getKlass()));
        }

        return projection;
    }

    @Override
    public AbstractMatrix project(DistanceMatrix dmat) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * @return the fracdelta
     */
    public float getFractionDelta() {
        return fracdelta;
    }

    /**
     * @param fracdelta the fracdelta to set
     */
    public void setFractionDelta(float fracdelta) {
        this.fracdelta = fracdelta;
    }

    /**
     * @return the nriteractions
     */
    public int getNumberIteractions() {
        return nriteractions;
    }

    /**
     * @param nriteractions the nriteractions to set
     */
    public void setNumberIteractions(int nriteractions) {
        this.nriteractions = nriteractions;
    }

    /**
     * @return the sampletype
     */
    public SampleType getSampleType() {
        return sampletype;
    }

    /**
     * @param sampletype the sampletype to set
     */
    public void setSampleType(SampleType sampletype) {
        this.sampletype = sampletype;
    }

    private AbstractMatrix getSampleData(AbstractMatrix matrix,
            AbstractDissimilarity diss, int samplesize) throws IOException {

        AbstractMatrix sampledata = null;

        if (sampletype == SampleType.CLUSTERING) {
            BKmeans bkmeans = new BKmeans(samplesize);
            bkmeans.execute(diss, matrix);
            sampledata = bkmeans.getCentroids();
        } else if (sampletype == SampleType.SEMI_CLUSTERING) {
            int largesample = (int) Math.pow(matrix.getRowCount(), 0.75f);
            HashSet<Integer> sample = new HashSet<Integer>();

            while (sample.size() < largesample) {
                sample.add((int) (Math.random() * (matrix.getRowCount() - 1)));
            }

            AbstractMatrix auxdata = new DenseMatrix();

            //creating the sample matrix
            Iterator<Integer> it = sample.iterator();
            while (it.hasNext()) {
                AbstractVector row = matrix.getRow(it.next());
                auxdata.addRow(new DenseVector(row.getValues()));
            }

            BKmeans bkmeans = new BKmeans(samplesize);
            bkmeans.execute(diss, auxdata);

            sampledata = bkmeans.getCentroids();
        } else {
            //create the sample matrix data
            sampledata = new DenseMatrix();

            HashSet<Integer> sample = new HashSet<Integer>();

            while (sample.size() < samplesize) {
                sample.add((int) (Math.random() * (matrix.getRowCount() - 1)));
            }

            //creating the sample matrix
            Iterator<Integer> it = sample.iterator();
            while (it.hasNext()) {
                AbstractVector row = matrix.getRow(it.next());
                sampledata.addRow(new DenseVector(row.getValues()));
            }
        }

        ////////////////////////////////
        //adding a base
        float[] vectmax = new float[sampledata.getDimensions()];
        Arrays.fill(vectmax, Float.NEGATIVE_INFINITY);

        float[] vectmin = new float[sampledata.getDimensions()];
        Arrays.fill(vectmin, Float.POSITIVE_INFINITY);

        for (int i = 0; i < sampledata.getRowCount(); i++) {
            float[] array = sampledata.getRow(i).getValues();

            for (int j = 0; j < array.length; j++) {
                if (vectmax[j] < array[j]) {
                    vectmax[j] = array[j];
                }

                if (vectmin[j] > array[j]) {
                    vectmin[j] = array[j];
                }
            }
        }

        for (int i = 0; i < sampledata.getDimensions(); i++) {
            float[] vect = new float[sampledata.getDimensions()];
            Arrays.fill(vect, 0.0f);
            vect[i] = (vectmax[i] + vectmin[i]) / 10;
            vect[i] = (Math.abs(vect[i]) > 0.00001f) ? vect[i] : 0.01f;
            sampledata.addRow(new DenseVector(vect));
        }

        return sampledata;
    }

    public static void main(String[] args) {
        try {
            MatrixReaderComp reader = new MatrixReaderComp();
            reader.setFilename("D:/Meus documentos/FERNANDO/Dados/mammals-10000.data-normcols.data");
//            reader.setFilename("D:\\Dados\\sampling10d2e5.0099-std.data");
//            reader.setFilename("D:\\Meus documentos\\FERNANDO\\Dados\\ssurface.data");
//            reader.setFilename("D:\\Meus documentos\\FERNANDO\\Dados\\swissroll.data");
//            reader.setFilename("D:\\Meus documentos\\FERNANDO\\Dados\\mammals-1000000.bin");

            reader.execute();
            AbstractMatrix matrix = reader.output();

//            BinaryMatrixWriter bmw = new BinaryMatrixWriter();
//            bmw.write(matrix, "D:/Meus documentos/FERNANDO/Dados/mammals-10000.data-normcols.bin");

//            StreamingMatrix matrix = new StreamingMatrix();
//            matrix.load("D:\\Dados\\mammals-1000000.bin");

            /////////////////////////////////////////////////////
            /////////////////////////////////////////////////////
            /////////////////////////////////////////////////////

            long start = System.currentTimeMillis();

            SampleProjectionComp spcomp = new SampleProjectionComp();
            spcomp.setDissimilarityType(DissimilarityType.EUCLIDEAN);
            spcomp.setFractionDelta(8.0f);
            spcomp.setNumberIterations(100);
            spcomp.setSampleType(SampleType.RANDOM);
            spcomp.input(matrix);
            spcomp.execute();
            AbstractMatrix proj1 = spcomp.output();

            long finish = System.currentTimeMillis();

            Logger.getLogger(SampleProjection.class.getName()).log(Level.INFO,
                    "Sample Projection (SProj) time: " + (finish - start) / 1000.0f + "s");

            ProjectionModelComp mcomp1 = new ProjectionModelComp();
            mcomp1.input(proj1);
            mcomp1.execute();
            ProjectionModel model1 = mcomp1.output();

            ProjectionViewComp fcomp1 = new ProjectionViewComp();
            fcomp1.input(model1);
            fcomp1.execute();

            StressComp scomp1 = new StressComp();
            scomp1.setStressType(StressType.NORMALIZED_KRUSKAL);
            LightWeightDistanceMatrix dmat = new LightWeightDistanceMatrix(matrix, new Euclidean());
            scomp1.input(proj1, dmat);
            scomp1.execute();
            System.out.println(scomp1.output());
        } catch (IOException ex) {
            Logger.getLogger(SampleProjection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private float fracdelta = 8.0f;
    private int nriteractions = 50;
    private SampleType sampletype = SampleType.CLUSTERING;
}
