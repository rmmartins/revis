/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package projection.technique.isomap;

import java.io.IOException;
import org.openide.util.lookup.ServiceProvider;
import projection.technique.ProjectionComp;
import vispipeline.basics.annotations.VisComponent;
import vispipeline.basics.interfaces.AbstractComponent;
import vispipeline.basics.interfaces.AbstractParametersView;
import vispipeline.distance.dissimilarity.AbstractDissimilarity;
import vispipeline.distance.dissimilarity.DissimilarityFactory;
import vispipeline.distance.dissimilarity.DissimilarityFactory.DissimilarityType;

/**
 *
 * @author Fernando Vieira Paulovich
 */
@VisComponent(hierarchy = "Projection.Technique",
name = "Isometric Feature Mapping (ISOMAP)",
description = "Project points from a multidimensional space to the plane " +
"preserving the geodesic distance relations.",
howtocite = "Tenenbaum, J. B.; Silva, V.; Langford, J. C. A global " +
"geometric framework for nonlinear dimensionality reduction. Science, " +
"v. 290, n. 5500, p. 2319-2323, 2000.")
@ServiceProvider(service=AbstractComponent.class)
public class ISOMAPProjectionComp extends ProjectionComp implements
        AbstractComponent {

    @Override
    public void execute() throws IOException {
        //projecting
        ISOMAPProjection isomap = new ISOMAPProjection();
        isomap.setNumberNeighbors(nrneighbors);

        if (matrix != null) { //using a matrix
            AbstractDissimilarity diss = DissimilarityFactory.getInstance(disstype);
            projection = isomap.project(matrix, diss);
        } else if (dmat != null) { //using a distance matrix
            projection = isomap.project(dmat);
        } else {
            throw new IOException("A distance matrix or a points matrix should " +
                    "be provided.");
        }
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new ISOMAPProjectionParamView(this);
        }

        return paramview;
    }

    @Override
    public void reset() {
        projection = null;
        matrix = null;
        dmat = null;
    }

    /**
     * @return the nrneighbors
     */
    public int getNumberNeighbors() {
        return nrneighbors;
    }

    /**
     * @param nrneighbors the nrneighbors to set
     */
    public void setNumberNeighbors(int nrneighbors) {
        this.nrneighbors = nrneighbors;
    }

    /**
     * @return the disstype
     */
    public DissimilarityType getDissimilarityType() {
        return disstype;
    }

    /**
     * @param disstype the disstype to set
     */
    public void setDissimilarityType(DissimilarityType diss) {
        this.disstype = diss;
    }

    public static final long serialVersionUID = 1L;
    private int nrneighbors = 10;
    private DissimilarityType disstype = DissimilarityType.EUCLIDEAN;
    private transient ISOMAPProjectionParamView paramview;
    
}
