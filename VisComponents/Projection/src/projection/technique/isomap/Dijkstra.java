/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Projection Explorer (PEx), based on the code presented 
 * in:
 * 
 * http://snippets.dzone.com/tag/dijkstra
 *
 * How to cite this work:
 *  
@inproceedings{paulovich2007pex,
author = {Fernando V. Paulovich and Maria Cristina F. Oliveira and Rosane 
Minghim},
title = {The Projection Explorer: A Flexible Tool for Projection-based 
Multidimensional Visualization},
booktitle = {SIBGRAPI '07: Proceedings of the XX Brazilian Symposium on 
Computer Graphics and Image Processing (SIBGRAPI 2007)},
year = {2007},
isbn = {0-7695-2996-8},
pages = {27--34},
doi = {http://dx.doi.org/10.1109/SIBGRAPI.2007.39},
publisher = {IEEE Computer Society},
address = {Washington, DC, USA},
}
 *  
 * PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package projection.technique.isomap;

import vispipeline.datamining.neighbors.Pair;
import java.util.Arrays;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class Dijkstra {

    public Dijkstra(Pair[][] neighborhood, int nrNodes) {
        this.n = nrNodes;
        this.d = new float[this.n];

        //creating the adjascent matrix
        this.dist = new float[this.n][];

        for (int i = 0; i < neighborhood.length; i++) {
            this.dist[i] = new float[this.n];
            Arrays.fill(this.dist[i], 0.0f);
            for (int j = 0; j < neighborhood[i].length; j++) {
                float distance = neighborhood[i][j].value;

                if (distance > 0.0f) {
                    this.dist[i][neighborhood[i][j].index] = distance;
                }
            }
        }
    }

    public float[] execute(int source) {
        boolean visited[] = new boolean[this.n];

        for (int i = 0; i < this.n; ++i) {
            d[i] = Float.POSITIVE_INFINITY;
            visited[i] = false; // the i-th element has not yet been visited
        }

        this.d[source] = 0.0f;

        for (int k = 0; k < n; ++k) {
            int mini = -1;
            for (int i = 0; i < n; ++i) {
                if (!visited[i] && ((mini == -1) || (d[i] < d[mini]))) {
                    mini = i;
                }
            }

            visited[mini] = true;

            for (int i = 0; i < n; ++i) {
                if (dist[mini][i] > 0.0f) {
                    if (d[mini] + dist[mini][i] < d[i]) {
                        d[i] = d[mini] + dist[mini][i];
                    }
                }
            }
        }

        return this.d;
    }

    private int n; // The number of nodes in the graph
    private float dist[][]; // dist[i][j] is the distance between node i and j; or 0 if there is no direct connection
    private float d[]; // d[i] is the length of the shortest path between the source (s) and node i
}
