/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package projection.technique.plsp;

import java.io.IOException;
import org.openide.util.lookup.ServiceProvider;
import projection.technique.ProjectionComp;
import vispipeline.basics.annotations.VisComponent;
import vispipeline.basics.interfaces.AbstractComponent;
import vispipeline.basics.interfaces.AbstractParametersView;
import vispipeline.distance.dissimilarity.AbstractDissimilarity;
import vispipeline.distance.dissimilarity.DissimilarityFactory;
import vispipeline.distance.dissimilarity.DissimilarityFactory.DissimilarityType;

/**
 *
 * @author Fernando Vieira Paulovich
 */
@VisComponent(hierarchy = "Projection.Technique",
name = "Piecewise Least Square Projection (P-LSP)",
description = "Project points from a multidimensional space to the plane " +
"preserving the neighborhood relations.")
@ServiceProvider(service=AbstractComponent.class)
public class PLSPProjection2DComp extends ProjectionComp implements
        AbstractComponent {

    @Override
    public void execute() throws IOException {
        //projecting
        PLSPProjection2D plsp = new PLSPProjection2D();
        plsp.setFractionDelta(fracdelta);
        plsp.setNumberIterations(nriterations);
        plsp.setNumberNeighbors(nrneighbors);

        if (matrix != null) { //using a matrix
            AbstractDissimilarity diss = DissimilarityFactory.getInstance(disstype);
            projection = plsp.project(matrix, diss);
        } else {
            throw new IOException("A points matrix should be provided.");
        }
    }
    
    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new PLSPProjection2DParamView(this);
        }

        return paramview;
    }

    @Override
    public void reset() {
        projection = null;
        matrix = null;
    }

    /**
     * @return the nriterations
     */
    public int getNumberIterations() {
        return nriterations;
    }

    /**
     * @param nriterations the nriterations to set
     */
    public void setNumberIterations(int nriterations) {
        this.nriterations = nriterations;
    }

    /**
     * @return the fracdelta
     */
    public float getFractionDelta() {
        return fracdelta;
    }

    /**
     * @param fracdelta the fracdelta to set
     */
    public void setFractionDelta(float fracdelta) {
        this.fracdelta = fracdelta;
    }

    /**
     * @return the nrneighbors
     */
    public int getNumberNeighbors() {
        return nrneighbors;
    }

    /**
     * @param nrneighbors the nrneighbors to set
     */
    public void setNumberNeighbors(int nrneighbors) {
        this.nrneighbors = nrneighbors;
    }

    /**
     * @return the disstype
     */
    public DissimilarityType getDissimilarityType() {
        return disstype;
    }

    /**
     * @param disstype the disstype to set
     */
    public void setDissimilarityType(DissimilarityType diss) {
        this.disstype = diss;
    }

    public int getNumberInstances() {
        if (matrix != null) {
            return matrix.getRowCount();
        }

        return 0;
    }

    public static final long serialVersionUID = 1L;
    private int nriterations = 50;
    private float fracdelta = 8.0f;
    private int nrneighbors = 10;
    private DissimilarityType disstype = DissimilarityType.EUCLIDEAN;
    private transient PLSPProjection2DParamView paramview;
    
}
