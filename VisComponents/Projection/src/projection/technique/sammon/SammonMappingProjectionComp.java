/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package projection.technique.sammon;

import java.io.IOException;
import org.openide.util.lookup.ServiceProvider;
import projection.technique.ProjectionComp;
import vispipeline.basics.annotations.VisComponent;
import vispipeline.basics.interfaces.AbstractComponent;
import vispipeline.basics.interfaces.AbstractParametersView;
import vispipeline.distance.dissimilarity.AbstractDissimilarity;
import vispipeline.distance.dissimilarity.DissimilarityFactory;
import vispipeline.distance.dissimilarity.DissimilarityFactory.DissimilarityType;

/**
 *
 * @author Fernando Vieira Paulovich
 */
@VisComponent(hierarchy = "Projection.Technique",
name = "Sammon's Mapping",
description = "Project points from a multidimensional space to the plane " +
"preserving the distance relations.",
howtocite = "Sammon, J. W. A nonlinear mapping for data structure " +
"analysis. IEEE Transactions on Computers, v. 18, n. 5, p. 401-409, 1969.")
@ServiceProvider(service=AbstractComponent.class)
public class SammonMappingProjectionComp extends ProjectionComp implements
        AbstractComponent {

    @Override
    public void execute() throws IOException {
        //projecting
        SammonMappingProjection sammon = new SammonMappingProjection();
        sammon.setMagicFactor(mfactor);
        sammon.setNumberIterations(nriterations);

        if (matrix != null) { //using a matrix
            AbstractDissimilarity diss = DissimilarityFactory.getInstance(disstype);
            projection = sammon.project(matrix, diss);
        } else if (dmat != null) { //using a distance matrix
            projection = sammon.project(dmat);
        } else {
            throw new IOException("A distance matrix or a points matrix should " +
                    "be provided.");
        }
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new SammonMappingProjectionParamView(this);
        }

        return paramview;
    }

    @Override
    public void reset() {
        projection = null;
        matrix = null;
        dmat = null;
    }

    /**
     * @return the nriterations
     */
    public int getNumberIterations() {
        return nriterations;
    }

    /**
     * @param nriterations the nriterations to set
     */
    public void setNumberIterations(int nriterations) {
        this.nriterations = nriterations;
    }

    /**
     * @return the fracdelta
     */
    public float getMagicFactor() {
        return mfactor;
    }

    /**
     * @param fracdelta the fracdelta to set
     */
    public void setMagicFactor(float mfactor) {
        this.mfactor = mfactor;
    }

    /**
     * @return the disstype
     */
    public DissimilarityType getDissimilarityType() {
        return disstype;
    }

    /**
     * @param disstype the disstype to set
     */
    public void setDissimilarityType(DissimilarityType diss) {
        this.disstype = diss;
    }

    public int getNumberInstances() {
        if (matrix != null) {
            return matrix.getRowCount();
        } else if (dmat != null) {
            return dmat.getElementCount();
        }

        return 0;
    }

    public static final long serialVersionUID = 1L;
    private DissimilarityType disstype = DissimilarityType.EUCLIDEAN;
    private int nriterations = 50;
    private float mfactor = 0.3f;
    private transient SammonMappingProjectionParamView paramview;
    
}
