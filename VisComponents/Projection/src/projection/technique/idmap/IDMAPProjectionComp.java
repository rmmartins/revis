/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package projection.technique.idmap;

import java.io.IOException;
import org.openide.util.lookup.ServiceProvider;
import projection.technique.ProjectionComp;
import projection.technique.idmap.IDMAPProjection.InitializationType;
import vispipeline.basics.annotations.Parameter;
import vispipeline.basics.annotations.VisComponent;
import vispipeline.basics.interfaces.AbstractComponent;
import vispipeline.basics.interfaces.AbstractParametersView;
import vispipeline.distance.dissimilarity.AbstractDissimilarity;
import vispipeline.distance.dissimilarity.DissimilarityFactory;
import vispipeline.distance.dissimilarity.DissimilarityFactory.DissimilarityType;

/**
 *
 * @author Fernando Vieira Paulovich
 */
@VisComponent(hierarchy = "Projection.Technique",
name = "Interactive Document Map (IDMAP)",
description = "Project points from a multidimensional space to the plane " +
"preserving the distance relations.",
howtocite = "Minghim, R.; Paulovich, F. V.; Lopes, A. A. Content-based text " +
"mapping using multidimensional projections for exploration of document " +
"collections. IS&T/SPIE Symposium on Electronic Imaging - Visualization " +
"and Data Analysis, San Jose, CA, USA, Jan., 2006.")
@ServiceProvider(service=AbstractComponent.class)
public class IDMAPProjectionComp extends ProjectionComp implements
        AbstractComponent {

    @Override
    public void execute() throws IOException {
        //projecting
        IDMAPProjection idmap = new IDMAPProjection();
        idmap.setFractionDelta(fracdelta);
        idmap.setInitialization(ini);
        idmap.setNumberIterations(nriterations);

        if (matrix != null) { //using a matrix
            AbstractDissimilarity diss = DissimilarityFactory.getInstance(disstype);
            projection = idmap.project(matrix, diss);
        } else if (dmat != null) { //using a distance matrix
            projection = idmap.project(dmat);
        } else {
            throw new IOException("A distance matrix or a points matrix should " +
                    "be provided.");
        }
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new IDMAPProjectionParamView(this);
        }

        return paramview;
    }

    @Override
    public void reset() {
        projection = null;
        matrix = null;
        dmat = null;
    }

    /**
     * @return the projtype
     */
    public InitializationType getInitialization() {
        return ini;
    }

    /**
     * @param projtype the projtype to set
     */
    public void setInitialization(InitializationType ini) {
        this.ini = ini;
    }

    /**
     * @return the nriterations
     */
    public int getNumberIterations() {
        return nriterations;
    }

    /**
     * @param nriterations the nriterations to set
     */
    public void setNumberIterations(int nriterations) {
        this.nriterations = nriterations;
    }

    /**
     * @return the fracdelta
     */
    public float getFractionDelta() {
        return fracdelta;
    }

    /**
     * @param fracdelta the fracdelta to set
     */
    public void setFractionDelta(float fracdelta) {
        this.fracdelta = fracdelta;
    }

    /**
     * @return the disstype
     */
    public DissimilarityType getDissimilarityType() {
        return disstype;
    }

    /**
     * @param disstype the disstype to set
     */
    public void setDissimilarityType(DissimilarityType diss) {
        this.disstype = diss;
    }

    public static final long serialVersionUID = 1L;
    @Parameter public InitializationType ini = InitializationType.FASTMAP;
    @Parameter public DissimilarityType disstype = DissimilarityType.EUCLIDEAN;
    @Parameter public int nriterations = 50;
    @Parameter public float fracdelta = 8.0f;

    private transient IDMAPProjectionParamView paramview;
    
}
