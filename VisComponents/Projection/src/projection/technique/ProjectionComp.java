/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package projection.technique;

import java.io.IOException;
import org.openide.util.lookup.ServiceProvider;
import vispipeline.basics.annotations.Param;
import vispipeline.basics.annotations.Return;
import vispipeline.basics.annotations.VisComponent;
import vispipeline.basics.interfaces.AbstractComponent;
import vispipeline.basics.interfaces.AbstractParametersView;
import vispipeline.distance.DistanceMatrix;
import vispipeline.matrix.AbstractMatrix;

/**
 *
 * @author Rafael Messias Martins
 */
@VisComponent(hierarchy = "Projection.Technique",
name = "Projection",
description = "Base component for projection techniques.",
howtocite = "Minghim, R.; Paulovich, F. V.; Lopes, A. A. Content-based text " +
"mapping using multidimensional projections for exploration of document " +
"collections. IS&T/SPIE Symposium on Electronic Imaging - Visualization " +
"and Data Analysis, San Jose, CA, USA, Jan., 2006.",
type = VisComponent.BASE)
@ServiceProvider(service=AbstractComponent.class)
public class ProjectionComp implements AbstractComponent {
    
    // AbstractComponent

    @Override
    public void execute() throws IOException {
        // implement your own Projection
    }

    public void input(@Param(name = "points matrix") AbstractMatrix matrix) {
        this.matrix = matrix;
    }

    public void input(@Param(name = "distance matrix") DistanceMatrix dmat) {
        this.dmat = dmat;
    }
    
    @Return(name = "Projection") public AbstractMatrix output() {
        return projection;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        return null;
    }

    @Override
    public void reset() {
        projection = null;
        matrix = null;
        dmat = null;
    }

    public boolean isDistanceMatrixProvided() {
        return (dmat != null);
    }

    protected transient AbstractMatrix projection;
    protected transient AbstractMatrix matrix;
    protected transient DistanceMatrix dmat;
}
