/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package projection.technique.lsp;

import java.io.IOException;
import org.openide.util.lookup.ServiceProvider;
import projection.technique.ProjectionComp;
import projection.technique.lsp.LSPProjection2D.ControlPointsType;
import vispipeline.basics.annotations.VisComponent;
import vispipeline.basics.interfaces.AbstractComponent;
import vispipeline.basics.interfaces.AbstractParametersView;
import vispipeline.distance.dissimilarity.AbstractDissimilarity;
import vispipeline.distance.dissimilarity.DissimilarityFactory;
import vispipeline.distance.dissimilarity.DissimilarityFactory.DissimilarityType;

/**
 *
 * @author Fernando Vieira Paulovich
 */
@VisComponent(hierarchy = "Projection.Technique",
name = "Least Square Projection (LSP)",
description = "Project points from a multidimensional space to the plane " +
"preserving the neighborhood relations.",
howtocite = "Paulovich, F. V.; Nonato, L. G.; Minghim, R.; Levkowitz, H. Least " +
"Square Projection: a fast high precision multidimensional projection " +
"technique and its application to document mapping. IEEE Transactions " +
"on Visualization and Computer Graphics, v. 14, p. 564-575, 2008. ")
@ServiceProvider(service=AbstractComponent.class)
public class LSPProjection2DComp extends ProjectionComp implements
        AbstractComponent {

    @Override
    public void execute() throws IOException {

//        System.out.println("LSP input:");
//        for (int i = 0; i < matrix.getRowCount(); i++) {
//            System.out.printf("%f, %f\n", matrix.getRow(i).getValue(0),
//                    matrix.getRow(i).getValue(1));
//        }

        //projecting
        LSPProjection2D lsp = new LSPProjection2D();
        lsp.setFractionDelta(fracdelta);
        lsp.setNumberIterations(nriterations);
        lsp.setNumberNeighbors(nrneighbors);
        lsp.setNumberControlPoints(nrcontrolpoints);

        if (matrix != null) { //using a matrix
            AbstractDissimilarity diss = DissimilarityFactory.getInstance(disstype);
            lsp.setControlPointsChoice(ControlPointsType.KMEANS);
            projection = lsp.project(matrix, diss);
        } else if (dmat != null) { //using a distance matrix
            lsp.setControlPointsChoice(ControlPointsType.KMEDOIDS);
            projection = lsp.project(dmat);
        } else {
            throw new IOException("A distance matrix or a points matrix should " +
                    "be provided.");
        }
        
//        System.out.println("LSP output:");
//        for (int i = 0; i < projection.getRowCount(); i++) {
//            System.out.printf("%f, %f\n", projection.getRow(i).getValue(0),
//                    projection.getRow(i).getValue(1));
//        }
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new LSPProjection2DParamView(this);
        }

        return paramview;
    }

    @Override
    public void reset() {
        projection = null;
        matrix = null;
        dmat = null;
    }

    /**
     * @return the nriterations
     */
    public int getNumberIterations() {
        return nriterations;
    }

    /**
     * @param nriterations the nriterations to set
     */
    public void setNumberIterations(int nriterations) {
        this.nriterations = nriterations;
    }

    /**
     * @return the fracdelta
     */
    public float getFractionDelta() {
        return fracdelta;
    }

    /**
     * @param fracdelta the fracdelta to set
     */
    public void setFractionDelta(float fracdelta) {
        this.fracdelta = fracdelta;
    }

    /**
     * @return the nrneighbors
     */
    public int getNumberNeighbors() {
        return nrneighbors;
    }

    /**
     * @param nrneighbors the nrneighbors to set
     */
    public void setNumberNeighbors(int nrneighbors) {
        this.nrneighbors = nrneighbors;
    }

    /**
     * @return the nrcontrolpoints
     */
    public int getNumberControlPoints() {
        return nrcontrolpoints;
    }

    /**
     * @param nrcontrolpoints the nrcontrolpoints to set
     */
    public void setNumberControlPoints(int nrcontrolpoints) {
        this.nrcontrolpoints = nrcontrolpoints;
    }

    /**
     * @return the disstype
     */
    public DissimilarityType getDissimilarityType() {
        return disstype;
    }

    /**
     * @param disstype the disstype to set
     */
    public void setDissimilarityType(DissimilarityType diss) {
        this.disstype = diss;
    }
    
    public int getNumberInstances() {
        if (matrix != null) {
            return matrix.getRowCount();
        } else if (dmat != null) {
            return dmat.getElementCount();
        }

        return 0;
    }

    public static final long serialVersionUID = 1L;
    private int nriterations = 50;
    private float fracdelta = 8.0f;
    private int nrneighbors = 10;
    private int nrcontrolpoints = 10;
    private DissimilarityType disstype = DissimilarityType.EUCLIDEAN;
    private transient LSPProjection2DParamView paramview;
    
}
