/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package projection.view.selection.coordination;

import projection.view.selection.*;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import projection.coordination.ColorIdentityCoordinator;
import vispipeline.visualization.coordination.AbstractCoordinator;
import vispipeline.visualization.model.AbstractInstance;
import vispipeline.visualization.view.ModelViewer;
import vispipeline.visualization.view.selection.AbstractSelection;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class ColorIdentityCoordinationSelection extends AbstractSelection {

    public ColorIdentityCoordinationSelection(ModelViewer viewer) {
        super(viewer);
    }

    @Override
    public void selected(ArrayList<AbstractInstance> selinst) {
        if (viewer.getModel() != null && selinst.size() > 0) {
            DynamicColorSelection selection = new DynamicColorSelection(viewer);
            selection.selected(selinst);
            if (selection.getColor() != null) {
                for (AbstractCoordinator ac : viewer.getCoordinators()) {
                    if (ac instanceof ColorIdentityCoordinator) {
                        ac.coordinate(new ArrayList<AbstractInstance>(selinst), selection.getColor());
                    }
                }
                viewer.getModel().setSelectedInstances(selinst);
                viewer.getModel().notifyObservers();
            }
        }
    }

    @Override
    public ImageIcon getIcon() {
//        return new ImageIcon(getClass().getResource("/toolbarButtonGraphics/development/WebComponentAdd16.gif"));
        return new ImageIcon();
    }

    @Override
    public String toString() {
        return "Color Identity Coordination Selection";
    }

//    public static void main(String[] args) {
//        try {
//            MatrixReaderComp reader = new MatrixReaderComp();
//            reader.setFilename("C:\\Users\\User\\Desktop\\Fibras\\pbc2009icdm\\software\\matlab\\fiber_5_beginEnd_classified_NotNormalized.bin");
//            reader.execute();
//
//            PLSPProjection2DComp plsp = new PLSPProjection2DComp();
//            plsp.input(reader.output());
//            plsp.execute();
//
//            ProjectionModelComp model1 = new ProjectionModelComp();
//            model1.input(plsp.output());
//            model1.execute();
//            ProjectionModelComp model2 = new ProjectionModelComp();
//            model2.input(plsp.output());
//            model2.execute();
//
//            ColorIdentityCoordinatorComp coord = new ColorIdentityCoordinatorComp();
//            coord.execute();
//
//            ProjectionFrameComp fcomp1 = new ProjectionFrameComp();
//            fcomp1.input(model1.output());
//            fcomp1.attach(coord.output());
//            fcomp1.execute();
//            ProjectionFrameComp fcomp2 = new ProjectionFrameComp();
//            fcomp2.input(model2.output());
//            fcomp2.attach(coord.output());
//            fcomp2.execute();
//        } catch (IOException ex) {
//            Logger.getLogger(ProjectionFrame.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }

}
