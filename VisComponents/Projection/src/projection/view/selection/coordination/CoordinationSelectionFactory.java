/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package projection.view.selection.coordination;

import projection.coordination.ColorIdentityCoordinator;
import vispipeline.visualization.coordination.AbstractCoordinator;
import vispipeline.visualization.coordination.IdentityCoordinator;
import vispipeline.visualization.view.ModelViewer;
import vispipeline.visualization.view.selection.AbstractSelection;
import vispipeline.visualization.view.selection.coordination.IdentityCoordinationSelection;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class CoordinationSelectionFactory {

    public static AbstractSelection getInstance(AbstractCoordinator coordinator,
            ModelViewer viewer) {
        if (coordinator instanceof IdentityCoordinator) {
            return new IdentityCoordinationSelection(viewer);
        }
        else if(coordinator instanceof ColorIdentityCoordinator){
            return new ColorIdentityCoordinationSelection(viewer);
        }
        return null;
    }

}
