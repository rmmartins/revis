/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package projection.view.selection;

import java.util.ArrayList;
import javax.swing.ImageIcon;
import vispipeline.visualization.model.AbstractInstance;
import vispipeline.visualization.view.ModelViewer;
import vispipeline.visualization.view.selection.AbstractSelection;


/**
 *
 * @author Fernando Vieira Paulovich
 */
public class SplittingSelection extends AbstractSelection {

    public SplittingSelection(ModelViewer viewer) {
        super(viewer);
    }

    @Override
    public void selected(ArrayList<AbstractInstance> selinst) {
    }

    @Override
    public ImageIcon getIcon() {
//        return new ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/Cut16.gif"));
        return new ImageIcon();
    }

    @Override
    public String toString() {
        return "Split Selection";
    }

}
