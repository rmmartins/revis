/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package projection.view;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import projection.model.ProjectionModel;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Logger;
import org.openide.util.lookup.ServiceProvider;
import vispipeline.basics.annotations.Param;
import vispipeline.basics.annotations.Parameter;
import vispipeline.basics.annotations.VisComponent;
import vispipeline.basics.interfaces.AbstractComponent;
import vispipeline.basics.interfaces.AbstractParametersView;
import vispipeline.visualization.coordination.AbstractCoordinator;
import vispipeline.visualization.view.ModelViewerFrame;

/**
 *
 * @author Fernando Vieira Paulovich
 */
@VisComponent(hierarchy = "Projection.View",
name = "Projection View Frame",
description = "Display a projection model.")
@ServiceProvider(service = AbstractComponent.class)
public class ProjectionViewComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        if (model != null) {
            ProjectionModelViewer pview = new ProjectionModelViewer();
            pview.setSize(600, 600);
            pview.setModel(model);

            frame = new ModelViewerFrame();
            frame.setModelViewer(pview);

            // frame position
            if (framePosition != null) {
                frame.setLocation(framePosition);
            }
            // frame size
            if (frameSize != null) {
                frame.setSize(frameSize);
            }
            else {
                frame.setSize(650, 650);
            }
            frame.addWindowListener(new MyWindowListener());

            frame.setModal(false);
            frame.setVisible(true);
            frame.setTitle(title);

            
//            frame.addSelection(new RangeSelection(pview));
//            frame.addSelection(new CovarianceSelection(pview));
//            frame.addSelection(new DynamicColorSelection(pview));

//            Logger.getLogger(getClass().getName()).info(frame.getView().toString());

            if (coordinators != null) {
                for (int i = 0; i < coordinators.size(); i++) {
                    frame.addCoordinator(coordinators.get(i));
                }
            }
        } else {
            throw new IOException("A projection model should be provided.");
        }
    }

    public void input(@Param(name = "projection model") ProjectionModel model) {
        this.model = model;
    }

    public void attach(@Param(name = "Coordinator") AbstractCoordinator coordinator) {
        if (coordinators == null) {
            coordinators = new ArrayList<AbstractCoordinator>();
        }

        if (coordinator != null) {
            coordinators.add(coordinator);
        }
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new ProjectionViewParamView(this);
        }

        return paramview;
    }

    @Override
    public void reset() {
        model = null;
        coordinators = null;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
    public static final long serialVersionUID = 1L;
    protected String title = "";

    protected transient ProjectionModel model;
    protected transient ProjectionViewParamView paramview;
    protected transient ArrayList<AbstractCoordinator> coordinators;

    protected ModelViewerFrame frame = null;

    @Parameter public Dimension frameSize = null;
    @Parameter public Point framePosition = null;
    @Parameter public boolean saveFrameAtts = true;

    public class MyWindowListener extends WindowAdapter {

        @Override
        public void windowClosing(WindowEvent e) {
            if (saveFrameAtts) {
                frameSize = frame.getSize();
                framePosition = frame.getLocation();
            }
            frame.dispose();
        }

    }

}
