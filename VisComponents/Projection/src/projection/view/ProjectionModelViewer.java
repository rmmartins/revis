/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package projection.view;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Observable;
import projection.model.ProjectionInstance;
import projection.model.ProjectionModel;
import vispipeline.visualization.color.ColorScalePanel;
import vispipeline.visualization.model.AbstractInstance;
import vispipeline.visualization.model.AbstractModel;
import vispipeline.visualization.view.ModelViewer;

/**
 *
 * @author F. Paulovich, R. Martins
 */
public class ProjectionModelViewer extends ModelViewer {

    @Override
    public void setModel(AbstractModel model) {
        if (model instanceof ProjectionModel) {
            if (model != null) {

                Dimension size = getSize();
                size.height = (int) (size.height * 0.75f);
                size.width = (int) (size.width * 0.95f);
                ((ProjectionModel) model).fitToSize(size);

                super.setModel(model);
//                Logger.getLogger(getClass().getName()).info("Passou setModel");

//                Scalar scalar = ((ProjectionModel) model).getSelectedScalar();

//                if (scalar != null) {
//                    updateScalars(scalar);
//                } else {
//                    updateScalars(((ProjectionModel) model).getScalars().get(0));
//                }

//                super.setModel((ProjectionModel) model);

                if (csp != null)
                    this.remove(csp);
                this.csp = new ColorScalePanel(this);
                this.csp.setColorTable(getModel().getColorTable());
                this.csp.setPreferredSize(new Dimension(200, 12));               
                this.add(this.csp);

            }
        }
    }

    @Override
    public void draw(java.awt.Graphics2D g) {
        if (model != null && image == null) {
            Dimension size = ((ProjectionModel)model).getSize();

//            Logger.getLogger(getClass().getName()).info(size.toString());
            
            image = new BufferedImage(size.width + 10, size.height + 10,
                    BufferedImage.TYPE_INT_RGB);

            java.awt.Graphics2D g2Buffer = image.createGraphics();
            g2Buffer.setColor(this.getBackground());
            g2Buffer.fillRect(0, 0, size.width + 10, size.height + 10);

            if (highqualityrender) {
                g2Buffer.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                        RenderingHints.VALUE_ANTIALIAS_ON);
            } else {
                g2Buffer.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                        RenderingHints.VALUE_ANTIALIAS_OFF);
            }

            // This shouldn't be done by the model
            ((ProjectionModel) model).draw(image, highqualityrender);

//            Logger.getLogger(getClass().getName()).info("passou o draw");

            g2Buffer.dispose();
        }

        if (image != null) {
            g.drawImage(image, 0, 0, null);
        }
        
    }

    public boolean isHighQualityRender() {
        return highqualityrender;
    }

    public void setHighQualityRender(boolean highqualityrender) {
        this.highqualityrender = highqualityrender;

        cleanImage();
        repaint();
    }

    public void cleanImage() {
        image = null;
    }

    public boolean isShowInstanceLabel() {
        return showinstancelabel;
    }

    public void setShowInstanceLabel(boolean showinstancelabel) {
        this.showinstancelabel = showinstancelabel;

        cleanImage();
        repaint();
    }

        /**
     * This method is called when the model is changed. It can (must) be used to
     * re-create the visual representation of the model.
     */
    public void update(Observable o, Object arg) {        
        if (model != null) {
            cleanImage();
            repaint();
        }
    }

    @Override
    public void zoom(float rate) {
        ProjectionModel pmodel = (ProjectionModel) model;
        pmodel.zoom(rate);
    }

    @Override
    public AbstractInstance getInstanceByPosition(Point p) {
        ProjectionModel pmodel = (ProjectionModel) model;
        return pmodel.getInstanceByPosition(p);
    }

    @Override
    public ArrayList<AbstractInstance> getInstancesByPosition(Polygon p) {
        ProjectionModel pmodel = (ProjectionModel) model;
        ArrayList<AbstractInstance> ret = new ArrayList<AbstractInstance>();
        for (ProjectionInstance pi : pmodel.getInstancesByPosition(p)) {
            ret.add(pi);
        }
        return ret;
    }

    @Override
    public ArrayList<AbstractInstance> getInstancesByPosition(Rectangle r) {
        ProjectionModel pmodel = (ProjectionModel) model;
        ArrayList<AbstractInstance> ret = new ArrayList<AbstractInstance>();
        for (ProjectionInstance pi : pmodel.getInstancesByPosition(r)) {
            ret.add(pi);
        }
        return ret;
    }

    @Override
    public String toString() {
        return "Projection";
    }

    @Override
    public ProjectionModel getModel() {
        return (ProjectionModel) super.getModel();
    }

    private BufferedImage image;
    private boolean highqualityrender = true;
    private ColorScalePanel csp;

}
