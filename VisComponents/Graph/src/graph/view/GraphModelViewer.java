/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package graph.view;

import graph.model.GraphModel;
import projection.view.ProjectionModelViewer;
import vispipeline.visualization.model.AbstractModel;

/**
 *
 * @author Rafael
 */
public class GraphModelViewer extends ProjectionModelViewer {

    @Override
    public void setModel(AbstractModel model) {
        if (model instanceof GraphModel) {
            super.setModel(model);
        }
    }

    @Override
    public GraphModel getModel() {
        return (GraphModel) super.getModel();
    }
    
}
