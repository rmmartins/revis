/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package graph.view;

import graph.model.GraphModel;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import org.openide.util.lookup.ServiceProvider;
import vispipeline.basics.annotations.Param;
import vispipeline.basics.annotations.Parameter;
import vispipeline.basics.annotations.VisComponent;
import vispipeline.basics.interfaces.AbstractComponent;
import vispipeline.basics.interfaces.AbstractParametersView;
import vispipeline.visualization.coordination.AbstractCoordinator;
import vispipeline.visualization.view.ModelViewerFrame;

/**
 *
 * @author Fernando Vieira Paulovich
 */
@VisComponent(hierarchy = "Graph.View",
name = "Graph View Frame",
description = "Display a graph model.")
@ServiceProvider(service=AbstractComponent.class)
public class GraphFrameComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        if (model != null) {

            frame = new GraphViewFrame();           
            
            // frame position
            if (framePosition != null) {
                frame.setLocation(framePosition);
            }
            // frame size
            if (frameSize != null) {
                frame.setSize(frameSize);
            }
            else {
                frame.setSize(650, 650);
            }
            frame.addWindowListener(new MyWindowListener());

            // at this point we know the frame size
            GraphModelViewer gview = new GraphModelViewer();
            gview.setSize(frame.getWidth(), frame.getHeight());
            gview.setModel(model);
            frame.setModelViewer(gview);

            frame.setModal(false);
            frame.setVisible(true);
            frame.setTitle(title);

            if (coordinators != null) {
                for (int i = 0; i < coordinators.size(); i++) {
//                    frame.addCoordinator(coordinators.get(i));
                }
            }
        } else {
            throw new IOException("A projection model should be provided.");
        }
    }

    public void input(@Param(name = "graph model") GraphModel model) {
        this.model = model;
    }

    public void attach(@Param(name = "Coordinator") AbstractCoordinator coordinator) {
        if (coordinators == null) {
            coordinators = new ArrayList<AbstractCoordinator>();
        }

        if (coordinator != null) {
            coordinators.add(coordinator);
        }
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new GraphFrameParamView(this);
        }

        return paramview;
    }

    @Override
    public void reset() {
        model = null;
        coordinators = null;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle () {
        return title;
    }

    public static final long serialVersionUID = 1L;    
    protected transient GraphModel model;
    protected transient AbstractParametersView paramview;
    protected transient ArrayList<AbstractCoordinator> coordinators;
    protected ModelViewerFrame frame;

    @Parameter public Dimension frameSize = null;
    @Parameter public Point framePosition = null;
    @Parameter public boolean saveFrameAtts = true;
    @Parameter public String title = "";

    public class MyWindowListener extends WindowAdapter {
        @Override
        public void windowClosing(WindowEvent e) {
            if (saveFrameAtts) {
                frameSize = frame.getSize();
                framePosition = frame.getLocation();
            }
            frame.dispose();
        }

    }
}
