/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package graph.view;

import graph.forcelayout.ForceDirectLayout;
import graph.model.Connectivity;
import graph.model.GraphModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import vispipeline.visualization.model.Scalar;
import vispipeline.visualization.view.ModelViewer;
import vispipeline.visualization.view.ModelViewerFrame;

/**
 *
 * @author Rafael
 */
public class GraphViewFrame extends ModelViewerFrame implements Observer {

    public GraphViewFrame() {
        // edges
        edgesMenu = new JMenu("Edges");
        menuBar.add(edgesMenu);
    }

    @Override
    public void updateScalars(Scalar scalar) {                        
        super.updateScalars(scalar);
        repaint();
    }

    @Override
    public void setModelViewer(ModelViewer view) {
        super.setModelViewer(view);
        view.getModel().addObserver(this);
        // edges
        final GraphModel model = (GraphModel) view.getModel();
        for (final Connectivity c : model.getConnectivities()) {
            if (!c.getName().equals("...")) {
                final JCheckBoxMenuItem menuItem =
                        new JCheckBoxMenuItem(c.getName());
                edgesMenu.add(menuItem);
                menuItem.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        if (menuItem.isSelected())
                            model.setSelectedConnectivity(c);
                        else
                            model.setSelectedConnectivity(null);                        
                    }
                });
            }
        }
        updateScalars(view.getModel().getSelectedScalar());
    }

    public void update(Observable o, Object arg) {
//        AbstractModel model = (AbstractModel) o;
//        logger.info("update");
//        if (model.getScalars().size() != scalarCombo.getModel().getSize()) {
//            logger.info("sizes are different");
//            // update scalars
//            Scalar selected = model.getSelectedScalar();
//            Scalar[] scalars = model.getScalars().toArray(new Scalar[0]);
//            scalarCombo.setModel(new DefaultComboBoxModel(scalars));
//            scalarCombo.setSelectedItem(selected);
//        }
//        updateScalars();
    }    
    
    protected JMenu edgesMenu;
    private boolean start = true;
    private ForceDirectLayout force;
}
