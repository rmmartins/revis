/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package textprocessing.topic;


import graph.model.GraphModel;
import java.io.IOException;
import org.openide.util.lookup.ServiceProvider;
import textprocessing.corpus.Corpus;
import vispipeline.basics.annotations.Param;
import vispipeline.basics.annotations.Return;
import vispipeline.basics.annotations.VisComponent;
import vispipeline.basics.interfaces.AbstractComponent;
import vispipeline.basics.interfaces.AbstractParametersView;
import vispipeline.matrix.AbstractMatrix;

/**
 *
 * @author Rafael
 */
@VisComponent(hierarchy = "Systematic Review",
name = "Covariance Topic",
description = "....")
@ServiceProvider(service=AbstractComponent.class)
public class CovarianceTopicComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        topic = new CovarianceTopic(graph, corpus);
    }

    public void input(
            @Param(name = "Corpus") Corpus corpus,
            @Param(name = "Graph") GraphModel graph) {
        this.corpus = corpus;
        this.graph = graph;
    }

    @Return(name = "Topic") public CovarianceTopic output() {
        return topic;
    }

    @Override
    public void reset() {
        topic = null;
        graph = null;
        corpus = null;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        return null;
    }

    volatile protected CovarianceTopic topic;
    volatile protected GraphModel graph;
    volatile protected Corpus corpus;

}
