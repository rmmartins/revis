/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package textprocessing.topic;

import graph.view.GraphModelViewer;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import vispipeline.visualization.model.AbstractInstance;

/**
 *
 * @author Rafael
 */
public class TextGraphModelViewer extends GraphModelViewer {

    public TextGraphModelViewer() {
        addMouseMotionListener(new MouseAdapter() {

            @Override
            public void mouseMoved(MouseEvent e) {
                selectedTopic = null;
                for (Topic t : topics) {
                    if (t != null) {
                        Rectangle r = t.getRectangle();
                        if (r != null && r.contains(e.getPoint())) {
                            selectedTopic = t;
                        }
                    }
                }
            }

        });

        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (selectedTopic != null) {
                    ArrayList<AbstractInstance> inst = getInstancesByPosition(
                            selectedTopic.getRectangle());
                    getSelection().selected(inst);
                    getCoordination().selected(inst);
                }
            }
        });
    }

    public void addTopic(Topic topic) {
        this.topics.add(topic);
    }

    public void removeTopic(int index) {
        topics.remove(index);
    }

    public List<Topic> getTopics() {
        return topics;
    }

    @Override
    public void draw(Graphics2D g) {
//        logger.info("draw");
        if (!(getSelection() instanceof TopicSelection)) {
            Topic.setShowTopics(false);
//            logger.info("setShowTopics = false");
        }
        super.draw(g);
//        logger.info("topics size = " + topics.size());
//        logger.info("isShowTopics = " + Topic.isShowTopics());
        if (topics.size() > 0 && Topic.isShowTopics()) {
            for (Topic topic : topics) {
                boolean selected = false;
                if (Topic.highlightLabels && topic.equals(selectedTopic)) {
                    selected = true;
                    topic.showThisLabel = true;
                } else {
                    topic.showThisLabel = false;
                }
                topic.drawTopic(g, Font.getFont(Font.SANS_SERIF), selected);
            }
        }
    }

    private List<Topic> topics = new ArrayList<Topic>();
    private Topic selectedTopic = null;

    private Logger logger = Logger.getLogger(this.getClass().getSimpleName());

}
