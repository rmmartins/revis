/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Projection Explorer (PEx).
 *
 * How to cite this work:
 *  
@inproceedings{paulovich2007pex,
author = {Fernando V. Paulovich and Maria Cristina F. Oliveira and Rosane 
Minghim},
title = {The Projection Explorer: A Flexible Tool for Projection-based 
Multidimensional Visualization},
booktitle = {SIBGRAPI '07: Proceedings of the XX Brazilian Symposium on 
Computer Graphics and Image Processing (SIBGRAPI 2007)},
year = {2007},
isbn = {0-7695-2996-8},
pages = {27--34},
doi = {http://dx.doi.org/10.1109/SIBGRAPI.2007.39},
publisher = {IEEE Computer Society},
address = {Washington, DC, USA},
}
 *  
 * PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package textprocessing.topic;

import graph.model.GraphModel;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import projection.model.ProjectionInstance;
import textprocessing.corpus.Corpus;
import vispipeline.datamining.clustering.BKmeans;
import vispipeline.distance.dissimilarity.Euclidean;
import vispipeline.matrix.AbstractMatrix;
import vispipeline.matrix.dense.DenseMatrix;
import vispipeline.matrix.dense.DenseVector;
import vispipeline.visualization.model.AbstractInstance;
import vispipeline.visualization.model.Scalar;

/**
 *
 * @author Fernando Vieira Paulovich, Roberto Pinho
 */
public class TopicClusters {

    private static final String TOPIC_SCALAR = "Topic";
    private static final int KMEANS = 1;

    public TopicClusters(TextGraphModelViewer viewer, Corpus corpus) {
        this.viewer = viewer;
        this.corpus = corpus;
    }

    public void execute() throws IOException {
        GraphModel graph = viewer.getModel();
//        graph.getTopicData().getTermSetRun().clear();
//        graph.getTopicData().getTermSetRunW().clear();
//        graph.addScalar(TOPIC_SCALAR);

        //Create the clusters
        AbstractMatrix matrix = new DenseMatrix();
        for (int i = 0; i < graph.getInstances().size(); i++) {
            float[] point = new float[2];
            ProjectionInstance instance =
                    (ProjectionInstance) graph.getInstances().get(i);
            point[0] = instance.getX();
            point[1] = instance.getY();
            matrix.addRow(new DenseVector(point));
        }

        ArrayList<ArrayList<Integer>> clusters = null;
        String scalarName = null;

        if (clusteringType == KMEANS) {
            String inputValue = (String) JOptionPane.showInputDialog(null,
                    "Choose the number of clusters:", "Defining the Number of Clusters",
                    JOptionPane.QUESTION_MESSAGE, null, null,
                    (Object) Integer.toString((int) Math.sqrt(graph.getInstances().size())));

            if (inputValue == null) {
                return;
            }

            nrclusters = Integer.parseInt(inputValue);

            try {
                BKmeans km = new BKmeans(nrclusters);
                clusters = km.execute(new Euclidean(), matrix);
                scalarName = "KMeans-" + clusters.size();
            } catch (IOException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            }
//        } else {
//            Jdbscan2D dbscan = new Jdbscan2D();
//            clusters = dbscan.execute(new Euclidean(), matrix);
//            scalarName = "DBScan-" + clusters.size();
        }

        Scalar s = graph.addScalar(scalarName);
        for (int c = 0; c < clusters.size(); c++) {
            for (int v = 0; v < clusters.get(c).size(); v++) {
                graph.getInstances().get(clusters.get(c).get(v)).setScalarValue(s, c);
            }
        }

        viewer.getTopics().clear();
//        if (OpenDialog.checkCorpus(graph, parent)) {
        //for each cluster
        for (int c = 0; c < clusters.size(); c++) {
            ArrayList<AbstractInstance> vertex = new ArrayList<AbstractInstance>();

            for (int v = 0; v < clusters.get(c).size(); v++) {
                vertex.add(graph.getInstances().get(clusters.get(c).get(v)));
            }

//                Topic topic = TopicFactory.getInstance(graph,
//                        graph.getTopicData(),
//                        graph.getCorpus(), vertex);

            Topic topic = new CovarianceTopic(graph, corpus);
            topic.createTopic(vertex);

//                System.out.println("Added Topic: " + topic + ", rect: " + topic.getRectangle());
            viewer.addTopic(topic);
        }
//        }

//        this.gv.updateScalars(graph.getScalarByName(PExConstants.TOPICS));
//        this.gv.updateImage();
        Topic.setShowTopics(true);
        viewer.getModel().notifyObservers();
    }

    public int getNrClusters() {
        return nrclusters;
    }

    private TextGraphModelViewer viewer;
    private int clusteringType = KMEANS;
    private Corpus corpus;
    private int nrclusters;
    
}
