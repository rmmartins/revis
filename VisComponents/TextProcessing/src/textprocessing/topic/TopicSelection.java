/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package textprocessing.topic;

import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import vispipeline.visualization.model.AbstractInstance;
import vispipeline.visualization.view.selection.AbstractSelection;

/**
 *
 * @author Rafael
 */
public class TopicSelection extends AbstractSelection {

    public TopicSelection(TextGraphModelViewer viewer, Topic topic) {
        super(viewer);
        this.topic = topic;
    }

    @Override
    public void selected(ArrayList<AbstractInstance> selinst) {        
        TextGraphModelViewer v = (TextGraphModelViewer) viewer;
        if (v.getModel() != null) {
            v.getModel().setSelectedInstances(selinst);
            v.getTopics().clear();            
            Topic.setShowTopics(true);
            topic.createTopic(selinst);
            v.getTopics().add(topic);
            v.getModel().setChanged();
            v.getModel().notifyObservers();
        }
    }

    @Override
    public ImageIcon getIcon() {
        return new ImageIcon(getClass().getResource("comment.png"));
    }

    @Override
    public String toString() {
        return "Topic Selection";
    }

    private Topic topic;

}
