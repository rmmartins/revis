/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package textprocessing.corpus.zip;

import java.awt.Color;
import java.io.IOException;
import org.openide.util.lookup.ServiceProvider;
import textprocessing.corpus.CorpusComp;
import vispipeline.basics.annotations.VisComponent;
import vispipeline.basics.interfaces.AbstractComponent;
import vispipeline.basics.interfaces.AbstractParametersView;
import vispipeline.engine.Output;

/**
 *
 * @author Fernando Vieira Paulovich
 */
@VisComponent(hierarchy = "Text Processing",
name = "Zip File Corpus",
description = "Represents a set of text documents in a *.zip file.")
@ServiceProvider(service=AbstractComponent.class)
public class ZipCorpusComp extends CorpusComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {        
        if (url.trim().length() > 0) {
            corpus = new ZipCorpus(url, nrGrams);
        } else {            
            throw new IOException("A corpus file name must be provided.");
        }
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new ZipCorpusParamView(this);
        }

        return paramview;
    }

    private transient ZipCorpusParamView paramview;    
}
