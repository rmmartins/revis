/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package textprocessing.corpus;

import java.io.IOException;
import org.openide.util.lookup.ServiceProvider;
import vispipeline.basics.annotations.Return;
import vispipeline.basics.annotations.VisComponent;
import vispipeline.basics.interfaces.AbstractComponent;
import vispipeline.basics.interfaces.AbstractParametersView;
import vispipeline.basics.annotations.Parameter;

/**
 *
 * @author rmmartins, Fernando Vieira Paulovich
 */
@VisComponent(hierarchy = "Text Processing",
name = "Corpus",
description = "Represents a set of text documents.",
type = VisComponent.BASE)
@ServiceProvider(service=AbstractComponent.class)
public class CorpusComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        // implement for your own Corpus
    }

    @Return(name = "Corpus") public Corpus output() {
        return corpus;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        // implement for your own Corpus
        return null;
    }

    @Override
    public void reset() {
        corpus = null;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the nrGrams
     */
    public int getNumberGrams() {
        return nrGrams;
    }

    /**
     * @param nrGrams the nrGrams to set
     */
    public void setNumberGrams(int nrGrams) {
        this.nrGrams = nrGrams;
    }

    public static final long serialVersionUID = 1L;
    
    // Parameters
    @Parameter public String url = "";

    protected int nrGrams = 1;
    protected transient Corpus corpus;
    
}
