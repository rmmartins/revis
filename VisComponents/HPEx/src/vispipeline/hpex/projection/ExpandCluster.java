/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.projection;

import vispipeline.hpex.util.Cluster;
import vispipeline.hpex.projection.lsp.LSP;
import vispipeline.hpex.topic.TopicExtractor;
import vispipeline.hpex.util.BKMeans;
import vispipeline.hpex.util.HPExConstants;
import vispipeline.hpex.util.Instance;
import vispipeline.hpex.util.Util;
import vispipeline.hpex.view.draw.ClusterElement;
import vispipeline.hpex.view.draw.ClusterHierarchy;
import vispipeline.hpex.view.draw.ClusterHierarchy.Scalar;
import vispipeline.hpex.view.draw.DataInstanceElement;
import vispipeline.hpex.view.draw.GLRenderer;
import vispipeline.hpex.view.draw.GraphicalElement;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class expand a cluster projectoin its children clusters.
 * 
 * @author Fernando Vieira Paulovich
 */
public class ExpandCluster {

    public void expand(ClusterHierarchy hierarchy, ClusterElement element) throws IOException {
        Parameters param = hierarchy.getParameters();
        Cluster cluster = element.getCluster();
        element.removeChildren();

        /* IF (cluster.chsize > min) 
         *   new_clusters = create \sqrt{element.children.chsize} clusters
         *   create a ClusterElement for each cluster and attach them to the element
         * ELSE
         *   create a DataInstanceElement for each vector and attach them to the element
         */
        long start = System.currentTimeMillis();

        if (cluster.getInstancesCount() > param.minElements) {
            int nrcluster = (int) Math.sqrt(cluster.getInstancesCount());
            BKMeans bkmeans = new BKMeans(nrcluster);
            ArrayList<Cluster> new_clusters = bkmeans.execute(param.diss, cluster);

            for (Cluster c : new_clusters) {
                ClusterElement ce = new ClusterElement(c);
                element.addChild(ce);
            }
        } else {
            int size = cluster.getInstancesCount();
            for (int i = 0; i < size; i++) {
                Instance ins = cluster.getInstance(i);

                if (ins instanceof DataInstanceElement) {
//                    ((DataInstanceElement) ins).setTitle(
//                            cluster.getInstance(i).getVector().getTitle());

                    element.addChild((DataInstanceElement) ins);
                } else {
                    Logger.getLogger(ExpandCluster.class.getName()).log(Level.SEVERE,
                            "The instance must be a DataInstance, but for some " +
                            "reason it is not true.");
                }
            }
        }

        long finish = System.currentTimeMillis();
        String message = "Expading: " + cluster.getInstancesCount() + " children. " +
                "Time clustering: " + (finish - start) / 1000.0f + "s, ";

        //Project
        start = System.currentTimeMillis();
        this.project(element, param);
        finish = System.currentTimeMillis();
        message += "Time to project: " + (finish - start) / 1000.0f + "s, ";

        Util.checkNaN(element, "ExpandCluster: After projection");

        //Create Scalars
        this.createScalars(hierarchy, element);

        //Extract the topics
        start = System.currentTimeMillis();
        this.createTopics(hierarchy, element);
        finish = System.currentTimeMillis();
        message += "Time to extract the topics: " + (finish - start) / 1000.0f + "s";

//        Logger.getLogger(ExpandCluster.class.getName()).log(Level.INFO, message);
    }

    private void createTopics(ClusterHierarchy hierarchy, ClusterElement element) throws IOException {
        ArrayList<GraphicalElement> children = element.getChildren();
        int size = children.size();

        for (int i = 0; i < size; i++) {
            GraphicalElement aux = children.get(i);

            if (aux instanceof ClusterElement) {
                TopicExtractor te = new TopicExtractor();
                ArrayList<String> topic = te.extractTopic(hierarchy, (ClusterElement) aux);
                ((ClusterElement) aux).setTopic(topic);
            }
        }
    }

    private void createScalars(ClusterHierarchy hierarchy, ClusterElement element) throws IOException {
        //getting the children of the cluster
        ArrayList<GraphicalElement> children = element.getChildren();
        int chsize = children.size();

        //creating scalars
        ClusterHierarchy.Scalar defaultScalar = hierarchy.addScalar(HPExConstants.DOTS);
        ClusterHierarchy.Scalar classScalar = hierarchy.addScalar(HPExConstants.CLASS);
        ClusterHierarchy.Scalar densityScalar = hierarchy.addScalar(HPExConstants.DENSITY);

        float total = element.getCluster().getInstancesCount();

        for (int i = 0; i < chsize; i++) {
            GraphicalElement aux = children.get(i);

            if (aux instanceof ClusterElement) {
                ClusterElement cge = (ClusterElement) aux;
                float value = cge.getCluster().getInstancesCount();
                cge.setScalarValue(densityScalar, value / total);
                cge.setScalarValue(classScalar, this.getClass(cge));
            } else {
                aux.setScalarValue(densityScalar, 0.0f);
                aux.setScalarValue(classScalar, aux.getVector().getKlass());
            }

            aux.setScalarValue(defaultScalar, 0.5f);
        }

        //creating the scalars resulted from queries        
        for (int i = 0; i < chsize; i++) {
            GraphicalElement el = children.get(i);

            if (el instanceof ClusterElement) {
                Cluster cluster = ((ClusterElement) el).getCluster();
                int size = cluster.getInstancesCount();

                for (Scalar scalar : hierarchy.getScalars()) {
                    if (!scalar.name.equals(HPExConstants.DOTS) &&
                            !scalar.name.equals(HPExConstants.CLASS) &&
                            !scalar.name.equals(HPExConstants.DENSITY)) {

                        float freq = 0;

                        for (int j = 0; j < size; j++) {
                            Instance ins = cluster.getInstance(j);

                            if (ins instanceof DataInstanceElement) {
                                DataInstanceElement die = (DataInstanceElement) ins;
                                freq += die.getScalarValue(scalar);
                            }
                        }

                        el.setScalarValue(scalar, (freq / size));
                    }
                }
            }
        }
    }

    private void project(ClusterElement element, Parameters param) throws IOException {
        //getting the children of the cluster
        ArrayList<GraphicalElement> children = element.getChildren();
        int size = children.size();

        ////////////////////////////////////////////////////////////////////
        //Defining the clusters' size proportional to the number of documents
        float total = element.getCluster().getInstancesCount();
        float sumff = 0.0f;
        for (int i = 0; i < size; i++) {
            GraphicalElement aux = children.get(i);
            if (aux instanceof ClusterElement) {
                ClusterElement cge = (ClusterElement) aux;
                float f = cge.getCluster().getInstancesCount() / total;
                sumff += f * f;
            }
        }

        float R = element.getSize();
        float k = (float) Math.sqrt((R * R * param.parentArea) / (sumff));

        for (int i = 0; i < size; i++) {
            GraphicalElement aux = children.get(i);

            if (aux instanceof ClusterElement) {
                ClusterElement cge = (ClusterElement) aux;
                float clusterSize = k * cge.getCluster().getInstancesCount() / total;

                if (clusterSize < GLRenderer.WINDOW_SIZE / 200) {
                    cge.setSize(GLRenderer.WINDOW_SIZE / 200);
                } else {
                    cge.setSize(clusterSize);
                }
            }
        }

        if (size > 1) {
            ////////////////////////////////////////////////////////////////
            //projecting the clusters
            LSP lsp = new LSP(param);
//            lsp.project(param.diss, element);

            ////////////////////////////////////////////////////////////////
            //calculating the centroid of the projection
            float centroidx = 0.0f, centroidy = 0.0f;
            for (int i = 0; i < size; i++) {
                centroidx += children.get(i).getX();
                centroidy += children.get(i).getY();
            }

            centroidx /= size;
            centroidy /= size;

            //moving the centroid to the origin
            for (int i = 0; i < size; i++) {
                GraphicalElement ge = children.get(i);
                ge.setX(ge.getX() - centroidx);
                ge.setY(ge.getY() - centroidy);
            }

            //normalizing the projection to fit inside
            //the radius defined by the cluster
            float max = Float.MIN_VALUE;
            for (int i = 0; i < size; i++) {
                GraphicalElement aux = children.get(i);
                float x = aux.getX();
                float y = aux.getY();
                float norm = (float) Math.sqrt(x * x + y * y);

                if (norm > max) {
                    max = norm;
                }
            }

            float clusterSize = element.getSize();
            for (int i = 0; i < size; i++) {
                GraphicalElement aux = children.get(i);
                aux.setX(clusterSize * (aux.getX() / max));
                aux.setY(clusterSize * (aux.getY() / max));
            }

            //moving the points so the centroid of the children match the
            //cluster centroid
            for (int i = 0; i < size; i++) {
                GraphicalElement ge = children.get(i);
                ge.setX(ge.getX() + element.getX());
                ge.setY(ge.getY() + element.getY());
            }

            Util.checkNaN(element, "ExpandCluster: After normalizing");

            ///////////////////////////////////////////////////////////////
            //spreading the clusters to avoid overlap
            if (children.get(0) instanceof ClusterElement) {
                Spreader sp = new Spreader(element);
                sp.execute(param.nrIterations * 5, true);
            } else {

                //defining the data instance size based on the size 
                //of the clusters
                float radius = element.getSize();
                float nr = element.getCluster().getInstancesCount();
                float inssize = (float) Math.sqrt(Math.PI * radius * radius / nr);

                for (GraphicalElement ge : element.getChildren()) {
                    if (ge instanceof DataInstanceElement) {
                        ge.setSize(inssize / 2);
                    }
                }

                Spreader sp = new Spreader(element);
                sp.execute(param.nrIterations * 5, false);
            }

            Util.checkNaN(element, "ExpandCluster: After spreading");

        } else {
            GraphicalElement ge = children.get(0);
            ge.setX(element.getX());
            ge.setY(element.getY());
            ge.setSize(element.getSize());
        }
    }

    /**
     * This methos returns the class identifier for a cluster. If more than 
     * 70% of the data instances belonging to a cluster belongs to the same
     * class, it is the cluster class, otherwise it is assigned a common 
     * value for the non-well classified clusters.
     * 
     * @param cge The cluster to extract the class.
     * @return The cluster's class.
     */
    private float getClass(ClusterElement cge) {
        float[] nrEl = new float[5000];
        Arrays.fill(nrEl, 0);

        for (int i = 0; i < cge.getCluster().getInstancesCount(); i++) {
            int index = (int) cge.getCluster().getInstance(i).getVector().getKlass();
            if (index < nrEl.length) {
                nrEl[index]++;
            }
        }

        float max = 0;
        int index = 0;
        for (int i = 0; i < nrEl.length; i++) {
            if (max < nrEl[i]) {
                max = nrEl[i];
                index = i;
            }
        }

        return (max / cge.getCluster().getInstancesCount() > 0.7f) ? index : 0.5f;
    }

}
