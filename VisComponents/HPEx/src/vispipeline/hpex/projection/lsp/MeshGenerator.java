/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.projection.lsp;

import vispipeline.hpex.util.KNN;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import vispipeline.distance.DistanceMatrix;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class MeshGenerator {

    public MeshGenerator(int nrneighbors) {
        this.nrneighbors = nrneighbors;
    }

    public ArrayList<ArrayList<Integer>> execute(DistanceMatrix dmat) {
        //execute the KNN algorithm
        KNN knn = new KNN(this.nrneighbors);
        ArrayList<ArrayList<Integer>> neighbors = knn.execute(dmat);

        //ensuring that the mesh is entirely connected
        HashSet<Integer> visited = new HashSet<Integer>();
        HashSet<Integer> tovisit = new HashSet<Integer>();
        HashSet<Integer> notvisited = new HashSet<Integer>();

        //init the sets
        tovisit.add(0);
        for (int i = 1; i < neighbors.size(); i++) {
            notvisited.add(i);
        }

        while (notvisited.size() > 0) {
            if (tovisit.size() > 0) {
                int next = tovisit.iterator().next();
                visited.add(next);
                tovisit.remove(new Integer(next));
                notvisited.remove(new Integer(next));

                for (int i = 0; i < neighbors.get(next).size(); i++) {
                    if (!visited.contains((int) neighbors.get(next).get(i))) {
                        tovisit.add((int) neighbors.get(next).get(i));
                    }
                }
            } else {
//                //get the latest element already visited
//                Iterator<Integer> visited_it = visited.iterator();
//                int last_visited = -1;
//                while (visited_it.hasNext()) {
//                    last_visited = visited_it.next();
//                }
//
//                //getting the nearest element from the latest one between 
//                //the non visited elements
//                Iterator<Integer> notvisited_it = notvisited.iterator();
//                int closest = -1;
//                float min = Float.MAX_VALUE;
//
//                while (notvisited_it.hasNext()) {
//                    int aux = notvisited_it.next();
//                    float distance = dmat.getDistance(aux, last_visited);
//
//                    if (min > distance) {
//                        min = distance;
//                        closest = aux;
//                    }
//                }
//
//                notvisited.remove(new Integer(closest));
//                tovisit.add(closest);
//
//                neighbors.get(last_visited).add(closest);
//                neighbors.get(closest).add(last_visited);

                int next = notvisited.iterator().next();
                notvisited.remove(new Integer(next));
                tovisit.add(next);

                Iterator<Integer> visited_it = visited.iterator();

                int closest = 0;
                float min = Float.MAX_VALUE;

                while (visited_it.hasNext()) {
                    int aux = visited_it.next();
                    float distance = dmat.getDistance(aux, next);

                    if (min > distance) {
                        min = distance;
                        closest = aux;
                    }
                }

                neighbors.get(next).add(closest);
                neighbors.get(closest).add(next);
            }
        }

        return neighbors;
    }

    private int nrneighbors;
}
