/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.projection.lsp;

import vispipeline.hpex.util.Cluster;
import vispipeline.hpex.projection.*;
import vispipeline.hpex.util.BKMeans;
import vispipeline.hpex.util.Instance;
import vispipeline.hpex.util.Util;
import vispipeline.hpex.view.draw.ClusterElement;
import vispipeline.hpex.view.draw.GraphicalElement;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import lspsolver.Solver;
import vispipeline.distance.DistanceMatrix;
import vispipeline.distance.dissimilarity.AbstractDissimilarity;
import vispipeline.matrix.sparse.SparseMatrix;

/**
 * Project the children of a given cluster using the Least-Square Projection 
 * (LSP) algorithm.
 * 
 * @author Fernando Vieira Paulovich
 */
public class LSP {

    public LSP(Parameters param) {
        this.param = param;
    }

    /**
     * Project the children of a cluster to the plane.
     * @param diss The dissimilarity measure.
     * @param cluster The cluster which the children will be projected.
     * @throws java.io.IOException
     */
    public void project(AbstractDissimilarity diss, ClusterElement cluster) throws IOException {
        //create the anchors for the projection
        this.createAnchors(diss, cluster);

        //get the number of element on the cluster
        int nrel = cluster.getChildren().size();

        ////////////////////////////////////////////
        //choose the control points

        //define the number of control points
        int nrcp = (nrel < 5) ? nrel : 5;
        if (nrel > 15) {
            nrcp = (nrel / 10 > 10) ? nrel / 10 : 10;
        }

        //choose the control points
        Cluster c_aux = new Cluster();
        for (GraphicalElement ge : cluster.getChildren()) {
            c_aux.addInstance(new Cluster.InstanceStub(ge.getVector()));
        }

        BKMeans bkmeans = new BKMeans(nrcp);
        bkmeans.execute(diss, c_aux);
        ArrayList<Instance> centroids = bkmeans.getCentroids();
        nrcp = centroids.size();

        SparseMatrix cpmatrix = new SparseMatrix();
        for (Instance ci : centroids) {
            cpmatrix.addRow(ci.getVector());
        }

        ////////////////////////////////////////////
        //project the control points
        DistanceMatrix dmat = new DistanceMatrix(cpmatrix, diss);
        Fastmap2D fastmap = new Fastmap2D();
        float[][] projection = fastmap.project(dmat);

        Util.checkNaN(projection, "LSP: After Fastmap2D");

        if (projection.length > 3) {
            ForceScheme force = new ForceScheme(param.fracDelta);
            force.setAnchors(anchdmat, anchproj);
            force.execute(projection, param.nrIterations, dmat);
        }

        Util.checkNaN(projection, "LSP: After Force");

        ////////////////////////////////////////////
        //adding the anchors as control points
        int nranchors = 0;
        if (this.anchors != null) {
            nranchors = this.anchors.size();

            //adding the sparse vectors for the anchors
            for (GraphicalElement ge : anchors) {
                cpmatrix.addRow(ge.getVector());
            }
        }

        ////////////////////////////////////////////
        //create the neighbor graph        

        //defining the number of neighbors
        if (nrel <= 10) {
            param.nrNeighbors = (nrel >= 3) ? 3 : nrel;
        } else if (nrel < 30) {
            param.nrNeighbors = 5;
        }

        //adding the sparse vectors which really represent the elements. The
        //first nrcp+nranchors lines are the control points. They are not real 
        //elements.
        for (GraphicalElement ge : cluster.getChildren()) {
            cpmatrix.addRow(ge.getVector());
        }

        dmat = new DistanceMatrix(cpmatrix, diss);
        MeshGenerator gen = new MeshGenerator(param.nrNeighbors);
        ArrayList<ArrayList<Integer>> mesh = gen.execute(dmat);

        ////////////////////////////////////////////
        //create  and solve the linear system
        int nColumns = nrcp + nranchors + nrel;
        int nRows = nColumns + nrcp + nranchors;

        Solver solver = new Solver(nRows, nColumns);

        //creating matrix A
        for (int i = 0; i < mesh.size(); i++) {
            solver.addToA(i, i, 1.0f);

            for (int j = 0; j < mesh.get(i).size(); j++) {
                solver.addToA(i, (int) mesh.get(i).get(j), (-(1.0f / mesh.get(i).size())));
            }
        }

        for (int i = 0; i < nrcp + nranchors; i++) {
            solver.addToA((nColumns + i), i, 20.0f);
        }

        //creating matrix B
        for (int i = 0; i < nrcp; i++) {
            solver.addToB((nColumns + i), 0, projection[i][0]);
            solver.addToB((nColumns + i), 1, projection[i][1]);
        }

        //adding the anchors to the matrix B
        for (int i = 0; i < nranchors; i++) {
            solver.addToB((nColumns + i + nrcp), 0, anchproj[i][0]);
            solver.addToB((nColumns + i + nrcp), 1, anchproj[i][1]);
        }

        //solve the system        
        float[] result = solver.solve();

        Util.checkNaN(result, "LSP: After Solver");

        ////////////////////////////////////////////
        //set the coordinates to the children of the cluster
        ArrayList<GraphicalElement> children = cluster.getChildren();

        for (int i = 2 * (nrcp + nranchors),  j = 0; i < result.length; i += 2, j++) {
            children.get(j).setX(result[i]);
            children.get(j).setY(result[i + 1]);
        }

        Util.checkNaN(cluster, "LSP: LSP result");
    }

    /**
     * getting the neighbors to be anchors.
     * @param diss
     * @param element
     * @throws java.io.IOException
     */
    private void createAnchors(AbstractDissimilarity diss, ClusterElement element) throws IOException {
        if (element.getParent() != null) {
            anchors = new ArrayList<GraphicalElement>();

            //create a delaunay triangulation with the brothers of the
            //element been projected and get as neighbors the elements
            //that shares a edge with this elements on the trialgulation
            ClusterElement parent = (ClusterElement) element.getParent();
            ArrayList<GraphicalElement> brothers = parent.getChildren();

            float[][] bcoords = new float[brothers.size()][];
            for (int i = 0; i < bcoords.length; i++) {
                bcoords[i] = new float[2];
                bcoords[i][0] = brothers.get(i).getX();
                bcoords[i][1] = brothers.get(i).getY();
            }

            HashSet<GraphicalElement> nei_aux = new HashSet<GraphicalElement>();

            if (bcoords.length > 2) {
                Delaunay qdelaunay = new Delaunay();
                int[][] tri = qdelaunay.execute(bcoords);

                int elid = brothers.indexOf(element);
                for (int i = 0; i < tri.length; i++) {
                    if (elid == tri[i][0]) {
                        nei_aux.add(brothers.get(tri[i][1]));
                    } else if (elid == tri[i][1]) {
                        nei_aux.add(brothers.get(tri[i][0]));
                    }
                }
            } else {
                for (GraphicalElement ge : brothers) {
                    nei_aux.add(ge);
                }
            }

            for (GraphicalElement ge : nei_aux) {
                anchors.add(ge);
            }

            //projecting the neighbors into the border of the cluster been
            //projected
            if (anchors.size() > 0) {
                anchproj = new float[anchors.size()][];

                for (int i = 0; i < anchproj.length; i++) {
                    anchproj[i] = new float[2];

                    float x1 = element.getX();
                    float y1 = element.getY();
                    float x2 = anchors.get(i).getX();
                    float y2 = anchors.get(i).getY();

                    float norm = (float) Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));

                    anchproj[i][0] = x1 + (element.getSize()) * ((x2 - x1) / norm);
                    anchproj[i][1] = y1 + (element.getSize()) * ((y2 - y1) / norm);
                }

                Util.checkNaN(anchproj, "LSP: After Anchor Projection");

                //creating the sparse matrix
                SparseMatrix spm = new SparseMatrix();

                //adding the anchors
                for (GraphicalElement ge : anchors) {
                    spm.addRow(ge.getVector());
                }

                //adding the data instances
                for (GraphicalElement ge : element.getChildren()) {
                    spm.addRow(ge.getVector());
                }

                anchdmat = new DistanceMatrix(spm, diss);
                normalize(anchproj);

                Util.checkNaN(anchproj, "LSP: After Normalize Anchor Projection");

            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.WARNING,
                        "The cluster element has parent but it was not possible " +
                        "to define anchors to create the projection. Number of " +
                        "brothers: " + brothers.size());
            }
        }
    }

    private void normalize(float[][] projection) {
        float maxx = projection[0][0];
        float minx = projection[0][0];
        float maxy = projection[0][1];
        float miny = projection[0][1];

        for (int i = 1; i < projection.length; i++) {
            float x = projection[i][0];
            float y = projection[i][1];

            if (x > maxx) {
                maxx = x;
            } else if (x < minx) {
                minx = x;
            }

            if (y > maxy) {
                maxy = y;
            } else if (y < miny) {
                miny = y;
            }
        }

        //setting the minimum X and Y equals to zero
        for (int i = 0; i < projection.length; i++) {
            projection[i][0] -= minx;
            projection[i][1] -= miny;
        }

        float max = maxx;
        if (maxy > maxx) {
            max = maxy;
        }

        for (int i = 0; i < projection.length; i++) {
            if ((max - minx) > EPSILON) {
                projection[i][0] = projection[i][0] / (max - minx);
            } else {
                projection[i][0] = 0.0f;
            }

            if ((max - miny) > EPSILON) {
                projection[i][1] = projection[i][1] / (max - miny);
            } else {
                projection[i][1] = 0.0f;
            }
        }
    }

    public static final float EPSILON = 0.00001f;
    private DistanceMatrix anchdmat;
    private float[][] anchproj;
    private ArrayList<GraphicalElement> anchors;
    private Parameters param;
}
