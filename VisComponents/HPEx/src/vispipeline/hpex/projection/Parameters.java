/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.projection;

import vispipeline.hpex.textpreprocess.stopwords.StopwordsList;
import vispipeline.hpex.textpreprocess.stopwords.StopwordsListManager;
import java.io.Serializable;
import textprocessing.corpus.Corpus;
import vispipeline.distance.dissimilarity.AbstractDissimilarity;
import vispipeline.distance.dissimilarity.CosineBased;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class Parameters implements Serializable {

    //corpus pre-processing parameters
    public int lunhLowerCut = 10;
    public int lunhUpperCut = -1;
//    public Preprocessor.MatrixType matrixType = Preprocessor.MatrixType.TF_IDF;
    public boolean stemming = true;
    public boolean stopwords = true;
    public StopwordsList stopwordsList = StopwordsListManager.ENGLISH;
    public transient Corpus corpus;
    //topics
    public StopwordsList stopwordsTopics = StopwordsListManager.ENGLISH;
    //projection parameters
    public int nrIterations = 100;
    public float fracDelta = 8.0f;
    public float parentArea = 0.5f;
    public int minElements = 3;
    public int nrNeighbors = 8;
    public AbstractDissimilarity diss = new CosineBased();
    //serial identification
    private static final long serialVersionUID = 27L;
}
