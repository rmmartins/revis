/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.projection;

import vispipeline.hpex.view.draw.ClusterElement;
import vispipeline.hpex.view.draw.GraphicalElement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import vispipeline.distance.DistanceMatrix;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class Fastmap2D {

    public void project(DistanceMatrix dmat, ClusterElement cluster) {
        float[][] projection = this.project(dmat);

        ArrayList<GraphicalElement> children = cluster.getChildren();
        for (int i = 0; i < children.size(); i++) {
            children.get(i).setX(projection[i][0]);
            children.get(i).setY(projection[i][1]);
        }
    }

    public float[][] project(DistanceMatrix dmat) {
        float[][] projection = new float[dmat.getElementCount()][];
        for (int i = 0; i < dmat.getElementCount(); i++) {
            projection[i] = new float[2];
        }

        try {
            dmat = (DistanceMatrix) dmat.clone();

            if (dmat.getElementCount() < 4) {
                this.doTheFastmapLessThan4Points(dmat, projection);
            } else {
                this.doTheFastmap(dmat, projection);
            }

            this.normalize(projection);

        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(Fastmap2D.class.getName()).log(Level.SEVERE, null, ex);
        }

        return projection;
    }

    private void doTheFastmapLessThan4Points(DistanceMatrix dmat, float[][] projection) {
        if (dmat.getElementCount() == 1) {
            projection[0][0] = 0.0f;
            projection[0][1] = 0.0f;
        } else if (dmat.getElementCount() == 2) {
            projection[0][0] = 0.0f;
            projection[0][1] = 0.0f;
            projection[1][0] = dmat.getDistance(0, 1);
            projection[1][1] = 0.0f;
        } else if (dmat.getElementCount() == 3) {
            projection[0][0] = 0.0f;
            projection[0][1] = 0.0f;
            projection[1][0] = dmat.getDistance(0, 1);
            projection[1][1] = 0.0f;
            projection[2][0] = dmat.getDistance(0, 1);
            projection[2][1] = dmat.getDistance(1, 2);
        }
    }

    private void doTheFastmap(DistanceMatrix dmat, float[][] projection) {
        //projecting the first dimension
        this.project(dmat, projection, 0);

        //updating the distances table with equation 4 of Faloutsos' 
        //paper (in detail below)
        this.updateDistances(dmat, projection, 0);

        //projecting the second dimension
        this.project(dmat, projection, 1);
    }

    private void project(DistanceMatrix dmat, float[][] projection, int dimension) {
        //choosen pivots for this recursion
        int[] lvchoosen = this.chooseDistantObjects(dmat);
        float lvdistance = dmat.getDistance(lvchoosen[0], lvchoosen[1]);

        //if the distance between the pivots is 0, then set 0 for each instance 
        //for this dimension
        if (lvdistance == 0) {
            //for each instance in the table
            for (int lvi = 0; lvi < dmat.getElementCount(); lvi++) {
                projection[lvi][dimension] = 0.0f;
            }
        } else { //if the distance is not equal to 0, then
            //instances iterator
            for (int lvi = 0; lvi < dmat.getElementCount(); lvi++) {
                float lvxi = (float) ((Math.pow(dmat.getDistance(lvchoosen[0], lvi), 2) +
                        Math.pow(dmat.getDistance(lvchoosen[0], lvchoosen[1]), 2) -
                        Math.pow(dmat.getDistance(lvi, lvchoosen[1]), 2)) /
                        (2 * dmat.getDistance(lvchoosen[0], lvchoosen[1])));

                projection[lvi][dimension] = lvxi;
            }
        }
    }

    private int[] chooseDistantObjects(DistanceMatrix dmat) {
        int[] choosen = new int[2];
        int x = 0, y = 0;
        for (int i = 1; i < dmat.getElementCount(); i++) {
            for (int j = 0; j < i; j++) {
                if (dmat.getDistance(x, y) < dmat.getDistance(i, j)) {
                    x = i;
                    y = j;
                }
            }
        }
        choosen[0] = x;
        choosen[1] = y;
        return choosen;
    }

    private void updateDistances(DistanceMatrix dmat, float[][] projection, int dimension) {
        //for each instance
        for (int lvinst = 0; lvinst < dmat.getElementCount(); lvinst++) {
            //for each another instance
            for (int lvinst2 = lvinst + 1; lvinst2 < dmat.getElementCount(); lvinst2++) {
                float instCoord = projection[lvinst][dimension];
                float inst2Coord = projection[lvinst2][dimension];
                float value = (float) (Math.sqrt(Math.abs(Math.pow(dmat.getDistance(lvinst, lvinst2), 2) -
                        Math.pow((instCoord - inst2Coord), 2))));

                dmat.setDistance(lvinst, lvinst2, value);
            }
        }
    }

    private void normalize(float[][] projection) {
        float maxx = projection[0][0];
        float minx = projection[0][0];
        float maxy = projection[0][1];
        float miny = projection[0][1];

        for (int i = 1; i < projection.length; i++) {
            float x = projection[i][0];
            float y = projection[i][1];

            if (x > maxx) {
                maxx = x;
            } else if (x < minx) {
                minx = x;
            }

            if (y > maxy) {
                maxy = y;
            } else if (y < miny) {
                miny = y;
            }
        }

        float max = maxx;
        if (maxy > maxx) {
            max = maxy;
        }

        for (int i = 0; i < projection.length; i++) {
            if (max > minx) {
                projection[i][0] = (projection[i][0] - minx) / (max - minx);
            } else {
                projection[i][0] = 0.0f;
            }

            if (max > miny) {
                projection[i][1] = (projection[i][1] - miny) / (max - miny);
            } else {
                projection[i][1] = 0.0f;
            }
        }
    }

}
