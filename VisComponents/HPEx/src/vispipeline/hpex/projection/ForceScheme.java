/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.projection;

import vispipeline.hpex.view.draw.ClusterElement;
import vispipeline.hpex.view.draw.GraphicalElement;
import java.util.ArrayList;
import vispipeline.distance.DistanceMatrix;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class ForceScheme {

    public ForceScheme(float fracDelta) {
        this.fracDelta = fracDelta;
    }

    public void execute(ClusterElement cluster, int nrIterations, DistanceMatrix dmat) {
        ArrayList<GraphicalElement> children = cluster.getChildren();
        int size = children.size();

        float[][] projection = new float[size][];
        for (int i = 0; i < size; i++) {
            projection[i] = new float[2];
            projection[i][0] = children.get(i).getX();
            projection[i][1] = children.get(i).getY();
        }

        this.execute(projection, nrIterations, dmat);

        for (int i = 0; i < size; i++) {
            children.get(i).setX(projection[i][0]);
            children.get(i).setY(projection[i][1]);
        }
    }

    public void execute(float[][] projection, int nrIterations, DistanceMatrix dmat) {
        this.createIndex(projection.length);

        for (int i = 0; i < nrIterations; i++) {
            this.iteration(projection, dmat);
        }
    }

    /**
     * Sets the anchors to the projection. 
     * @param anchdmat The distance matrix between the anchors and the real data
     * instances. The first N elements of this matrix are the anchors, were N
     * is the number of anchors.
     * @param anchproj The anchors projection on the plane.
     */
    public void setAnchors(DistanceMatrix anchdmat, float[][] anchproj) {
        if (anchproj != null && anchproj.length > 0) {
            this.normalize(anchproj);
            this.anchdmat = anchdmat;
            this.anchproj = anchproj;
        }
    }

    private void createIndex(int nrelements) {
        ArrayList<Integer> index_aux = new ArrayList<Integer>();
        for (int i = 0; i < nrelements; i++) {
            index_aux.add(i);
        }

        this.index = new int[nrelements];

        for (int i = 0,  j = 0; j < this.index.length; i++, j++) {
            int ind = (int) Math.pow(2, i);

            if (ind >= index_aux.size()) {
                ind = 0;
            }

            this.index[j] = index_aux.get(ind);
            index_aux.remove(ind);
        }
    }

    private void iteration(float[][] projection, DistanceMatrix dmat) {
        this.pushToAnchors(projection, dmat);

        //iteration between the data instances
        for (int i = 0; i < index.length; i++) {
            int ins1 = index[i];

            if (i == index.length / 2) {
                this.pushToAnchors(projection, dmat);
            }

            for (int j = 0; j < index.length; j++) {
                int ins2 = index[j];

                if (ins1 == ins2) {
                    continue;
                }

                //distance between projected instances
                float dr2 = this.distR2(projection[ins1], projection[ins2]);

                //the multi-dimensional distances normalized
                float normdrn = 0.0f;
                if ((dmat.getMaxDistance() - dmat.getMinDistance()) > EPSILON) {
                    normdrn = (dmat.getDistance(ins1, ins2) - dmat.getMinDistance()) /
                            (dmat.getMaxDistance() - dmat.getMinDistance());
                }

                //calculating the distance to move the point
                float delta = (dr2 - normdrn) / fracDelta;

                //moving ins2 -> ins1
                projection[ins2][0] = projection[ins2][0] + delta *
                        ((projection[ins1][0] - projection[ins2][0]) / dr2);
                projection[ins2][1] = projection[ins2][1] + delta *
                        ((projection[ins1][1] - projection[ins2][1]) / dr2);


            }
        }
    }

    private void pushToAnchors(float[][] projection, DistanceMatrix dmat) {
        //iteration between the anchors and the data instances
        if (this.anchdmat != null) {
            int nranchors = this.anchproj.length;

            for (int i = 0; i < nranchors; i++) {
                for (int j = 0; j < index.length; j++) {
                    int ins2 = index[j];

                    //distance between projected instances
                    float dr2 = this.distR2(this.anchproj[i], projection[ins2]);

                    //the multi-dimensional distances normalized
                    float normdrn = 0.0f;
                    if ((dmat.getMaxDistance() - dmat.getMinDistance()) > EPSILON) {
                        normdrn = (anchdmat.getDistance(i, ins2 + nranchors) - dmat.getMinDistance()) /
                                (dmat.getMaxDistance() - dmat.getMinDistance());
                    }

                    //calculating the distance to move the point
                    float delta = (normdrn - dr2) / (fracDelta * 0.5f);

                    //moving ins2 -> ins1
                    projection[ins2][0] = projection[ins2][0] + delta *
                            ((projection[ins2][0] - anchproj[i][0]) / dr2);
                    projection[ins2][1] = projection[ins2][1] + delta *
                            ((projection[ins2][1] - anchproj[i][1]) / dr2);
                }
            }
        }

    }

    private float distR2(float[] pointa, float[] pointb) {
        float x1x2 = (pointa[0] - pointb[0]);
        float y1y2 = (pointa[1] - pointb[1]);
        float dist = (float) Math.sqrt(x1x2 * x1x2 + y1y2 * y1y2);

        if (dist > EPSILON) {
            return dist;
        } else {
            return EPSILON;
        }
    }

    private void normalize(float[][] projection) {
        float maxx = projection[0][0];
        float minx = projection[0][0];
        float maxy = projection[0][1];
        float miny = projection[0][1];

        for (int i = 1; i < projection.length; i++) {
            float x = projection[i][0];
            float y = projection[i][1];

            if (x > maxx) {
                maxx = x;
            } else if (x < minx) {
                minx = x;
            }

            if (y > maxy) {
                maxy = y;
            } else if (y < miny) {
                miny = y;
            }
        }

        float max = maxx;
        if (maxy > maxx) {
            max = maxy;
        }

        for (int i = 0; i < projection.length; i++) {
            if ((max - minx) > EPSILON) {
                projection[i][0] = (projection[i][0] - minx) / (max - minx);
            } else {
                projection[i][0] = 0.0f;
            }

            if ((max - miny) > EPSILON) {
                projection[i][1] = (projection[i][1] - miny) / (max - miny);
            } else {
                projection[i][1] = 0.0f;
            }
        }
    }

    private float[][] anchproj;
    private DistanceMatrix anchdmat;
    private int[] index;
    private float fracDelta;
    public static final float EPSILON = 0.0000001f;
}
