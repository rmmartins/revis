/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.projection;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class Delaunay {

    public int[][] execute(float[][] points) {
        Triangulation tri = new Triangulation(points);
        this.triangulate(tri);

        int[][] edges = new int[tri.nEdges][];

        for (int i = 0; i < tri.nEdges; i++) {
            edges[i] = new int[2];
            edges[i][0] = tri.edges[i].s;
            edges[i][1] = tri.edges[i].t;
        }

        return edges;
    }

    public static void perturb(float[][] points) {
        for (int i = 0; i < points.length; i++) {
            for (int j = 0; j < points.length; j++) {
                if (i == j) {
                    continue;
                }

                //coincident points
                if (points[i][0] - points[j][0] < EPSILON &&
                        points[i][1] - points[j][1] < EPSILON) {
                    points[j][0] = points[j][0] + ((float) Math.random() - 0.5f) / 100.0f;
                    points[j][1] = points[j][1] + ((float) Math.random() - 0.5f) / 100.0f;
                }
            }
        }
    }

    private void triangulate(Triangulation tri) {
        // Find closest neighbours and add edges to triangulation. 
        int[] neighbours = findClosestNeighbours(tri.points);

        // Create seed edges and add it to the triangulation. 
        tri.addEdge(neighbours[0], neighbours[1], Triangulation.Undefined,
                Triangulation.Undefined);

        int nFaces = 0;
        int currentEdge = 0;
        while (currentEdge < tri.nEdges) {
            if (tri.edges[currentEdge].l == Triangulation.Undefined) {
                completeFacet(currentEdge, tri, nFaces);
            }
            if (tri.edges[currentEdge].r == Triangulation.Undefined) {
                completeFacet(currentEdge, tri, nFaces);
            }
            currentEdge++;
        }
    }

    // Find the two closest points.  
    private int[] findClosestNeighbours(RealPoint p[]) {
        int[] neighbors = new int[2];
        neighbors[0] = 0;
        neighbors[1] = 0;

        float min = Float.MAX_VALUE;
        for (int i = 0; i < p.length - 1; i++) {
            for (int j = i + 1; j < p.length; j++) {
                float d = p[i].distanceSq(p[j]);
                if (d < min) {
                    neighbors[0] = i;
                    neighbors[1] = j;
                    min = d;
                }
            }
        }

        return neighbors;
    }

    /* 
     * Complete a facet by looking for the circle free points to the left
     * of the edges "e_i".  Add the facet to the triangulation.
     *
     * This function is a bit long and may be better split.
     */
    private void completeFacet(int eI, Triangulation tri, int nFaces) {
        // Cache s and t. 
        int s, t;
        if (tri.edges[eI].l == Triangulation.Undefined) {
            s = tri.edges[eI].s;
            t = tri.edges[eI].t;
        } else if (tri.edges[eI].r == Triangulation.Undefined) {
            s = tri.edges[eI].t;
            t = tri.edges[eI].s;
        } else {
            return;
        }

        // Find a points on left of edges.
        int u;
        for (u = 0; u < tri.points.length; u++) {
            if (u == s || u == t) {
                continue;
            }

            if (Vector.crossProduct(tri.points[s], tri.points[t], tri.points[u]) > 0.0) {
                break;
            }
        }

        // Find best points on left of edges. 
        int bP = u;
        if (bP < tri.points.length) {
            Circle bC = new Circle();
            bC.circumCircle(tri.points[s], tri.points[t], tri.points[bP]);

            for (u = bP + 1; u < tri.points.length; u++) {
                if (u == s || u == t) {
                    continue;
                }

                float cP = Vector.crossProduct(tri.points[s], tri.points[t], tri.points[u]);

                if (cP > 0.0) {
                    if (bC.inside(tri.points[u])) {
                        bP = u;
                        bC.circumCircle(tri.points[s], tri.points[t], tri.points[u]);
                    }
                }
            }
        }

        // Add new triangle or update edges info if s-t is on hull. 
        if (bP < tri.points.length) {
            // Update face information of edges being completed. 
            tri.updateLeftFace(eI, s, t, nFaces);
            nFaces++;

            // Add new edges or update face info of old edges. 
            eI = tri.findEdge(bP, s);
            if (eI == Triangulation.Undefined) {
                // New edges.
                eI = tri.addEdge(bP, s, nFaces, Triangulation.Undefined);
            } else {
                // Old edges. 
                tri.updateLeftFace(eI, bP, s, nFaces);
            }

            // Add new edges or update face info of old edges. 
            eI = tri.findEdge(t, bP);
            if (eI == Triangulation.Undefined) {
                // New edges. 
                eI = tri.addEdge(t, bP, nFaces, Triangulation.Undefined);
            } else {
                // Old edges.  
                tri.updateLeftFace(eI, t, bP, nFaces);
            }
        } else {
            tri.updateLeftFace(eI, s, t, Triangulation.Universe);
        }
    }

    private static final float EPSILON = 0.00001f;
}
/*
 * Point class.  RealPoint to avoid clash with java.awt.Point.
 */

class RealPoint {

    public RealPoint() {
        x = y = 0.0f;
    }

    public RealPoint(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public void set(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public float distance(RealPoint p) {
        float dx, dy;
        dx = p.x - x;
        dy = p.y - y;
        return (float) Math.sqrt((double) (dx * dx + dy * dy));
    }

    public float distanceSq(RealPoint p) {
        float dx, dy;
        dx = p.x - x;
        dy = p.y - y;
        return (float) (dx * dx + dy * dy);
    }

    public float x,  y;
}

/* 
 * Edge class. Edges have two vertices, s and t, and two faces,
 * l (left) and r (right). The triangulation representation and
 * the Delaunay triangulation algorithms require edges.
 */
class Edge {

    public Edge() {
        s = t = 0;
    }

    public Edge(int s, int t) {
        this.s = s;
        this.t = t;
    }

    public int s,  t;
    public int l,  r;
}


/*
 * Vector class.  A few elementary vector operations.
 */
class Vector {

    public Vector() {
        u = v = 0.0f;
    }

    public Vector(RealPoint p1, RealPoint p2) {
        u = p2.x - p1.x;
        v = p2.y - p1.y;
    }

    public Vector(float u, float v) {
        this.u = u;
        this.v = v;
    }

    public float dotProduct(Vector v) {
        return u * v.u + this.v * v.v;
    }

    public static float dotProduct(RealPoint p1, RealPoint p2, RealPoint p3) {
        float u1, v1, u2, v2;

        u1 = p2.x - p1.x;
        v1 = p2.y - p1.y;
        u2 = p3.x - p1.x;
        v2 = p3.y - p1.y;

        return u1 * u2 + v1 * v2;
    }

    public float crossProduct(Vector v) {
        return u * v.v - this.v * v.u;
    }

    public static float crossProduct(RealPoint p1, RealPoint p2, RealPoint p3) {
        float u1, v1, u2, v2;

        u1 = p2.x - p1.x;
        v1 = p2.y - p1.y;
        u2 = p3.x - p1.x;
        v2 = p3.y - p1.y;

        return u1 * v2 - v1 * u2;
    }

    public void setRealPoints(RealPoint p1, RealPoint p2) {
        u = p2.x - p1.x;
        v = p2.y - p1.y;
    }

    public float u,  v;
}

/*
 * Circle class. Circles are fundamental to computation of Delaunay 
 * triangulations.  In particular, an operation which computes a 
 * circle defined by three points is required.
 */
class Circle {

    /* 
     * Tests if a points lies inside the circle instance.
     */
    public boolean inside(RealPoint p) {
        if (c.distanceSq(p) < r * r) {
            return true;
        } else {
            return false;
        }
    }

    /* 
     * Compute the circle defined by three points (circumcircle). 
     */
    public void circumCircle(RealPoint p1, RealPoint p2, RealPoint p3) {
        float cp;

        cp = Vector.crossProduct(p1, p2, p3);
        if (cp != 0.0) {
            float p1Sq, p2Sq, p3Sq;
            float num, den;
            float cx, cy;

            p1Sq = p1.x * p1.x + p1.y * p1.y;
            p2Sq = p2.x * p2.x + p2.y * p2.y;
            p3Sq = p3.x * p3.x + p3.y * p3.y;
            num = p1Sq * (p2.y - p3.y) + p2Sq * (p3.y - p1.y) + p3Sq * (p1.y - p2.y);
            cx = num / (2.0f * cp);
            num = p1Sq * (p3.x - p2.x) + p2Sq * (p1.x - p3.x) + p3Sq * (p2.x - p1.x);
            cy = num / (2.0f * cp);

            c.set(cx, cy);
        }

        // Radius 
        r = c.distance(p1);
    }

    public RealPoint c = new RealPoint();
    public float r;
}

/*
 * Triangulation class.  A triangulation is represented as a set of
 * points and the edges which form the triangulation.
 */
class Triangulation {

    public Triangulation(float[][] points) {
        // Allocate points.         
        this.points = new RealPoint[points.length];
        for (int i = 0; i < points.length; i++) {
            this.points[i] = new RealPoint(points[i][0], points[i][1]);
        }

        // Allocate edges. 
        maxEdges = 3 * points.length - 6; // Max number of edges.
        edges = new Edge[maxEdges];
        for (int i = 0; i < maxEdges; i++) {
            edges[i] = new Edge();
        }
        nEdges = 0;
    }

    public void addTriangle(int s, int t, int u) {
        addEdge(s, t);
        addEdge(t, u);
        addEdge(u, s);
    }

    public int addEdge(int s, int t) {
        return addEdge(s, t, Undefined, Undefined);
    }

    /* 
     * Adds an edges to the triangulation. Store edges with lowest
     * vertex first (easier to debug and makes no other difference).
     */
    public int addEdge(int s, int t, int l, int r) {
        int e;

        // Add edges if not already in the triangulation. 
        e = findEdge(s, t);
        if (e == Undefined) {
            if (s < t) {
                edges[nEdges].s = s;
                edges[nEdges].t = t;
                edges[nEdges].l = l;
                edges[nEdges].r = r;
                return nEdges++;
            } else {
                edges[nEdges].s = t;
                edges[nEdges].t = s;
                edges[nEdges].l = r;
                edges[nEdges].r = l;
                return nEdges++;
            }
        } else {
            return Undefined;
        }
    }

    public int findEdge(int s, int t) {
        boolean edgeExists = false;
        int i;

        for (i = 0; i < nEdges; i++) {
            if (edges[i].s == s && edges[i].t == t ||
                    edges[i].s == t && edges[i].t == s) {
                edgeExists = true;
                break;
            }
        }

        if (edgeExists) {
            return i;
        } else {
            return Undefined;
        }
    }

    /* 
     * Update the left face of an edges. 
     */
    public void updateLeftFace(int eI, int s, int t, int f) {
        if (!((edges[eI].s == s && edges[eI].t == t) ||
                (edges[eI].s == t && edges[eI].t == s))) {
            throw new RuntimeException("updateLeftFace: adj. matrix and edges table mismatch");
        }
        if (edges[eI].s == s && edges[eI].l == Triangulation.Undefined) {
            edges[eI].l = f;
        } else if (edges[eI].t == s && edges[eI].r == Triangulation.Undefined) {
            edges[eI].r = f;
        } else {
            throw new RuntimeException("updateLeftFace: attempt to overwrite edges info");
        }
    }

    public static final int Undefined = -1;
    public static final int Universe = 0;
    public RealPoint points[];
    public int nEdges;
    public int maxEdges;
    public Edge edges[];
}
