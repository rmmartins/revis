/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.projection;

import vispipeline.hpex.topic.TopicExtractor;
import vispipeline.hpex.util.Cluster;
import vispipeline.hpex.util.HPExConstants;
import vispipeline.hpex.util.Instance;
import vispipeline.hpex.view.draw.ClusterElement;
import vispipeline.hpex.view.draw.ClusterHierarchy;
import vispipeline.hpex.view.draw.ClusterHierarchy.Scalar;
import vispipeline.hpex.view.draw.DataInstanceElement;
import vispipeline.hpex.view.draw.GraphicalElement;
import vispipeline.hpex.view.draw.listeners.JoinListener;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class JoinClusters {

    public void join(ClusterHierarchy hierarchy, ArrayList<GraphicalElement> selected) {
        int size = selected.size();

        if (size > 1) {
            //creating the new ce with all data instances of the 
            //selected clusters
            Cluster newcluster = this.createCluster(selected);

            //project the ce
            ClusterElement newce = this.projectCluster(selected, newcluster);

            //getting the common parent
            ClusterElement common = this.findCommonAncestor(selected);

            //area of new ce
            float newarea = 0.0f;

            //re-construct its ancestor that is a child of the common
            //parent
            for (int i = 0; i < selected.size(); i++) {
                GraphicalElement ge = selected.get(i);

                //find the ancestor that is a child of the common parent
                ClusterElement torebuild = null;

                GraphicalElement aux = ge;
                while (aux != null && common != aux.getParent()) {
                    aux = (GraphicalElement) aux.getParent();
                }

                torebuild = (ClusterElement) aux;

                if (torebuild != ge) {
                    //store the number of instances before removing it
                    int nrorigins = torebuild.getCluster().getInstancesCount();

                    //remove the data instances from the node to be rebuilt
                    if (ge instanceof DataInstanceElement) {
                        torebuild.getCluster().removeInstance((DataInstanceElement) ge);
                    } else {
                        ClusterElement ce = (ClusterElement) ge;
                        for (int j = 0; j < ce.getCluster().getInstancesCount(); j++) {
                            torebuild.getCluster().removeInstance(ce.getCluster().getInstance(j));
                        }
                    }

                    //rebuild the node size
                    int newnrins = torebuild.getCluster().getInstancesCount();
                    float origsize = torebuild.getSize();
                    float newsize = (float) Math.sqrt((newnrins * origsize * origsize) / nrorigins);
                    torebuild.setSize(newsize);

                    //accumlating the area not used
                    newarea += (Math.PI * origsize * origsize) - (Math.PI * newsize * newsize);

                    //rebuild the node topic
                    TopicExtractor te = new TopicExtractor();
                    ArrayList<String> topic = te.extractTopic(hierarchy, torebuild);
                    torebuild.setTopic(topic);
                } else {
                    float origsize = torebuild.getSize();
                    newarea += (Math.PI * origsize * origsize);
                    ge.removeFromParent();
                }
            }

            //define the size of the new ce
            float newsize = (float) Math.sqrt((newarea / Math.PI));
            newce.setSize(newsize);

            //extract the topic
            TopicExtractor te = new TopicExtractor();
            ArrayList<String> topic = te.extractTopic(hierarchy, newce);
            newce.setTopic(topic);

            //attaching the new ce
            common.addChild(newce);

            //creating the scalars
            this.createScalars(hierarchy, common);

            //change all children of the common parent since their position
            //can change
            for (GraphicalElement ge : common.getChildren()) {
                if (ge instanceof ClusterElement) {
                    ClusterElement ce = (ClusterElement) ge;

                    //change the projection state of this node to not project
                    //to force it to reproject its children, recreating the 
                    //hierarchy under this node
                    ce.setChildrenProjectionState(ClusterElement.ChildrenProjectionState.NOT_PROJECTED);

                    //remove all children from the node
                    for (int j = ce.getChildren().size() - 1; j >= 0; j--) {
                        ce.getChildren().get(j).removeFromParent();
                    }
                }

                //change the draw state of this node to draw
                ge.setDrawState(GraphicalElement.DrawState.DRAWN);
            }

            //spreading to avoid overlapping
            Spreader sp = new Spreader(common);
            sp.execute(500, false);

            //cleannig the selected clusters
            JoinListener.clear();
        }
    }

    private void createScalars(ClusterHierarchy hierarchy, ClusterElement common) {
        ClusterHierarchy.Scalar defaultScalar = hierarchy.addScalar(HPExConstants.DOTS);
        ClusterHierarchy.Scalar classScalar = hierarchy.addScalar(HPExConstants.CLASS);
        ClusterHierarchy.Scalar densityScalar = hierarchy.addScalar(HPExConstants.DENSITY);

        float total = common.getCluster().getInstancesCount();

        for (GraphicalElement ge : common.getChildren()) {
            if (ge instanceof ClusterElement) {
                ClusterElement ce = (ClusterElement) ge;
                float value = ce.getCluster().getInstancesCount();

                ce.setScalarValue(densityScalar, value / total);
                ce.setScalarValue(classScalar, this.getClass(ce));

                //creating the scalars resulted from queries 
                Cluster cluster = ((ClusterElement) ce).getCluster();
                int count = cluster.getInstancesCount();

                for (Scalar scalar : hierarchy.getScalars()) {
                    if (!scalar.name.equals(HPExConstants.DOTS) &&
                            !scalar.name.equals(HPExConstants.CLASS) &&
                            !scalar.name.equals(HPExConstants.DENSITY)) {

                        float freq = 0;

                        for (int j = 0; j < count; j++) {
                            Instance ins = cluster.getInstance(j);

                            if (ins instanceof DataInstanceElement) {
                                DataInstanceElement die = (DataInstanceElement) ins;
                                freq += die.getScalarValue(scalar);
                            }
                        }

                        ce.setScalarValue(scalar, (freq / count));
                    }
                }
            }

            ge.setScalarValue(defaultScalar, 0.5f);
        }
    }

    private ClusterElement findCommonAncestor(ArrayList<GraphicalElement> selected) {
        ClusterElement common = null;

        if (selected.size() > 1) {
            //getting the ancestors of the first selected graphical element
            ArrayList<ClusterElement> ancestors = new ArrayList<ClusterElement>();
            ClusterElement parent = (ClusterElement) selected.get(0).getParent();
            while (parent != null) {
                ancestors.add(parent);
                parent = (ClusterElement) parent.getParent();
            }

            int common_index = 0;

            for (int i = 1; i < selected.size(); i++) {
                //finding the common parent with the next graphical element
                parent = (ClusterElement) selected.get(i);

                do {
                    parent = (ClusterElement) parent.getParent();

                    for (int j = common_index; j < ancestors.size(); j++) {
                        if (parent == ancestors.get(j)) {
                            common_index = j;
                            parent = null;
                            break;
                        }
                    }
                } while (parent != null);
            }

            common = ancestors.get(common_index);
        }

        return common;
    }

    private Cluster createCluster(ArrayList<GraphicalElement> selected) {
        Cluster newcluster = new Cluster();
        int size = selected.size();

        for (int i = 0; i < size; i++) {
            GraphicalElement ge = selected.get(i);

            if (ge instanceof ClusterElement) {
                ClusterElement ce = (ClusterElement) ge;

                for (int j = 0; j < ce.getCluster().getInstancesCount(); j++) {
                    Instance ci = ce.getCluster().getInstance(j);
                    newcluster.addInstance(ci);
                }
            } else {
                newcluster.addInstance((DataInstanceElement) ge);
            }
        }
        return newcluster;
    }

    private ClusterElement projectCluster(ArrayList<GraphicalElement> selected,
            Cluster newcluster) {
        int size = selected.size();

        //calculating the weights
        float[] weights = new float[size];

        for (int i = 0; i < size; i++) {
            GraphicalElement ge = selected.get(i);

            if (ge instanceof ClusterElement) {
                ClusterElement ce = (ClusterElement) ge;
                weights[i] = ce.getCluster().getInstancesCount();
            } else {
                weights[i] = 1.0f;
            }
        }

        //creating the ce element
        ClusterElement newce = new ClusterElement(newcluster);

        //defining the position of the ce
        float x = 0.0f;
        float y = 0.0f;

        for (int i = 0; i < size; i++) {
            GraphicalElement ge = selected.get(i);
            x += ge.getX() * (weights[i] / newcluster.getInstancesCount());
            y += ge.getY() * (weights[i] / newcluster.getInstancesCount());
        }

        newce.setX(x);
        newce.setY(y);

        return newce;
    }

    private float getClass(ClusterElement cge) {
        float[] nrEl = new float[5000];
        Arrays.fill(nrEl, 0);

        for (int i = 0; i < cge.getCluster().getInstancesCount(); i++) {
            int index = (int) cge.getCluster().getInstance(i).getVector().getKlass();
            if (index < nrEl.length) {
                nrEl[index]++;
            }
        }

        float max = 0;
        int index = 0;
        for (int i = 0; i < nrEl.length; i++) {
            if (max < nrEl[i]) {
                max = nrEl[i];
                index = i;
            }
        }

        return (max / cge.getCluster().getInstancesCount() > 0.7f) ? index : 0.5f;
    }

}
