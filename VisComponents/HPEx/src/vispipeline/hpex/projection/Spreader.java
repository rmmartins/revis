/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.projection;

import vispipeline.hpex.util.Util;
import vispipeline.hpex.view.draw.ClusterElement;
import vispipeline.hpex.view.draw.DataInstanceElement;
import vispipeline.hpex.view.draw.GLRenderer;
import vispipeline.hpex.view.draw.GraphicalElement;
import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class Spreader {

    /** 
     * Creates a new instance of Spreader 
     * @param cluster 
     */
    public Spreader(ClusterElement cluster) {
        this.cluster = cluster;

        ArrayList<GraphicalElement> children = cluster.getChildren();
        int size = children.size();

        //defining the overlaps
        overlap = new ArrayList<ArrayList<Integer>>();

        for (int i = 0; i < size; i++) {
            float x1 = children.get(i).getX();
            float y1 = children.get(i).getY();
            float size1 = children.get(i).getSize();

            overlap.add(new ArrayList<Integer>());

            for (int j = 0; j < children.size(); j++) {
                if (i == j) {
                    continue;
                }

                float x2 = children.get(j).getX();
                float y2 = children.get(j).getY();
                float size2 = children.get(j).getSize();

                float dist = (float) Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));

                if (dist < size1 + size2) {
                    overlap.get(i).add(j);
                }
            }
        }

        //putting all clusters inside the parent cluster
        float cx = cluster.getX();
        float cy = cluster.getY();
        float cr = cluster.getSize();
        float delta = 0.0f;

        for (int i = 0; i < size; i++) {
            float x = children.get(i).getX();
            float y = children.get(i).getY();
            float r = children.get(i).getSize();
            float dist = (float) Math.sqrt((x - cx) * (x - cx) + (y - cy) * (y - cy)) + r;

            float diff = dist - cr;
            if (diff > delta) {
                delta = diff;
            }
        }

        for (int i = 0; i < size; i++) {
            float x = children.get(i).getX();
            float y = children.get(i).getY();
            float norm = (float) Math.sqrt((x - cx) * (x - cx) + (y - cy) * (y - cy));

            if (norm > EPSILON) {
                float newx = x + delta * ((cx - x) / norm);
                float newy = y + delta * ((cy - y) / norm);
                
                children.get(i).setX(newx);
                children.get(i).setY(newy);
            }
        }

        Util.checkNaN(cluster, "Spreader: putting inside parent cluster");

        //avoiding that two clusters have the same center
        ArrayList<Point> point_aux = new ArrayList<Point>();
        for (GraphicalElement ge : children) {
            point_aux.add(new Point(ge));
        }

        Collections.sort(point_aux);

        for (int i = 0; i < point_aux.size() - 1; i++) {
            if (Math.abs(point_aux.get(i).ge.getX() - point_aux.get(i + 1).ge.getX()) < EPSILON &&
                    Math.abs(point_aux.get(i).ge.getY() - point_aux.get(i + 1).ge.getY()) < EPSILON) {
                point_aux.get(i).ge.setX(point_aux.get(i).ge.getX() +
                        (point_aux.get(i).ge.getSize() + point_aux.get(i + 1).ge.getSize()) / 4);
                point_aux.get(i).ge.setY(point_aux.get(i).ge.getY() +
                        (point_aux.get(i).ge.getSize() + point_aux.get(i + 1).ge.getSize()) / 4);
            }
        }

        Util.checkNaN(cluster, "Spreader: avoiding same center");
    }

    public void execute(int nrIterations, boolean threshold) {
        ArrayList<GraphicalElement> children = this.cluster.getChildren();
        int[] index = this.order(children);

        boolean changepos = true;
        for (int i = 0; i < nrIterations && changepos; i++) {
            changepos = this.iteration(index, children, threshold);
        }
    }

    private boolean iteration(int[] index, ArrayList<GraphicalElement> children, boolean threshold) {
        int size = children.size();
        boolean changepos = false;

        for (int i = 0; i < size; i++) {
            int ins1 = index[i];

            for (int ins2 = 0; ins2 < size; ins2++) {
                if (ins1 == ins2) {
                    continue;
                }

                //distance between cluster centers
                float dr2 = this.distR2(children.get(ins1), children.get(ins2));

                //Sum of clusters radius
                float size1 = children.get(ins1).getSize();
                float size2 = children.get(ins2).getSize();

                if (children.get(ins1) instanceof DataInstanceElement) {
                    size1 *= 0.4f;
                }

                if (children.get(ins2) instanceof DataInstanceElement) {
                    size2 *= 0.4f;
                }

                float sumRadius = size1 + size2;

                //if the instansces does not originally overlap add a threshold do the
                //distance between them
                if (threshold && !overlap.get(ins1).contains(ins2)) {
                    sumRadius += cluster.getSize() / 10.0f;
                }

                if ((sumRadius - dr2) > cluster.getSize() / (GLRenderer.WINDOW_SIZE * 0.5f)) {
                    float delta = (sumRadius - dr2) * 0.25f;

                    float cx = this.cluster.getX();
                    float cy = this.cluster.getY();
                    float csize = this.cluster.getSize();

                    //moving ins2 -> ins1 half of delta
                    float x1 = children.get(ins2).getX() + 0.75f * delta *
                            ((children.get(ins2).getX() - children.get(ins1).getX()) / dr2);
                    float y1 = children.get(ins2).getY() + 0.75f * delta *
                            ((children.get(ins2).getY() - children.get(ins1).getY()) / dr2);

                    float d1 = (float) Math.sqrt((x1 - cx) * (x1 - cx) + (y1 - cy) * (y1 - cy));

                    if ((d1 + children.get(ins2).getSize()) > csize) {
                        x1 = x1 + ((d1 - csize) + children.get(ins2).getSize()) * (cx - x1) / d1;
                        y1 = y1 + ((d1 - csize) + children.get(ins2).getSize()) * (cy - y1) / d1;
                    }

                    //moving ins1 -> ins2 half of delta
                    float x2 = children.get(ins1).getX() + 0.25f * delta *
                            ((children.get(ins1).getX() - children.get(ins2).getX()) / dr2);
                    float y2 = children.get(ins1).getY() + 0.25f * delta *
                            ((children.get(ins1).getY() - children.get(ins2).getY()) / dr2);

                    float d2 = (float) Math.sqrt((x2 - cx) * (x2 - cx) + (y2 - cy) * (y2 - cy));

                    if ((d2 + children.get(ins1).getSize()) > csize) {
                        x2 = x2 + ((d2 - csize) + children.get(ins1).getSize()) * (cx - x2) / d2;
                        y2 = y2 + ((d2 - csize) + children.get(ins1).getSize()) * (cy - y2) / d2;
                    }

                    children.get(ins2).setX(x1);
                    children.get(ins2).setY(y1);

                    children.get(ins1).setX(x2);
                    children.get(ins1).setY(y2);

                    changepos = true;
                }
            }
        }

        return changepos;
    }

    private int[] order(ArrayList<GraphicalElement> children) {
        ArrayList<Pair> pairs = new ArrayList<Pair>();

        for (int i = 0; i < children.size(); i++) {
            pairs.add(new Pair(i, children.get(i).getSize()));
        }

        Collections.sort(pairs);

        int[] index = new int[pairs.size()];

        for (int i = 0; i < pairs.size(); i++) {
            index[i] = pairs.get(i).index;
        }

        return index;
    }

    private float distR2(GraphicalElement geA, GraphicalElement geB) {
        float x1x2 = (geA.getX() - geB.getX());
        float y1y2 = (geA.getY() - geB.getY());
        float dist = (float) Math.sqrt(x1x2 * x1x2 + y1y2 * y1y2);

        if (dist > EPSILON) {
            return dist;
        } else {
            return EPSILON;
        }
    }

    public class Pair implements Comparable {

        public Pair(int index, float value) {
            this.index = index;
            this.value = value;
        }

        public int compareTo(Object o) {
            float diff = this.value - ((Pair) o).value;
            return (diff == 0.0f) ? 0 : (diff < 0.0f) ? 1 : -1;
        }

        public int index;
        public float value;
    }

    public class Point implements Comparable {

        public Point(GraphicalElement ge) {
            this.ge = ge;
        }

        public int compareTo(Object o) {
            if (o instanceof Point) {
                if (this.ge.getX() - ((Point) o).ge.getX() == EPSILON) {
                    if (this.ge.getY() - ((Point) o).ge.getY() == EPSILON) {
                        return 0;
                    } else if (this.ge.getY() - ((Point) o).ge.getY() > EPSILON) {
                        return 1;
                    } else {
                        return -1;
                    }
                } else if (this.ge.getX() - ((Point) o).ge.getX() > EPSILON) {
                    return 1;
                } else {
                    return -1;
                }
            } else {
                return -1;
            }
        }

        public GraphicalElement ge;
    }

    public static final float EPSILON = 0.00001f;
    private ClusterElement cluster;
    private ArrayList<ArrayList<Integer>> overlap;
}
