/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.projection;

import vispipeline.hpex.view.HPExView;
import vispipeline.hpex.view.draw.ClusterElement;
import vispipeline.hpex.view.draw.ClusterHierarchy;
import vispipeline.hpex.view.draw.GraphicalElement;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class ExpandAllClusters {

    public void expand(ClusterHierarchy hierarchy, HPExView view) {
        for (GraphicalElement ge : hierarchy.getRoot().getChildren()) {
            if (ge instanceof ClusterElement) {
                this.expand((ClusterElement) ge, hierarchy, view);
            }
        }

        view.getProjectionPanel().getRenderer().updateImage();
        view.getProjectionPanel().repaint();
        view.updateTree();
    }

    private void expand(ClusterElement ce, ClusterHierarchy hierarchy, HPExView view) {
        if (ce.getDrawState() == GraphicalElement.DrawState.DRAWN) {
            view.push(ce);
        }

        if (ce.getChildrenProjectionState() == ClusterElement.ChildrenProjectionState.NOT_PROJECTED) {
            try {
                ExpandCluster exp = new ExpandCluster();
                exp.expand(hierarchy, ce);
                ce.setChildrenProjectionState(ClusterElement.ChildrenProjectionState.PROJECTED);
            } catch (IOException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            }
        }

        ce.setDrawState(GraphicalElement.DrawState.EXPANDED);
        for (GraphicalElement ge_aux : ce.getChildren()) {
            ge_aux.setDrawState(GraphicalElement.DrawState.DRAWN);
            ge_aux.setColor(view.getScalar(), view.getProjectionPanel().getColorTable());

            if (ge_aux instanceof ClusterElement) {
                this.expand((ClusterElement) ge_aux, hierarchy, view);
            }
        }
    }

}
