/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.view.color;

/**
 * This class represents the Linearized Optimal Color Scale (LOCS).
 * 
 * @author Fernando Vieira Paulovich
 */
public class LocsCS extends ColorScale {

    /** Creates a new instance of LocsCS */
    public LocsCS(String name) {
        super(name);
        
        colors = new float[256][];
        colors[  0] = new float[]{0, 0, 0};
        colors[  1] = new float[]{0, 0, 0};
        colors[  2] = new float[]{0, 0, 0};
        colors[  3] = new float[]{1, 0, 0};
        colors[  4] = new float[]{2, 0, 0};
        colors[  5] = new float[]{2, 0, 0};
        colors[  6] = new float[]{3, 0, 0};
        colors[  7] = new float[]{3, 0, 0};
        colors[  8] = new float[]{4, 0, 0};
        colors[  9] = new float[]{5, 0, 0};
        colors[ 10] = new float[]{5, 0, 0};
        colors[ 11] = new float[]{6, 0, 0};
        colors[ 12] = new float[]{7, 0, 0};
        colors[ 13] = new float[]{7, 0, 0};
        colors[ 14] = new float[]{8, 0, 0};
        colors[ 15] = new float[]{9, 0, 0};
        colors[ 16] = new float[]{9, 0, 0};
        colors[ 17] = new float[]{10, 0, 0};
        colors[ 18] = new float[]{11, 0, 0};
        colors[ 19] = new float[]{12, 0, 0};
        colors[ 20] = new float[]{13, 0, 0};
        colors[ 21] = new float[]{14, 0, 0};
        colors[ 22] = new float[]{15, 0, 0};
        colors[ 23] = new float[]{16, 0, 0};
        colors[ 24] = new float[]{17, 0, 0};
        colors[ 25] = new float[]{18, 0, 0};
        colors[ 26] = new float[]{19, 0, 0};
        colors[ 27] = new float[]{20, 0, 0};
        colors[ 28] = new float[]{21, 0, 0};
        colors[ 29] = new float[]{22, 0, 0};
        colors[ 30] = new float[]{23, 0, 0};
        colors[ 31] = new float[]{25, 0, 0};
        colors[ 32] = new float[]{26, 0, 0};
        colors[ 33] = new float[]{27, 0, 0};
        colors[ 34] = new float[]{28, 0, 0};
        colors[ 35] = new float[]{30, 0, 0};
        colors[ 36] = new float[]{31, 0, 0};
        colors[ 37] = new float[]{33, 0, 0};
        colors[ 38] = new float[]{34, 0, 0};
        colors[ 39] = new float[]{35, 0, 0};
        colors[ 40] = new float[]{37, 0, 0};
        colors[ 41] = new float[]{39, 0, 0};
        colors[ 42] = new float[]{40, 0, 0};
        colors[ 43] = new float[]{43, 0, 0};
        colors[ 44] = new float[]{45, 0, 0};
        colors[ 45] = new float[]{46, 0, 0};
        colors[ 46] = new float[]{49, 0, 0};
        colors[ 47] = new float[]{51, 0, 0};
        colors[ 48] = new float[]{53, 0, 0};
        colors[ 49] = new float[]{54, 0, 0};
        colors[ 50] = new float[]{56, 0, 0};
        colors[ 51] = new float[]{58, 0, 0};
        colors[ 52] = new float[]{60, 0, 0};
        colors[ 53] = new float[]{62, 0, 0};
        colors[ 54] = new float[]{64, 0, 0};
        colors[ 55] = new float[]{67, 0, 0};
        colors[ 56] = new float[]{69, 0, 0};
        colors[ 57] = new float[]{71, 0, 0};
        colors[ 58] = new float[]{74, 0, 0};
        colors[ 59] = new float[]{76, 0, 0};
        colors[ 60] = new float[]{80, 0, 0};
        colors[ 61] = new float[]{81, 0, 0};
        colors[ 62] = new float[]{84, 0, 0};
        colors[ 63] = new float[]{86, 0, 0};
        colors[ 64] = new float[]{89, 0, 0};
        colors[ 65] = new float[]{92, 0, 0};
        colors[ 66] = new float[]{94, 0, 0};
        colors[ 67] = new float[]{97, 0, 0};
        colors[ 68] = new float[]{100, 0, 0};
        colors[ 69] = new float[]{103, 0, 0};
        colors[ 70] = new float[]{106, 0, 0};
        colors[ 71] = new float[]{109, 0, 0};
        colors[ 72] = new float[]{112, 0, 0};
        colors[ 73] = new float[]{115, 0, 0};
        colors[ 74] = new float[]{117, 0, 0};
        colors[ 75] = new float[]{122, 0, 0};
        colors[ 76] = new float[]{126, 0, 0};
        colors[ 77] = new float[]{128, 0, 0};
        colors[ 78] = new float[]{131, 0, 0};
        colors[ 79] = new float[]{135, 0, 0};
        colors[ 80] = new float[]{135, 0, 0};
        colors[ 81] = new float[]{135, 1, 0};
        colors[ 82] = new float[]{135, 2, 0};
        colors[ 83] = new float[]{135, 3, 0};
        colors[ 84] = new float[]{135, 4, 0};
        colors[ 85] = new float[]{135, 6, 0};
        colors[ 86] = new float[]{135, 6, 0};
        colors[ 87] = new float[]{135, 8, 0};
        colors[ 88] = new float[]{135, 9, 0};
        colors[ 89] = new float[]{135, 10, 0};
        colors[ 90] = new float[]{135, 11, 0};
        colors[ 91] = new float[]{135, 13, 0};
        colors[ 92] = new float[]{135, 13, 0};
        colors[ 93] = new float[]{135, 15, 0};
        colors[ 94] = new float[]{135, 17, 0};
        colors[ 95] = new float[]{135, 17, 0};
        colors[ 96] = new float[]{135, 19, 0};
        colors[ 97] = new float[]{135, 21, 0};
        colors[ 98] = new float[]{135, 22, 0};
        colors[ 99] = new float[]{135, 23, 0};
        colors[100] = new float[]{135, 25, 0};
        colors[101] = new float[]{135, 26, 0};
        colors[102] = new float[]{135, 27, 0};
        colors[103] = new float[]{135, 29, 0};
        colors[104] = new float[]{135, 31, 0};
        colors[105] = new float[]{135, 32, 0};
        colors[106] = new float[]{135, 33, 0};
        colors[107] = new float[]{135, 35, 0};
        colors[108] = new float[]{135, 36, 0};
        colors[109] = new float[]{135, 38, 0};
        colors[110] = new float[]{135, 40, 0};
        colors[111] = new float[]{135, 42, 0};
        colors[112] = new float[]{135, 44, 0};
        colors[113] = new float[]{135, 46, 0};
        colors[114] = new float[]{135, 47, 0};
        colors[115] = new float[]{135, 49, 0};
        colors[116] = new float[]{135, 51, 0};
        colors[117] = new float[]{135, 52, 0};
        colors[118] = new float[]{135, 54, 0};
        colors[119] = new float[]{135, 56, 0};
        colors[120] = new float[]{135, 57, 0};
        colors[121] = new float[]{135, 59, 0};
        colors[122] = new float[]{135, 62, 0};
        colors[123] = new float[]{135, 63, 0};
        colors[124] = new float[]{135, 65, 0};
        colors[125] = new float[]{135, 67, 0};
        colors[126] = new float[]{135, 69, 0};
        colors[127] = new float[]{135, 72, 0};
        colors[128] = new float[]{135, 73, 0};
        colors[129] = new float[]{135, 76, 0};
        colors[130] = new float[]{135, 78, 0};
        colors[131] = new float[]{135, 80, 0};
        colors[132] = new float[]{135, 82, 0};
        colors[133] = new float[]{135, 84, 0};
        colors[134] = new float[]{135, 87, 0};
        colors[135] = new float[]{135, 88, 0};
        colors[136] = new float[]{135, 90, 0};
        colors[137] = new float[]{135, 93, 0};
        colors[138] = new float[]{135, 95, 0};
        colors[139] = new float[]{135, 98, 0};
        colors[140] = new float[]{135, 101, 0};
        colors[141] = new float[]{135, 103, 0};
        colors[142] = new float[]{135, 106, 0};
        colors[143] = new float[]{135, 107, 0};
        colors[144] = new float[]{135, 110, 0};
        colors[145] = new float[]{135, 113, 0};
        colors[146] = new float[]{135, 115, 0};
        colors[147] = new float[]{135, 118, 0};
        colors[148] = new float[]{135, 121, 0};
        colors[149] = new float[]{135, 124, 0};
        colors[150] = new float[]{135, 127, 0};
        colors[151] = new float[]{135, 129, 0};
        colors[152] = new float[]{135, 133, 0};
        colors[153] = new float[]{135, 135, 0};
        colors[154] = new float[]{135, 138, 0};
        colors[155] = new float[]{135, 141, 0};
        colors[156] = new float[]{135, 144, 0};
        colors[157] = new float[]{135, 148, 0};
        colors[158] = new float[]{135, 150, 0};
        colors[159] = new float[]{135, 155, 0};
        colors[160] = new float[]{135, 157, 0};
        colors[161] = new float[]{135, 160, 0};
        colors[162] = new float[]{135, 163, 0};
        colors[163] = new float[]{135, 166, 0};
        colors[164] = new float[]{135, 170, 0};
        colors[165] = new float[]{135, 174, 0};
        colors[166] = new float[]{135, 177, 0};
        colors[167] = new float[]{135, 180, 0};
        colors[168] = new float[]{135, 184, 0};
        colors[169] = new float[]{135, 188, 0};
        colors[170] = new float[]{135, 192, 0};
        colors[171] = new float[]{135, 195, 0};
        colors[172] = new float[]{135, 200, 0};
        colors[173] = new float[]{135, 203, 0};
        colors[174] = new float[]{135, 205, 0};
        colors[175] = new float[]{135, 210, 0};
        colors[176] = new float[]{135, 214, 0};
        colors[177] = new float[]{135, 218, 0};
        colors[178] = new float[]{135, 222, 0};
        colors[179] = new float[]{135, 226, 0};
        colors[180] = new float[]{135, 231, 0};
        colors[181] = new float[]{135, 236, 0};
        colors[182] = new float[]{135, 239, 0};
        colors[183] = new float[]{135, 244, 0};
        colors[184] = new float[]{135, 249, 0};
        colors[185] = new float[]{135, 254, 0};
        colors[186] = new float[]{135, 255, 1};
        colors[187] = new float[]{135, 255, 5};
        colors[188] = new float[]{135, 255, 10};
        colors[189] = new float[]{135, 255, 15};
        colors[190] = new float[]{135, 255, 20};
        colors[191] = new float[]{135, 255, 23};
        colors[192] = new float[]{135, 255, 28};
        colors[193] = new float[]{135, 255, 33};
        colors[194] = new float[]{135, 255, 38};
        colors[195] = new float[]{135, 255, 43};
        colors[196] = new float[]{135, 255, 45};
        colors[197] = new float[]{135, 255, 49};
        colors[198] = new float[]{135, 255, 54};
        colors[199] = new float[]{135, 255, 59};
        colors[200] = new float[]{135, 255, 65};
        colors[201] = new float[]{135, 255, 70};
        colors[202] = new float[]{135, 255, 74};
        colors[203] = new float[]{135, 255, 80};
        colors[204] = new float[]{135, 255, 84};
        colors[205] = new float[]{135, 255, 90};
        colors[206] = new float[]{135, 255, 95};
        colors[207] = new float[]{135, 255, 98};
        colors[208] = new float[]{135, 255, 104};
        colors[209] = new float[]{135, 255, 110};
        colors[210] = new float[]{135, 255, 116};
        colors[211] = new float[]{135, 255, 120};
        colors[212] = new float[]{135, 255, 125};
        colors[213] = new float[]{135, 255, 131};
        colors[214] = new float[]{135, 255, 137};
        colors[215] = new float[]{135, 255, 144};
        colors[216] = new float[]{135, 255, 149};
        colors[217] = new float[]{135, 255, 154};
        colors[218] = new float[]{135, 255, 158};
        colors[219] = new float[]{135, 255, 165};
        colors[220] = new float[]{135, 255, 172};
        colors[221] = new float[]{135, 255, 179};
        colors[222] = new float[]{135, 255, 186};
        colors[223] = new float[]{135, 255, 191};
        colors[224] = new float[]{135, 255, 198};
        colors[225] = new float[]{135, 255, 203};
        colors[226] = new float[]{135, 255, 211};
        colors[227] = new float[]{135, 255, 216};
        colors[228] = new float[]{135, 255, 224};
        colors[229] = new float[]{135, 255, 232};
        colors[230] = new float[]{135, 255, 240};
        colors[231] = new float[]{135, 255, 248};
        colors[232] = new float[]{135, 255, 254};
        colors[233] = new float[]{135, 255, 255};
        colors[234] = new float[]{140, 255, 255};
        colors[235] = new float[]{146, 255, 255};
        colors[236] = new float[]{153, 255, 255};
        colors[237] = new float[]{156, 255, 255};
        colors[238] = new float[]{161, 255, 255};
        colors[239] = new float[]{168, 255, 255};
        colors[240] = new float[]{172, 255, 255};
        colors[241] = new float[]{177, 255, 255};
        colors[242] = new float[]{182, 255, 255};
        colors[243] = new float[]{189, 255, 255};
        colors[244] = new float[]{192, 255, 255};
        colors[245] = new float[]{199, 255, 255};
        colors[246] = new float[]{204, 255, 255};
        colors[247] = new float[]{210, 255, 255};
        colors[248] = new float[]{215, 255, 255};
        colors[249] = new float[]{220, 255, 255};
        colors[250] = new float[]{225, 255, 255};
        colors[251] = new float[]{232, 255, 255};
        colors[252] = new float[]{236, 255, 255};
        colors[253] = new float[]{240, 255, 255};
        colors[254] = new float[]{248, 255, 255};
        colors[255] = new float[]{255, 255, 255};
    }

    @Override
    public float[] getColor(float value) {
        float[] retValue = super.getColor(value);
        return new float[]{retValue[0] / 255.0f, retValue[1] / 255.0f, retValue[2] / 255.0f};
    }

}