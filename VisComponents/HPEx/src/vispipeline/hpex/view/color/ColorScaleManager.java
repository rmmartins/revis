/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.view.color;

import java.util.ArrayList;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class ColorScaleManager {

    public static final ColorScale GRAY = new GrayCS("Gray");
    public static final ColorScale HEATED_OBJECTS = new HeatedObjectCS("Heated Objects");
    public static final ColorScale LINEAR_GRAY = new LinGrayCS("Linear Gray");
    public static final ColorScale LOCS = new LocsCS("LOCS");
    public static final ColorScale RAINBOW = new RainbowCS("Rainbow");
    public static final ColorScale UNDEFINED = new UndefinedCS("Undefined");
    
    private ColorScaleManager() {
        scales = new ArrayList<ColorScale>();
        scales.add(ColorScaleManager.GRAY);
        scales.add(ColorScaleManager.HEATED_OBJECTS);
        scales.add(ColorScaleManager.LINEAR_GRAY);
        scales.add(ColorScaleManager.LOCS);
        scales.add(ColorScaleManager.RAINBOW);
        scales.add(ColorScaleManager.UNDEFINED);
    }    
    
    public static ColorScaleManager getInstance() {
        if (_instance == null) {
            _instance = new ColorScaleManager();
        }
        return _instance;
    }

    public ArrayList<ColorScale> getColorScales() {
        return scales;
    }

    private static ColorScaleManager _instance;
    private ArrayList<ColorScale> scales;
}
