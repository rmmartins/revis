/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.view.color;

/**
 * This class represents the Gray Color Scale.
 * 
 * @author Fernando Vieira Paulovich
 */
public class GrayCS extends ColorScale {

    /** Creates a new instance of GrayCS */
    public GrayCS(String name) {
        super(name);
        
        colors = new float[256][];
        colors[  0] = new float[]{0, 0, 0};
        colors[  1] = new float[]{1, 1, 1};
        colors[  2] = new float[]{2, 2, 2};
        colors[  3] = new float[]{3, 3, 3};
        colors[  4] = new float[]{4, 4, 4};
        colors[  5] = new float[]{5, 5, 5};
        colors[  6] = new float[]{6, 6, 6};
        colors[  7] = new float[]{7, 7, 7};
        colors[  8] = new float[]{8, 8, 8};
        colors[  9] = new float[]{9, 9, 9};
        colors[ 10] = new float[]{10, 10, 10};
        colors[ 11] = new float[]{11, 11, 11};
        colors[ 12] = new float[]{12, 12, 12};
        colors[ 13] = new float[]{13, 13, 13};
        colors[ 14] = new float[]{14, 14, 14};
        colors[ 15] = new float[]{15, 15, 15};
        colors[ 16] = new float[]{16, 16, 16};
        colors[ 17] = new float[]{17, 17, 17};
        colors[ 18] = new float[]{18, 18, 18};
        colors[ 19] = new float[]{19, 19, 19};
        colors[ 20] = new float[]{20, 20, 20};
        colors[ 21] = new float[]{21, 21, 21};
        colors[ 22] = new float[]{22, 22, 22};
        colors[ 23] = new float[]{23, 23, 23};
        colors[ 24] = new float[]{24, 24, 24};
        colors[ 25] = new float[]{25, 25, 25};
        colors[ 26] = new float[]{26, 26, 26};
        colors[ 27] = new float[]{27, 27, 27};
        colors[ 28] = new float[]{28, 28, 28};
        colors[ 29] = new float[]{29, 29, 29};
        colors[ 30] = new float[]{30, 30, 30};
        colors[ 31] = new float[]{31, 31, 31};
        colors[ 32] = new float[]{32, 32, 32};
        colors[ 33] = new float[]{33, 33, 33};
        colors[ 34] = new float[]{34, 34, 34};
        colors[ 35] = new float[]{35, 35, 35};
        colors[ 36] = new float[]{36, 36, 36};
        colors[ 37] = new float[]{37, 37, 37};
        colors[ 38] = new float[]{38, 38, 38};
        colors[ 39] = new float[]{39, 39, 39};
        colors[ 40] = new float[]{40, 40, 40};
        colors[ 41] = new float[]{41, 41, 41};
        colors[ 42] = new float[]{42, 42, 42};
        colors[ 43] = new float[]{43, 43, 43};
        colors[ 44] = new float[]{44, 44, 44};
        colors[ 45] = new float[]{45, 45, 45};
        colors[ 46] = new float[]{46, 46, 46};
        colors[ 47] = new float[]{47, 47, 47};
        colors[ 48] = new float[]{48, 48, 48};
        colors[ 49] = new float[]{49, 49, 49};
        colors[ 50] = new float[]{50, 50, 50};
        colors[ 51] = new float[]{51, 51, 51};
        colors[ 52] = new float[]{52, 52, 52};
        colors[ 53] = new float[]{53, 53, 53};
        colors[ 54] = new float[]{54, 54, 54};
        colors[ 55] = new float[]{55, 55, 55};
        colors[ 56] = new float[]{56, 56, 56};
        colors[ 57] = new float[]{57, 57, 57};
        colors[ 58] = new float[]{58, 58, 58};
        colors[ 59] = new float[]{59, 59, 59};
        colors[ 60] = new float[]{60, 60, 60};
        colors[ 61] = new float[]{61, 61, 61};
        colors[ 62] = new float[]{62, 62, 62};
        colors[ 63] = new float[]{63, 63, 63};
        colors[ 64] = new float[]{64, 64, 64};
        colors[ 65] = new float[]{65, 65, 65};
        colors[ 66] = new float[]{66, 66, 66};
        colors[ 67] = new float[]{67, 67, 67};
        colors[ 68] = new float[]{68, 68, 68};
        colors[ 69] = new float[]{69, 69, 69};
        colors[ 70] = new float[]{70, 70, 70};
        colors[ 71] = new float[]{71, 71, 71};
        colors[ 72] = new float[]{72, 72, 72};
        colors[ 73] = new float[]{73, 73, 73};
        colors[ 74] = new float[]{74, 74, 74};
        colors[ 75] = new float[]{75, 75, 75};
        colors[ 76] = new float[]{76, 76, 76};
        colors[ 77] = new float[]{77, 77, 77};
        colors[ 78] = new float[]{78, 78, 78};
        colors[ 79] = new float[]{79, 79, 79};
        colors[ 80] = new float[]{80, 80, 80};
        colors[ 81] = new float[]{81, 81, 81};
        colors[ 82] = new float[]{82, 82, 82};
        colors[ 83] = new float[]{83, 83, 83};
        colors[ 84] = new float[]{84, 84, 84};
        colors[ 85] = new float[]{85, 85, 85};
        colors[ 86] = new float[]{86, 86, 86};
        colors[ 87] = new float[]{87, 87, 87};
        colors[ 88] = new float[]{88, 88, 88};
        colors[ 89] = new float[]{89, 89, 89};
        colors[ 90] = new float[]{90, 90, 90};
        colors[ 91] = new float[]{91, 91, 91};
        colors[ 92] = new float[]{92, 92, 92};
        colors[ 93] = new float[]{93, 93, 93};
        colors[ 94] = new float[]{94, 94, 94};
        colors[ 95] = new float[]{95, 95, 95};
        colors[ 96] = new float[]{96, 96, 96};
        colors[ 97] = new float[]{97, 97, 97};
        colors[ 98] = new float[]{98, 98, 98};
        colors[ 99] = new float[]{99, 99, 99};
        colors[100] = new float[]{100, 100, 100};
        colors[101] = new float[]{101, 101, 101};
        colors[102] = new float[]{102, 102, 102};
        colors[103] = new float[]{103, 103, 103};
        colors[104] = new float[]{104, 104, 104};
        colors[105] = new float[]{105, 105, 105};
        colors[106] = new float[]{106, 106, 106};
        colors[107] = new float[]{107, 107, 107};
        colors[108] = new float[]{108, 108, 108};
        colors[109] = new float[]{109, 109, 109};
        colors[110] = new float[]{110, 110, 110};
        colors[111] = new float[]{111, 111, 111};
        colors[112] = new float[]{112, 112, 112};
        colors[113] = new float[]{113, 113, 113};
        colors[114] = new float[]{114, 114, 114};
        colors[115] = new float[]{115, 115, 115};
        colors[116] = new float[]{116, 116, 116};
        colors[117] = new float[]{117, 117, 117};
        colors[118] = new float[]{118, 118, 118};
        colors[119] = new float[]{119, 119, 119};
        colors[120] = new float[]{120, 120, 120};
        colors[121] = new float[]{121, 121, 121};
        colors[122] = new float[]{122, 122, 122};
        colors[123] = new float[]{123, 123, 123};
        colors[124] = new float[]{124, 124, 124};
        colors[125] = new float[]{125, 125, 125};
        colors[126] = new float[]{126, 126, 126};
        colors[127] = new float[]{127, 127, 127};
        colors[128] = new float[]{128, 128, 128};
        colors[129] = new float[]{129, 129, 129};
        colors[130] = new float[]{130, 130, 130};
        colors[131] = new float[]{131, 131, 131};
        colors[132] = new float[]{132, 132, 132};
        colors[133] = new float[]{133, 133, 133};
        colors[134] = new float[]{134, 134, 134};
        colors[135] = new float[]{135, 135, 135};
        colors[136] = new float[]{136, 136, 136};
        colors[137] = new float[]{137, 137, 137};
        colors[138] = new float[]{138, 138, 138};
        colors[139] = new float[]{139, 139, 139};
        colors[140] = new float[]{140, 140, 140};
        colors[141] = new float[]{141, 141, 141};
        colors[142] = new float[]{142, 142, 142};
        colors[143] = new float[]{143, 143, 143};
        colors[144] = new float[]{144, 144, 144};
        colors[145] = new float[]{145, 145, 145};
        colors[146] = new float[]{146, 146, 146};
        colors[147] = new float[]{147, 147, 147};
        colors[148] = new float[]{148, 148, 148};
        colors[149] = new float[]{149, 149, 149};
        colors[150] = new float[]{150, 150, 150};
        colors[151] = new float[]{151, 151, 151};
        colors[152] = new float[]{152, 152, 152};
        colors[153] = new float[]{153, 153, 153};
        colors[154] = new float[]{154, 154, 154};
        colors[155] = new float[]{155, 155, 155};
        colors[156] = new float[]{156, 156, 156};
        colors[157] = new float[]{157, 157, 157};
        colors[158] = new float[]{158, 158, 158};
        colors[159] = new float[]{159, 159, 159};
        colors[160] = new float[]{160, 160, 160};
        colors[161] = new float[]{161, 161, 161};
        colors[162] = new float[]{162, 162, 162};
        colors[163] = new float[]{163, 163, 163};
        colors[164] = new float[]{164, 164, 164};
        colors[165] = new float[]{165, 165, 165};
        colors[166] = new float[]{166, 166, 166};
        colors[167] = new float[]{167, 167, 167};
        colors[168] = new float[]{168, 168, 168};
        colors[169] = new float[]{169, 169, 169};
        colors[170] = new float[]{170, 170, 170};
        colors[171] = new float[]{171, 171, 171};
        colors[172] = new float[]{172, 172, 172};
        colors[173] = new float[]{173, 173, 173};
        colors[174] = new float[]{174, 174, 174};
        colors[175] = new float[]{175, 175, 175};
        colors[176] = new float[]{176, 176, 176};
        colors[177] = new float[]{177, 177, 177};
        colors[178] = new float[]{178, 178, 178};
        colors[179] = new float[]{179, 179, 179};
        colors[180] = new float[]{180, 180, 180};
        colors[181] = new float[]{181, 181, 181};
        colors[182] = new float[]{182, 182, 182};
        colors[183] = new float[]{183, 183, 183};
        colors[184] = new float[]{184, 184, 184};
        colors[185] = new float[]{185, 185, 185};
        colors[186] = new float[]{186, 186, 186};
        colors[187] = new float[]{187, 187, 187};
        colors[188] = new float[]{188, 188, 188};
        colors[189] = new float[]{189, 189, 189};
        colors[190] = new float[]{190, 190, 190};
        colors[191] = new float[]{191, 191, 191};
        colors[192] = new float[]{192, 192, 192};
        colors[193] = new float[]{193, 193, 193};
        colors[194] = new float[]{194, 194, 194};
        colors[195] = new float[]{195, 195, 195};
        colors[196] = new float[]{196, 196, 196};
        colors[197] = new float[]{197, 197, 197};
        colors[198] = new float[]{198, 198, 198};
        colors[199] = new float[]{199, 199, 199};
        colors[200] = new float[]{200, 200, 200};
        colors[201] = new float[]{201, 201, 201};
        colors[202] = new float[]{202, 202, 202};
        colors[203] = new float[]{203, 203, 203};
        colors[204] = new float[]{204, 204, 204};
        colors[205] = new float[]{205, 205, 205};
        colors[206] = new float[]{206, 206, 206};
        colors[207] = new float[]{207, 207, 207};
        colors[208] = new float[]{208, 208, 208};
        colors[209] = new float[]{209, 209, 209};
        colors[210] = new float[]{210, 210, 210};
        colors[211] = new float[]{211, 211, 211};
        colors[212] = new float[]{212, 212, 212};
        colors[213] = new float[]{213, 213, 213};
        colors[214] = new float[]{214, 214, 214};
        colors[215] = new float[]{215, 215, 215};
        colors[216] = new float[]{216, 216, 216};
        colors[217] = new float[]{217, 217, 217};
        colors[218] = new float[]{218, 218, 218};
        colors[219] = new float[]{219, 219, 219};
        colors[220] = new float[]{220, 220, 220};
        colors[221] = new float[]{221, 221, 221};
        colors[222] = new float[]{222, 222, 222};
        colors[223] = new float[]{223, 223, 223};
        colors[224] = new float[]{224, 224, 224};
        colors[225] = new float[]{225, 225, 225};
        colors[226] = new float[]{226, 226, 226};
        colors[227] = new float[]{227, 227, 227};
        colors[228] = new float[]{228, 228, 228};
        colors[229] = new float[]{229, 229, 229};
        colors[230] = new float[]{230, 230, 230};
        colors[231] = new float[]{231, 231, 231};
        colors[232] = new float[]{232, 232, 232};
        colors[233] = new float[]{233, 233, 233};
        colors[234] = new float[]{234, 234, 234};
        colors[235] = new float[]{235, 235, 235};
        colors[236] = new float[]{236, 236, 236};
        colors[237] = new float[]{237, 237, 237};
        colors[238] = new float[]{238, 238, 238};
        colors[239] = new float[]{239, 239, 239};
        colors[240] = new float[]{240, 240, 240};
        colors[241] = new float[]{241, 241, 241};
        colors[242] = new float[]{242, 242, 242};
        colors[243] = new float[]{243, 243, 243};
        colors[244] = new float[]{244, 244, 244};
        colors[245] = new float[]{245, 245, 245};
        colors[246] = new float[]{246, 246, 246};
        colors[247] = new float[]{247, 247, 247};
        colors[248] = new float[]{248, 248, 248};
        colors[249] = new float[]{249, 249, 249};
        colors[250] = new float[]{250, 250, 250};
        colors[251] = new float[]{251, 251, 251};
        colors[252] = new float[]{252, 252, 252};
        colors[253] = new float[]{253, 253, 253};
        colors[254] = new float[]{254, 254, 254};
        colors[255] = new float[]{255, 255, 255};
    }

    @Override
    public float[] getColor(float value) {
        float[] retValue = super.getColor(value);
        return new float[]{retValue[0] / 255.0f, retValue[1] / 255.0f, retValue[2] / 255.0f};
    }

}
