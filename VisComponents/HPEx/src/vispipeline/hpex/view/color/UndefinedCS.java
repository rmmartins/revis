/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.view.color;

/**
 * This class represents an undefined color scale, normally called rainbow
 * color scale, but it is not the rainbow scale (I call it SBT - Sistema
 * Brasileiro de Televisao - color scale).
 * 
 * @author Fernando Vieira Paulovich
 */
public class UndefinedCS extends ColorScale {

    /**
     * Creates a new instance of UndefinedCS
     */
    public UndefinedCS(String name) {
        super(name);

        colors = new float[256][];

        for (int i = 0; i < colors.length; i++) {
            float aux = (float) i / colors.length;

            if (aux < 0.25f) {
                //red - > yellow
                this.colors[i] = new float[]{1.0f, 4 * aux, 0.0f};
            } else if (aux >= 0.25f && aux < 0.5f) {
                //green -> cyan
                this.colors[i] = new float[]{(-4 * aux + 2), 1.0f, 0.0f};
            } else if (aux >= 0.5f && aux < 0.75f) {
                //bulue -> cyan
                this.colors[i] = new float[]{0.0f, 1.0f, (4 * aux - 2)};
            } else {
                //blue -> pink
                this.colors[i] = new float[]{0.0f, (-4 * aux + 4), 1.0f};
            }
        }
    }

}
