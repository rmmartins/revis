/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.view.color;

/**
 * This class represents the Linear Gray Color Scale.
 * 
 * @author Fernando Vieira Paulovich
 */
public class LinGrayCS extends ColorScale {

    /** Creates a new instance of LinGrayCS */
    public LinGrayCS(String name) {
        super(name);        
        
        colors = new float[256][];
        colors[  0] = new float[]{0, 0, 0};
        colors[  1] = new float[]{0, 0, 0};
        colors[  2] = new float[]{0, 0, 0};
        colors[  3] = new float[]{0, 0, 0};
        colors[  4] = new float[]{0, 0, 0};
        colors[  5] = new float[]{0, 0, 0};
        colors[  6] = new float[]{0, 0, 0};
        colors[  7] = new float[]{1, 1, 1};
        colors[  8] = new float[]{1, 1, 1};
        colors[  9] = new float[]{1, 1, 1};
        colors[ 10] = new float[]{1, 1, 1};
        colors[ 11] = new float[]{1, 1, 1};
        colors[ 12] = new float[]{1, 1, 1};
        colors[ 13] = new float[]{1, 1, 1};
        colors[ 14] = new float[]{1, 1, 1};
        colors[ 15] = new float[]{1, 1, 1};
        colors[ 16] = new float[]{2, 2, 2};
        colors[ 17] = new float[]{2, 2, 2};
        colors[ 18] = new float[]{2, 2, 2};
        colors[ 19] = new float[]{2, 2, 2};
        colors[ 20] = new float[]{2, 2, 2};
        colors[ 21] = new float[]{2, 2, 2};
        colors[ 22] = new float[]{2, 2, 2};
        colors[ 23] = new float[]{3, 3, 3};
        colors[ 24] = new float[]{3, 3, 3};
        colors[ 25] = new float[]{3, 3, 3};
        colors[ 26] = new float[]{3, 3, 3};
        colors[ 27] = new float[]{3, 3, 3};
        colors[ 28] = new float[]{3, 3, 3};
        colors[ 29] = new float[]{3, 3, 3};
        colors[ 30] = new float[]{4, 4, 4};
        colors[ 31] = new float[]{4, 4, 4};
        colors[ 32] = new float[]{4, 4, 4};
        colors[ 33] = new float[]{4, 4, 4};
        colors[ 34] = new float[]{4, 4, 4};
        colors[ 35] = new float[]{5, 5, 5};
        colors[ 36] = new float[]{5, 5, 5};
        colors[ 37] = new float[]{5, 5, 5};
        colors[ 38] = new float[]{5, 5, 5};
        colors[ 39] = new float[]{5, 5, 5};
        colors[ 40] = new float[]{6, 6, 6};
        colors[ 41] = new float[]{6, 6, 6};
        colors[ 42] = new float[]{6, 6, 6};
        colors[ 43] = new float[]{6, 6, 6};
        colors[ 44] = new float[]{6, 6, 6};
        colors[ 45] = new float[]{7, 7, 7};
        colors[ 46] = new float[]{7, 7, 7};
        colors[ 47] = new float[]{7, 7, 7};
        colors[ 48] = new float[]{7, 7, 7};
        colors[ 49] = new float[]{7, 7, 7};
        colors[ 50] = new float[]{8, 8, 8};
        colors[ 51] = new float[]{8, 8, 8};
        colors[ 52] = new float[]{9, 9, 9};
        colors[ 53] = new float[]{9, 9, 9};
        colors[ 54] = new float[]{9, 9, 9};
        colors[ 55] = new float[]{9, 9, 9};
        colors[ 56] = new float[]{10, 10, 10};
        colors[ 57] = new float[]{10, 10, 10};
        colors[ 58] = new float[]{10, 10, 10};
        colors[ 59] = new float[]{10, 10, 10};
        colors[ 60] = new float[]{10, 10, 10};
        colors[ 61] = new float[]{11, 11, 11};
        colors[ 62] = new float[]{11, 11, 11};
        colors[ 63] = new float[]{12, 12, 12};
        colors[ 64] = new float[]{12, 12, 12};
        colors[ 65] = new float[]{12, 12, 12};
        colors[ 66] = new float[]{13, 13, 13};
        colors[ 67] = new float[]{13, 13, 13};
        colors[ 68] = new float[]{14, 14, 14};
        colors[ 69] = new float[]{14, 14, 14};
        colors[ 70] = new float[]{15, 15, 15};
        colors[ 71] = new float[]{15, 15, 15};
        colors[ 72] = new float[]{15, 15, 15};
        colors[ 73] = new float[]{16, 16, 16};
        colors[ 74] = new float[]{16, 16, 16};
        colors[ 75] = new float[]{17, 17, 17};
        colors[ 76] = new float[]{17, 17, 17};
        colors[ 77] = new float[]{18, 18, 18};
        colors[ 78] = new float[]{18, 18, 18};
        colors[ 79] = new float[]{19, 19, 19};
        colors[ 80] = new float[]{19, 19, 19};
        colors[ 81] = new float[]{19, 19, 19};
        colors[ 82] = new float[]{19, 19, 19};
        colors[ 83] = new float[]{19, 19, 19};
        colors[ 84] = new float[]{20, 20, 20};
        colors[ 85] = new float[]{20, 20, 20};
        colors[ 86] = new float[]{22, 22, 22};
        colors[ 87] = new float[]{22, 22, 22};
        colors[ 88] = new float[]{22, 22, 22};
        colors[ 89] = new float[]{23, 23, 23};
        colors[ 90] = new float[]{23, 23, 23};
        colors[ 91] = new float[]{24, 24, 24};
        colors[ 92] = new float[]{24, 24, 24};
        colors[ 93] = new float[]{26, 26, 26};
        colors[ 94] = new float[]{26, 26, 26};
        colors[ 95] = new float[]{26, 26, 26};
        colors[ 96] = new float[]{27, 27, 27};
        colors[ 97] = new float[]{27, 27, 27};
        colors[ 98] = new float[]{29, 29, 29};
        colors[ 99] = new float[]{29, 29, 29};
        colors[100] = new float[]{30, 30, 30};
        colors[101] = new float[]{30, 30, 30};
        colors[102] = new float[]{32, 32, 32};
        colors[103] = new float[]{32, 32, 32};
        colors[104] = new float[]{32, 32, 32};
        colors[105] = new float[]{32, 32, 32};
        colors[106] = new float[]{32, 32, 32};
        colors[107] = new float[]{34, 34, 34};
        colors[108] = new float[]{34, 34, 34};
        colors[109] = new float[]{35, 35, 35};
        colors[110] = new float[]{35, 35, 35};
        colors[111] = new float[]{35, 35, 35};
        colors[112] = new float[]{37, 37, 37};
        colors[113] = new float[]{37, 37, 37};
        colors[114] = new float[]{39, 39, 39};
        colors[115] = new float[]{39, 39, 39};
        colors[116] = new float[]{41, 41, 41};
        colors[117] = new float[]{41, 41, 41};
        colors[118] = new float[]{41, 41, 41};
        colors[119] = new float[]{43, 43, 43};
        colors[120] = new float[]{43, 43, 43};
        colors[121] = new float[]{45, 45, 45};
        colors[122] = new float[]{45, 45, 45};
        colors[123] = new float[]{46, 46, 46};
        colors[124] = new float[]{46, 46, 46};
        colors[125] = new float[]{46, 46, 46};
        colors[126] = new float[]{47, 47, 47};
        colors[127] = new float[]{47, 47, 47};
        colors[128] = new float[]{49, 49, 49};
        colors[129] = new float[]{49, 49, 49};
        colors[130] = new float[]{51, 51, 51};
        colors[131] = new float[]{51, 51, 51};
        colors[132] = new float[]{52, 52, 52};
        colors[133] = new float[]{52, 52, 52};
        colors[134] = new float[]{52, 52, 52};
        colors[135] = new float[]{54, 54, 54};
        colors[136] = new float[]{54, 54, 54};
        colors[137] = new float[]{56, 56, 56};
        colors[138] = new float[]{56, 56, 56};
        colors[139] = new float[]{59, 59, 59};
        colors[140] = new float[]{59, 59, 59};
        colors[141] = new float[]{59, 59, 59};
        colors[142] = new float[]{61, 61, 61};
        colors[143] = new float[]{61, 61, 61};
        colors[144] = new float[]{64, 64, 64};
        colors[145] = new float[]{64, 64, 64};
        colors[146] = new float[]{67, 67, 67};
        colors[147] = new float[]{67, 67, 67};
        colors[148] = new float[]{67, 67, 67};
        colors[149] = new float[]{69, 69, 69};
        colors[150] = new float[]{69, 69, 69};
        colors[151] = new float[]{72, 72, 72};
        colors[152] = new float[]{72, 72, 72};
        colors[153] = new float[]{75, 75, 75};
        colors[154] = new float[]{75, 75, 75};
        colors[155] = new float[]{76, 76, 76};
        colors[156] = new float[]{76, 76, 76};
        colors[157] = new float[]{76, 76, 76};
        colors[158] = new float[]{78, 78, 78};
        colors[159] = new float[]{78, 78, 78};
        colors[160] = new float[]{81, 81, 81};
        colors[161] = new float[]{81, 81, 81};
        colors[162] = new float[]{84, 84, 84};
        colors[163] = new float[]{84, 84, 84};
        colors[164] = new float[]{84, 84, 84};
        colors[165] = new float[]{87, 87, 87};
        colors[166] = new float[]{87, 87, 87};
        colors[167] = new float[]{91, 91, 91};
        colors[168] = new float[]{91, 91, 91};
        colors[169] = new float[]{94, 94, 94};
        colors[170] = new float[]{94, 94, 94};
        colors[171] = new float[]{94, 94, 94};
        colors[172] = new float[]{97, 97, 97};
        colors[173] = new float[]{97, 97, 97};
        colors[174] = new float[]{101, 101, 101};
        colors[175] = new float[]{101, 101, 101};
        colors[176] = new float[]{104, 104, 104};
        colors[177] = new float[]{104, 104, 104};
        colors[178] = new float[]{107, 107, 107};
        colors[179] = new float[]{107, 107, 107};
        colors[180] = new float[]{107, 107, 107};
        colors[181] = new float[]{108, 108, 108};
        colors[182] = new float[]{108, 108, 108};
        colors[183] = new float[]{112, 112, 112};
        colors[184] = new float[]{112, 112, 112};
        colors[185] = new float[]{116, 116, 116};
        colors[186] = new float[]{116, 116, 116};
        colors[187] = new float[]{116, 116, 116};
        colors[188] = new float[]{120, 120, 120};
        colors[189] = new float[]{120, 120, 120};
        colors[190] = new float[]{124, 124, 124};
        colors[191] = new float[]{124, 124, 124};
        colors[192] = new float[]{128, 128, 128};
        colors[193] = new float[]{128, 128, 128};
        colors[194] = new float[]{128, 128, 128};
        colors[195] = new float[]{132, 132, 132};
        colors[196] = new float[]{132, 132, 132};
        colors[197] = new float[]{136, 136, 136};
        colors[198] = new float[]{136, 136, 136};
        colors[199] = new float[]{141, 141, 141};
        colors[200] = new float[]{141, 141, 141};
        colors[201] = new float[]{145, 145, 145};
        colors[202] = new float[]{145, 145, 145};
        colors[203] = new float[]{145, 145, 145};
        colors[204] = new float[]{147, 147, 147};
        colors[205] = new float[]{147, 147, 147};
        colors[206] = new float[]{150, 150, 150};
        colors[207] = new float[]{150, 150, 150};
        colors[208] = new float[]{154, 154, 154};
        colors[209] = new float[]{154, 154, 154};
        colors[210] = new float[]{154, 154, 154};
        colors[211] = new float[]{159, 159, 159};
        colors[212] = new float[]{159, 159, 159};
        colors[213] = new float[]{164, 164, 164};
        colors[214] = new float[]{164, 164, 164};
        colors[215] = new float[]{169, 169, 169};
        colors[216] = new float[]{169, 169, 169};
        colors[217] = new float[]{169, 169, 169};
        colors[218] = new float[]{174, 174, 174};
        colors[219] = new float[]{174, 174, 174};
        colors[220] = new float[]{179, 179, 179};
        colors[221] = new float[]{179, 179, 179};
        colors[222] = new float[]{185, 185, 185};
        colors[223] = new float[]{185, 185, 185};
        colors[224] = new float[]{190, 190, 190};
        colors[225] = new float[]{190, 190, 190};
        colors[226] = new float[]{190, 190, 190};
        colors[227] = new float[]{195, 195, 195};
        colors[228] = new float[]{195, 195, 195};
        colors[229] = new float[]{195, 195, 195};
        colors[230] = new float[]{195, 195, 195};
        colors[231] = new float[]{201, 201, 201};
        colors[232] = new float[]{201, 201, 201};
        colors[233] = new float[]{201, 201, 201};
        colors[234] = new float[]{207, 207, 207};
        colors[235] = new float[]{207, 207, 207};
        colors[236] = new float[]{212, 212, 212};
        colors[237] = new float[]{212, 212, 212};
        colors[238] = new float[]{218, 218, 218};
        colors[239] = new float[]{218, 218, 218};
        colors[240] = new float[]{218, 218, 218};
        colors[241] = new float[]{224, 224, 224};
        colors[242] = new float[]{224, 224, 224};
        colors[243] = new float[]{230, 230, 230};
        colors[244] = new float[]{230, 230, 230};
        colors[245] = new float[]{237, 237, 237};
        colors[246] = new float[]{237, 237, 237};
        colors[247] = new float[]{243, 243, 243};
        colors[248] = new float[]{243, 243, 243};
        colors[249] = new float[]{243, 243, 243};
        colors[250] = new float[]{249, 249, 249};
        colors[251] = new float[]{249, 249, 249};
        colors[252] = new float[]{252, 252, 252};
        colors[253] = new float[]{252, 252, 252};
        colors[254] = new float[]{252, 252, 252};
        colors[255] = new float[]{255, 255, 255};
    }

    @Override
    public float[] getColor(float value) {
        float[] retValue = super.getColor(value);
        return new float[]{retValue[0] / 255.0f, retValue[1] / 255.0f, retValue[2] / 255.0f};
    }

}