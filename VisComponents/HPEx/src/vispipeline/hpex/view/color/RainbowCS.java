/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.view.color;

/**
 * This class represents the true Rainbow Color Scale.
 * 
 * @author Fernando Vieira Paulovich
 */
public class RainbowCS extends ColorScale {

    /** Creates a new instance of RainbowCS */
    public RainbowCS(String name) {
        super(name);
        
        colors = new float[256][];
        colors[  0] = new float[]{0, 0, 0};
        colors[  1] = new float[]{45, 0, 36};
        colors[  2] = new float[]{56, 0, 46};
        colors[  3] = new float[]{60, 0, 49};
        colors[  4] = new float[]{67, 0, 54};
        colors[  5] = new float[]{70, 0, 59};
        colors[  6] = new float[]{71, 0, 61};
        colors[  7] = new float[]{75, 0, 68};
        colors[  8] = new float[]{74, 0, 73};
        colors[  9] = new float[]{74, 0, 77};
        colors[ 10] = new float[]{73, 0, 81};
        colors[ 11] = new float[]{71, 0, 87};
        colors[ 12] = new float[]{69, 1, 90};
        colors[ 13] = new float[]{68, 2, 94};
        colors[ 14] = new float[]{66, 3, 97};
        colors[ 15] = new float[]{63, 6, 102};
        colors[ 16] = new float[]{61, 7, 106};
        colors[ 17] = new float[]{58, 10, 109};
        colors[ 18] = new float[]{56, 12, 113};
        colors[ 19] = new float[]{53, 15, 116};
        colors[ 20] = new float[]{48, 18, 119};
        colors[ 21] = new float[]{47, 20, 121};
        colors[ 22] = new float[]{44, 23, 124};
        colors[ 23] = new float[]{41, 27, 128};
        colors[ 24] = new float[]{40, 28, 129};
        colors[ 25] = new float[]{37, 32, 132};
        colors[ 26] = new float[]{34, 36, 134};
        colors[ 27] = new float[]{29, 43, 137};
        colors[ 28] = new float[]{25, 52, 138};
        colors[ 29] = new float[]{24, 57, 139};
        colors[ 30] = new float[]{24, 62, 141};
        colors[ 31] = new float[]{24, 64, 142};
        colors[ 32] = new float[]{23, 65, 142};
        colors[ 33] = new float[]{23, 69, 143};
        colors[ 34] = new float[]{23, 71, 142};
        colors[ 35] = new float[]{23, 71, 142};
        colors[ 36] = new float[]{23, 73, 142};
        colors[ 37] = new float[]{23, 75, 142};
        colors[ 38] = new float[]{23, 75, 142};
        colors[ 39] = new float[]{23, 78, 142};
        colors[ 40] = new float[]{23, 80, 142};
        colors[ 41] = new float[]{23, 80, 142};
        colors[ 42] = new float[]{23, 82, 141};
        colors[ 43] = new float[]{23, 85, 141};
        colors[ 44] = new float[]{23, 85, 141};
        colors[ 45] = new float[]{23, 87, 140};
        colors[ 46] = new float[]{23, 87, 140};
        colors[ 47] = new float[]{24, 90, 140};
        colors[ 48] = new float[]{24, 90, 140};
        colors[ 49] = new float[]{24, 93, 139};
        colors[ 50] = new float[]{24, 93, 139};
        colors[ 51] = new float[]{24, 93, 139};
        colors[ 52] = new float[]{24, 93, 139};
        colors[ 53] = new float[]{24, 97, 139};
        colors[ 54] = new float[]{24, 97, 139};
        colors[ 55] = new float[]{25, 101, 138};
        colors[ 56] = new float[]{25, 101, 138};
        colors[ 57] = new float[]{25, 104, 137};
        colors[ 58] = new float[]{25, 104, 137};
        colors[ 59] = new float[]{25, 104, 137};
        colors[ 60] = new float[]{26, 108, 137};
        colors[ 61] = new float[]{26, 108, 137};
        colors[ 62] = new float[]{27, 111, 136};
        colors[ 63] = new float[]{27, 111, 136};
        colors[ 64] = new float[]{27, 111, 136};
        colors[ 65] = new float[]{27, 115, 135};
        colors[ 66] = new float[]{27, 115, 135};
        colors[ 67] = new float[]{28, 118, 134};
        colors[ 68] = new float[]{28, 118, 134};
        colors[ 69] = new float[]{29, 122, 133};
        colors[ 70] = new float[]{29, 122, 133};
        colors[ 71] = new float[]{29, 122, 133};
        colors[ 72] = new float[]{29, 122, 133};
        colors[ 73] = new float[]{29, 125, 132};
        colors[ 74] = new float[]{29, 125, 132};
        colors[ 75] = new float[]{30, 128, 131};
        colors[ 76] = new float[]{30, 128, 131};
        colors[ 77] = new float[]{31, 131, 130};
        colors[ 78] = new float[]{31, 131, 130};
        colors[ 79] = new float[]{31, 131, 130};
        colors[ 80] = new float[]{32, 134, 128};
        colors[ 81] = new float[]{32, 134, 128};
        colors[ 82] = new float[]{33, 137, 127};
        colors[ 83] = new float[]{33, 137, 127};
        colors[ 84] = new float[]{33, 137, 127};
        colors[ 85] = new float[]{34, 140, 125};
        colors[ 86] = new float[]{34, 140, 125};
        colors[ 87] = new float[]{35, 142, 123};
        colors[ 88] = new float[]{35, 142, 123};
        colors[ 89] = new float[]{36, 145, 121};
        colors[ 90] = new float[]{36, 145, 121};
        colors[ 91] = new float[]{36, 145, 121};
        colors[ 92] = new float[]{37, 147, 118};
        colors[ 93] = new float[]{37, 147, 118};
        colors[ 94] = new float[]{38, 150, 116};
        colors[ 95] = new float[]{38, 150, 116};
        colors[ 96] = new float[]{40, 152, 113};
        colors[ 97] = new float[]{40, 152, 113};
        colors[ 98] = new float[]{41, 154, 111};
        colors[ 99] = new float[]{41, 154, 111};
        colors[100] = new float[]{42, 156, 108};
        colors[101] = new float[]{42, 156, 108};
        colors[102] = new float[]{43, 158, 106};
        colors[103] = new float[]{43, 158, 106};
        colors[104] = new float[]{43, 158, 106};
        colors[105] = new float[]{45, 160, 104};
        colors[106] = new float[]{45, 160, 104};
        colors[107] = new float[]{46, 162, 101};
        colors[108] = new float[]{46, 162, 101};
        colors[109] = new float[]{48, 164, 99};
        colors[110] = new float[]{48, 164, 99};
        colors[111] = new float[]{50, 166, 97};
        colors[112] = new float[]{50, 166, 97};
        colors[113] = new float[]{51, 168, 95};
        colors[114] = new float[]{53, 170, 93};
        colors[115] = new float[]{53, 170, 93};
        colors[116] = new float[]{53, 170, 93};
        colors[117] = new float[]{55, 172, 91};
        colors[118] = new float[]{55, 172, 91};
        colors[119] = new float[]{57, 174, 88};
        colors[120] = new float[]{57, 174, 88};
        colors[121] = new float[]{59, 175, 86};
        colors[122] = new float[]{62, 177, 84};
        colors[123] = new float[]{64, 178, 82};
        colors[124] = new float[]{64, 178, 82};
        colors[125] = new float[]{67, 180, 80};
        colors[126] = new float[]{67, 180, 80};
        colors[127] = new float[]{69, 181, 79};
        colors[128] = new float[]{72, 183, 77};
        colors[129] = new float[]{72, 183, 77};
        colors[130] = new float[]{72, 183, 77};
        colors[131] = new float[]{75, 184, 76};
        colors[132] = new float[]{77, 186, 74};
        colors[133] = new float[]{80, 187, 73};
        colors[134] = new float[]{83, 189, 72};
        colors[135] = new float[]{87, 190, 72};
        colors[136] = new float[]{91, 191, 71};
        colors[137] = new float[]{95, 192, 70};
        colors[138] = new float[]{99, 193, 70};
        colors[139] = new float[]{103, 194, 70};
        colors[140] = new float[]{107, 195, 70};
        colors[141] = new float[]{111, 196, 70};
        colors[142] = new float[]{111, 196, 70};
        colors[143] = new float[]{115, 196, 70};
        colors[144] = new float[]{119, 197, 70};
        colors[145] = new float[]{123, 197, 70};
        colors[146] = new float[]{130, 198, 71};
        colors[147] = new float[]{133, 199, 71};
        colors[148] = new float[]{137, 199, 72};
        colors[149] = new float[]{140, 199, 72};
        colors[150] = new float[]{143, 199, 73};
        colors[151] = new float[]{143, 199, 73};
        colors[152] = new float[]{147, 199, 73};
        colors[153] = new float[]{150, 199, 74};
        colors[154] = new float[]{153, 199, 74};
        colors[155] = new float[]{156, 199, 75};
        colors[156] = new float[]{160, 200, 76};
        colors[157] = new float[]{167, 200, 78};
        colors[158] = new float[]{170, 200, 79};
        colors[159] = new float[]{173, 200, 79};
        colors[160] = new float[]{173, 200, 79};
        colors[161] = new float[]{177, 200, 80};
        colors[162] = new float[]{180, 200, 81};
        colors[163] = new float[]{183, 199, 82};
        colors[164] = new float[]{186, 199, 82};
        colors[165] = new float[]{190, 199, 83};
        colors[166] = new float[]{196, 199, 85};
        colors[167] = new float[]{199, 198, 85};
        colors[168] = new float[]{199, 198, 85};
        colors[169] = new float[]{203, 198, 86};
        colors[170] = new float[]{206, 197, 87};
        colors[171] = new float[]{212, 197, 89};
        colors[172] = new float[]{215, 196, 90};
        colors[173] = new float[]{218, 195, 91};
        colors[174] = new float[]{224, 194, 94};
        colors[175] = new float[]{224, 194, 94};
        colors[176] = new float[]{230, 193, 96};
        colors[177] = new float[]{233, 192, 98};
        colors[178] = new float[]{236, 190, 100};
        colors[179] = new float[]{238, 189, 104};
        colors[180] = new float[]{240, 188, 106};
        colors[181] = new float[]{240, 188, 106};
        colors[182] = new float[]{242, 187, 110};
        colors[183] = new float[]{244, 185, 114};
        colors[184] = new float[]{245, 184, 116};
        colors[185] = new float[]{247, 183, 120};
        colors[186] = new float[]{248, 182, 123};
        colors[187] = new float[]{248, 182, 123};
        colors[188] = new float[]{250, 181, 125};
        colors[189] = new float[]{251, 180, 128};
        colors[190] = new float[]{252, 180, 130};
        colors[191] = new float[]{253, 180, 133};
        colors[192] = new float[]{253, 180, 133};
        colors[193] = new float[]{254, 180, 134};
        colors[194] = new float[]{254, 179, 138};
        colors[195] = new float[]{255, 179, 142};
        colors[196] = new float[]{255, 179, 145};
        colors[197] = new float[]{255, 179, 145};
        colors[198] = new float[]{255, 179, 152};
        colors[199] = new float[]{255, 180, 161};
        colors[200] = new float[]{255, 180, 164};
        colors[201] = new float[]{255, 180, 167};
        colors[202] = new float[]{255, 180, 167};
        colors[203] = new float[]{255, 181, 169};
        colors[204] = new float[]{255, 181, 170};
        colors[205] = new float[]{255, 182, 173};
        colors[206] = new float[]{255, 183, 176};
        colors[207] = new float[]{255, 183, 176};
        colors[208] = new float[]{255, 184, 179};
        colors[209] = new float[]{255, 185, 179};
        colors[210] = new float[]{255, 185, 182};
        colors[211] = new float[]{255, 186, 182};
        colors[212] = new float[]{255, 186, 182};
        colors[213] = new float[]{255, 187, 185};
        colors[214] = new float[]{255, 188, 185};
        colors[215] = new float[]{255, 189, 188};
        colors[216] = new float[]{255, 189, 188};
        colors[217] = new float[]{255, 190, 188};
        colors[218] = new float[]{255, 191, 191};
        colors[219] = new float[]{255, 192, 191};
        colors[220] = new float[]{255, 194, 194};
        colors[221] = new float[]{255, 194, 194};
        colors[222] = new float[]{255, 197, 197};
        colors[223] = new float[]{255, 198, 198};
        colors[224] = new float[]{255, 200, 200};
        colors[225] = new float[]{255, 201, 201};
        colors[226] = new float[]{255, 201, 201};
        colors[227] = new float[]{255, 202, 202};
        colors[228] = new float[]{255, 203, 203};
        colors[229] = new float[]{255, 205, 205};
        colors[230] = new float[]{255, 206, 206};
        colors[231] = new float[]{255, 206, 206};
        colors[232] = new float[]{255, 208, 208};
        colors[233] = new float[]{255, 209, 209};
        colors[234] = new float[]{255, 211, 211};
        colors[235] = new float[]{255, 215, 215};
        colors[236] = new float[]{255, 216, 216};
        colors[237] = new float[]{255, 216, 216};
        colors[238] = new float[]{255, 218, 218};
        colors[239] = new float[]{255, 219, 219};
        colors[240] = new float[]{255, 221, 221};
        colors[241] = new float[]{255, 223, 223};
        colors[242] = new float[]{255, 226, 226};
        colors[243] = new float[]{255, 228, 228};
        colors[244] = new float[]{255, 230, 230};
        colors[245] = new float[]{255, 230, 230};
        colors[246] = new float[]{255, 232, 232};
        colors[247] = new float[]{255, 235, 235};
        colors[248] = new float[]{255, 237, 237};
        colors[249] = new float[]{255, 240, 240};
        colors[250] = new float[]{255, 243, 243};
        colors[251] = new float[]{255, 246, 246};
        colors[252] = new float[]{255, 249, 249};
        colors[253] = new float[]{255, 251, 251};
        colors[254] = new float[]{255, 253, 253};
        colors[255] = new float[]{255, 255, 255};
    }

    @Override
    public float[] getColor(float value) {
        float[] retValue = super.getColor(value);
        return new float[]{retValue[0] / 255.0f, retValue[1] / 255.0f, retValue[2] / 255.0f};
    }

}
