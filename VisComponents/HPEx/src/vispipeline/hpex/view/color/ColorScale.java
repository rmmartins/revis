/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.view.color;

import java.io.Serializable;

/**
 * This abstract class represents a Color Scale.
 * 
 * @author Fernando Vieira Paulovich
 */
public abstract class ColorScale implements Serializable {

    public ColorScale(String name) {
        this.name = name;
    }

    /**
     * Returns an RGB color based on a given value. This value must be between
     * 0 and 1. The RGB color values are between 0 and 1 on a float vector. The 
     * first position is the red component, the second is the green component, 
     * and the the third is the blue component of the color. 
     * @param value The color value.
     * @return The RBG color (a float vector with 3 positions).
     */
    public float[] getColor(float value) {
        assert (value >= 0.0f && value <= 1.0f) :
                "ERROR: the color value must be between 0 and 1!\n" +
                "Value " + value + " not valid.";

        float maxlength = ((this.colors.length - 1) / 2) +
                (max * ((this.colors.length - 1) / 2));
        float minlength = (min * ((this.colors.length - 1) / 2));

        if (reverse) {
            value = 1 - value;
            maxlength = ((this.colors.length - 1) / 2) +
                    ((1 - min) * ((this.colors.length - 1) / 2));
            minlength = ((1 - max) * ((this.colors.length - 1) / 2));
        }

        int index = (int) ((value * (maxlength - minlength)) + minlength);

        return (this.colors.length >= index) ? this.colors[index] : this.colors[this.colors.length - 1];
    }

    /**
     * Returns the number of colors associated to this color scale.
     * @return The numnber of colors.
     */
    public int getNumberColors() {
        return this.colors.length;
    }

    /**
     * Returns the minimum value associated to this scale.
     * @return The minimum scale value.
     */
    public float getMin() {
        return min;
    }

    /**
     * Changes the minimum value associated to this scale. It changes the 
     * starting point of the range colors. This value must be between 0 and 1.
     * @param min The new minimum color value.
     */
    public void setMin(float min) {
        assert (min >= 0.0f && min <= 1.0f) :
                "ERROR: the min color value must be between 0 and 1!";

        ColorScale.min = min;
    }

    /**
     * Returns the maximum value associated to this scale.
     * @return The maximum scale value.
     */
    public float getMax() {
        return max;
    }

    /**
     * Changes the maximum value associated to this scale. It changes the 
     * finishing point of the range colors. This value must be between 0 and 1.
     * @param max The new maximum color value.
     */
    public void setMax(float max) {
        assert (min >= 0.0f && min <= 1.0f) :
                "ERROR: the max color value must be between 0 and 1!";

        ColorScale.max = max;
    }

    /**
     * Returns if the cllor scale ir reversed.
     * @return True if the scale is reversed, false otherwise.
     */
    public boolean isReverse() {
        return reverse;
    }

    /**
     * Changes the color scale to be reversed or not.
     * @param reverse True to reserve the color scale and false to not.
     */
    public void setReverse(boolean reverse) {
        ColorScale.reverse = reverse;
    }

//    public String getName() {
//        return this.name;
//    }
    
    @Override
    public String toString() {
        return name;
    }

    private static float min = 0.0f;
    private static float max = 1.0f;
    private static boolean reverse = false;
    protected float[][] colors;
    protected String name;
    protected static final long serialVersionUID = 27L;
}
