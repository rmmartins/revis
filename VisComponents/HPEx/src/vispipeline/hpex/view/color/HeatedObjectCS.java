/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.view.color;

/**
 * This class represents the Heated Objects Color Scale.
 * 
 * @author Fernando Vieira Paulovich
 */
public class HeatedObjectCS extends ColorScale {

    /** Creates a new instance of HeatedObjectCS */
    public HeatedObjectCS(String name) {
        super(name);
        
        colors = new float[256][];
        colors[  0] = new float[]{0, 0, 0};
        colors[  1] = new float[]{35, 0, 0};
        colors[  2] = new float[]{52, 0, 0};
        colors[  3] = new float[]{60, 0, 0};
        colors[  4] = new float[]{63, 1, 0};
        colors[  5] = new float[]{64, 2, 0};
        colors[  6] = new float[]{68, 5, 0};
        colors[  7] = new float[]{69, 6, 0};
        colors[  8] = new float[]{72, 8, 0};
        colors[  9] = new float[]{74, 10, 0};
        colors[ 10] = new float[]{77, 12, 0};
        colors[ 11] = new float[]{78, 14, 0};
        colors[ 12] = new float[]{81, 16, 0};
        colors[ 13] = new float[]{83, 17, 0};
        colors[ 14] = new float[]{85, 19, 0};
        colors[ 15] = new float[]{86, 20, 0};
        colors[ 16] = new float[]{89, 22, 0};
        colors[ 17] = new float[]{91, 24, 0};
        colors[ 18] = new float[]{92, 25, 0};
        colors[ 19] = new float[]{94, 26, 0};
        colors[ 20] = new float[]{95, 28, 0};
        colors[ 21] = new float[]{98, 30, 0};
        colors[ 22] = new float[]{100, 31, 0};
        colors[ 23] = new float[]{102, 33, 0};
        colors[ 24] = new float[]{103, 34, 0};
        colors[ 25] = new float[]{105, 35, 0};
        colors[ 26] = new float[]{106, 36, 0};
        colors[ 27] = new float[]{108, 38, 0};
        colors[ 28] = new float[]{109, 39, 0};
        colors[ 29] = new float[]{111, 40, 0};
        colors[ 30] = new float[]{112, 42, 0};
        colors[ 31] = new float[]{114, 43, 0};
        colors[ 32] = new float[]{115, 44, 0};
        colors[ 33] = new float[]{117, 45, 0};
        colors[ 34] = new float[]{119, 47, 0};
        colors[ 35] = new float[]{119, 47, 0};
        colors[ 36] = new float[]{120, 48, 0};
        colors[ 37] = new float[]{122, 49, 0};
        colors[ 38] = new float[]{123, 51, 0};
        colors[ 39] = new float[]{125, 52, 0};
        colors[ 40] = new float[]{125, 52, 0};
        colors[ 41] = new float[]{126, 53, 0};
        colors[ 42] = new float[]{128, 54, 0};
        colors[ 43] = new float[]{129, 56, 0};
        colors[ 44] = new float[]{129, 56, 0};
        colors[ 45] = new float[]{131, 57, 0};
        colors[ 46] = new float[]{132, 58, 0};
        colors[ 47] = new float[]{134, 59, 0};
        colors[ 48] = new float[]{134, 59, 0};
        colors[ 49] = new float[]{136, 61, 0};
        colors[ 50] = new float[]{137, 62, 0};
        colors[ 51] = new float[]{137, 62, 0};
        colors[ 52] = new float[]{139, 63, 0};
        colors[ 53] = new float[]{139, 63, 0};
        colors[ 54] = new float[]{140, 65, 0};
        colors[ 55] = new float[]{142, 66, 0};
        colors[ 56] = new float[]{142, 66, 0};
        colors[ 57] = new float[]{143, 67, 0};
        colors[ 58] = new float[]{143, 67, 0};
        colors[ 59] = new float[]{145, 68, 0};
        colors[ 60] = new float[]{145, 68, 0};
        colors[ 61] = new float[]{146, 70, 0};
        colors[ 62] = new float[]{146, 70, 0};
        colors[ 63] = new float[]{148, 71, 0};
        colors[ 64] = new float[]{148, 71, 0};
        colors[ 65] = new float[]{149, 72, 0};
        colors[ 66] = new float[]{149, 72, 0};
        colors[ 67] = new float[]{151, 73, 0};
        colors[ 68] = new float[]{151, 73, 0};
        colors[ 69] = new float[]{153, 75, 0};
        colors[ 70] = new float[]{153, 75, 0};
        colors[ 71] = new float[]{154, 76, 0};
        colors[ 72] = new float[]{154, 76, 0};
        colors[ 73] = new float[]{154, 76, 0};
        colors[ 74] = new float[]{156, 77, 0};
        colors[ 75] = new float[]{156, 77, 0};
        colors[ 76] = new float[]{157, 79, 0};
        colors[ 77] = new float[]{157, 79, 0};
        colors[ 78] = new float[]{159, 80, 0};
        colors[ 79] = new float[]{159, 80, 0};
        colors[ 80] = new float[]{159, 80, 0};
        colors[ 81] = new float[]{160, 81, 0};
        colors[ 82] = new float[]{160, 81, 0};
        colors[ 83] = new float[]{162, 82, 0};
        colors[ 84] = new float[]{162, 82, 0};
        colors[ 85] = new float[]{163, 84, 0};
        colors[ 86] = new float[]{163, 84, 0};
        colors[ 87] = new float[]{165, 85, 0};
        colors[ 88] = new float[]{165, 85, 0};
        colors[ 89] = new float[]{166, 86, 0};
        colors[ 90] = new float[]{166, 86, 0};
        colors[ 91] = new float[]{166, 86, 0};
        colors[ 92] = new float[]{168, 87, 0};
        colors[ 93] = new float[]{168, 87, 0};
        colors[ 94] = new float[]{170, 89, 0};
        colors[ 95] = new float[]{170, 89, 0};
        colors[ 96] = new float[]{171, 90, 0};
        colors[ 97] = new float[]{171, 90, 0};
        colors[ 98] = new float[]{173, 91, 0};
        colors[ 99] = new float[]{173, 91, 0};
        colors[100] = new float[]{174, 93, 0};
        colors[101] = new float[]{174, 93, 0};
        colors[102] = new float[]{176, 94, 0};
        colors[103] = new float[]{176, 94, 0};
        colors[104] = new float[]{177, 95, 0};
        colors[105] = new float[]{177, 95, 0};
        colors[106] = new float[]{179, 96, 0};
        colors[107] = new float[]{179, 96, 0};
        colors[108] = new float[]{180, 98, 0};
        colors[109] = new float[]{182, 99, 0};
        colors[110] = new float[]{182, 99, 0};
        colors[111] = new float[]{183, 100, 0};
        colors[112] = new float[]{183, 100, 0};
        colors[113] = new float[]{185, 102, 0};
        colors[114] = new float[]{185, 102, 0};
        colors[115] = new float[]{187, 103, 0};
        colors[116] = new float[]{187, 103, 0};
        colors[117] = new float[]{188, 104, 0};
        colors[118] = new float[]{188, 104, 0};
        colors[119] = new float[]{190, 105, 0};
        colors[120] = new float[]{191, 107, 0};
        colors[121] = new float[]{191, 107, 0};
        colors[122] = new float[]{193, 108, 0};
        colors[123] = new float[]{193, 108, 0};
        colors[124] = new float[]{194, 109, 0};
        colors[125] = new float[]{196, 110, 0};
        colors[126] = new float[]{196, 110, 0};
        colors[127] = new float[]{197, 112, 0};
        colors[128] = new float[]{197, 112, 0};
        colors[129] = new float[]{199, 113, 0};
        colors[130] = new float[]{200, 114, 0};
        colors[131] = new float[]{200, 114, 0};
        colors[132] = new float[]{202, 116, 0};
        colors[133] = new float[]{202, 116, 0};
        colors[134] = new float[]{204, 117, 0};
        colors[135] = new float[]{205, 118, 0};
        colors[136] = new float[]{205, 118, 0};
        colors[137] = new float[]{207, 119, 0};
        colors[138] = new float[]{208, 121, 0};
        colors[139] = new float[]{208, 121, 0};
        colors[140] = new float[]{210, 122, 0};
        colors[141] = new float[]{211, 123, 0};
        colors[142] = new float[]{211, 123, 0};
        colors[143] = new float[]{213, 124, 0};
        colors[144] = new float[]{214, 126, 0};
        colors[145] = new float[]{214, 126, 0};
        colors[146] = new float[]{216, 127, 0};
        colors[147] = new float[]{217, 128, 0};
        colors[148] = new float[]{217, 128, 0};
        colors[149] = new float[]{219, 130, 0};
        colors[150] = new float[]{221, 131, 0};
        colors[151] = new float[]{221, 131, 0};
        colors[152] = new float[]{222, 132, 0};
        colors[153] = new float[]{224, 133, 0};
        colors[154] = new float[]{224, 133, 0};
        colors[155] = new float[]{225, 135, 0};
        colors[156] = new float[]{227, 136, 0};
        colors[157] = new float[]{227, 136, 0};
        colors[158] = new float[]{228, 137, 0};
        colors[159] = new float[]{230, 138, 0};
        colors[160] = new float[]{230, 138, 0};
        colors[161] = new float[]{231, 140, 0};
        colors[162] = new float[]{233, 141, 0};
        colors[163] = new float[]{233, 141, 0};
        colors[164] = new float[]{234, 142, 0};
        colors[165] = new float[]{236, 144, 0};
        colors[166] = new float[]{236, 144, 0};
        colors[167] = new float[]{238, 145, 0};
        colors[168] = new float[]{239, 146, 0};
        colors[169] = new float[]{241, 147, 0};
        colors[170] = new float[]{241, 147, 0};
        colors[171] = new float[]{242, 149, 0};
        colors[172] = new float[]{244, 150, 0};
        colors[173] = new float[]{244, 150, 0};
        colors[174] = new float[]{245, 151, 0};
        colors[175] = new float[]{247, 153, 0};
        colors[176] = new float[]{247, 153, 0};
        colors[177] = new float[]{248, 154, 0};
        colors[178] = new float[]{250, 155, 0};
        colors[179] = new float[]{251, 156, 0};
        colors[180] = new float[]{251, 156, 0};
        colors[181] = new float[]{253, 158, 0};
        colors[182] = new float[]{255, 159, 0};
        colors[183] = new float[]{255, 159, 0};
        colors[184] = new float[]{255, 160, 0};
        colors[185] = new float[]{255, 161, 0};
        colors[186] = new float[]{255, 163, 0};
        colors[187] = new float[]{255, 163, 0};
        colors[188] = new float[]{255, 164, 0};
        colors[189] = new float[]{255, 165, 0};
        colors[190] = new float[]{255, 167, 0};
        colors[191] = new float[]{255, 167, 0};
        colors[192] = new float[]{255, 168, 0};
        colors[193] = new float[]{255, 169, 0};
        colors[194] = new float[]{255, 169, 0};
        colors[195] = new float[]{255, 170, 0};
        colors[196] = new float[]{255, 172, 0};
        colors[197] = new float[]{255, 173, 0};
        colors[198] = new float[]{255, 173, 0};
        colors[199] = new float[]{255, 174, 0};
        colors[200] = new float[]{255, 175, 0};
        colors[201] = new float[]{255, 177, 0};
        colors[202] = new float[]{255, 178, 0};
        colors[203] = new float[]{255, 179, 0};
        colors[204] = new float[]{255, 181, 0};
        colors[205] = new float[]{255, 181, 0};
        colors[206] = new float[]{255, 182, 0};
        colors[207] = new float[]{255, 183, 0};
        colors[208] = new float[]{255, 184, 0};
        colors[209] = new float[]{255, 187, 7};
        colors[210] = new float[]{255, 188, 10};
        colors[211] = new float[]{255, 189, 14};
        colors[212] = new float[]{255, 191, 18};
        colors[213] = new float[]{255, 192, 21};
        colors[214] = new float[]{255, 193, 25};
        colors[215] = new float[]{255, 195, 29};
        colors[216] = new float[]{255, 197, 36};
        colors[217] = new float[]{255, 198, 40};
        colors[218] = new float[]{255, 200, 43};
        colors[219] = new float[]{255, 202, 51};
        colors[220] = new float[]{255, 204, 54};
        colors[221] = new float[]{255, 206, 61};
        colors[222] = new float[]{255, 207, 65};
        colors[223] = new float[]{255, 210, 72};
        colors[224] = new float[]{255, 211, 76};
        colors[225] = new float[]{255, 214, 83};
        colors[226] = new float[]{255, 216, 91};
        colors[227] = new float[]{255, 219, 98};
        colors[228] = new float[]{255, 221, 105};
        colors[229] = new float[]{255, 223, 109};
        colors[230] = new float[]{255, 225, 116};
        colors[231] = new float[]{255, 228, 123};
        colors[232] = new float[]{255, 232, 134};
        colors[233] = new float[]{255, 234, 142};
        colors[234] = new float[]{255, 237, 149};
        colors[235] = new float[]{255, 239, 156};
        colors[236] = new float[]{255, 240, 160};
        colors[237] = new float[]{255, 243, 167};
        colors[238] = new float[]{255, 246, 174};
        colors[239] = new float[]{255, 248, 182};
        colors[240] = new float[]{255, 249, 185};
        colors[241] = new float[]{255, 252, 193};
        colors[242] = new float[]{255, 253, 196};
        colors[243] = new float[]{255, 255, 204};
        colors[244] = new float[]{255, 255, 207};
        colors[245] = new float[]{255, 255, 211};
        colors[246] = new float[]{255, 255, 218};
        colors[247] = new float[]{255, 255, 222};
        colors[248] = new float[]{255, 255, 225};
        colors[249] = new float[]{255, 255, 229};
        colors[250] = new float[]{255, 255, 233};
        colors[251] = new float[]{255, 255, 236};
        colors[252] = new float[]{255, 255, 240};
        colors[253] = new float[]{255, 255, 244};
        colors[254] = new float[]{255, 255, 247};
        colors[255] = new float[]{255, 255, 255};
    }

    @Override
    public float[] getColor(float value) {
        float[] retValue = super.getColor(value);
        return new float[]{retValue[0] / 255.0f, retValue[1] / 255.0f, retValue[2] / 255.0f};
    }

}