/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.view.color;

/**
 * This class represents a color table. It is associated to a Color Scale, and
 * returns the colors according to this scale.
 * 
 * @author Fernando Vieira Paulovich
 */
public class ColorTable {

    /** 
     * Creates a new instance of ColorTable. Initially it is associated to a
     * Heated Objetcs color scale.
     */
    public ColorTable() {
        this.colorScale = ColorScaleManager.UNDEFINED;
    }

    /**
     * Returns an RGB color based on a given value. This value must be between
     * 0 and 1. The RGB color values are between 0 and 1 on a float vector. The 
     * first position is the red component, the second is the green component, 
     * and the the third is the blue component of the color. 
     * @param value The color value.
     * @return The RBG color (a float vector with 3 positions).
     */
    public float[] getColor(float value) {
        return this.colorScale.getColor(value);
    }

    /**
     * Returns the number of colors associated to this color scale.
     * @return The numnber of colors.
     */
    public int getNumberColors() {
        return this.colorScale.getNumberColors();
    }

    /**
     * Returns the color scale associated to this color table.
     * @return The color scale
     */
    public ColorScale getColorScale() {
        return this.colorScale;
    }

    /**
     * Changes the color scale associated to this color table.
     * @param colorScale The new color scale.
     */
    public void setColorScale(ColorScale colorScale) {
        this.colorScale = colorScale;
    }

    public ColorScale colorScale;
}
