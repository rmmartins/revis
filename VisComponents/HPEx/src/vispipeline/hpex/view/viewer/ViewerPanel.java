/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.view.viewer;

import com.sun.opengl.util.GLUT;
import java.io.IOException;
import java.util.ArrayList;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLJPanel;

/**
 * This panel is used to drawn graphical elements. This is not automatically 
 * associated with a GLRender object. The subclasses must creates a GLRender
 * and assign it to the 'render' attribute.
 * 
 * @author Fernando Vieira Paulovich
 */
public class ViewerPanel extends GLJPanel {

    /**
     * Creates a new instance of GLPanel
     * @param glcaps the GL parameters are drawn.
     */
    public ViewerPanel(GLCapabilities glcaps) {
        super(glcaps);
        this.renderer = new ViewerRenderer(this);
    }

    /**
     * Saves the projection as a PDF image.
     * @param filename The file name.
     * @throws IOException Throws an exception if something goes wrong.
     */
    public void saveImage(String filename) throws IOException {
        if (this.renderer != null) {
            this.renderer.exportImage(filename);
        }
    }

    /**
     * Returns the renderer associated with this panel.
     * @return The renderer.
     */
    public ViewerRenderer getRenderer() {
        return renderer;
    }

    /**
     * Return the points which will be drawn.
     * @return The points.
     */
    public ArrayList<Point> getPoints() {
        return points;
    }

    /**
     * Changes the points to be drawn.
     * @param points The new points.
     */
    public void setPoints(ArrayList<Point> points) {
        this.points = points;
        this.renderer.updateImage();
    }

    public static class Point {

        public Point() {
            this(0.0f, 0.0f, 0.0f, new float[]{0.5f, 0.5f, 0.5f});
        }

        public Point(float x, float y, float z) {
            this(x, y, z, new float[]{0.5f, 0.5f, 0.5f});
        }

        public Point(float x, float y, float z, float[] color) {
            this.x = x;
            this.y = y;
            this.z = z;
            this.color = color;
        }

        public void draw(GLAutoDrawable gLAutoDrawable) {
            GL gl = gLAutoDrawable.getGL();
            GLUT glut = new GLUT();

            gl.glPushMatrix();
            gl.glColor3fv(color, 0);
            gl.glTranslatef(x, y, z);
//            glut.glutSolidSphere(size, 10, 10);
            glut.glutSolidCube(size);
            gl.glPopMatrix();
        }

        public float x;
        public float y;
        public float z;
        public static float size = 5.0f;
        public float[] color;
    }

    private ArrayList<Point> points;
    private ViewerRenderer renderer;
}
