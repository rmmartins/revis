/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.view.viewer;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class PointsReader {

    public static float[][] read(String filename) throws java.io.IOException {
        float[][] points = null;
        BufferedReader in = null;
        
        try {
            in = new BufferedReader(new java.io.FileReader(filename));
            
            String line = in.readLine(); //read the reader            
            int nrpoints = Integer.parseInt(in.readLine()); //read the number of points
            line = in.readLine(); //read the dimensions
            line = in.readLine(); //read the attributes
            
            //creating the points
            points = new float[nrpoints][];
            int index=0;
            
            //Read the points
            while ((line = in.readLine()) != null) {
                StringTokenizer t = new StringTokenizer(line, ";");
                
                String id = t.nextToken(); //points id
                
                points[index] = new float[4];
                points[index][0] = Float.parseFloat(t.nextToken());
                points[index][1] = Float.parseFloat(t.nextToken());
                points[index][2] = Float.parseFloat(t.nextToken());
                points[index][3] = Float.parseFloat(t.nextToken());
                
                index++;
            }

        } catch (FileNotFoundException e) {
            throw new java.io.IOException("File " + filename + " does not exist!");
        } catch (java.io.IOException e) {
            throw new java.io.IOException("Problems reading the file " + filename + " : " + e.getMessage());
        } finally {
            //fechar o arquivo
            if (in != null) {
                try {
                    in.close();
                } catch (IOException ex) {
                    Logger.getLogger(PointsReader.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return points;
    }

    public static void normalize(float[][] points) {
        Pair[] max_min = new Pair[points[0].length];
        for (int i = 0; i < max_min.length; i++) {
            max_min[i] = new Pair();
        }

        for (int i = 0; i < points.length; i++) {
            for (int j = 0; j < max_min.length; j++) {
                if (points[i][j] > max_min[j].max) {
                    max_min[j].max = points[i][j];
                }

                if (points[i][j] < max_min[j].min) {
                    max_min[j].min = points[i][j];
                }
            }
        }

        for (int i = 0; i < points.length; i++) {
            for (int j = 0; j < max_min.length; j++) {
                if (max_min[j].max > max_min[j].min) {
                    points[i][j] = (points[i][j] - max_min[j].min) /
                            (max_min[j].max - max_min[j].min);

                    if (j < 3) {
                        points[i][j] -= 0.5f;
                    }

                } else {
                    points[i][j] = 0.0f;
                }
            }
        }
    }

    public static class Pair {

        public float max = Float.NEGATIVE_INFINITY;
        public float min = Float.POSITIVE_INFINITY;
    }

}
