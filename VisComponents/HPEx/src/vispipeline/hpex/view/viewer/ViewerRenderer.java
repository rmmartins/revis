/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.view.viewer;

import vispipeline.hpex.view.draw.GLRenderer;
import vispipeline.hpex.view.draw.tridimensional.GLRenderer3D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;
import jogl2ps.GL2PS;

/**
 * This class is an abstract GL renderer.
 * 
 * @author Fernando Vieira Paulovich
 */
public class ViewerRenderer implements GLEventListener {

    /**
     * Creates a new instance of GLRenderer and associates it with GLPanel.
     * @param panel GLPanel panel associated with this renderer.
     */
    public ViewerRenderer(ViewerPanel panel) {
        this.panel = panel;
        this.trackball = new ViewerTrackball(panel);

        if (this.panel != null) {
            panel.addGLEventListener(this);
            ZoomListener zoomListener = new ZoomListener(panel);
            PanListener panListener = new PanListener(panel);
        }

        this.glu = new GLU();
    }

    /**
     * Reset the transformations parameters, so that none transformation is 
     * applied to the projection image. Also, it only draws the first clusters
     * of the hierarchy. Do not forget to call redisplay() method to change 
     * the image.
     */
    public void reset() {
        this.panx = 0;
        this.pany = 0;

        this.win = GLRenderer.WINDOW_SIZE;
        this.rotmat = null;
        this.trackball = new ViewerTrackball(this.panel);

        this.viewport = new int[4];
        this.mvmatrix = new double[16];
        this.projmatrix = new double[16];

        this.redisplay = true;
        this.updateImage = true;

        if (gLAutoDrawable != null) {
            gLAutoDrawable.getContext().makeCurrent();
            defineVisualParameters(gLAutoDrawable);
            gLAutoDrawable.getContext().release();
        }
    }

    /**
     * This method forces the projection image to be redisplayed. This does not
     * recreates the image. The image will only be different if some 
     * transformation was applied to the projection, such as zoom or pan.
     */
    public void redisplay() {
        this.redisplay = true;
    }

    /**
     * This method forces the projection image to be recreated. It only needs 
     * to be used if the projection was changed. For instance, if the colors 
     * were changed, if a graphical element was added or removed. If only
     * transformations were applied (zoom or pan), the redisplay() method is
     * indicated. This methods also forces a redisplay.
     */
    public void updateImage() {
        this.redisplay = true;
        this.updateImage = true;
    }

    /**
     * Export the image as a PDF file.
     * @param filename The file name.
     */
    public void exportImage(String filename) {
        GL2PS jogl2ps = new GL2PS();

        jogl2ps.openFile(filename);
        int state = GL2PS.GL2PS_OVERFLOW, buffsize = 0;

        gLAutoDrawable.getContext().makeCurrent();

        while (state == GL2PS.GL2PS_OVERFLOW) {
            buffsize += 1024 * 1024;

            jogl2ps.gl2psBeginPage("Image exported from H-PEx",
                    "Fernando Vieira Paulovich",
                    null,
                    GL2PS.GL2PS_PDF,
                    GL2PS.GL2PS_SIMPLE_SORT,
                    GL2PS.GL2PS_DRAW_BACKGROUND | GL2PS.GL2PS_USE_CURRENT_VIEWPORT,
                    GL.GL_RGBA,
                    0,
                    null,
                    0,
                    0,
                    0,
                    buffsize,
                    filename);

            if (gLAutoDrawable != null) {
                ArrayList<ViewerPanel.Point> points = this.panel.getPoints();
                int size = points.size();

                for (int i = 0; i < size; i++) {
                    points.get(i).draw(gLAutoDrawable);
                }
            }

            state = jogl2ps.gl2psEndPage();
        }

        gLAutoDrawable.getContext().release();

        jogl2ps.closeFile();
    }

    /**
     * Draws the graphical elements on the GLContext.
     * @param gLAutoDrawable The GLAutoDrawable where the objects are drawn.
     */
    public void createImage(GLAutoDrawable gLAutoDrawable) {
        ArrayList<ViewerPanel.Point> points = this.panel.getPoints();

        if (points != null) {
            GL gl = gLAutoDrawable.getGL();

            gl.glNewList(this.imageId, gl.GL_COMPILE);

            //draw the points            
            int size = points.size();

            for (int i = 0; i < size; i++) {
                points.get(i).draw(gLAutoDrawable);
            }

            gl.glEndList();
        }
    }

    /**
     * It is a JOGL method where the visualization parameters are initialized.
     * @param gLAutoDrawable the GL context
     */
    public void init(GLAutoDrawable gLAutoDrawable) {
        this.gLAutoDrawable = gLAutoDrawable;
        GL gl = gLAutoDrawable.getGL();

        gl.glEnable(GL.GL_CULL_FACE);
        gl.glEnable(GL.GL_LIGHTING);
        gl.glEnable(GL.GL_LIGHT0);
        gl.glEnable(GL.GL_DEPTH_TEST);
        gl.glEnable(GL.GL_COLOR_MATERIAL);
        gl.glShadeModel(GL.GL_SMOOTH);

        //creating the cluster list
        this.imageId = gl.glGenLists(1);

        this.lighting(gl);

        //anti-aliasing enabling
        gl.glEnable(GL.GL_BLEND);
        gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);

        gl.glEnable(GL.GL_LINE_SMOOTH);
        gl.glHint(GL.GL_LINE_SMOOTH_HINT, GL.GL_NICEST);
    }

    public void displayChanged(GLAutoDrawable gLAutoDrawable, boolean b, boolean b0) {
    }

    /**
     * This is a JOGL method called to draw the objects.
     * @param gLAutoDrawable the GL context
     */
    public void display(GLAutoDrawable gLAutoDrawable) {
        if (this.redisplay) {
            this.gLAutoDrawable = gLAutoDrawable;
            GL gl = gLAutoDrawable.getGL();

            gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f); //backgroung color
            gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

            gl.glMatrixMode(GL.GL_MODELVIEW);
            gl.glLoadIdentity();

            this.rotmat = trackball.getRotMatrix(this.rotmat);
            if (this.rotmat != null) {
                gl.glMultMatrixf(this.rotmat, 0);
            }

            //used for gluUnproject
            gl.glGetIntegerv(GL.GL_VIEWPORT, this.viewport, 0);
            gl.glGetDoublev(GL.GL_MODELVIEW_MATRIX, this.mvmatrix, 0);
            gl.glGetDoublev(GL.GL_PROJECTION_MATRIX, this.projmatrix, 0);

            if (this.updateImage) {
                this.createImage(gLAutoDrawable);
                this.updateImage = false;
            }

            gl.glCallList(this.imageId);

            gl.glFlush(); //execute all commands

            this.redisplay = false;
        }
    }

    /**
     * This is a JOGL method called when the window is resized. It keeps the 
     * aspect ratio of the objects on the resized window.
     * @param gLAutoDrawable the GL context
     * @param x the left coordinate of the window
     * @param y the bottom coordinate of the window
     * @param w the window's width
     * @param h the window's height
     */
    public void reshape(GLAutoDrawable gLAutoDrawable, int x, int y, int w, int h) {
        this.gLAutoDrawable = gLAutoDrawable;
        GL gl = gLAutoDrawable.getGL();

        if (h == 0) {
            h = 1;
        }
        this.width = w;
        this.height = h;

        gl.glViewport(0, 0, w, h);

        if (gLAutoDrawable != null) {
            defineVisualParameters(gLAutoDrawable);
        }

        this.redisplay = true;
        this.updateImage = true;
    }

    private void lighting(GL gl) {
        float[] luzAmbiente = {0.3f, 0.3f, 0.3f, 1.0f};
        float[] especularidade = {0.8f, 0.8f, 0.8f, 1.0f};
        float especMaterial = 15f; //between 0 and 128 - brightness concentration

        gl.glMaterialfv(GL.GL_FRONT, GL.GL_SPECULAR, especularidade, 0);
        gl.glMaterialf(GL.GL_FRONT, GL.GL_SHININESS, especMaterial);
        gl.glLightModelfv(GL.GL_LIGHT_MODEL_AMBIENT, luzAmbiente, 0);

        //Define a luz 1
        float[] luzDifusa = new float[]{0.75f, 0.75f, 0.75f, 1.0f};// "cor"
        float[] luzEspecular = new float[]{0.7f, 0.7f, 0.7f, 1.0f};// "brilho"
        float[] posicaoLuz = new float[]{GLRenderer3D.WINDOW_SIZE * 5.0f,
            GLRenderer3D.WINDOW_SIZE * 5.0f, GLRenderer3D.WINDOW_SIZE * 5.0f, 1.0f
        };

        // Define os par�metros da luz de n�mero 0
        gl.glLightfv(GL.GL_LIGHT0, GL.GL_AMBIENT, luzAmbiente, 0);
        gl.glLightfv(GL.GL_LIGHT0, GL.GL_DIFFUSE, luzDifusa, 0);
        gl.glLightfv(GL.GL_LIGHT0, GL.GL_SPECULAR, luzEspecular, 0);
        gl.glLightfv(GL.GL_LIGHT0, GL.GL_POSITION, posicaoLuz, 0);
    }

    private void defineVisualParameters(GLAutoDrawable gLAutoDrawable) {
        this.gLAutoDrawable = gLAutoDrawable;

        GL gl = gLAutoDrawable.getGL();

        gl.glMatrixMode(GL.GL_PROJECTION);
        gl.glLoadIdentity();

        if (width <= height) {
            gl.glOrtho(-win + panx, win + panx, -win * (height / width) + pany,
                    win * (height / width) + pany, -GLRenderer3D.WINDOW_SIZE * 10.0f,
                    GLRenderer3D.WINDOW_SIZE * 10.0f);
        } else {
            gl.glOrtho(-win * (width / height) + panx, win * (width / height) + panx,
                    -win + pany, win + pany, -GLRenderer3D.WINDOW_SIZE * 10.0f,
                    GLRenderer3D.WINDOW_SIZE * 10.0f);
        }
    }

    /**
     * This class implements the Zoom listener.
     */
    public class ZoomListener extends MouseAdapter implements MouseMotionListener {

        /** Creates a new instance of ZoomListener
         * @param panel The panel which this listener is attached.
         */
        public ZoomListener(ViewerPanel panel) {
            panel.addMouseMotionListener(this);
            panel.addMouseListener(this);
        }

        @Override
        public void mouseMoved(MouseEvent e) {
        }

        @Override
        public void mouseDragged(MouseEvent e) {
            if (this.execute) {
                if (e.getY() > this.prevy) {
                    this.prevy = e.getY();
                    if (win < GLRenderer.WINDOW_SIZE * 10) {
                        win *= 1.10f;
                    }
                } else {
                    this.prevy = e.getY();
                    if (win > 0) {
                        win *= 0.809f;
                    }
                }

                if (gLAutoDrawable != null) {
                    gLAutoDrawable.getContext().makeCurrent();
                    defineVisualParameters(gLAutoDrawable);
                    gLAutoDrawable.getContext().release();
                }

                redisplay = true;
                if (panel != null) {
                    panel.display();
                }
            }
        }

        @Override
        public void mousePressed(MouseEvent e) {
            super.mousePressed(e);
            if (e.getButton() == MouseEvent.BUTTON3) {
                this.prevy = e.getY();
                this.execute = true;
            } else {
                this.execute = false;
            }
        }

        private boolean execute = false;
        private int prevy = 0;
    }

    /**
     * This class implements a Pan listener.
     */
    public class PanListener extends MouseAdapter implements MouseMotionListener {

        /** Creates a new instance of PanListener
         * @param panel The panel which this listener is attached.
         */
        public PanListener(ViewerPanel panel) {
            panel.addMouseListener(this);
            panel.addMouseMotionListener(this);
        }

        @Override
        public void mouseMoved(MouseEvent e) {
        }

        @Override
        public void mouseDragged(MouseEvent e) {
            if (this.execute) {
                float diffx = ((float) (e.getX() - this.prevx));
                this.prevx = e.getX();
                panx -= diffx;

                float diffy = ((float) (e.getY() - this.prevy));
                this.prevy = e.getY();
                pany += diffy;


                if (gLAutoDrawable != null) {
                    gLAutoDrawable.getContext().makeCurrent();
                    defineVisualParameters(gLAutoDrawable);
                    gLAutoDrawable.getContext().release();
                }

                redisplay = true;
                if (panel != null) {
                    panel.display();
                }
            }
        }

        @Override
        public void mousePressed(MouseEvent e) {
            super.mousePressed(e);
            if (e.getButton() == MouseEvent.BUTTON2) {
                this.prevx = e.getX();
                this.prevy = e.getY();
                this.execute = true;
            } else {
                this.execute = false;
            }
        }

        private boolean execute = false;
        private int prevy = 0;
        private int prevx = 0;
    }

    public static final float WINDOW_SIZE = 1000.0f;
    private boolean redisplay = true;
    private boolean updateImage = true;
    private ViewerPanel panel;
    private int imageId;
    private float win;
    private float panx;
    private float pany;
    private float height;
    private float width;
    private ViewerTrackball trackball;
    private float rotmat[];
    private GLAutoDrawable gLAutoDrawable;
    private GLU glu;
    private int viewport[] = new int[4];
    private double mvmatrix[] = new double[16];
    private double projmatrix[] = new double[16];
}
