/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.view;

import vispipeline.hpex.util.SystemPropertiesManager;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class Splash extends javax.swing.JFrame {

    static {
        splash = new Splash();
    }

    private Splash() {
        this.setVisible(false);
        this.setUndecorated(true);
        this.setAlwaysOnTop(true);

        SplashPanel sp = new SplashPanel();
        this.getContentPane().setLayout(new BorderLayout());
        this.getContentPane().add(sp, BorderLayout.CENTER);
        this.setSize(sp.getWidth(), sp.getHeight());

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension size = this.getSize();
        
        this.setLocation((screenSize.width - size.width) / 2,
                (screenSize.height - size.height) / 2);
    }

    public static Splash getInstance() {
        return splash;
    }

    public void start() {
        this.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        this.setVisible(true);
    }

    public void stop() {
        this.setVisible(false);
        this.dispose();
    }

    public static class SplashPanel extends javax.swing.JPanel {

        public SplashPanel() {
            MediaTracker media = new MediaTracker(this);
            toolkit = Toolkit.getDefaultToolkit();
            image = toolkit.getImage(SplashPanel.class.getResource(imgName));

            if (image != null) {
                try {
                    media.addImage(image, 0);
                    media.waitForID(0);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Splash.class.getName()).log(Level.SEVERE, 
                            null, ex);
                }
            }

            this.setPreferredSize(new java.awt.Dimension(image.getWidth(this),
                    image.getHeight(this)));
            this.setSize(image.getWidth(this), image.getHeight(this));
        }

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            g.drawImage(image, 0, 0, this.getBackground(), this);

            Graphics2D g2 = (Graphics2D) g;
            g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

            g.setColor(Color.DARK_GRAY);
            String version = SystemPropertiesManager.getInstance().getProperty("TOOL.VERSION");
            g.setFont(new Font("Verdana", Font.BOLD, 14));
            g.drawString(version, 265, 92);
        }

        private String imgName = "/splash/welcome.png";
        private Toolkit toolkit;
        private Image image;
    }

    private static Splash splash;
}
