/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.view.tools;

import vispipeline.hpex.projection.Parameters;
import vispipeline.hpex.textpreprocess.stopwords.StopwordsList;
import vispipeline.hpex.textpreprocess.stopwords.StopwordsListManager;
import vispipeline.hpex.util.SaveDialog;
import vispipeline.hpex.util.filefilter.STPFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class StopwordsManager extends javax.swing.JDialog {

    /** Creates new form StopwordsManager */
    private StopwordsManager(java.awt.Frame parent) {
        super(parent);
        initComponents();
    }

    private StopwordsManager(java.awt.Dialog parent) {
        super(parent);
        initComponents();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        stopwordsPanel = new javax.swing.JPanel();
        addStopwordsPanel = new javax.swing.JPanel();
        addButton = new javax.swing.JButton();
        newScrollPane = new javax.swing.JScrollPane();
        newTable = new javax.swing.JTable();
        currentStopwordsPanel = new javax.swing.JPanel();
        stopwordsScrollPane = new javax.swing.JScrollPane();
        stopwordsTable = new javax.swing.JTable();
        removeButton = new javax.swing.JButton();
        mergePanel = new javax.swing.JPanel();
        mergeButton = new javax.swing.JButton();
        filePanel = new javax.swing.JPanel();
        stopwordsComboBox = new javax.swing.JComboBox();
        refreshButton = new javax.swing.JButton();
        buttonPanel = new javax.swing.JPanel();
        saveButton = new javax.swing.JButton();
        closeButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Stopwords List Manager");
        setModal(true);

        stopwordsPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Stopwords Manager"));
        stopwordsPanel.setLayout(new java.awt.BorderLayout(5, 5));

        addStopwordsPanel.setLayout(new java.awt.BorderLayout());

        addButton.setText("New");
        addButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addButtonActionPerformed(evt);
            }
        });
        addStopwordsPanel.add(addButton, java.awt.BorderLayout.SOUTH);

        newScrollPane.setBorder(javax.swing.BorderFactory.createTitledBorder("New Stopwords"));
        newScrollPane.setPreferredSize(new java.awt.Dimension(200, 350));
        newScrollPane.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                newScrollPaneMouseClicked(evt);
            }
        });
        newScrollPane.setViewportView(newTable);

        addStopwordsPanel.add(newScrollPane, java.awt.BorderLayout.CENTER);

        stopwordsPanel.add(addStopwordsPanel, java.awt.BorderLayout.EAST);

        currentStopwordsPanel.setLayout(new java.awt.BorderLayout());

        stopwordsScrollPane.setBorder(javax.swing.BorderFactory.createTitledBorder("Current Stopwords"));
        stopwordsScrollPane.setPreferredSize(new java.awt.Dimension(200, 350));
        stopwordsScrollPane.setViewportView(stopwordsTable);

        currentStopwordsPanel.add(stopwordsScrollPane, java.awt.BorderLayout.CENTER);

        removeButton.setText("Remove");
        removeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeButtonActionPerformed(evt);
            }
        });
        currentStopwordsPanel.add(removeButton, java.awt.BorderLayout.SOUTH);

        stopwordsPanel.add(currentStopwordsPanel, java.awt.BorderLayout.WEST);

        mergePanel.setLayout(new java.awt.GridBagLayout());

        mergeButton.setText("<< Merge");
        mergeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mergeButtonActionPerformed(evt);
            }
        });
        mergePanel.add(mergeButton, new java.awt.GridBagConstraints());

        stopwordsPanel.add(mergePanel, java.awt.BorderLayout.CENTER);

        filePanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Stopwords List"));
        filePanel.setLayout(new java.awt.BorderLayout(5, 5));

        stopwordsComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stopwordsComboBoxActionPerformed(evt);
            }
        });
        filePanel.add(stopwordsComboBox, java.awt.BorderLayout.CENTER);

        refreshButton.setText("Refresh");
        refreshButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                refreshButtonActionPerformed(evt);
            }
        });
        filePanel.add(refreshButton, java.awt.BorderLayout.EAST);

        stopwordsPanel.add(filePanel, java.awt.BorderLayout.NORTH);

        getContentPane().add(stopwordsPanel, java.awt.BorderLayout.CENTER);

        saveButton.setText("Save");
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(saveButton);

        closeButton.setText("Close");
        closeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                closeButtonActionPerformed(evt);
            }
        });
        buttonPanel.add(closeButton);

        getContentPane().add(buttonPanel, java.awt.BorderLayout.SOUTH);

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private void removeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeButtonActionPerformed
        int index = this.stopwordsTable.getSelectedRow();

        if (index != -1) {
            String stopword = (String) this.currentStopwodsTableModel.getValueAt(index, 0);
            this.currentStopwodsTableModel.removeRow(index);

            StopwordsList stp = (StopwordsList) this.stopwordsComboBox.getSelectedItem();
            stp.removeStopword(stopword);
        }
    }//GEN-LAST:event_removeButtonActionPerformed

    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
        try {
            StopwordsList curr = (StopwordsList) this.stopwordsComboBox.getSelectedItem();
            StopwordsList stp = (StopwordsList) curr.clone();
            String filename = "stopwords.stp";
            String directory = "./config";

            if (stp != null) {
                String name = JOptionPane.showInputDialog(this, 
                        "Enter the new stopwords list name:", stp.toString());

                if (name != null) {
                    int result = SaveDialog.showSaveDialog(new STPFilter(), this, 
                            directory, filename);

                    if (result == JFileChooser.APPROVE_OPTION) {
                        try {
                            //save the new stopwords list
                            filename = SaveDialog.getFilename();
                            stp.setName(name);
                            stp.save(filename);

                            //update the stopwords lists
                            StopwordsListManager.getInstance().updateStopwordslists();

                            //update the combo stopwords lists
                            this.stopwordsComboBox.removeAllItems();
                            for (StopwordsList s : StopwordsListManager.getInstance().getStopwordsLists()) {
                                this.stopwordsComboBox.addItem(s);
                            }

                            //selecting the new stopwordlist
                            for (int i = 0; i < this.stopwordsComboBox.getItemCount(); i++) {
                                if (this.stopwordsComboBox.getItemAt(i).toString().equals(name)) {
                                    this.stopwordsComboBox.setSelectedItem(this.stopwordsComboBox.getItemAt(i));
                                }
                            }
                        } catch (IOException ex) {
                            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(StopwordsManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_saveButtonActionPerformed

    private void newScrollPaneMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_newScrollPaneMouseClicked
        if (evt.getClickCount() == 2) {
            this.addButtonActionPerformed(null);
        }
    }//GEN-LAST:event_newScrollPaneMouseClicked

    private void mergeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mergeButtonActionPerformed
        ArrayList<String> stopwords = new ArrayList<String>();
        this.newTable.editCellAt(0, 0);

        for (int i = 0; i < this.newStopwodsTableModel.getRowCount(); i++) {
            String value = (String) this.newStopwodsTableModel.getValueAt(i, 0);

            if (value.trim().length() > 0) {
                stopwords.add(value);
            }
        }

        StopwordsList stp = (StopwordsList) this.stopwordsComboBox.getSelectedItem();
        stp.addStopwords(stopwords);

        this.initModels();
        this.stopwordsTable.setModel(this.currentStopwodsTableModel);
        this.newTable.setModel(this.newStopwodsTableModel);

        for (String stopword : stp.getStopwords()) {
            String[] row = new String[1];
            row[0] = stopword;
            this.currentStopwodsTableModel.addRow(row);
        }
        
    }//GEN-LAST:event_mergeButtonActionPerformed

    private void addButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addButtonActionPerformed
        this.newStopwodsTableModel.addRow(new String[]{""});
        this.newTable.editCellAt(this.newStopwodsTableModel.getRowCount() - 1, 0);
        this.newTable.requestFocusInWindow();
    }//GEN-LAST:event_addButtonActionPerformed

    private void closeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_closeButtonActionPerformed
        StopwordsList stp = (StopwordsList) this.stopwordsComboBox.getSelectedItem();

        if (this.par != null && stp != null) {
            this.par.stopwordsList = stp;
        }

        this.setVisible(false);
    }//GEN-LAST:event_closeButtonActionPerformed

    private void stopwordsComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stopwordsComboBoxActionPerformed
        StopwordsList stp = (StopwordsList) this.stopwordsComboBox.getSelectedItem();

        if (this.par != null && stp != null) {
            this.par.stopwordsList = stp;

            this.initModels();
            this.stopwordsTable.setModel(this.currentStopwodsTableModel);
            this.newTable.setModel(this.newStopwodsTableModel);

            for (String stopword : this.par.stopwordsList.getStopwords()) {
                String[] row = new String[1];
                row[0] = stopword;
                this.currentStopwodsTableModel.addRow(row);
            }
        }
    }//GEN-LAST:event_stopwordsComboBoxActionPerformed

    private void refreshButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_refreshButtonActionPerformed
        StopwordsList stp = (StopwordsList) this.stopwordsComboBox.getSelectedItem();

        if (stp != null) {
            try {
                stp.refresh();

                this.initModels();
                this.stopwordsTable.setModel(this.currentStopwodsTableModel);
                this.newTable.setModel(this.newStopwodsTableModel);

                for (String stopword : this.par.stopwordsList.getStopwords()) {
                    String[] row = new String[1];
                    row[0] = stopword;
                    this.currentStopwodsTableModel.addRow(row);
                }
            } catch (IOException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_refreshButtonActionPerformed

    public static StopwordsManager getInstance(java.awt.Container parent) {
        if (instance == null || instance.getParent() != parent) {
            if (parent instanceof javax.swing.JFrame) {
                instance = new StopwordsManager((javax.swing.JFrame) parent);
            } else {
                instance = new StopwordsManager((javax.swing.JDialog) parent);
            }
        }

        return instance;
    }

    public void show(Parameters par) {
        this.par = null;

        this.initModels();
        this.stopwordsTable.setModel(this.currentStopwodsTableModel);
        this.newTable.setModel(this.newStopwodsTableModel);

        //loading all stopwords lists
        this.stopwordsComboBox.removeAllItems();
        for (StopwordsList stp : StopwordsListManager.getInstance().getStopwordsLists()) {
            this.stopwordsComboBox.addItem(stp);
        }

        this.par = par;
        this.stopwordsComboBox.setSelectedItem(par.stopwordsList);

        this.setLocationRelativeTo(this.getParent());
        this.setVisible(true);
    }

    private void initModels() {
        String[] titulos = new String[]{"Stopwords"};
        this.currentStopwodsTableModel = new DefaultTableModel(null, titulos);
        this.newStopwodsTableModel = new DefaultTableModel(null, titulos);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new StopwordsManager(new javax.swing.JFrame()).setVisible(true);
            }

        });
    }

    private Parameters par;
    private static StopwordsManager instance;
    private DefaultTableModel currentStopwodsTableModel;
    private DefaultTableModel newStopwodsTableModel;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addButton;
    private javax.swing.JPanel addStopwordsPanel;
    private javax.swing.JPanel buttonPanel;
    private javax.swing.JButton closeButton;
    private javax.swing.JPanel currentStopwordsPanel;
    private javax.swing.JPanel filePanel;
    private javax.swing.JButton mergeButton;
    private javax.swing.JPanel mergePanel;
    private javax.swing.JScrollPane newScrollPane;
    private javax.swing.JTable newTable;
    private javax.swing.JButton refreshButton;
    private javax.swing.JButton removeButton;
    private javax.swing.JButton saveButton;
    private javax.swing.JComboBox stopwordsComboBox;
    private javax.swing.JPanel stopwordsPanel;
    private javax.swing.JScrollPane stopwordsScrollPane;
    private javax.swing.JTable stopwordsTable;
    // End of variables declaration//GEN-END:variables
}
