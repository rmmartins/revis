/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.view.draw;

import vispipeline.hpex.util.Instance;
import vispipeline.hpex.view.draw.bidimensional.DataInstanceHelper2D;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import vispipeline.matrix.sparse.SparseVector;

/**
 * This class represents a data instance graphical element.
 * 
 * @author Fernando Vieira Paulovich
 */
public class DataInstanceElement extends GraphicalElement implements Instance {

    /**
     * Creates a new instance of DataInstanceGraphicalElement
     * @param vector The data instance (a sparse vector).
     * @param vtopic The vector used to extract the topic.
     */
    public DataInstanceElement(SparseVector vector, SparseVector vtopic) {
        this.vector = vector;
        this.vtopic = vtopic;
    }

    public SparseVector getTopicVector() {
        return this.vtopic;
    }

    public SparseVector getVector() {
        return this.vector;
    }

    public ElementHelper getElementHelper() {
        return helper;
    }

    /**
     * Changes the helper associated to the DataInstanceElement objects.
     * @param aHelper The new Data Instance helper.
     */
    public static void setDataInstaneHelper(ElementHelper aHelper) {
        helper = aHelper;
    }

    /**
     * Serializes the static attributes. This method is neccessay since the 
     * static attributes are not automatically serialized on Java. This method 
     * must be called just after a hierarchy be serialized.
     * @param os The stream to write the attribute.
     * @throws IOException Throws an exception if something goes wrong.
     */
    public static void serializeStaticState(ObjectOutputStream os) throws IOException {
        os.writeObject(helper);
    }

    /**
     * De-serializes the static attributes. This method is neccessay since the 
     * static attributes are not automatically de-serialized on Java. This method 
     * must be called just after a hierarchy be de-serialized.
     * @param os The stream to read the attribute.
     * @throws IOException Throws an exception if something goes wrong.
     */
    public static void deserializeStaticState(ObjectInputStream os) throws IOException {
        try {
            helper = (ElementHelper) os.readObject();
        } catch (ClassNotFoundException ex) {
            throw new IOException(ex.getMessage());
        }
    }

    @Override
    public String toString() {
        return this.title;
    }

    protected SparseVector vector;
    protected SparseVector vtopic;
    private static ElementHelper helper = new DataInstanceHelper2D();
    private static final long serialVersionUID = 27L;
}
