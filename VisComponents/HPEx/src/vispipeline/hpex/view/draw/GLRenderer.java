/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.view.draw;

import com.sun.opengl.util.GLUT;
import vispipeline.hpex.view.color.ColorTable;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;
import jogl2ps.GL2PS;

/**
 * This class is an abstract GL renderer.
 * 
 * @author Fernando Vieira Paulovich
 */
public abstract class GLRenderer implements GLEventListener {

    /**
     * Creates a new instance of GLRenderer and associates it with GLPanel.
     * @param panel GLPanel panel associated with this renderer.
     */
    public GLRenderer(GLPanel panel) {
        this.panel = panel;

        this.glut = new GLUT();
        this.glu = new GLU();
    }

    /**
     * Draws the lines to join the clusters.
     * @param drawable The context to draw.
     */
    public abstract void drawJoinLines(GLAutoDrawable drawable);

    /**
     * Reset the transformations parameters, so that none transformation is 
     * applied to the projection image. Do not forget to call redisplay()
     * method to change the image.
     */
    public abstract void reset();

    protected abstract void defineVisualParameters(GLAutoDrawable gLAutoDrawable);

    public float getWin() {
        return win;
    }

    public void setWin(float win) {
        this.win = win;
    }

    public float getPanx() {
        return panx;
    }

    public void setPanx(float panx) {
        this.panx = panx;
    }

    public float getPany() {
        return pany;
    }

    public void setPany(float pany) {
        this.pany = pany;
    }

    /**
     * This method forces the projection image to be redisplayed. This does not
     * recreates the image. The image will only be different if some 
     * transformation was applied to the projection, such as zoom or pan.
     */
    public void redisplay() {
        this.redisplay = true;

        if (drawable != null) {
            drawable.getContext().makeCurrent();
            this.defineVisualParameters(drawable);
            drawable.getContext().release();
        }
    }

    /**
     * This method forces the projection image to be recreated. It only needs 
     * to be used if the projection was changed. For instance, if the colors 
     * were changed, if a graphical element was added or removed. If only
     * transformations were applied (zoom or pan), the redisplay() method is
     * indicated. This methods also forces a redisplay.
     */
    public void updateImage() {
        this.redisplay();
        this.updateImage = true;
    }

    /**
     * Returns the backround scheme color of this render. 
     * @return The background scheme color.
     */
    public Background.Color getBackgroundColor() {
        return bkgColor;
    }

    /**
     * Changes the background scheme color of this render.
     * @param bkgColor The new background scheme color.
     */
    public void setBackgroundColor(Background.Color bkgColor) {
        this.bkgColor = bkgColor;
    }

    /**
     * Draws the graphical elements on the GLContext.
     * @param drawable The GLAutoDrawable where the objects are drawn.
     */
    public void createImage(GLAutoDrawable drawable) {
        GL gl = drawable.getGL();
        ClusterHierarchy hierarchy = this.panel.getHierarchy();

        if (hierarchy != null) {
            gl.glNewList(this.imageId, gl.GL_COMPILE);
            hierarchy.draw(false, bkgColor, drawable);
            gl.glEndList();

            gl.glNewList(this.overviewId, gl.GL_COMPILE);
            hierarchy.draw(true, bkgColor, drawable);
            gl.glEndList();
        }
    }

    public void exportImage(String filename) {
        GL2PS jogl2ps = new GL2PS();
        ClusterHierarchy hierarchy = this.panel.getHierarchy();

        jogl2ps.openFile(filename);
        int state = GL2PS.GL2PS_OVERFLOW, buffsize = 0;

        drawable.getContext().makeCurrent();

        while (state == GL2PS.GL2PS_OVERFLOW) {
            buffsize += 1024 * 1024;

            jogl2ps.gl2psBeginPage("Image exported from H-PEx",
                    "Fernando Vieira Paulovich",
                    null,
                    GL2PS.GL2PS_PDF,
                    GL2PS.GL2PS_SIMPLE_SORT,
                    GL2PS.GL2PS_DRAW_BACKGROUND | GL2PS.GL2PS_USE_CURRENT_VIEWPORT,
                    GL.GL_RGBA,
                    0,
                    null,
                    0,
                    0,
                    0,
                    buffsize,
                    filename);

            if (hierarchy != null) {
                hierarchy.draw(false, bkgColor, drawable);
                this.drawColorScale(panel.getColorTable(), drawable);
                this.drawJoinLines(drawable);
            }

            state = jogl2ps.gl2psEndPage();
        }

        drawable.getContext().release();

        jogl2ps.closeFile();
    }

    private void drawColorScale(ColorTable table, GLAutoDrawable drawable) {
        GL gl = drawable.getGL();
        GL2PS jogl2ps = new GL2PS();
        gl.glMatrixMode(GL.GL_MODELVIEW);
        gl.glPushMatrix();
        gl.glLoadIdentity();

        boolean lighting = gl.glIsEnabled(GL.GL_LIGHTING);
        gl.glDisable(GL.GL_LIGHTING);

        float ini_x = -win * (width / height) + panx;
        float ini_y = win + pany;
        float size = win / 2.0f;

        if (width < height) {
            ini_x = -win + panx;
            ini_y = win * (height / width) + pany;
        }

        //drawing the min and max legend
        gl.glColor3fv(bkgColor.getClusterColor(), 0);
        gl.glRasterPos3f(ini_x + size * 0.15f, ini_y - size * 0.125f, 1.0f);
        jogl2ps.gl2psText("min", "Helvetica", 12);
        glut.glutBitmapString(GLUT.BITMAP_HELVETICA_12, "min");

        gl.glRasterPos3f(ini_x + size * 1.35f, ini_y - size * 0.125f, 1.0f);
        jogl2ps.gl2psText("max", "Helvetica", 12);
        glut.glutBitmapString(GLUT.BITMAP_HELVETICA_12, "max");

        //drawig the scale
        gl.glTranslatef(ini_x + size * 0.8f, ini_y - size * 0.1f, 0.0f);
        gl.glScalef(size, size * 0.07f, 0);

        for (int i = 0; i < 100; i++) {
            float index1 = ((float) i) / ((float) 100);
            float index2 = ((float) i + 1) / ((float) 100);

            float[] color = table.getColor(index1);
            gl.glColor3f(color[0], color[1], color[2]);

            gl.glBegin(GL.GL_QUADS);
            gl.glVertex2f(index1 - 0.5f, -0.5f);
            gl.glVertex2f(index2 - 0.5f, -0.5f);
            gl.glVertex2f(index2 - 0.5f, 0.5f);
            gl.glVertex2f(index1 - 0.5f, 0.5f);
            gl.glEnd();
        }

        gl.glPopMatrix();

        if (lighting) {
            gl.glEnable(GL.GL_LIGHTING);
        }
    }

    public static final float WINDOW_SIZE = 1000.0f;
    protected Background.Color bkgColor = Background.GRAY;
    protected boolean redisplay = true;
    protected boolean updateImage = true;
    protected GLPanel panel;
    protected int imageId;
    protected int overviewId;
    protected GLAutoDrawable drawable;
    protected GLUT glut;
    protected GLU glu;
    protected float win;
    protected float panx;
    protected float pany;
    protected float height;
    protected float width;
}
