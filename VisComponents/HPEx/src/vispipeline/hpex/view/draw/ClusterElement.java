/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.view.draw;

import vispipeline.hpex.util.Cluster;
import vispipeline.hpex.view.draw.bidimensional.ClusterHelper2D;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import vispipeline.matrix.sparse.SparseVector;

/**
 * This class represents a cluster graphical element.
 * 
 * @author Fernando Vieira Paulovich
 */
public class ClusterElement extends GraphicalElement {

    /**
     * This enumeration represents the projection state of the gchildren of 
     * this cluster. It can be: PROJECTED, indicating that its gchildren are
     * already project; NOT_PROJECTED, indicating that its gchildren is not 
     * projected.
     */
    public static enum ChildrenProjectionState {

        PROJECTED, NOT_PROJECTED
    }

    /**
     * Creates a new instance of ClusterGraphicalElement
     * @param cluster The cluster that this graphical element represents.
     */
    public ClusterElement(Cluster cluster) {
        this.cluster = cluster;
    }

    public ChildrenProjectionState getChildrenProjectionState() {
        return childrenProjState;
    }

    public void setChildrenProjectionState(ChildrenProjectionState childrenProjState) {
        this.childrenProjState = childrenProjState;
    }

    public SparseVector getVector() {
        return this.cluster.getMean().getVector();
    }

    public ElementHelper getElementHelper() {
        return helper;
    }

    /**
     * Changes the renderer associated to ClusterElement objects.
     * @param aHelper The new cluster renderer.
     */
    public static void setClusterHelper(ElementHelper aHelper) {
        helper = aHelper;
    }

    /**
     * Returns the cluster that this graphical elements represents.
     * @return The cluster.
     */
    public Cluster getCluster() {
        return cluster;
    }

    /**
     * Adds a graphical element (a cluster or a data instance) as a child of
     * this graphical element.
     * @param child The new child of this graphical element.
     */
    public void addChild(GraphicalElement child) {
        super.add(child);
    }

    /**
     * Returns all children of this cluster graphical element.
     * @return The cluster children.
     */
    public ArrayList<GraphicalElement> getChildren() {
        return children;
    }

    /**
     * Removes all children of this element.
     */
    public void removeChildren() {
        for (GraphicalElement ge : this.children) {
            ge.setParent(null);
        }

        this.children = new ArrayList<GraphicalElement>();
    }

    /**
     * Serializes the static attributes. This method is neccessay since the 
     * static attributes are not automatically serialized on Java. This method 
     * must be called just after a hierarchy be serialized.
     * @param os The stream to write the attribute.
     * @throws IOException Throws an exception if something goes wrong.
     */
    public static void serializeStaticState(ObjectOutputStream os) throws IOException {
        os.writeObject(helper);
    }

    /**
     * De-serializes the static attributes. This method is neccessay since the 
     * static attributes are not automatically de-serialized on Java. This method 
     * must be called just after a hierarchy be de-serialized.
     * @param os The stream to read the attribute.
     * @throws IOException Throws an exception if something goes wrong.
     */
    public static void deserializeStaticState(ObjectInputStream os) throws IOException {
        try {
            helper = (ElementHelper) os.readObject();
        } catch (ClassNotFoundException ex) {
            throw new IOException(ex.getMessage());
        }
    }

    /**
     * Returns the topic associeted to this element. If there is not any topic
     * it returns null.
     * @return The topic associated to this element.
     */
    public ArrayList<String> getTopic() {
        return topic;
    }

    /**
     * Changes the topics associated to this element. Also changes the title
     * according to the new topic.
     * @param topic The new topic
     */
    public void setTopic(ArrayList<String> topic) {
        this.topic = topic;
        this.title = "";

        for (int i = 0; i < topic.size(); i++) {
            this.title += topic.get(i);
            if (i < topic.size() - 1) {
                this.title += ",";
            }
        }
    }

    @Override
    public String toString() {
        return "[" + this.cluster.getInstancesCount() + "]: " + this.title;
    }

    protected Cluster cluster;
    private ArrayList<String> topic;
    private static ElementHelper helper = new ClusterHelper2D();
    private ChildrenProjectionState childrenProjState = ChildrenProjectionState.NOT_PROJECTED;
    private static final long serialVersionUID = 27L;
}
