/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.view.draw.listeners;

import vispipeline.hpex.view.HPExView;
import vispipeline.hpex.view.draw.ClusterElement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public abstract class ExpandListener implements MouseListener {

    public static boolean isAnimate() {
        return animate;
    }

    public static void setAnimate(boolean aAnimate) {
        animate = aAnimate;
    }
    
    protected void animate(final ClusterElement ce, final HPExView view) {
        //centering the clusters and zooming in
        Thread t = new Thread() {

            @Override
            public void run() {
                try {
                    if (animate) {
                        int steps = 8;
                        float xstep = (view.getProjectionPanel().getRenderer().getPanx() - ce.getX()) / steps;
                        float ystep = (view.getProjectionPanel().getRenderer().getPany() - ce.getY()) / steps;
                        float zstep = (0.9f * (view.getProjectionPanel().getRenderer().getWin() - ce.getSize())) / steps;

                        for (int i = 0; i < steps; i++) {
                            view.getProjectionPanel().getRenderer().setPanx(view.getProjectionPanel().getRenderer().getPanx() - xstep);
                            view.getProjectionPanel().getRenderer().setPany(view.getProjectionPanel().getRenderer().getPany() - ystep);
                            view.getProjectionPanel().getRenderer().setWin(view.getProjectionPanel().getRenderer().getWin() - zstep);

                            view.getProjectionPanel().getRenderer().redisplay();
                            view.getProjectionPanel().repaint();

                            Thread.sleep(100);
                        }

                        Thread.sleep(300);
                    }

                    view.getProjectionPanel().getRenderer().updateImage();
                    view.getProjectionPanel().repaint();
                    view.updateTree();                    
                    
                } catch (InterruptedException ex) {
                    Logger.getLogger(SimpleExpandListener.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        };

        t.start();
    }

    protected static boolean animate = true;
}
