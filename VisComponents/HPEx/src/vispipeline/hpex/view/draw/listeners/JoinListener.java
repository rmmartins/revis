/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.view.draw.listeners;

import vispipeline.hpex.view.HPExView;
import vispipeline.hpex.view.draw.ClusterHierarchy;
import vispipeline.hpex.view.draw.GraphicalElement;
import java.util.ArrayList;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class JoinListener implements MouseListener {

    public void mouseClicked(GraphicalElement ge, ClusterHierarchy hierarchy, HPExView view) {
        if (selected.contains(ge)) {
            selected.remove(ge);
        } else {
            selected.add(ge);
        }

        view.getProjectionPanel().getRenderer().updateImage();
        view.getProjectionPanel().repaint();
    }

    public void mouseMoved(GraphicalElement ge, ClusterHierarchy hierarchy, HPExView view) {
    }

    /**
     * Returns only the elements which are drawn on the screen.
     * @return
     */
    public static ArrayList<GraphicalElement> getSelected() {
        ArrayList<GraphicalElement> sel_visual = new ArrayList<GraphicalElement>();

        for (GraphicalElement ge : selected) {
            if (ge.getDrawState() == GraphicalElement.DrawState.DRAWN) {
                sel_visual.add(ge);
            }
        }

        return sel_visual;
    }

    public static void add(GraphicalElement ge) {
        selected.add(ge);
    }

    public static void remove(GraphicalElement ge) {
        selected.remove(ge);
    }

    public static void clear() {
        selected.clear();
    }

    private static ArrayList<GraphicalElement> selected = new ArrayList<GraphicalElement>();
}
