/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.view.draw;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Enumeration;
import javax.swing.tree.MutableTreeNode;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class GraphicalTreeNode implements MutableTreeNode, Serializable {

    public GraphicalTreeNode() {
        if (this instanceof ClusterElement) {
            allowsChildren = true;
            children = new ArrayList<GraphicalElement>();
        } else {
            allowsChildren = false;
        }
    }

    public void add(MutableTreeNode newChild) {
        if (newChild != null && newChild.getParent() == this) {
            insert(newChild, getChildCount() - 1);
        } else {
            insert(newChild, getChildCount());
        }
    }

    public void insert(MutableTreeNode newChild, int index) {
        if (newChild == null) {
            throw new IllegalArgumentException("new child is null");
        } else if (isNodeAncestor(newChild)) {
            throw new IllegalArgumentException("new child is an ancestor");
        }

        if (newChild instanceof GraphicalElement) {
            MutableTreeNode oldParent = (MutableTreeNode) newChild.getParent();

            if (oldParent != null) {
                oldParent.remove(newChild);
            }

            newChild.setParent(this);
            if (children == null) {
                children = new ArrayList<GraphicalElement>();
            }

            int size = children.size();
            for (int i = 0; i < index - size + 1; i++) {
                children.add(null);
            }
            children.set(index, (GraphicalElement) newChild);
        } else {
            throw new IllegalArgumentException("adding a not GraphicalElement");
        }
    }

    public void remove(int index) {
        MutableTreeNode child = (MutableTreeNode) getChildAt(index);
        children.remove(index);
        child.setParent(null);
    }

    public void remove(MutableTreeNode node) {
        if (node == null) {
            throw new IllegalArgumentException("argument is null");
        }

        if (!isNodeChild(node)) {
            throw new IllegalArgumentException("argument is not a child");
        }

        remove(getIndex(node));	// linear search
    }

    public void setUserObject(Object object) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void removeFromParent() {
        MutableTreeNode aux = (MutableTreeNode) getParent();
        if (aux != null) {
            aux.remove(this);
        }
    }

    public void setParent(MutableTreeNode newParent) {
        if (newParent instanceof ClusterElement || newParent == null) {
            parent = (ClusterElement) newParent;
        } else {
            throw new IllegalArgumentException("parent is not ClusterElement");
        }
    }

    public javax.swing.tree.TreeNode getChildAt(int childIndex) {
        if (children == null) {
            throw new ArrayIndexOutOfBoundsException("node has no children");
        }
        return (GraphicalTreeNode) children.get(childIndex);
    }

    public int getChildCount() {
        if (children == null) {
            return 0;
        } else {
            return children.size();
        }
    }

    public javax.swing.tree.TreeNode getParent() {
        return parent;
    }

    public int getIndex(javax.swing.tree.TreeNode node) {
        if (node == null) {
            throw new IllegalArgumentException("argument is null");
        }

        if (!isNodeChild(node)) {
            return -1;
        }
        return children.indexOf(node);	// linear search
    }

    public boolean getAllowsChildren() {
        return allowsChildren;
    }

    public boolean isLeaf() {
        return (getChildCount() == 0);
    }

    public boolean isRoot() {
        return getParent() == null;
    }

    public Enumeration children() {
        Enumeration<GraphicalElement> en = new Enumeration() {

            public boolean hasMoreElements() {
                if (children != null) {
                    return (children.size() > index);
                } else {
                    return false;
                }
            }

            public Object nextElement() {
                if (children != null) {
                    return children.get(index++);
                } else {
                    return null;
                }
            }

            private int index = 0;
        };

        return en;
    }

    public boolean isNodeChild(javax.swing.tree.TreeNode aNode) {
        boolean retval;

        if (aNode == null) {
            retval = false;
        } else {
            if (getChildCount() == 0) {
                retval = false;
            } else {
                retval = (aNode.getParent() == this);
            }
        }

        return retval;
    }

    public boolean isNodeAncestor(javax.swing.tree.TreeNode anotherNode) {
        if (anotherNode == null) {
            return false;
        }

        javax.swing.tree.TreeNode ancestor = this;

        do {
            if (ancestor == anotherNode) {
                return true;
            }
        } while ((ancestor = ancestor.getParent()) != null);

        return false;
    }

    protected boolean allowsChildren;
    protected GraphicalElement parent;
    protected ArrayList<GraphicalElement> children;
    private static final long serialVersionUID = 27L;
}
