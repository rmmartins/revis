/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.view.draw;

import java.util.ArrayList;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class Background {

    public static final Color BLACK = new Color("black",
            new float[]{0.0f, 0.0f, 0.0f},
            new float[]{0.5f, 0.5f, 0.5f},
            new float[]{0.99f, 0.99f, 0.0f});
    
     public static final Color GRAY = new Color("dark gray",
            new float[]{0.25f, 0.25f, 0.25f},
            new float[]{0.75f, 0.75f, 0.75f},
            new float[]{0.99f, 0.99f, 0.0f});
     
    public static final Color WHITE = new Color("white",
            new float[]{1.0f, 1.0f, 1.0f},
            new float[]{0.15f, 0.15f, 0.15f},
            new float[]{0.45f, 0.45f, 0.0f});
    
    private Background() {
        colors = new ArrayList<Color>();
        colors.add(Background.BLACK);
        colors.add(Background.GRAY);
        colors.add(Background.WHITE);
    }

    public static Background getInstance() {
        if (_instance == null) {
            _instance = new Background();
        }
        return _instance;
    }

    public ArrayList<Color> getColors() {
        return colors;
    }

    public static class Color {

        public Color(String name, float[] backgroundColor, float[] clusterColor,
                float[] instanceColor) {
            this.name = name;
            this.backgroundColor = backgroundColor;
            this.clusterColor = clusterColor;
            this.instanceColor = instanceColor;
        }

        public float[] getBackgroundColor() {
            return backgroundColor;
        }

        public float[] getClusterColor() {
            return clusterColor;
        }

        public float[] getInstanceColor() {
            return instanceColor;
        }
        
        @Override
        public String toString() {
            return name;
        }

        private String name = "none";
        private float[] backgroundColor = new float[]{0.0f, 0.0f, 0.0f};
        private float[] clusterColor = new float[]{0.0f, 0.0f, 0.0f};
        private float[] instanceColor = new float[]{0.0f, 0.0f, 0.0f};
    }

    private static Background _instance;
    private ArrayList<Color> colors;
}
