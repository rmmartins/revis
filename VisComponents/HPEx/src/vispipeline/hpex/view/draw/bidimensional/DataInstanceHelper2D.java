/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.view.draw.bidimensional;

import com.sun.opengl.util.GLUT;
import vispipeline.hpex.view.draw.Background;
import vispipeline.hpex.view.draw.GraphicalElement;
import vispipeline.hpex.view.draw.ElementHelper;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import jogl2ps.GL2PS;

/**
 * This class draws a 2D data instance.
 * 
 * @author Fernando Vieira Paulovich
 */
public class DataInstanceHelper2D extends ElementHelper {

    public void draw(boolean overview, Background.Color bkgColor, GraphicalElement el, GLAutoDrawable drawable) {
        GL gl = drawable.getGL();
        GLUT glut = new GLUT();
        GL2PS jogl2ps = new GL2PS();

        if (el.isShowTitle() && !overview) {
            gl.glColor3fv(bkgColor.getInstanceColor(), 0);
            gl.glRasterPos3f(el.getX(), el.getY(), 0.5f);

            String text = el.toString();
            jogl2ps.gl2psText(text, "Helvetica", 12);
            glut.glutBitmapString(GLUT.BITMAP_HELVETICA_12, text);
        }

        gl.glPushMatrix();
        gl.glTranslatef(el.getX() - (el.getSize() / 2.0f),
                el.getY() - (el.getSize() / 2.0f), 0.0f);

        gl.glColor3fv(el.getColor(), 0);

        gl.glBegin(GL.GL_QUADS);
        gl.glVertex2f(0.0f, 0.0f);
        gl.glVertex2f(el.getSize(), 0.0f);
        gl.glVertex2f(el.getSize(), el.getSize());
        gl.glVertex2f(0.0f, el.getSize());
        gl.glEnd();

//        gl.glLineWidth(0.25f);
//        jogl2ps.gl2psLineWidth(0.25f);

        gl.glColor3fv(bkgColor.getClusterColor(), 0);
        gl.glBegin(GL.GL_LINE_LOOP);
        gl.glVertex2f(0.0f, 0.0f);
        gl.glVertex2f(el.getSize(), 0.0f);
        gl.glVertex2f(el.getSize(), el.getSize());
        gl.glVertex2f(0.0f, el.getSize());
        gl.glEnd();

//        gl.glLineWidth(1.0f);
//        jogl2ps.gl2psLineWidth(1.0f);

        gl.glPopMatrix();
    }

    public boolean isInside(GraphicalElement el, double[] objPos) {
        return (objPos[0] < el.getX() + (el.getSize() / 2.0f) &&
                objPos[0] > el.getX() - (el.getSize() / 2.0f)) &&
                (objPos[1] < el.getY() + (el.getSize() / 2.0f) &&
                objPos[1] > el.getY() - (el.getSize() / 2.0f));
    }

    private static final long serialVersionUID = 27L;
}
