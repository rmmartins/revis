/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.view.draw.bidimensional;

import vispipeline.hpex.view.draw.GLRenderer;
import vispipeline.hpex.view.draw.GraphicalElement;
import vispipeline.hpex.view.draw.listeners.JoinListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import jogl2ps.GL2PS;

/**
 * This class represents a 2D render. This class must be linked with a GLPanel2D 
 * in order to be possible to properly render the projection. This class adds 
 * zoom, pan, mouse click, and mouse moved listeners to the panel.
 *
 * @author Fernando Vieira Paulovich
 */
public class GLRenderer2D extends GLRenderer {

    /**
     * Creates a new instance of GLRenderer2D and adds listeners to the panel
     * attached to this renderer.
     * @param panel GLPanel2D panel where the listeners are attached.
     */
    public GLRenderer2D(GLPanel2D panel) {
        super(panel);

        if (this.panel != null) {
            panel.addGLEventListener(this);
            MouseClickedListener mouseClickedListener = new MouseClickedListener(panel);
            ZoomListener zoomListener = new ZoomListener(panel);
            PanListener panListener = new PanListener(panel);
            MouseMovedListener mouseMovedListener = new MouseMovedListener(panel);
        }
    }

    @Override
    public void reset() {
        this.setPanx(0);
        this.setPany(0);
        this.setWin(GLRenderer.WINDOW_SIZE);

        this.viewport = new int[4];
        this.mvmatrix = new double[16];
        this.projmatrix = new double[16];

        if (this.drawable != null) {
            drawable.getContext().makeCurrent();
            this.defineVisualParameters(this.drawable);
            drawable.getContext().release();
        }

        this.redisplay = true;
        this.updateImage = true;
    }

    /**
     * It is a JOGL method where the visualization parameters are initialized.
     * @param gLAutoDrawable the GL context
     */
    public void init(GLAutoDrawable gLAutoDrawable) {
        this.drawable = gLAutoDrawable;

        GL gl = gLAutoDrawable.getGL();

        gl.glClearDepth(1.0f);
        gl.glEnable(GL.GL_DEPTH_TEST);
        gl.glDepthFunc(GL.GL_LEQUAL);
        gl.glShadeModel(GL.GL_FLAT);

        //creating the cluster list
        this.imageId = gl.glGenLists(1);
        this.overviewId = gl.glGenLists(1);

        //anti-aliasing enabling        
        gl.glEnable(GL.GL_BLEND);
        gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);

        gl.glEnable(GL.GL_LINE_SMOOTH);
        gl.glHint(GL.GL_LINE_SMOOTH_HINT, GL.GL_NICEST);
    }

    public void displayChanged(GLAutoDrawable gLAutoDrawable, boolean b, boolean b0) {
        this.drawable = gLAutoDrawable;
    }

    /**
     * This is a JOGL method called to draw the objects.
     * @param gLAutoDrawable the GL context
     */
    public void display(GLAutoDrawable gLAutoDrawable) {
        if (this.redisplay) {
            this.drawable = gLAutoDrawable;
            GL gl = gLAutoDrawable.getGL();

            //////////////////////////////////////////////////////////////////
            //drawing the main image
            this.defineVisualParameters(gLAutoDrawable);
            gl.glViewport(0, 0, (int) this.width, (int) this.height);

            float[] bkg = this.bkgColor.getBackgroundColor();
            gl.glClearColor(bkg[0], bkg[1], bkg[2], 1.0f); //backgroung color
            gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

            //used for gluUnproject
            gl.glGetIntegerv(GL.GL_VIEWPORT, this.viewport, 0);
            gl.glGetDoublev(GL.GL_MODELVIEW_MATRIX, this.mvmatrix, 0);
            gl.glGetDoublev(GL.GL_PROJECTION_MATRIX, this.projmatrix, 0);

            if (this.updateImage) {
                this.createImage(gLAutoDrawable);
                this.updateImage = false;
            }

            gl.glCallList(this.imageId);

            this.drawJoinLines(gLAutoDrawable);

            //////////////////////////////////////////////////////////////////
            //drawing the viewport
            gl.glMatrixMode(GL.GL_PROJECTION);
            gl.glLoadIdentity();

            if (width <= height) {
                gl.glOrtho(-GLRenderer.WINDOW_SIZE, GLRenderer.WINDOW_SIZE,
                        -GLRenderer.WINDOW_SIZE * (height / width),
                        GLRenderer.WINDOW_SIZE * (height / width), -1, 1);
            } else {
                gl.glOrtho(-GLRenderer.WINDOW_SIZE * (width / height),
                        GLRenderer.WINDOW_SIZE * (width / height),
                        -GLRenderer.WINDOW_SIZE, GLRenderer.WINDOW_SIZE, -1, 1);
            }

            if (width > height) {
                gl.glViewport((int) ((this.height - this.width) / 10),
                        (int) this.height * 4 / 5,
                        (int) this.width / 5, (int) this.height / 5);
            } else {
                gl.glViewport(0, (int) ((this.height * 4 / 5) - ((this.width - this.height) / 10)),
                        (int) this.width / 5, (int) this.height / 5);
            }

            gl.glColor3fv(bkgColor.getBackgroundColor(), 0);
            gl.glBegin(GL.GL_QUADS);
            gl.glVertex2f(-GLRenderer.WINDOW_SIZE, -GLRenderer.WINDOW_SIZE);
            gl.glVertex2f(GLRenderer.WINDOW_SIZE, -GLRenderer.WINDOW_SIZE);
            gl.glVertex2f(GLRenderer.WINDOW_SIZE, GLRenderer.WINDOW_SIZE);
            gl.glVertex2f(-GLRenderer.WINDOW_SIZE, GLRenderer.WINDOW_SIZE);
            gl.glEnd();

            gl.glColor3fv(bkgColor.getClusterColor(), 0);
            gl.glBegin(GL.GL_LINE_LOOP);
            gl.glVertex2f(-GLRenderer.WINDOW_SIZE, -GLRenderer.WINDOW_SIZE);
            gl.glVertex2f(GLRenderer.WINDOW_SIZE, -GLRenderer.WINDOW_SIZE);
            gl.glVertex2f(GLRenderer.WINDOW_SIZE, GLRenderer.WINDOW_SIZE);
            gl.glVertex2f(-GLRenderer.WINDOW_SIZE, GLRenderer.WINDOW_SIZE);
            gl.glEnd();

            gl.glCallList(this.overviewId);

            gl.glColor3f(0.0f, 1.0f, 0.0f);
            gl.glBegin(GL.GL_LINE_LOOP);
            gl.glVertex2f(-win + panx, -win + pany);
            gl.glVertex2f(win + panx, -win + pany);
            gl.glVertex2f(win + panx, win + pany);
            gl.glVertex2f(-win + panx, win + pany);
            gl.glEnd();
 
            gl.glFlush(); //execute all commands

            this.redisplay = false;
        }
    }

    /**
     * This is a JOGL method called when the window is resized. It keeps the 
     * aspect ratio of the objects on the resized window.
     * @param gLAutoDrawable the GL context
     * @param x the left coordinate of the window
     * @param y the bottom coordinate of the window
     * @param w the window's width
     * @param h the window's height
     */
    public void reshape(GLAutoDrawable gLAutoDrawable, int x, int y, int w, int h) {
        this.drawable = gLAutoDrawable;

        GL gl = gLAutoDrawable.getGL();
        if (h == 0) {
            h = 1;
        }

        this.width = w;
        this.height = h;

        gl.glViewport(0, 0, w, h);

        this.defineVisualParameters(gLAutoDrawable);

        this.redisplay = true;
        this.updateImage = true;
    }

    @Override
    protected void defineVisualParameters(GLAutoDrawable gLAutoDrawable) {
        GL gl = gLAutoDrawable.getGL();

        gl.glMatrixMode(GL.GL_PROJECTION);
        gl.glLoadIdentity();

        if (width <= height) {
            gl.glOrtho(-win + panx, win + panx, -win * (height / width) + pany,
                    win * (height / width) + pany, -1, 1);
        } else {
            gl.glOrtho(-win * (width / height) + panx, win * (width / height) + panx,
                    -win + pany, win + pany, -1, 1);
        }
    }

    public void drawJoinLines(GLAutoDrawable drawable) {
        GL gl = drawable.getGL();
        GL2PS jogl2ps = new GL2PS();

        ArrayList<GraphicalElement> selected = JoinListener.getSelected();
        int size = selected.size();

        if (size > 1) {
            gl.glLineWidth(2.0f);
            jogl2ps.gl2psLineWidth(2.0f);
            gl.glColor3fv(bkgColor.getClusterColor(), 0);

            for (int i = 0; i < size - 1; i++) {
                gl.glBegin(GL.GL_LINES);
                gl.glVertex3f(selected.get(i).getX(), selected.get(i).getY(), 0.5f);
                gl.glVertex3f(selected.get(i + 1).getX(), selected.get(i + 1).getY(), 0.5f);
                gl.glEnd();
            }

            gl.glLineWidth(1.0f);
            jogl2ps.gl2psLineWidth(1.0f);
        }
    }

    /**
     * This class implements the mouse clicked listener. This automatically 
     * calls the mouseClicked() method from the attached panel. 
     */
    public class MouseClickedListener extends MouseAdapter {

        /** Creates a new instance of MouseClickedListener
         * @param panel The panel which this listener is attached.
         */
        public MouseClickedListener(GLPanel2D panel) {
            panel.addMouseListener(this);
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            super.mouseClicked(e);

            if (e.getButton() == MouseEvent.BUTTON1) {
                double[] worldPos = this.getWorldCoordinates(e.getX(),
                        e.getY(), 0.0);

                if (panel != null) {
                    panel.mouseClicked(e, worldPos);
                }
            }
        }

        private double[] getWorldCoordinates(double x, double y, double z) {
            double worldPos[] = new double[4];//returned xyz coords
            double realy = viewport[3] - (int) y - 1;
            glu.gluUnProject(x, realy, 0, mvmatrix, 0, projmatrix, 0, viewport,
                    0, worldPos, 0);
            return worldPos;
        }

    }

    /**
     * This class implements the panel zoom.
     */
    public class ZoomListener extends MouseAdapter implements MouseMotionListener {

        /** Creates a new instance of ZoomListener
         * @param panel The panel which this listener is attached.
         */
        public ZoomListener(GLPanel2D panel) {
            panel.addMouseListener(this);
            panel.addMouseMotionListener(this);
        }

        public void mouseMoved(MouseEvent e) {
        }

        public void mouseDragged(MouseEvent e) {
            if (this.execute) {
                if (e.getY() > this.prevy) {
                    this.prevy = e.getY();
                    if (win < GLRenderer.WINDOW_SIZE * 10) {
                        setWin(win * 1.10f);
                    }
                } else {
                    this.prevy = e.getY();
                    if (win > 0) {
                        setWin(win * 0.809f);
                    }
                }

                if (drawable != null) {
                    drawable.getContext().makeCurrent();
                    defineVisualParameters(drawable);
                    drawable.getContext().release();
                }

                redisplay = true;
                if (panel != null) {
                    panel.repaint();
                }
            }
        }

        @Override
        public void mousePressed(MouseEvent e) {
            super.mousePressed(e);
            if (e.getButton() == MouseEvent.BUTTON3) {
                this.prevy = e.getY();
                this.execute = true;
            } else {
                this.execute = false;
            }
        }

        private boolean execute = false;
        private int prevy = 0;
    }

    /**
     * This class implements the panel Pan.
     */
    public class PanListener extends MouseAdapter implements MouseMotionListener {

        /** Creates a new instance of PanListener
         * @param panel The panel which this listener is attached.
         */
        public PanListener(GLPanel2D panel) {
            panel.addMouseListener(this);
            panel.addMouseMotionListener(this);
        }

        public void mouseMoved(MouseEvent e) {
        }

        public void mouseDragged(MouseEvent e) {
            if (this.execute) {
                float diffx = ((float) (e.getX() - this.prevx));
                this.prevx = e.getX();
                setPanx(panx - diffx);

                float diffy = ((float) (e.getY() - this.prevy));
                this.prevy = e.getY();
                setPany(getPany() + diffy);

                if (drawable != null) {
                    drawable.getContext().makeCurrent();
                    defineVisualParameters(drawable);
                    drawable.getContext().release();
                }

                redisplay = true;
                if (panel != null) {
                    panel.repaint();
                }
            }
        }

        @Override
        public void mousePressed(MouseEvent e) {
            super.mousePressed(e);
            if (e.getButton() == MouseEvent.BUTTON1) {
                this.prevx = e.getX();
                this.prevy = e.getY();
                this.execute = true;
            } else {
                this.execute = false;
            }
        }

        private boolean execute = false;
        private int prevy = 0;
        private int prevx = 0;
    }

    /**
     * This class implements the mouse moved listener. This automatically 
     * calls the mouseMoved() method from the attached panel. 
     */
    public class MouseMovedListener extends MouseMotionAdapter {

        /** Creates a new instance of MouseMovedListener
         * @param panel The panel which this listener is attached.
         */
        public MouseMovedListener(GLPanel2D panel) {
            panel.addMouseMotionListener(this);
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            double[] worldPos = new double[4];
            double realy = viewport[3] - (int) e.getY() - 1;
            glu.gluUnProject(e.getX(), realy, 0, mvmatrix, 0, projmatrix, 0,
                    viewport, 0, worldPos, 0);
            if (panel != null) {
                panel.mouseMoved(e, worldPos);
            }
        }

    }

    private int viewport[] = new int[4];
    private double mvmatrix[] = new double[16];
    private double projmatrix[] = new double[16];
}
