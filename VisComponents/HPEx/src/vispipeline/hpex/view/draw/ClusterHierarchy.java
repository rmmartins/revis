/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.view.draw;

import vispipeline.hpex.util.Cluster;
import vispipeline.hpex.projection.*;
import vispipeline.hpex.util.Instance;
import vispipeline.hpex.view.color.ColorTable;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Stack;
import javax.media.opengl.GLAutoDrawable;
import textprocessing.corpus.Corpus;
import textprocessing.processing.Ngram;

/**
 * This class represents a hierarchy of clusters.
 * 
 * @author Fernando Vieira Paulovich
 */
public class ClusterHierarchy implements Serializable {

    /**
     * Creates a new instance of ClusterHierarchy.
     * @param cluster 
     * @param param 
     */
    public ClusterHierarchy(Cluster cluster, Parameters param) {
        this.param = param;

        //creating the visual element
        this.root = new ClusterElement(cluster);

        //setting the values used to project the children of root rootcluster
        this.root.setSize(GLRenderer.WINDOW_SIZE * 0.9f);
        this.root.setX(0.0f);
        this.root.setY(0.0f);
        this.root.setTitle("topics");
    }

    public Parameters getParameters() {
        return this.param;
    }

    public ClusterElement getRoot() {
        return root;
    }

    /**
     * Only draws the first clusters of the hierarchy. Do not forget to call 
     * GLRender.updateImage() method to change the image.
     */
    public void reset() {
        java.util.Iterator<GraphicalElement> it = this.iterator();
        while (it.hasNext()) {
            GraphicalElement ge = it.next();
            ge.setDrawState(GraphicalElement.DrawState.NOT_DRAWN);
        }

        for (GraphicalElement ge : this.root.getChildren()) {
            ge.setDrawState(GraphicalElement.DrawState.DRAWN);
        }
    }

    public void draw(boolean overview, Background.Color bkgColor, GLAutoDrawable drawable) {
        java.util.Iterator<GraphicalElement> it = this.iteratorDraw();

        while (it.hasNext()) {
            GraphicalElement ge = it.next();
            ge.getElementHelper().draw(overview, bkgColor, ge, drawable);
        }
    }

    /**
     * Return a graphical element if a give position is inside of such
     * element.
     * @param objPos The position.
     * @return The graphical element if the point is inside a element, null
     * otherwise
     */
    public GraphicalElement getElementByPosition(double[] objPos) {
        java.util.Iterator<GraphicalElement> it = this.iteratorDraw();

        while (it.hasNext()) {
            GraphicalElement ge = it.next();
            if (ge.getElementHelper().isInside(ge, objPos)) {
                return ge;
            }
        }

        return null;
    }

    /**
     * 
     * @param scalar
     * @param table 
     */
    public void colorAs(Scalar scalar, ColorTable table) {
        java.util.Iterator<GraphicalElement> it = this.iterator();
        while (it.hasNext()) {
            GraphicalElement ge = it.next();
            ge.setColor(scalar, table);
        }
    }

    /**
     * 
     * @param name
     * @return 
     */
    public Scalar addScalar(String name) {
        Scalar scalar = new Scalar(name);

        if (!this.scalars.contains(scalar)) {
            this.scalars.add(scalar);
        }

        scalar.index = this.scalars.indexOf(scalar);

        return this.scalars.get(scalar.index);
    }

    /**
     * 
     * @return
     */
    public ArrayList<Scalar> getScalars() {
        return this.scalars;
    }

    /**
     * 
     * @param name
     * @return
     */
    public Scalar getScalarByName(String name) {
        for (Scalar s : this.scalars) {
            if (s.name.equals(name)) {
                return s;
            }
        }

        return null;
    }

    /**
     * 
     * @param scalar
     * @return
     */
    public boolean removeScalar(Scalar scalar) {
        if (this.scalars.contains(scalar)) {
            //removing the scalar from the graphical elements
            java.util.Iterator<GraphicalElement> it = this.iterator();
            while (it.hasNext()) {
                GraphicalElement ge = it.next();
                ge.removeScalar(scalar);
            }

            //removing the scalar from the hierarchy
            this.scalars.remove(scalar);

            //updating the indexes
            for (int i = 0; i < this.scalars.size(); i++) {
                this.scalars.get(i).index = i;
            }

            return true;
        }
        return false;
    }

    /**
     * 
     * @return
     */
    public Corpus getCorpus() {
        return param.corpus;
    }

    /**
     * 
     * @param corpus
     */
    public void setCorpus(Corpus corpus) {
        this.param.corpus = corpus;
    }

    /**
     * 
     * @return
     */
    public ArrayList<String> getAttributes() {
        return attr;
    }

    /**
     * 
     * @param attr
     */
    public void setAttributes(ArrayList<String> attr) {
        this.attr = attr;
    }

    /**
     * Returns all graphical elements currently on the hierarchy.
     * @return All graphical elements.
     */
    public java.util.Iterator<GraphicalElement> iterator() {
        return new ClusterHierarchy.Iterator();
    }

    /**
     * Returns the graphical elements draw on the screen.
     * @return The showed graphical elements.
     */
    public java.util.Iterator<GraphicalElement> iteratorDraw() {
        return new ClusterHierarchy.IteratorDraw();
    }

    /**
     * 
     * @param words 
     * @param scalar 
     * @throws IOException
     */
    public void createQueryScalar(ArrayList<String> words, Scalar scalar) throws IOException {
        ////////////////////////////////////////////////////////////////////////
        //creating the scalar for the data instances
        Cluster rootcluster = root.getCluster();
        float max = Float.MIN_VALUE;

        for (int i = 0; i < rootcluster.getInstancesCount(); i++) {
            Instance ins = rootcluster.getInstance(i);

            if (ins instanceof DataInstanceElement) {
                DataInstanceElement die = (DataInstanceElement) ins;

                ArrayList<Ngram> ngrams = param.corpus.getNgrams(die.getVector().getId());
                int freqs[] = new int[words.size()];
                Arrays.fill(freqs, 0);

                int nrngrams = ngrams.size();
                for (int j = 0; j < nrngrams; j++) {
                    Ngram n = ngrams.get(j);

                    for (int k = 0; k < freqs.length; k++) {
                        if (n.ngram.toLowerCase().indexOf(words.get(k)) > -1) {
                            freqs[k] += n.frequency;
                        }
                    }
                }

                float freq = Integer.MAX_VALUE;
                for (int j = 0; j < freqs.length; j++) {
                    freq = Math.min(freq, freqs[j]);
                }

                max = Math.max(max, freq);

                die.setScalarValue(scalar, freq);
            }
        }

        scalar.max = 1.0f;
        scalar.min = 0.0f;

        //normalizing the scalar values
        for (int i = 0; i < rootcluster.getInstancesCount(); i++) {
            Instance ins = rootcluster.getInstance(i);

            if (ins instanceof DataInstanceElement) {
                DataInstanceElement die = (DataInstanceElement) ins;
                float value = die.getScalarValue(scalar);
                die.setScalarValue(scalar, (value / max));
            }
        }

        ////////////////////////////////////////////////////////////////////////
        //creating the scalars for the clusters
        java.util.Iterator<GraphicalElement> it = this.iterator();

        while (it.hasNext()) {
            GraphicalElement el = it.next();

            if (el instanceof ClusterElement) {
                Cluster cluster = ((ClusterElement) el).getCluster();
                float freq = 0;

                int size = cluster.getInstancesCount();
                for (int i = 0; i < size; i++) {
                    Instance ins = cluster.getInstance(i);

                    if (ins instanceof DataInstanceElement) {
                        DataInstanceElement die = (DataInstanceElement) ins;
                        freq += die.getScalarValue(scalar);
                    }
                }

                el.setScalarValue(scalar, (freq / size));
            }
        }
    }

    public static class Scalar implements Serializable {

        public Scalar(String name) {
            this.name = name;
        }

        @Override
        public boolean equals(Object obj) {
            if (this.name != null && obj != null) {
                return this.name.equals(((Scalar) obj).name);
            } else {
                return false;
            }
        }

        @Override
        public int hashCode() {
            return this.name.hashCode();
        }

        @Override
        public String toString() {
            return this.name;
        }

        public String name = "";
        public float max = Float.MIN_VALUE;
        public float min = Float.MAX_VALUE;
        public int index = 0;
        private static final long serialVersionUID = 27L;
    }

    private class Iterator implements java.util.Iterator<GraphicalElement> {

        public Iterator() {
            this.elements = new Stack<GraphicalElement>();
            for (int i = 0; i < root.getChildren().size(); i++) {
                this.elements.push(root.getChildren().get(i));
            }
        }

        public boolean hasNext() {
            return (!this.elements.empty());
        }

        public GraphicalElement next() {
            if (this.elements.empty()) {
                throw new RuntimeException("There is not a next element!");
            }

            GraphicalElement el = this.elements.pop();

            if (el instanceof ClusterElement && el != null) {
                ArrayList<GraphicalElement> ch = ((ClusterElement) el).getChildren();
                for (GraphicalElement ge : ch) {
                    this.elements.push(ge);
                }
            }

            return el;
        }

        public void remove() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        private Stack<GraphicalElement> elements;
    }

    private class IteratorDraw implements java.util.Iterator<GraphicalElement> {

        public IteratorDraw() {
            this.elements = new Stack<GraphicalElement>();
            for (int i = 0; i < root.getChildren().size(); i++) {
                this.elements.push(root.getChildren().get(i));
            }
        }

        public boolean hasNext() {
            return (!this.elements.empty());
        }

        public GraphicalElement next() {
            if (this.elements.empty()) {
                throw new RuntimeException("There is not a next element!");
            }

            GraphicalElement el = this.elements.pop();

            if (el.getDrawState() == GraphicalElement.DrawState.EXPANDED) {
                ArrayList<GraphicalElement> ch = ((ClusterElement) el).getChildren();
                for (GraphicalElement ge : ch) {
                    if (ge.getDrawState() == GraphicalElement.DrawState.EXPANDED ||
                            ge.getDrawState() == GraphicalElement.DrawState.DRAWN) {
                        this.elements.push(ge);
                    }
                }

                el = next();
            }

            return el;
        }

        public void remove() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        private Stack<GraphicalElement> elements;
    }

    private ArrayList<String> attr;
    private ArrayList<Scalar> scalars = new ArrayList<Scalar>();
    private ClusterElement root;
    private Parameters param;
    private static final long serialVersionUID = 27L;
}
