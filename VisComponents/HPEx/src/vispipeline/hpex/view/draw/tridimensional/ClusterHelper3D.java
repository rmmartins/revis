/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.view.draw.tridimensional;

import com.sun.opengl.util.GLUT;
import vispipeline.hpex.view.draw.Background;
import vispipeline.hpex.view.draw.GraphicalElement;
import vispipeline.hpex.view.draw.ElementHelper;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import jogl2ps.GL2PS;

/**
 * This class draws a 3D cluster.
 * 
 * @author Fernando Vieira Paulovich
 */
public class ClusterHelper3D extends ElementHelper {

    public void draw(boolean overview, Background.Color bkgColor, GraphicalElement el, GLAutoDrawable drawable) {
        GL gl = drawable.getGL();
        GLUT glut = new GLUT();
        GL2PS jogl2ps = new GL2PS();

        if (el.isShowTitle() && !overview) {
            gl.glDisable(GL.GL_LIGHTING);
            gl.glColor3fv(bkgColor.getClusterColor(), 0);
            gl.glRasterPos3f(el.getX(), el.getY(), el.getZ() + el.getSize() * 2.5f);

            jogl2ps.gl2psText(el.toString(), "Helvetica", 12);
            glut.glutBitmapString(GLUT.BITMAP_HELVETICA_12, el.toString());

            //draw a line from the title to the center of the sphere that
            //represents the cluster
            gl.glBegin(GL.GL_LINES);
            gl.glVertex3f(el.getX(), el.getY(), 0.0f);
            gl.glVertex3f(el.getX(), el.getY(), el.getZ() + el.getSize() * 2.5f);
            gl.glEnd();

            gl.glEnable(GL.GL_LIGHTING);
        }

        gl.glPushMatrix();
        gl.glColor3fv(el.getColor(), 0);
        gl.glTranslatef(el.getX(), el.getY(), el.getZ());
        glut.glutSolidSphere(el.getSize(), 30, 30);
        gl.glPopMatrix();
    }

    public boolean isInside(GraphicalElement el, double[] objPos) {
        float dist = (float) Math.sqrt((el.getX() - objPos[0]) * (el.getX() - objPos[0]) +
                (el.getY() - objPos[1]) * (el.getY() - objPos[1]) +
                (el.getZ() - objPos[2]) * (el.getZ() - objPos[2]));
        return (dist <= el.getSize() * 1.1f);
    }

    private static final long serialVersionUID = 27L;
}
