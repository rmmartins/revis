/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.view.draw.tridimensional;

import vispipeline.hpex.view.HPExView;
import vispipeline.hpex.view.color.ColorTable;
import vispipeline.hpex.view.draw.GLPanel;
import javax.media.opengl.GLCapabilities;

/**
 * This class is a panel where 3D graphical elements are drawn, and it is 
 * attached to a GLRenderer3D
 * 
 * @author Fernando Vieira Paulovich
 */
public class GLPanel3D extends GLPanel {

    /**
     * Creates a new instance of GLPanel3D
     * @param glcaps The GL parameters.
     * @param colorTable The color table used to color the graphical elements
     * @param view The HPExView object where this panel will be included.
     */
    public GLPanel3D(GLCapabilities glcaps, ColorTable colorTable, HPExView view) {
        super(glcaps, colorTable, view);
        this.renderer = new GLRenderer3D(this);
    }

}
