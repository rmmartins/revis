/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.view.draw.tridimensional;

import com.sun.opengl.util.BufferUtil;
import vispipeline.hpex.view.draw.GLRenderer;
import vispipeline.hpex.view.draw.GraphicalElement;
import vispipeline.hpex.view.draw.listeners.JoinListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseMotionListener;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import jogl2ps.GL2PS;

/**
 * This class represents a 3D render. This class must be linked with a GLPanel3D 
 * in order to be possible to properly render the projection. This class adds 
 * zoom, pan, mouse click, and mouse moved listeners to the panel.  Also adds a 
 * trackball listener.
 * 
 * @author Fernando Vieira Paulovich
 */
public class GLRenderer3D extends GLRenderer {

    /** Creates a new instance of GLRenderer3D and adds listeners to the panel
     * attached to this renderer.
     * @param panel The panel which this render is attached.
     */
    public GLRenderer3D(GLPanel3D panel) {
        super(panel);
        this.trackball = new Trackball(panel);

        if (this.panel != null) {
            panel.addGLEventListener(this);
            MouseClickedListener mouseClickedListener = new MouseClickedListener(panel);
            ZoomListener zoomListener = new ZoomListener(panel);
            PanListener panListener = new PanListener(panel);
            MouseMovedListener mouseMovedListener = new MouseMovedListener(panel);
        }
    }

    @Override
    public void reset() {
        this.setPanx(0);
        this.setPany(0);

        this.setWin(GLRenderer.WINDOW_SIZE);
        this.rotmat = null;
        this.trackball = new Trackball((GLPanel3D) this.panel);

        this.viewport = new int[4];
        this.mvmatrix = new double[16];
        this.projmatrix = new double[16];

        if (this.drawable != null) {
            drawable.getContext().makeCurrent();
            this.defineVisualParameters(this.drawable);
            drawable.getContext().release();
        }

        this.redisplay = true;
        this.updateImage = true;
    }

    /**
     * It is a JOGL method where the visualization parameters are initialized.
     * @param gLAutoDrawable the GL context
     */
    public void init(GLAutoDrawable gLAutoDrawable) {
        this.drawable = gLAutoDrawable;
        GL gl = gLAutoDrawable.getGL();

        gl.glEnable(GL.GL_CULL_FACE);
        gl.glEnable(GL.GL_LIGHTING);
        gl.glEnable(GL.GL_LIGHT0);
        gl.glEnable(GL.GL_DEPTH_TEST);
        gl.glEnable(GL.GL_COLOR_MATERIAL);
        gl.glShadeModel(GL.GL_SMOOTH);

        //creating the cluster list
        this.imageId = gl.glGenLists(1);
        this.overviewId = gl.glGenLists(1);

        this.lighting(gl);

        //anti-aliasing enabling
        gl.glEnable(GL.GL_BLEND);
        gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);

        gl.glEnable(GL.GL_LINE_SMOOTH);
        gl.glHint(GL.GL_LINE_SMOOTH_HINT, GL.GL_NICEST);
    }

    public void displayChanged(GLAutoDrawable gLAutoDrawable, boolean b, boolean b0) {
        this.drawable = gLAutoDrawable;
    }

    /**
     * This is a JOGL method called to draw the objects.
     * @param gLAutoDrawable the GL context
     */
    public void display(GLAutoDrawable gLAutoDrawable) {
        if (this.redisplay) {
            this.drawable = gLAutoDrawable;
            GL gl = gLAutoDrawable.getGL();

            float[] bkg = this.bkgColor.getBackgroundColor();
            gl.glClearColor(bkg[0], bkg[1], bkg[2], 1.0f); //backgroung color
            gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

            gl.glMatrixMode(GL.GL_MODELVIEW);
            gl.glLoadIdentity();

            this.rotmat = trackball.getRotMatrix(this.rotmat);
            if (this.rotmat != null) {
                gl.glMultMatrixf(this.rotmat, 0);
            }

            //used for gluUnproject
            gl.glGetIntegerv(GL.GL_VIEWPORT, this.viewport, 0);
            gl.glGetDoublev(GL.GL_MODELVIEW_MATRIX, this.mvmatrix, 0);
            gl.glGetDoublev(GL.GL_PROJECTION_MATRIX, this.projmatrix, 0);

            if (this.updateImage) {
                this.createImage(gLAutoDrawable);
                this.updateImage = false;
            }

            gl.glCallList(this.imageId);

            this.drawJoinLines(gLAutoDrawable);

            gl.glFlush(); //execute all commands

            this.redisplay = false;
        }
    }

    /**
     * This is a JOGL method called when the window is resized. It keeps the 
     * aspect ratio of the objects on the resized window.
     * @param gLAutoDrawable the GL context
     * @param x the left coordinate of the window
     * @param y the bottom coordinate of the window
     * @param w the window's width
     * @param h the window's height
     */
    public void reshape(GLAutoDrawable gLAutoDrawable, int x, int y, int w, int h) {
        this.drawable = gLAutoDrawable;

        GL gl = gLAutoDrawable.getGL();
        if (h == 0) {
            h = 1;
        }
        this.width = w;
        this.height = h;

        gl.glViewport(0, 0, w, h);

        this.defineVisualParameters(this.drawable);

        this.redisplay = true;
        this.updateImage = true;
    }

    private void lighting(GL gl) {
        float[] luzAmbiente = {0.3f, 0.3f, 0.3f, 1.0f};
        float[] especularidade = {0.8f, 0.8f, 0.8f, 1.0f};
        float especMaterial = 15f; //between 0 and 128 - brightness concentration

        gl.glMaterialfv(GL.GL_FRONT, GL.GL_SPECULAR, especularidade, 0);
        gl.glMaterialf(GL.GL_FRONT, GL.GL_SHININESS, especMaterial);
        gl.glLightModelfv(GL.GL_LIGHT_MODEL_AMBIENT, luzAmbiente, 0);

        //Define a luz 1
        float[] luzDifusa = new float[]{0.75f, 0.75f, 0.75f, 1.0f};// "cor"
        float[] luzEspecular = new float[]{0.7f, 0.7f, 0.7f, 1.0f};// "brilho"
        float[] posicaoLuz = new float[]{GLRenderer3D.WINDOW_SIZE * 5.0f,
            GLRenderer3D.WINDOW_SIZE * 5.0f, GLRenderer3D.WINDOW_SIZE * 5.0f, 1.0f
        };

        // Define os par�metros da luz de n�mero 0
        gl.glLightfv(GL.GL_LIGHT0, GL.GL_AMBIENT, luzAmbiente, 0);
        gl.glLightfv(GL.GL_LIGHT0, GL.GL_DIFFUSE, luzDifusa, 0);
        gl.glLightfv(GL.GL_LIGHT0, GL.GL_SPECULAR, luzEspecular, 0);
        gl.glLightfv(GL.GL_LIGHT0, GL.GL_POSITION, posicaoLuz, 0);
    }

    @Override
    protected void defineVisualParameters(GLAutoDrawable gLAutoDrawable) {
        this.drawable = gLAutoDrawable;

        GL gl = gLAutoDrawable.getGL();

        gl.glMatrixMode(GL.GL_PROJECTION);
        gl.glLoadIdentity();

        if (width <= height) {
            gl.glOrtho(-win + panx, win + panx, -win * (height / width) + pany,
                    win * (height / width) + pany, -GLRenderer3D.WINDOW_SIZE * 10.0f,
                    GLRenderer3D.WINDOW_SIZE * 10.0f);
        } else {
            gl.glOrtho(-win * (width / height) + panx, win * (width / height) + panx,
                    -win + pany, win + pany, -GLRenderer3D.WINDOW_SIZE * 10.0f,
                    GLRenderer3D.WINDOW_SIZE * 10.0f);
        }
    }

    public void drawJoinLines(GLAutoDrawable drawable) {
        GL gl = drawable.getGL();
        GL2PS jogl2ps = new GL2PS();

        ArrayList<GraphicalElement> selected = JoinListener.getSelected();
        int size = selected.size();

        if (size > 1) {
            gl.glLineWidth(2.0f);
            jogl2ps.gl2psLineWidth(2.0f);
            gl.glColor3fv(bkgColor.getClusterColor(), 0);

            for (int i = 0; i < size - 1; i++) {
                gl.glBegin(GL.GL_LINES);
                gl.glVertex3f(selected.get(i).getX(), selected.get(i).getY(),
                        selected.get(i).getZ() + selected.get(i).getSize() * 1.1f);
                gl.glVertex3f(selected.get(i + 1).getX(), selected.get(i + 1).getY(),
                        selected.get(i + 1).getZ() + selected.get(i + 1).getSize() * 1.1f);
                gl.glEnd();
            }

            gl.glLineWidth(1.0f);
            jogl2ps.gl2psLineWidth(1.0f);
        }
    }

    /**
     * This class implements the Zoom listener.
     */
    public class ZoomListener extends MouseAdapter implements MouseMotionListener {

        /** Creates a new instance of ZoomListener
         * @param panel The panel which this listener is attached.
         */
        public ZoomListener(GLPanel3D panel) {
            panel.addMouseMotionListener(this);
            panel.addMouseListener(this);
        }

        public void mouseMoved(MouseEvent e) {
        }

        public void mouseDragged(MouseEvent e) {
            if (this.execute) {
                if (e.getY() > this.prevy) {
                    this.prevy = e.getY();
                    if (win < GLRenderer.WINDOW_SIZE * 10) {
                        setWin(win * 1.10f);
                    }
                } else {
                    this.prevy = e.getY();
                    if (win > 0) {
                        setWin(win * 0.809f);
                    }
                }

                if (drawable != null) {
                    drawable.getContext().makeCurrent();
                    defineVisualParameters(drawable);
                    drawable.getContext().release();
                }

                redisplay = true;
                if (panel != null) {
                    panel.repaint();
                }
            }
        }

        @Override
        public void mousePressed(MouseEvent e) {
            super.mousePressed(e);
            if (e.getButton() == MouseEvent.BUTTON3) {
                this.prevy = e.getY();
                this.execute = true;
            } else {
                this.execute = false;
            }
        }

        private boolean execute = false;
        private int prevy = 0;
    }

    /**
     * This class implements a Pan listener.
     */
    public class PanListener extends MouseAdapter implements MouseMotionListener {

        /** Creates a new instance of PanListener
         * @param panel The panel which this listener is attached.
         */
        public PanListener(GLPanel3D panel) {
            panel.addMouseListener(this);
            panel.addMouseMotionListener(this);
        }

        public void mouseMoved(MouseEvent e) {
        }

        public void mouseDragged(MouseEvent e) {
            if (this.execute) {
                float diffx = ((float) (e.getX() - this.prevx));
                this.prevx = e.getX();
                setPanx(panx - diffx);

                float diffy = ((float) (e.getY() - this.prevy));
                this.prevy = e.getY();
                setPany(getPany() + diffy);

                if (drawable != null) {
                    drawable.getContext().makeCurrent();
                    defineVisualParameters(drawable);
                    drawable.getContext().release();
                }

                redisplay = true;
                if (panel != null) {
                    panel.repaint();
                }
            }
        }

        @Override
        public void mousePressed(MouseEvent e) {
            super.mousePressed(e);
            if (e.getButton() == MouseEvent.BUTTON2) {
                this.prevx = e.getX();
                this.prevy = e.getY();
                this.execute = true;
            } else {
                this.execute = false;
            }
        }

        private boolean execute = false;
        private int prevy = 0;
        private int prevx = 0;
    }

    /**
     * This class implements the mouse clicked listener. This automatically 
     * calls the mouseClicked() method from the attached panel. 
     */
    public class MouseClickedListener extends MouseAdapter {

        /** Creates a new instance of MouseClickedListener
         * @param panel The panel which this listener is attached.
         */
        public MouseClickedListener(GLPanel3D panel) {
            panel.addMouseListener(this);
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            super.mouseClicked(e);

            if (e.getButton() == MouseEvent.BUTTON1) {
                if (drawable != null) {
                    drawable.getContext().makeCurrent();
                    GL gl = drawable.getGL();

                    double[] worldPos = new double[4];
                    double winX = e.getX();
                    double winY = viewport[3] - (int) e.getY() - 1;
                    FloatBuffer winZ = BufferUtil.newFloatBuffer(4);

                    gl.glReadPixels((int) winX, (int) winY, 1, 1,
                            GL.GL_DEPTH_COMPONENT, GL.GL_FLOAT, winZ);
                    glu.gluUnProject(winX, winY, winZ.get(), mvmatrix, 0,
                            projmatrix, 0, viewport, 0, worldPos, 0);

                    if (panel != null) {
                        panel.mouseClicked(e, worldPos);
                    }

                    drawable.getContext().release();
                }
            }
        }

    }

    /**
     * This class implements a Mouse Moved listener. This automatically 
     * calls the mouseMoved() method from the attached panel. 
     */
    public class MouseMovedListener extends MouseMotionAdapter {

        /** Creates a new instance of MouseMovedListener
         * @param panel The panel which this listener is attached.
         */
        public MouseMovedListener(GLPanel3D panel) {
            panel.addMouseMotionListener(this);
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            if (drawable != null) {
                drawable.getContext().makeCurrent();
                GL gl = drawable.getGL();

                double[] worldPos = new double[4];
                double winX = e.getX();
                double winY = viewport[3] - (int) e.getY() - 1;
                FloatBuffer winZ = BufferUtil.newFloatBuffer(4);

                gl.glReadPixels((int) winX, (int) winY, 1, 1,
                        GL.GL_DEPTH_COMPONENT, GL.GL_FLOAT, winZ);
                glu.gluUnProject(winX, winY, winZ.get(), mvmatrix, 0,
                        projmatrix, 0, viewport, 0, worldPos, 0);

                if (panel != null) {
                    panel.mouseMoved(e, worldPos);
                }

                drawable.getContext().release();
            }
        }

    }

    private Trackball trackball;
    private float rotmat[];
    private int viewport[] = new int[4];
    private double mvmatrix[] = new double[16];
    private double projmatrix[] = new double[16];
}
