/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.view.draw;

import java.io.Serializable;
import javax.media.opengl.GLAutoDrawable;

/**
 * This is an abstract class responsible for draw the graphical elements and
 * to determine if a position is inside the element.
 * 
 * @author Fernando Vieira Paulovich
 */
public abstract class ElementHelper implements Serializable {

    /**
     * Draws the graphical element.
     * @param overview Indicates if the image is an overview or not.
     * @param bkgColor The background color scheme.
     * @param element The element to be drawn.
     * @param drawable The GLAutoDrawable where the element is drawn.
     */
    public abstract void draw(boolean overview, Background.Color bkgColor, GraphicalElement element, GLAutoDrawable drawable);

    /**
     * Given the coordinates of a point on the object space, return 
     * true if this points is inside the graphical element and false otherwise.
     * @param element The graphical element.
     * @param objPos The point coordinates.
     * @return Return true if the point is inside the graphical element, false 
     * otherwise.
     */
    public abstract boolean isInside(GraphicalElement element, double[] objPos);

    private static final long serialVersionUID = 27L;
}
