/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.view.draw;

import vispipeline.hpex.view.HPExView;
import vispipeline.hpex.view.color.ColorTable;
import vispipeline.hpex.view.draw.bidimensional.ClusterHelper2D;
import vispipeline.hpex.view.draw.bidimensional.DataInstanceHelper2D;
import vispipeline.hpex.view.draw.bidimensional.GLPanel2D;
import vispipeline.hpex.view.draw.tridimensional.ClusterHelper3D;
import vispipeline.hpex.view.draw.tridimensional.DataInstanceHelper3D;
import vispipeline.hpex.view.draw.tridimensional.GLPanel3D;
import javax.media.opengl.GLCapabilities;

/**
 * This class retunrs a 2D or 3D panel depending on the type of panel. It also
 * changes the renderers of the Data Instance and Cluster graphical elements.
 * 
 * @author Fernando Vieira Paulovich
 */
public class PanelFactory {

    /**
     * This enumeration indicates if this element is (2D or 3D).
     */
    public static enum Type {

        BI_DIMENSIONAL, TRI_DIMENSIONAL
    }

    /**
     * Returns a new 2D or 3D GLPanel object depending on the type of panel.      
     * @param type The type of hierarchy to be drawn.
     * @param colorTable The color table used to color the graphical elements
     * on the GLPanel.
     * @param view The HPExView where this panel will be included.
     * @return The new GLPanel instance.
     */
    public static GLPanel newInstance(PanelFactory.Type type, ColorTable colorTable,
            HPExView view) {
        //creting the projection panel
        GLCapabilities glcaps = new GLCapabilities();
        glcaps.setAccumBlueBits(16);
        glcaps.setAccumGreenBits(16);
        glcaps.setAccumRedBits(16);
        glcaps.setDoubleBuffered(true);
        glcaps.setHardwareAccelerated(true);

        if (type == PanelFactory.Type.TRI_DIMENSIONAL) {
            DataInstanceElement.setDataInstaneHelper(new DataInstanceHelper3D());
            ClusterElement.setClusterHelper(new ClusterHelper3D());
            return new GLPanel3D(glcaps, colorTable, view);
        } else {
            DataInstanceElement.setDataInstaneHelper(new DataInstanceHelper2D());
            ClusterElement.setClusterHelper(new ClusterHelper2D());
            return new GLPanel2D(glcaps, colorTable, view);
        }
    }

}
