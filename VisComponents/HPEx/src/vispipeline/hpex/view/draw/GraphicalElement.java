/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.view.draw;

import vispipeline.hpex.view.draw.ClusterHierarchy.Scalar;
import vispipeline.hpex.view.color.ColorTable;
import java.util.ArrayList;
import vispipeline.matrix.sparse.SparseVector;

/**
 * This class represents a graphical element (which can be a cluster of data
 * instance, or a single data instance).
 * 
 * @author Fernando Vieira Paulovich
 */
public abstract class GraphicalElement extends GraphicalTreeNode {

    /**
     * This enumeration represents the drawn state of the graphical element. 
     * It can be : EXPANDED, indicating that a cluster element is expanded and 
     * its children are drawn; DRAWN, indicating that the element is drawn on the
     * screen; and NOT_DRAN, indicating that the element is neither drawn on the
     * screen nor expanded.
     */
    public static enum DrawState {

        EXPANDED, DRAWN, NOT_DRAWN
    }

    /**
     * Creates a new instance of GraphicalElement
     */
    public GraphicalElement() {
    }

    /**
     * Returns the vector associated with this graphical element. For data 
     * instances, this vector is the instance in itself. For clusters, this
     * vector is the cluster's representative, normally its centroid.
     * @return The vector associated to its graphical element.
     */
    public abstract SparseVector getVector();

    /**
     * Returns the element helper associated to this object.
     * @return The element helper.
     */
    public abstract ElementHelper getElementHelper();

    /**
     * Returns the color if this graphical element. A vector with 3 positions,
     * one for each component of an RGB color. 
     * @return The graphical element's color.
     */
    public float[] getColor() {
        return color;
    }

    /**
     * Changes the color of this graphical element acording to a color scale 
     * and a scalar. 
     * @param scalar The scalar used.
     * @param table The color table.
     */
    public void setColor(Scalar scalar, ColorTable table) {
        this.color = table.getColor(this.getNormalizedScalarValue(scalar));
    }

    /**
     * Returns the size of this graphical element on the object space.
     * @return The graphical element's size.
     */
    public float getSize() {
        return size;
    }

    /**
     * Changes the graphical element's size on the object space. 
     * @param size The new graphical element's size.
     */
    public void setSize(float size) {
        this.size = size;
    }

    /**
     * Returns the X coordinate of this graphical element on the object space.
     * @return The x-coordinate of this element.
     */
    public float getX() {
        return x;
    }

    /**
     * Changes the X coordinate of this graphical element on the object space.
     * @param x The new X coordinate.
     */
    public void setX(float x) {
        this.x = x;
    }

    /**
     * Returns the Y coordinate of this graphical element on the object space.
     * @return The y-coordinate of this element.
     */
    public float getY() {
        return y;
    }

    /**
     * Changes the Y coordinate of this graphical element on the object space.
     * @param y The new Y coordinate.
     */
    public void setY(float y) {
        this.y = y;
    }

    /**
     * Returns the Z coordinate of this graphical element on the object space.
     * @return The z-coordinate of this element.
     */
    public float getZ() {
        return z;
    }

    /**
     * Changes the Z coordinate of this graphical element on the object space.
     * @param z The new Z coordinate.
     */
    public void setZ(float z) {
        this.z = z;
    }

    /**
     * Returns the title associated to this graphical element.
     * @return The graphical element's title.
     */
    public String getTitle() {
        return title;
    }

    /**
     * Changes the title associated to this graphical element.
     * @param title The new title.
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Returns the state of this graphical element.
     * @return The graphical element's state.
     */
    public DrawState getDrawState() {
        return drawState;
    }

    /**
     * Changes the state of this graphical element.
     * @param drawState The new state.
     */
    public void setDrawState(DrawState drawState) {
        this.drawState = drawState;
    }

    /**
     * Returns if the title of this graphical element is shown.
     * @return Return true if the title is visible, or false otherwise.
     */
    public boolean isShowTitle() {
        return this.showTitle;
    }

    /**
     * Changes to show or not show the graphical element's title.
     * @param showTitle true forces the title to be visible and false makes it
     * not visible.
     */
    public void setShowTitle(boolean showTitle) {
        this.showTitle = showTitle;
    }

    /**
     * Adds or changes a value associated to a scalar. If this value is bigger 
     * or smaller than all value associated to this scalar, update it.
     * 
     * @param scalar The scalar.
     * @param value The value of the scalar.
     */
    public void setScalarValue(ClusterHierarchy.Scalar scalar, float value) {
        if (this.scalars.size() > scalar.index) {
            this.scalars.set(scalar.index, value);
        } else {
            int size = this.scalars.size();
            for (int i = 0; i < scalar.index - size; i++) {
                this.scalars.add(0.0f);
            }
            this.scalars.add(value);
        }

        if (value < scalar.min) {
            scalar.min = value;
        }

        if (value > scalar.max) {
            scalar.max = value;
        }
    }

    /**
     * Returns the value associated to a scalar normalized
     * between 0 and 1.
     * 
     * @param scalar The scalar.
     * @return The value of the scalar.
     */
    public float getNormalizedScalarValue(ClusterHierarchy.Scalar scalar) {
        if (this.scalars.size() > scalar.index && scalar.index > -1) {
            if (scalar.max - scalar.min > EPSILON) {
                float value = this.scalars.get(scalar.index);
                return (value - scalar.min) / (scalar.max - scalar.min);
            } else {
                return 0.0f;
            }
        } else {
            return 0.0f;
        }
    }

    /**
     * Returns the value associated to a scalar.
     * 
     * @param scalar The scalar.
     * @return The value of the scalar.
     */
    public float getScalarValue(ClusterHierarchy.Scalar scalar) {
        if (this.scalars.size() > scalar.index && scalar.index > -1) {
            return this.scalars.get(scalar.index);
        } else {
            return 0.0f;
        }
    }

    /**
     * Removes a scalar from this graphical element.
     * @param scalar The scalar to be removed.
     * @return Returns true if the element exist and false otherwise.
     */
    public boolean removeScalar(ClusterHierarchy.Scalar scalar) {
        if (this.scalars.size() > scalar.index && scalar.index > -1) {
            this.scalars.remove(scalar.index);
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return this.title;
    }

    private static float EPSILON = 0.0001f;
    private DrawState drawState = GraphicalElement.DrawState.NOT_DRAWN;
    protected float[] color = new float[]{0.5f, 0.5f, 0.5f};
    protected float x;
    protected float y;
    protected float z;
    protected float size;
    protected String title = "";
    protected boolean showTitle = false;
    private ArrayList<Float> scalars = new ArrayList<Float>();
    private static final long serialVersionUID = 27L;
}
