/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */
package vispipeline.hpex.view.draw;

import vispipeline.hpex.view.HPExView;
import vispipeline.hpex.view.color.ColorTable;
import vispipeline.hpex.view.draw.listeners.ListenerFactory;
import vispipeline.hpex.view.draw.listeners.MouseListener;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.io.IOException;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLJPanel;

/**
 * This panel is used to drawn graphical elements. This is not automatically 
 * associated with a GLRender object. The subclasses must creates a GLRender
 * and assign it to the 'render' attribute.
 * 
 * @author Fernando Vieira Paulovich
 */
public abstract class GLPanel extends GLJPanel {

    /**
     * Creates a new instance of GLPanel
     * @param glcaps the GL parameters are drawn.
     * @param colorTable The color table used to color the graphical elements.
     * @param view The HPExView object where this panel will be included.
     */
    public GLPanel(GLCapabilities glcaps, ColorTable colorTable, HPExView view) {
        super(glcaps);
        this.view = view;
        this.renderer = null;
        this.colorTable = colorTable;

        //add the label listener
        this.addMouseMotionListener(new LabelListener());
    }

    /**
     * Saves the projection as a EPS image.
     * @param filename The file name.
     * @throws IOException Throws an exception if something goes wrong.
     */
    public void saveImage(String filename) throws IOException {
        this.renderer.exportImage(filename);
    }

    /**
     * Returns the renderer associated with this panel.
     * @return The renderer.
     */
    public GLRenderer getRenderer() {
        return renderer;
    }

    /**
     * Returns the color table associated with this panel.
     * @return The color table.
     */
    public ColorTable getColorTable() {
        return this.colorTable;
    }

    /**
     * Return the hierarchy which will be drawn.
     * @return The hierarchy.
     */
    public ClusterHierarchy getHierarchy() {
        return hierarchy;
    }

    /**
     * Changes the hierarchy to be drawn.
     * @param hierarchy The new hierarhcy.
     */
    public void setHierarchy(ClusterHierarchy hierarchy) {
        this.hierarchy = hierarchy;
        this.renderer.updateImage();
    }

    /**
     * Returns the mouse listener type.
     * @return The mouse listener.
     */
    public ListenerFactory.Type getListenerType() {
        return listenerType;
    }

    /**
     * Changes the mouse listener type.
     * @param listenerType The new mouse listener.
     */
    public void setListenerType(ListenerFactory.Type listenerType) {
        this.listenerType = listenerType;
    }

    /**
     * This methos is automatically called when a click is executed on this
     * panel.
     * @param e The generated mouse event.
     * @param worldPos The object position resulting from the mouse click.
     */
    public void mouseClicked(MouseEvent e, double[] worldPos) {
        final GraphicalElement ge = this.hierarchy.getElementByPosition(worldPos);

        if (ge != null) {
            if (e.isControlDown()) {
                ge.setShowTitle(!ge.isShowTitle());
                this.renderer.updateImage();
                this.repaint();
            } else {
                MouseListener ml = ListenerFactory.getListener(this.listenerType);
                ml.mouseClicked(ge, hierarchy, view);
            }
        }
    }

    /**
     * This methos is automatically called when the mouse is moved on this
     * panel.
     * @param e The generated mouse event.
     * @param worldPos The object position resulting from the mouse moved.
     */
    public void mouseMoved(MouseEvent e, double[] worldPos) {
        GraphicalElement ge = this.hierarchy.getElementByPosition(worldPos);

        if (ge != null) {
            this.label = ge.toString();
        } else {
            this.label = null;
        }
    }

    @Override
    protected void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);

        if (this.label != null && this.labelPos != null) {
            Graphics2D g2 = (Graphics2D) graphics;

            g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            g2.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
            g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
            g2.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);

            java.awt.FontMetrics metrics = g2.getFontMetrics(g2.getFont());

            //Getting the label size
            int width = metrics.stringWidth(this.label.trim());
            int height = metrics.getAscent();

            g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 0.75f));
            g2.setPaint(java.awt.Color.BLACK);

            g2.fill(new java.awt.Rectangle(this.labelPos.x - 2, this.labelPos.y - height,
                    width + 4, height + 4));
            g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 1.0f));

            g2.setColor(new java.awt.Color(0.8f, 0.8f, 0.0f));
            g2.drawRect(this.labelPos.x - 2, this.labelPos.y - height, width + 4,
                    height + 4);

            //Drawing the label
            g2.drawString(this.label.trim(), this.labelPos.x, this.labelPos.y);
        }
    }

    /**
     * This class implements the label listener. Used to show the clusters or
     * date instances labels.
     */
    public class LabelListener extends MouseMotionAdapter {

        public LabelListener() {
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            labelPos = e.getPoint();
            repaint();
        }
    }
    private ListenerFactory.Type listenerType = ListenerFactory.Type.SIMPLE_EXPAND;
    protected ClusterHierarchy hierarchy;
    protected GLRenderer renderer;
    protected ColorTable colorTable;
    protected java.awt.Point labelPos;
    protected String label;
    protected HPExView view;
}
