/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.topic;

import vispipeline.hpex.util.Cluster;
import vispipeline.hpex.view.draw.ClusterElement;
import vispipeline.hpex.view.draw.DataInstanceElement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.SortedSet;
import java.util.TreeSet;
import vispipeline.matrix.sparse.SparseMatrix;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class CovarianceTopic {

    /**
     * Constructs an instance of CovarianceTopic
     * @param attr The attributes of the documents x terms matrix which results
     * on the cluster hierarchy.
     * @param nDocs The number os documents on the collection.
     */
    public CovarianceTopic(ArrayList<String> attr, int nDocs) {
        this.attr = attr;
        this.nDocs = nDocs;
    }

    /**
     * Extracts a topic from a cluster.
     * @param topics The topics of the parent clusters.
     * @param element The cluster element which the topic will be extracted.
     * @return The topic.
     */
    public SortedSet<String> create(ArrayList<String> topics, ClusterElement element) {
        Cluster cluster = element.getCluster();

        this.indexes = new int[attr.size()];
        this.index = 0;

        //creating an auxiliary matrix to store the vectors
        //from the cluster
        SparseMatrix spmaux = new SparseMatrix();        
        for (int i = 0; i < cluster.getInstancesCount(); i++) {
            spmaux.addRow(((DataInstanceElement)cluster.getInstance(i)).getTopicVector());
        }

        float[][] points = spmaux.toMatrix();

        //reduce the matrix to 200 dimensions to speed-up the topic extraction
        ArrayList<String> newAttributes = new ArrayList<String>();
        points = this.reduceMatrix(points, newAttributes);

        //add all parent topic as terms already used
        for (String ptopic : topics) {
            if (newAttributes.contains(ptopic)) {
                int index_aux = newAttributes.indexOf(ptopic);
                this.addIndex(index_aux);
            }
        }

        return this.extractTopic(points, newAttributes);
    }

    private SortedSet<String> extractTopic(float[][] points, ArrayList<String> newAttributes) {
        //Extracting the mean of the points
        float[] mean = new float[points.length];
        Arrays.fill(mean, 0.0f);
        for (int i = 0; i < points.length; i++) {
            //calculating
            for (int j = 0; j < points[i].length; j++) {
                mean[i] += points[i][j];
            }
            mean[i] /= points[i].length;

            //extracting
            for (int j = 0; j < points[i].length; j++) {
                points[i][j] -= mean[i];
            }
        }

        //Get the two attr with largest covariance
        float gcov1 = Float.MIN_VALUE;
        int icov = 0;
        int jcov = 0;
        for (int i = 0; i < points[0].length - 1; i++) {
            for (int j = points[0].length - 1; j > i; j--) {
                if (/*!this.containsIndex(i)&&*/!this.containsIndex(j)) {
                    float aux = this.covariance(points, i, j);
                    if (gcov1 < aux) {
                        gcov1 = aux;
                        icov = i;
                        jcov = j;
                    }
                }
            }
        }

        this.addIndex(icov);
        this.addIndex(jcov);

        SortedSet<String> topic = new TreeSet<String>();
        topic.add(newAttributes.get(icov));
        topic.add(newAttributes.get(jcov));

        for (int i = 0; i < points[0].length - 1; i++) {
            if (!this.containsIndex(i)) {
                float aux = (this.covariance(points, icov, i) +
                        this.covariance(points, jcov, i)) / 2;

                if (aux / gcov1 > 0.65f) {
                    topic.add(newAttributes.get(i));
                    this.addIndex(i);
                }
            }
        }

        return topic;
    }

    //calculate the covariance between columns a and b
    private float covariance(float[][] points, int a, int b) {
        float covariance = 0.0f;
        for (int i = 0; i < points.length; i++) {
            covariance += points[i][a] * points[i][b];
        }
        covariance /= (points.length - 1);
        return covariance;
    }

    private boolean containsIndex(int index) {
        for (int i = 0; i < this.index; i++) {
            if (this.indexes[i] == index) {
                return true;
            }
        }

        return false;
    }

    private void addIndex(int index) {
        this.indexes[this.index] = index;
        this.index++;
    }

    public class Pair implements Comparable {

        public Pair(int index, float freq) {
            this.index = index;
            this.freq = freq;
        }

        public int compareTo(Object o) {
            float diff = ((Pair) o).freq - this.freq;
            return (diff == 0.0f) ? 0 : (diff > 0.0f) ? 1 : -1;
        }

        public int index;
        public float freq;
    }

    private float[][] reduceMatrix(float[][] points, ArrayList<String> newAttributes) {
        ArrayList<Pair> pairs = new ArrayList<Pair>();

        for (int j = 0; j < points[0].length; j++) {
            float freq = 0.0f;
            for (int i = 0; i < points.length; i++) {
                freq += points[i][j];
            }
            pairs.add(new Pair(j, freq));
        }

        Collections.sort(pairs);

        //keep on the new points matrix no more than 200 dimensions
        float[][] newpoints = new float[points.length][];
        for (int i = 0; i < points.length; i++) {
            newpoints[i] = new float[(points[i].length < 200) ? points[i].length : 200];

            for (int j = 0; j < newpoints[i].length; j++) {
                newpoints[i][j] = points[i][pairs.get(j).index];
            }
        }

        for (int i = 0; i < newpoints[0].length; i++) {
            newAttributes.add(this.attr.get(pairs.get(i).index));
        }

        return newpoints;
    }

    private float nDocs;
    private ArrayList<String> attr;
    private int[] indexes;
    private int index;
}
