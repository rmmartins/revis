/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.topic;

import vispipeline.hpex.view.draw.ClusterElement;
import vispipeline.hpex.view.draw.ClusterHierarchy;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.SortedSet;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class TopicExtractor {

    /** Creates a new instance of TopicExtractor */
    public TopicExtractor() {
    }

    public ArrayList<String> extractTopic(ClusterHierarchy hierarchy, ClusterElement element) {
        ArrayList<String> topic = new ArrayList<String>();

        int nDocs = hierarchy.getRoot().getCluster().getInstancesCount();
        ArrayList<String> attr = hierarchy.getAttributes();

        //getting the topics of the ancestors
        ArrayList<String> parTopic = new ArrayList<String>();

        ClusterElement ce_temp = element;
        while (ce_temp.getParent() != null) {
            if (ce_temp.getParent() instanceof ClusterElement) {
                ArrayList<String> parTopic_aux = ((ClusterElement) ce_temp.getParent()).getTopic();

                if (parTopic_aux != null) {
                    for (String t : parTopic_aux) {
                        if (!parTopic.contains(t)) {
                            parTopic.add(t);
                        }
                    }
                }

                ce_temp = (ClusterElement) ce_temp.getParent();
            }
        }

        //creating the topic
        CovarianceTopic ct = new CovarianceTopic(attr, nDocs);
        SortedSet<String> topic_aux = ct.create(parTopic, element);

        Iterator<String> it = topic_aux.iterator();
        while (it.hasNext()) {
            topic.add(it.next());
        }

        return topic;
    }

}
