/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.util;

import java.io.IOException;
import java.util.ArrayList;
import vispipeline.distance.dissimilarity.AbstractDissimilarity;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class KMeans {

    /** Creates a new instance of KMeans
     * @param nrClusters 
     */
    public KMeans(int nrClusters) {
        this.nrClusters = nrClusters;
    }

    public ArrayList<Cluster> execute(AbstractDissimilarity diss,
            Cluster cluster) throws IOException {
        //create an initial configuration of the clusters
        this.initialize(diss, cluster);

        //control the looping
        boolean isModified = false;
        int iteration = 0;

        do {
            //storing the previous centroids
            ArrayList<Instance> prevCent = new ArrayList<Instance>();
            for (int i = 0; i < this.nrClusters; i++) {
                prevCent.add(this.clusters.get(i).getMean());
            }

            //creating the new clusters
            this.clusters = new ArrayList<Cluster>();
            for (int i = 0; i < this.nrClusters; i++) {
                this.clusters.add(new Cluster());
            }

            //assigning the instances to the nearest centroid
            int length = cluster.getInstancesCount();
            for (int i = 0; i < length; i++) {
                float dist = Float.MAX_VALUE;
                int index = 0;

                for (int j = 0; j < this.nrClusters; j++) {
                    float dist_aux = diss.calculate(cluster.getInstance(i).getVector(),
                            prevCent.get(j).getVector());
                    if (dist_aux < dist) {
                        dist = dist_aux;
                        index = j;
                    } else if ((dist_aux - dist) < EPSILON) {
                        if (this.clusters.get(index).getInstancesCount() >
                                this.clusters.get(j).getInstancesCount()) {
                            index = j;
                        }
                    }
                }

                this.clusters.get(index).addInstance(cluster.getInstance(i));
            }

            if (iteration != this.nrIterations - 1) {
                //if there is an empty cluster
                for (int i = 0; i < this.nrClusters; i++) {
                    if (this.clusters.get(i).getInstancesCount() == 0) {
                        //search the largest cluster
                        int nrEl = 0;
                        int index = 0;
                        for (int j = 0; j < this.nrClusters; j++) {
                            if (this.clusters.get(j).getInstancesCount() > nrEl) {
                                nrEl = this.clusters.get(j).getInstancesCount();
                                index = j;
                            }
                        }

                        //get a instance randomly
                        int rand = (int) (Math.random() * (nrEl - 1));
                        Instance ins = this.clusters.get(index).getInstance(rand);
                        this.clusters.get(index).removeInstance(ins);
                        this.clusters.get(i).addInstance(ins);
                    }
                }

                //verifying if the centroids were modified
                isModified = false;
                for (int i = 0; i < this.nrClusters; i++) {
                    if (!prevCent.get(i).equals(this.clusters.get(i).getMean())) {
                        isModified = true;
                        break;
                    }
                }
            }

        } while (isModified && ++iteration < this.nrIterations);

        //removing the empty clusters
        for (int i = this.clusters.size() - 1; i >= 0; i--) {
            if (this.clusters.get(i).getInstancesCount() == 0) {
                this.clusters.remove(i);
            }
        }

        return this.clusters;
    }

    public void setInitialCentroids(ArrayList<Instance> initialCentroids) throws IOException {
        if (this.nrClusters == initialCentroids.size()) {
            this.initialCentroids = initialCentroids;
        } else {
            throw new IOException("Wrong number of initial centroids.");
        }
    }

    private void initialize(AbstractDissimilarity diss, Cluster cluster) {
        //if the initial centroids are not given, initilize them
        //with some points
        if (initialCentroids == null) {
            int step = (cluster.getInstancesCount() - 1) / this.nrClusters;
            this.initialCentroids = new ArrayList<Instance>();
            for (int i = 0; i < this.nrClusters; i++) {
                this.initialCentroids.add(cluster.getInstance(i * step));
            }
        }

        //creating the clusters
        this.clusters = new ArrayList<Cluster>();
        for (int i = 0; i < this.nrClusters; i++) {
            this.clusters.add(new Cluster());
        }

        //assigning the instances to the nearest centroid
        int length = cluster.getInstancesCount();
        for (int i = 0; i < length; i++) {
            float dist = Float.MAX_VALUE;
            int index = 0;

            for (int j = 0; j < this.nrClusters; j++) {
                float dist_aux = diss.calculate(cluster.getInstance(i).getVector(),
                        this.initialCentroids.get(j).getVector());
                if (dist_aux < dist) {
                    dist = dist_aux;
                    index = j;
                }
            }

            this.clusters.get(index).addInstance(cluster.getInstance(i));
        }

        //if there is an empty cluster
        for (int i = 0; i < this.nrClusters; i++) {
            if (this.clusters.get(i).getInstancesCount() == 0) {
                //search the largest cluster
                int nrEl = 0;
                int index = 0;
                for (int j = 0; j < this.nrClusters; j++) {
                    if (this.clusters.get(j).getInstancesCount() > nrEl) {
                        nrEl = this.clusters.get(j).getInstancesCount();
                        index = j;
                    }
                }

                //get a instance randomly
                int rand = (int) (Math.random() * (nrEl - 1));
                Instance ins = this.clusters.get(index).getInstance(rand);
                this.clusters.get(index).removeInstance(ins);
                this.clusters.get(i).addInstance(ins);
                this.initialCentroids.set(i, ins);
            }
        }
    }

    private ArrayList<Instance> initialCentroids;
    private static final float EPSILON = 0.00001f;
    private ArrayList<Cluster> clusters;
    private int nrClusters;
    private int nrIterations = 15;
}
