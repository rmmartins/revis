/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.util;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public interface HPExConstants {

//    public static final String LABEL="label";
//    public static final String LABELS="labels";
//    public static final String CDATA="cdata";
//    public static final String FNAME="file name";
//    public static final String NGRAM="ngram";
//    public static final String TITLE="title";
//    public static final String KMEANS="kmeans-";
//    public static final String DBSCAN="DBSCAN-";
//    public static final String COVARIANCE="covariance-";
//    public static final String PIVOTS="pivots";
//    public static final String DELAUNAY="Delaunay";
//    
//    public static final String ALINK="hc-alink";
    
    
    public static final String DOTS = "...";
    public static final String CLASS = "class";
    public static final String DENSITY = "density";
    public static final String JOIN = "join-scalars";
    public static final String PATCOM = "[^A-Za-z������������������������������������]";
}
