/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.util;

import vispipeline.hpex.view.draw.ClusterElement;
import vispipeline.hpex.view.draw.ClusterHierarchy;
import vispipeline.hpex.view.draw.DataInstanceElement;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class HPExFileIO {

    /**
     * Creates a new instance of HPExFileIO
     */
    public HPExFileIO() {
    }

    public void write(String filename, ClusterHierarchy hierarchy) {
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(filename));
            out.writeObject(hierarchy);
            DataInstanceElement.serializeStaticState(out);
            ClusterElement.serializeStaticState(out);
            out.flush();
            out.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ClusterHierarchy read(String filename) throws IOException {
        ClusterHierarchy hierarchy = null;

        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(filename));
            hierarchy = (ClusterHierarchy) in.readObject();
            DataInstanceElement.deserializeStaticState(in);
            ClusterElement.deserializeStaticState(in);
            in.close();

            //resetting the hierarchy
            hierarchy.reset();

        } catch (FileNotFoundException e) {
            throw new IOException("Problems reading the file: " + e.getMessage());
        } catch (IOException e) {
            throw new IOException("Problems reading the file: " + e.getMessage());
        } catch (ClassNotFoundException e) {
            throw new IOException("Problems reading the file: " + e.getMessage());
        }

        return hierarchy;
    }

}
