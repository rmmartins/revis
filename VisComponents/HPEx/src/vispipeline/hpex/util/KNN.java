/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.util;

import java.util.ArrayList;
import vispipeline.distance.DistanceMatrix;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class KNN {

    public KNN(int nrneighbors) {
        this.nrneighbors = nrneighbors;
    }

    public ArrayList<ArrayList<Integer>> execute(DistanceMatrix dmat) {
        assert (dmat.getElementCount() >= nrneighbors) :
                "Error: requestued more neighbors than available points.";

        //The list with the points identification
        ArrayList<ArrayList<Integer>> idsList = new ArrayList<ArrayList<Integer>>();

        //The list of distances to each point
        ArrayList<ArrayList<Float>> distList = new ArrayList<ArrayList<Float>>();

        for (int i = 0; i < dmat.getElementCount(); i++) {
            //Create a list of distances with the maximum distance on the distance matrix
            ArrayList<Integer> ids = new ArrayList<Integer>();
            ArrayList<Float> distances = new ArrayList<Float>();

            for (int j = 0; j < this.nrneighbors; j++) {
                ids.add(0);
                distances.add(dmat.getMaxDistance());
            }

            idsList.add(ids);
            distList.add(distances);
        }

        //For each point
        for (int j = 0; j < dmat.getElementCount(); j++) {
            //find the K nearest neighbors
            for (int k = 0; k < dmat.getElementCount(); k++) {
                if (j != k) {
                    addDistance(distList.get(j), idsList.get(j), k, dmat.getDistance(j, k));
                }
            }
        }

        return idsList;
    }

    //Adiciona uma dist�ncia se a mesma for menor do que amaior dist�ncia da lista.
    //As dist�ncias nessa lista est�o em ordem decrescente.
    private boolean addDistance(ArrayList<Float> distList,
            ArrayList<Integer> idsList, int id, float value) {
        if (distList.get(0) > value) {
            int i = 0;
            while (i < distList.size() && distList.get(i) > value) {
                if (i < distList.size() - 1) {
                    distList.set(i, distList.get(i + 1));
                    idsList.set(i, idsList.get(i + 1));
                }
                i++;
            }
            distList.set(i - 1, value);
            idsList.set(i - 1, id);
            return true;
        }
        return false;
    }

    private int nrneighbors;
}
