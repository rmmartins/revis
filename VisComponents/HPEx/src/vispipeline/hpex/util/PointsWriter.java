/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.util;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import vispipeline.matrix.sparse.SparseMatrix;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class PointsWriter {

    /** Creates a new instance of PointsWriter */
    public PointsWriter(String filename) {
        this.filename = filename;
    }

    public void save(SparseMatrix matrix) throws IOException {
        BufferedWriter out = null;

        try {
            out = new BufferedWriter(new FileWriter(this.filename));

            //Writting the file header
            out.write("#Number objects: ");
            out.write(Integer.toString(matrix.getRowCount()));
            out.write("\n");
            out.write("#Number dimensions: ");
            out.write(Integer.toString(matrix.getRow(0).size()));
            out.write("\n");

            //writting the attributes
            ArrayList<String> attributes = matrix.getAttributes();

            if (attributes != null) {
                for (int i = 0; i < attributes.size() - 1; i++) {
                    out.write(attributes.get(i).replaceAll("<>", " ").trim());
                    out.write(";");
                }
                out.write(attributes.get(attributes.size() - 1).replaceAll("<>", " ").trim());
                out.write("\n");
            } else {
                for (int i = 0; i < matrix.getRow(0).size() - 1; i++) {
                    out.write("attr");
                    out.write(";");
                }
                out.write("attr");
                out.write("\n");
            }

            for (int i = 0; i < matrix.getRowCount(); i++) {
                //writting the filename or reference
                if (matrix.getRow(i).getId() > -1) {
                    out.write(Integer.toString(matrix.getRow(i).getId()));
                } else {
                    out.write("-1");
                }
                out.write(";");

                //writting the point
                float[] dense = matrix.getRow(i).toArray();
                for (int j = 0; j < dense.length; j++) {
                    out.write(Float.toString(dense[j]));
                    out.write(";");
                }

                //writting the point's class
                out.write(Float.toString(matrix.getRow(i).getKlass()));
                out.write("\n");
            }

        } catch (IOException e) {
            throw new IOException("Problems written \"" + this.filename + "\"!");
        } finally {
            if (out != null) {
                try {
                    out.flush();
                    out.close();
                } catch (IOException ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    private String filename;
}
