/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class SystemPropertiesManager {

    /** Creates a new instance of SystemPropertiesManager */
    private SystemPropertiesManager() {
        try {
            //l� o arquivo de propriedades
            File f = new File(this.filename);

            if (f.exists()) {
                this.properties = new Properties();
                FileInputStream in = new FileInputStream(this.filename);
                this.properties.load(in);
                in.close();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static SystemPropertiesManager getInstance() {
        if (_instance == null) {
            _instance = new SystemPropertiesManager();
        }
        return _instance;
    }

    public String getProperty(String id) {
        if (this.properties == null) {
            return "";
        } else {
            if (this.properties.containsKey(id)) {
                return this.properties.getProperty(id);
            } else {
                return "";
            }
        }
    }

    public void setProperty(String id, String value) {
        if (this.properties == null) {
            this.properties = new Properties();
        }
        this.properties.setProperty(id, value);

        try {
            FileOutputStream out = new FileOutputStream(this.filename);
            this.properties.store(out, "Recording the system's properties");
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private String filename = "config/system.properties";
    private Properties properties = null;
    private static SystemPropertiesManager _instance = null;
}
