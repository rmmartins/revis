/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import vispipeline.matrix.sparse.SparseMatrix;
import vispipeline.matrix.sparse.SparseVector;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class PointsReader {

    /** Creates a new instance of PointsReader
     * 
     * @param filename The file name
     */
    public PointsReader(String filename) {
        this.filename = filename;
    }

    public SparseMatrix read() throws IOException {
        SparseMatrix spm = new SparseMatrix();

        BufferedReader in = null;
        try {
            in = new BufferedReader(new FileReader(filename));
            String line = null;

            //Read the attributes
            ArrayList<String> attr = new ArrayList<String>();

            while ((line = in.readLine()) != null && line.trim().length() > 1) {
                //Ignore comments
                if (line.lastIndexOf('#') == -1) {
                    StringTokenizer t = new StringTokenizer(line, ";");

                    while (t.hasMoreTokens()) {
                        String token = t.nextToken();
                        attr.add(token.trim());
                    }
                    break;
                }
            }

            while ((line = in.readLine()) != null && line.trim().length() > 1) {
                //Ignore comments
                if (line.lastIndexOf('#') == -1) {
                    StringTokenizer t = new StringTokenizer(line, ";");

                    //read the filename/reference
                    int id = Integer.parseInt(t.nextToken());

                    //read the points
                    Vector<Float> point_aux = new Vector<Float>();

                    while (t.hasMoreTokens()) {
                        String token = t.nextToken();
                        point_aux.add(Float.parseFloat(token));
                    }

                    //the last point's coordinate is the class
                    float klass = point_aux.remove(point_aux.size() - 1);

                    float[] point = new float[point_aux.size()];
                    for (int i = 0; i < point.length; i++) {
                        point[i] = point_aux.get(i);
                    }

                    spm.addRow(new SparseVector(point, id, klass));
                }
            }

            //Remove the class attribute
            int nR = attr.size() - spm.getRow(0).size();
            for (int i = 0; i < nR; i++) {
                attr.remove(attr.size() - 1);
            }

            spm.setAttributes(attr);

        } catch (FileNotFoundException e) {
            throw new IOException("File " + filename + " does not exist!");
        } catch (IOException e) {
            throw new IOException("Problems reading the file " + filename + " : " + e.getMessage());
        } finally {
            //fechar o arquivo
            if (in != null) {
                try {
                    in.close();
                } catch (IOException ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return spm;
    }

    private String filename;
}
