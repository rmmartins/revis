/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.util;

import vispipeline.hpex.util.filefilter.HPExFileFilter;
import java.awt.Component;
import java.io.File;
import javax.swing.JFileChooser;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class OpenDialog {

    public static int showOpenDialog(HPExFileFilter filter, Component parent) {
        if (OpenDialog.dialog == null) {
            OpenDialog.dialog = new javax.swing.JFileChooser();
        }

        _filename = null;

        dialog.resetChoosableFileFilters();
        dialog.setAcceptAllFileFilterUsed(false);
        if (filter != null) {
            dialog.setFileFilter(filter);
        }
        dialog.setMultiSelectionEnabled(false);
        dialog.setDialogTitle("Open file");
        dialog.setSelectedFile(new File(""));

        SystemPropertiesManager m = SystemPropertiesManager.getInstance();
        dialog.setCurrentDirectory(new File(m.getProperty(filter.getProperty())));

        int result = dialog.showOpenDialog(parent);
        if (result == JFileChooser.APPROVE_OPTION) {
            _filename = dialog.getSelectedFile().getAbsolutePath();
            m.setProperty(filter.getProperty(), dialog.getSelectedFile().getParent());
        }

        return result;
    }

    public static String getFilename() {
        return _filename;
    }

    public static javax.swing.JFileChooser getJFileChooser() {
        if (OpenDialog.dialog == null) {
            OpenDialog.dialog = new javax.swing.JFileChooser();
        }

        return dialog;
    }

    private static String _filename;
    private static javax.swing.JFileChooser dialog;
}
