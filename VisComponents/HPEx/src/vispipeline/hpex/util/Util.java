/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.util;

import vispipeline.hpex.view.draw.ClusterElement;
import vispipeline.hpex.view.draw.ClusterHierarchy;
import vispipeline.hpex.view.draw.ClusterHierarchy.Scalar;
import vispipeline.hpex.view.draw.GraphicalElement;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import vispipeline.matrix.sparse.SparseVector;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class Util {

    public static int countFiles(String corpus) {
        int numberFiles = 0;

        //Capturing the filenames of the zip file
        ZipFile zip = null;
        try {
            zip = new ZipFile(corpus);
            Enumeration entries = zip.entries();
            while (entries.hasMoreElements()) {
                ZipEntry entry = (ZipEntry) entries.nextElement();
                if (!entry.isDirectory()) {
                    numberFiles++;
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (zip != null) {
                try {
                    zip.close();
                } catch (IOException ex) {
                    Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return numberFiles;
    }

    /**
     * Configures the system log.
     * @param console If true, the console log is activated, false it is not.
     * @param file If true, the file log is activated, false it is not.
     * @throws java.io.IOException
     */
    public static void log(boolean console, boolean file) throws IOException {
        String filename = "log%g.txt";
        int limit = 10000000; // 10 Mb
        int numLogFiles = 5;

        if (file) {
            FileHandler handler = new FileHandler(filename, limit, numLogFiles, true);
            handler.setFormatter(new SimpleFormatter());
//          handler.setFormatter(new XMLFormatter());
            handler.setLevel(Level.ALL);
            Logger.getLogger("").addHandler(handler);

            Logger.getLogger("Util").log(Level.INFO, "Adding the file logging.");
        } else {
            Logger.getLogger("Util").log(Level.INFO, "Removing the file logging.");

            Logger rootLogger = Logger.getLogger("");
            Handler[] handlers = rootLogger.getHandlers();

            for (int i = 0; i < handlers.length; i++) {
                if (handlers[i] instanceof FileHandler) {
                    rootLogger.removeHandler(handlers[i]);
                }
            }
        }

        //disable console log
        if (console) {
            Logger rootLogger = Logger.getLogger("");
            Handler[] handlers = rootLogger.getHandlers();

            for (int i = 0; i < handlers.length; i++) {
                if (handlers[i] instanceof ConsoleHandler) {
                    rootLogger.removeHandler(handlers[i]);
                }
            }

            rootLogger.addHandler(new ConsoleHandler());

            Logger.getLogger("Util").log(Level.INFO, "Adding console logging.");
        } else {
            Logger.getLogger("Util").log(Level.INFO, "Removing console logging.");

            Logger rootLogger = Logger.getLogger("");
            Handler[] handlers = rootLogger.getHandlers();

            for (int i = 0; i < handlers.length; i++) {
                if (handlers[i] instanceof ConsoleHandler) {
                    rootLogger.removeHandler(handlers[i]);
                }
            }
        }
    }

    public static void saveProjection(ClusterHierarchy hierarchy, String filename) {
        BufferedWriter out = null;

        try {
            out = new BufferedWriter(new FileWriter(filename));
            Scalar scalar = hierarchy.getScalarByName(HPExConstants.CLASS);

            out.write("#Projection exported by HPEx\r\n");
            out.write("x;y\r\n");

            Iterator<GraphicalElement> it = hierarchy.iteratorDraw();
            while (it.hasNext()) {
                GraphicalElement ge = it.next();

                int id = ge.getVector().getId();
                float x = ge.getX();
                float y = ge.getY();
                float klass = ge.getScalarValue(scalar);

                String line = id + ";" + x + ";" + y + ";" + klass + "\r\n";
                out.write(line);
            }
        } catch (IOException ex) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (out != null) {
                try {
                    out.flush();
                    out.close();
                } catch (IOException ex) {
                    Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public static void saveDataset(ClusterHierarchy hierarchy, String filename) {
        BufferedWriter out = null;

        try {
            out = new BufferedWriter(new FileWriter(filename));
            Scalar scalar = hierarchy.getScalarByName(HPExConstants.CLASS);

            out.write("#Data exported by HPEx\r\n");

            //writing the attributes
            int size = hierarchy.getRoot().getVector().toArray().length;

            out.write("C,");
            for (int i = 0; i < size; i++) {
                out.write("V");
                if (i < size - 1) {
                    out.write(",");
                }
            }
            out.write("\r\n");


            for (int i = 0; i < size + 1; i++) {
                out.write("REAL");
                if (i < size) {
                    out.write(",");
                }
            }
            out.write("\r\n");

            //wrtitting the data
            Iterator<GraphicalElement> it = hierarchy.iteratorDraw();
            while (it.hasNext()) {
                GraphicalElement ge = it.next();

                int id = ge.getVector().getId();
                float klass = ge.getScalarValue(scalar);
                SparseVector sv = ge.getVector();

//                out.write(Integer.toString(id));
                out.write(Float.toString(klass));
                out.write(",");

                float[] dense = sv.toArray();
                for (int i = 0; i < dense.length; i++) {
                    out.write(Float.toString(dense[i]));
                    out.write(",");
                }

//                out.write(Float.toString(klass));
                out.write("\r\n");
            }
        } catch (IOException ex) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (out != null) {
                try {
                    out.flush();
                    out.close();
                } catch (IOException ex) {
                    Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public static void checkNaN(float[][] points, String msg) {
        for (int i = 0; i < points.length; i++) {
            for (int j = 0; j < points[i].length; j++) {
                if (Float.isNaN(points[i][j])) {
                    for (float[] lin : points) {
                        for (float el : lin) {
                            System.out.print(el + " ");
                        }
                        System.out.println();
                    }

                    assert (!Float.isNaN(points[i][j])) : "NaN: " + msg;
                }
            }
        }
    }

    public static void checkNaN(float[] points, String msg) {
        for (int i = 0; i < points.length; i++) {
            if (Float.isNaN(points[i])) {
                for (float el : points) {
                    System.out.println(el);
                }

                assert (!Float.isNaN(points[i])) : "NaN: " + msg;
            }
        }
    }

    public static void checkNaN(ClusterElement element, String msg) {
        for (GraphicalElement ge : element.getChildren()) {
            if (Float.isNaN(ge.getX()) || Float.isNaN(ge.getY())) {

                for(GraphicalElement aux: element.getChildren()) {
                    System.out.println(aux.getX() + " , " + ge.getY());
                }
                
                assert (!Float.isNaN(ge.getX())) : "NaN: " + msg;
                assert (!Float.isNaN(ge.getY())) : "NaN: " + msg;
            }
        }
    }

}
