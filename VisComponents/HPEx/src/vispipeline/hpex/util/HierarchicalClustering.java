/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.util;

import java.util.ArrayList;
import vispipeline.distance.dissimilarity.AbstractDissimilarity;
import vispipeline.matrix.AbstractVector;
import vispipeline.matrix.sparse.SparseMatrix;
import vispipeline.matrix.sparse.SparseVector;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class HierarchicalClustering {

    public static enum Type {

        SLINK, CLINK, ALINK
    }

    /** Creates a new instance of HierarchicalClustering
     * @param nrClusters
     * @param type 
     */
    public HierarchicalClustering(int nrClusters, HierarchicalClustering.Type type) {
        this.nrClusters = nrClusters;
        this.type = type;
    }

    public ArrayList<Cluster> execute(AbstractDissimilarity diss, SparseMatrix spm) {
        this.init(spm, diss);

        //initially all cluster are single clusters
        ArrayList<Cluster> clusters = new ArrayList<Cluster>();

        for (int i = 0; i < spm.getRowCount(); i++) {
            Cluster c = new Cluster();
            AbstractVector av = spm.getRow(i);
            SparseVector sv = new SparseVector(av.getValues(), av.getId(), av.getKlass());
            c.addInstance(new Cluster.InstanceStub(sv));
            clusters.add(c);
        }

        //create the clusters
        while (clusters.size() > this.nrClusters) {
            int[] cmin = this.joinNearestClusters(clusters);

            //update distances
            this.updateDistanceMatrix(cmin);
        }

        return clusters;
    }

    private void init(SparseMatrix spm, AbstractDissimilarity diss) {
        //create the distance matrix
        this.dmat = new ArrayList<ArrayList<Float>>();

        for (int i = 0; i < spm.getRowCount(); i++) {
            ArrayList<Float> line = new ArrayList<Float>();
            for (int j = 0; j < spm.getRowCount(); j++) {
                line.add(0.0f);
            }
            this.dmat.add(line);
        }

        //fill the matrix with the distances
        for (int i = 0; i < spm.getRowCount(); i++) {
            for (int j = i + 1; j < spm.getRowCount(); j++) {
                if (i != j) {
                    float distance = diss.calculate(spm.getRow(i), spm.getRow(j));
                    dmat.get(i).set(j, distance);
                    dmat.get(j).set(i, distance);
                }
            }
        }
    }

    private int[] joinNearestClusters(ArrayList<Cluster> clusters) {
        float minDistance = dmat.get(0).get(1);
        int[] cmin = new int[2];
        cmin[0] = 0;
        cmin[1] = 1;

        //find the nearest clusters c1 and c2
        for (int c1 = 0; c1 < clusters.size(); c1++) {
            for (int c2 = c1 + 1; c2 < clusters.size(); c2++) {
                if (minDistance > dmat.get(c1).get(c2)) {
                    minDistance = dmat.get(c1).get(c2);
                    cmin[0] = c1;
                    cmin[1] = c2;
                }
            }
        }

        //put on c1 the elements of c2
        for (int i = 0; i < clusters.get(cmin[1]).getInstancesCount(); i++) {
            clusters.get(cmin[0]).addInstance(clusters.get(cmin[1]).getInstance(i));
        }

        //remove c2
        clusters.remove(cmin[1]);

        return cmin;
    }

    private void updateDistanceMatrix(int[] cmin) {
        if (this.type == HierarchicalClustering.Type.SLINK) {
            for (int i = 0; i < dmat.get(0).size(); i++) {
                if (dmat.get(cmin[0]).get(i) < dmat.get(cmin[1]).get(i)) {
                    dmat.get(cmin[0]).set(i, dmat.get(cmin[0]).get(i));
                } else {
                    dmat.get(cmin[0]).set(i, dmat.get(cmin[1]).get(i));
                }

                if (dmat.get(i).get(cmin[0]) < dmat.get(i).get(cmin[1])) {
                    dmat.get(i).set(cmin[0], dmat.get(i).get(cmin[0]));
                } else {
                    dmat.get(i).set(cmin[0], dmat.get(i).get(cmin[1]));
                }
            }

        } else if (this.type == HierarchicalClustering.Type.CLINK) {
            for (int i = 0; i < dmat.get(0).size(); i++) {
                if (dmat.get(cmin[0]).get(i) > dmat.get(cmin[1]).get(i)) {
                    dmat.get(cmin[0]).set(i, dmat.get(cmin[0]).get(i));
                } else {
                    dmat.get(cmin[0]).set(i, dmat.get(cmin[1]).get(i));
                }

                if (dmat.get(i).get(cmin[0]) > dmat.get(i).get(cmin[1])) {
                    dmat.get(i).set(cmin[0], dmat.get(i).get(cmin[0]));
                } else {
                    dmat.get(i).set(cmin[0], dmat.get(i).get(cmin[1]));
                }
            }

        } else if (this.type == HierarchicalClustering.Type.ALINK) {
            for (int i = 0; i < dmat.get(0).size(); i++) {
                dmat.get(cmin[0]).set(i, (dmat.get(cmin[0]).get(i) * 2 + dmat.get(cmin[1]).get(i)) / 2);
                dmat.get(i).set(cmin[0], (dmat.get(i).get(cmin[0]) * 2 + dmat.get(i).get(cmin[1])) / 2);
            }
        }

        //remove the line with distance to cluster c2
        dmat.remove(cmin[1]);

        //remove the column with distance to cluster c2
        for (int k = 0; k < dmat.size(); k++) {
            dmat.get(k).remove(cmin[1]);
        }
    }

    private int nrClusters;
    private HierarchicalClustering.Type type = HierarchicalClustering.Type.CLINK;
    private ArrayList<ArrayList<Float>> dmat;
}
