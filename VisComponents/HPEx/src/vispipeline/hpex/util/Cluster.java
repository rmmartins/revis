/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.util;

import java.io.Serializable;
import java.util.ArrayList;
import vispipeline.matrix.sparse.SparseVector;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class Cluster implements Serializable {

    /**
     * Creates a new instance of Cluster
     */
    public Cluster() {
        this.meanUpdated = false;
        this.size = 0;
    }

    public void addInstance(Instance instance) {
        this.meanUpdated = false;
        this.instances.add(instance);
        this.size = this.instances.size();
    }

    public boolean removeInstance(Instance instance) {
        this.meanUpdated = false;
        boolean removed = this.instances.remove(instance);
        this.size = this.instances.size();
        return removed;
    }

    public Instance getMean() {
        assert (this.instances.size() != 0) : "ERROR: empty cluster.";

        if (!this.meanUpdated) {
            ArrayList<SparseVector> vectors = new ArrayList<SparseVector>();

            for (int i = 0; i < this.size; i++) {
                vectors.add(instances.get(i).getVector());               
            }

            SparseVector m = SparseVectorUtils.mean(vectors);
            this.mean = new InstanceStub(m);
            this.meanUpdated = true;
        }
        
        return this.mean;
    }

    public void clear() {
        this.instances = new ArrayList<Instance>();
        this.mean = null;
        this.size = 0;
    }

    public int getInstancesCount() {
        return this.size;
    }

    public Instance getInstance(int index) {
        assert (this.size > index) : "ERROR: this vector does not exist.";
        return this.instances.get(index);
    }

    public static class InstanceStub implements Instance {

        public InstanceStub(SparseVector vector) {
            this.vector = vector;
        }

        public SparseVector getVector() {
            return this.vector;
        }

        private SparseVector vector;
    }

    private ArrayList<Instance> instances = new ArrayList<Instance>();
    private boolean meanUpdated;
    private Instance mean;
    private int size;
    private static final long serialVersionUID = 27L;
}
