/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Hierarchical Projection Explorer (H-PEx).
 *
 * H-PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * H-PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with H-PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.hpex.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import vispipeline.distance.dissimilarity.AbstractDissimilarity;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class BKMeans {

    /** Creates a new instance of BKMeans
     * 
     * @param nrClusters Number of clusters to be created
     */
    public BKMeans(int nrClusters) {
        this.nrClusters = nrClusters;
    }

    public ArrayList<Cluster> execute(AbstractDissimilarity diss, Cluster cluster) throws IOException {
        this.clusters = new ArrayList<Cluster>();

        //initially the gCluster has all elements
        Cluster gCluster = new Cluster();
        int size1 = cluster.getInstancesCount();
        for (int i = 0; i < size1; i++) {
            gCluster.addInstance(cluster.getInstance(i));
        }

        this.clusters.add(gCluster);

        for (int j = 0; j < this.nrClusters - 1; j++) {
            //Search the cluster with the bigger number of elements
            gCluster = this.getClusterToSplit(this.clusters);

            //split the greatest cluster into two clusters
            if (gCluster.getInstancesCount() > 1) {
                this.splitCluster(diss, gCluster);
            }
        }

        int removed = 0;

        //removing possible empty clusters
        for (int i = this.clusters.size() - 1; i >= 0; i--) {
            if (this.clusters.get(i).getInstancesCount() <= 0) {
                this.clusters.remove(i);
                removed++;
            }
        }

        if (removed > 0) {
            Logger.getLogger(this.getClass().getName()).log(Level.WARNING,
                    "The Bissection k-means algorithm is returning " +
                    "empty clusters. Removed: " + removed);
        }

        return this.clusters;
    }

    public ArrayList<Instance> getCentroids() {
        ArrayList<Instance> centroids = new ArrayList<Instance>();

        for (int i = 0; i < this.clusters.size(); i++) {
            centroids.add(this.clusters.get(i).getMean());
        }

        return centroids;
    }

    private Cluster getClusterToSplit(ArrayList<Cluster> clusters) {

        Cluster gCluster = clusters.get(0);
        for (int i = 0; i < clusters.size(); i++) {
            if (clusters.get(i).getInstancesCount() > gCluster.getInstancesCount()) {
                gCluster = clusters.get(i);
            }
        }

        return gCluster;
    }

    private void splitCluster(AbstractDissimilarity diss, Cluster gCluster) throws IOException {
        //Remove the cluster and its centroid
        this.clusters.remove(gCluster);

        //getting the two pivots
        Instance[] pivots = this.getPivots(diss, gCluster);

        //Create two new clusters
        Cluster cluster_1 = new Cluster();
        cluster_1.addInstance(pivots[0]);
        Instance centroid_1 = cluster_1.getMean();

        Cluster cluster_2 = new Cluster();
        cluster_2.addInstance(pivots[1]);
        Instance centroid_2 = cluster_2.getMean();

        int iterations = 0;
        Instance oldCentr_1 = null;
        Instance oldCentr_2 = null;

        do {
            oldCentr_1 = new Cluster.InstanceStub(cluster_1.getMean().getVector());
            oldCentr_2 = new Cluster.InstanceStub(cluster_2.getMean().getVector());

            cluster_1.clear();
            cluster_2.clear();

            //For each cluster
            int size = gCluster.getInstancesCount();
            for (int i = 0; i < size; i++) {
                float distCentr_1 = diss.calculate(gCluster.getInstance(i).getVector(),
                        centroid_1.getVector());
                float distCentr_2 = diss.calculate(gCluster.getInstance(i).getVector(),
                        centroid_2.getVector());

                if (distCentr_1 < distCentr_2) {
                    cluster_1.addInstance(gCluster.getInstance(i));
                } else if (distCentr_2 < distCentr_1) {
                    cluster_2.addInstance(gCluster.getInstance(i));
                } else {
                    if (cluster_1.getInstancesCount() > cluster_2.getInstancesCount()) {
                        cluster_2.addInstance(gCluster.getInstance(i));
                    } else {
                        cluster_1.addInstance(gCluster.getInstance(i));
                    }
                }
            }

            if (cluster_1.getInstancesCount() < 1) {
                cluster_1.addInstance(cluster_2.getInstance(0));
                cluster_2.removeInstance(cluster_2.getInstance(0));
            } else if (cluster_2.getInstancesCount() < 1) {
                cluster_2.addInstance(cluster_1.getInstance(0));
                cluster_1.removeInstance(cluster_1.getInstance(0));
            }

            centroid_1 = cluster_1.getMean();
            centroid_2 = cluster_2.getMean();

        } while (++iterations < this.nrIterations &&
                (!centroid_1.equals(oldCentr_1) || !centroid_2.equals(oldCentr_2)));

        //Add the two new clusters
        this.clusters.add(cluster_1);
        this.clusters.add(cluster_2);
    }

    private Instance[] getPivots(AbstractDissimilarity diss, Cluster gCluster) {
        ArrayList<Pivot> pivots_aux = new ArrayList<Pivot>();
        Instance[] pivots = new Instance[2];

        //choosing the first pivot
        pivots[0] = gCluster.getMean();

        float size = 1 + (gCluster.getInstancesCount() / 10);
        for (int i = 0; i < size; i++) {
            int el = (int) ((gCluster.getInstancesCount() / size) * i);
            Instance aux = gCluster.getInstance(el);
            float distance = diss.calculate(pivots[0].getVector(), aux.getVector());
            pivots_aux.add(new Pivot(distance, aux));
        }

        Collections.sort(pivots_aux);

        pivots[0] = pivots_aux.get((int) (pivots_aux.size() * 0.75f)).instance;

        //choosing the second pivot
        pivots_aux.clear();

        for (int i = 0; i < size; i++) {
            int el = (int) ((gCluster.getInstancesCount() / size) * i);
            Instance aux = gCluster.getInstance(el);
            float distance = diss.calculate(pivots[0].getVector(), aux.getVector());
            pivots_aux.add(new Pivot(distance, aux));
        }

        Collections.sort(pivots_aux);

        pivots[1] = pivots_aux.get((int) (pivots_aux.size() * 0.75f)).instance;

        return pivots;
    }

    public class Pivot implements Comparable {

        public Pivot(float distance, Instance instance) {
            this.distance = distance;
            this.instance = instance;
        }

        public int compareTo(Object o) {
            if (o instanceof Pivot) {
                if (this.distance - ((Pivot) o).distance == EPSILON) {
                    return 0;
                } else if (this.distance - ((Pivot) o).distance > EPSILON) {
                    return 1;
                } else {
                    return -1;
                }
            } else {
                return -1;
            }
        }

        public float distance;
        public Instance instance;
    }

    private static final float EPSILON = 0.00001f;
    private ArrayList<Cluster> clusters;
    private int nrClusters;
    private int nrIterations = 15;
}
