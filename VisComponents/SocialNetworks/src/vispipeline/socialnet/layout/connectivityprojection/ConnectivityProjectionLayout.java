/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Projection Explorer (PEx).
 *
 * How to cite this work:
 *
@inproceedings{paulovich2007pex,
author = {Fernando V. Paulovich and Maria Cristina F. Oliveira and Rosane
Minghim},
title = {The Projection Explorer: A Flexible Tool for Projection-based
Multidimensional Visualization},
booktitle = {SIBGRAPI '07: Proceedings of the XX Brazilian Symposium on
Computer Graphics and Image Processing (SIBGRAPI 2007)},
year = {2007},
isbn = {0-7695-2996-8},
pages = {27--34},
doi = {http://dx.doi.org/10.1109/SIBGRAPI.2007.39},
publisher = {IEEE Computer Society},
address = {Washington, DC, USA},
}
 *
 * PEx is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * PEx is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along
 * with PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.socialnet.layout.connectivityprojection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import projection.technique.Projection;
import vispipeline.distance.DistanceMatrix;
import vispipeline.matrix.AbstractMatrix;
import vispipeline.matrix.dense.DenseMatrix;
import vispipeline.matrix.dense.DenseVector;
import vispipeline.socialnet.graphdata.GraphData;
import vispipeline.socialnet.layout.Layout;
import vispipeline.socialnet.util.FloydWarshallShortestPath;


/**
 *
 * @author Gabriel de Faria Andery
 */
public class ConnectivityProjectionLayout extends Layout {

    boolean isNormalizeAttributes = false;
    boolean isUseConnectionsAsAttribute = false;
    boolean isCombineConnectivities = false;
    int relationshipPosition = 1;
    boolean isIgnoreWeight = false;
    boolean isWeightAsDistance = false;
    boolean isShortestPath = false;
    boolean isMatrixAsDistance = false;
    boolean isDirectedConection = false;

    @Override
    public DistanceMatrix layout(GraphData gdata) {
        float[][] layout = new float[gdata.getMatrix().size()][2];

        Map<String, Integer> index = new HashMap<String, Integer>();
        int x = 0;
        for (ArrayList<String> row : gdata.getMatrix()) {
            index.put(row.get(0), x++);
        }

//        if (view != null)
//            view.setStatus("Generating adjacency matrix...", 10);

        // connectivity position in file to be used
        int pos = 0;

        // if it's set to combine connectivities, position equals zero
        if (!isCombineConnectivities) {
            // connectivity 1 is column 2 in the relationships table (0: from, 1: to)
            pos = relationshipPosition + 1;
            if (pos < 2) pos = 2;
            else if (pos >= gdata.getRelationships().size()) pos = gdata.getRelationships().size() - 1;
        }

//        ArrayList<String> ids = gdata.getIds();
        ArrayList<ArrayList<String>> relMatrix = gdata.getRelationshipMatrix();

        // creates an adjacency matrix
        float[][] d = new float[index.size()][index.size()];
        for (int i = 0; i < index.size(); i++)
            for (int j = 0; j < index.size(); j++)
                if (i == j) d[i][j] = 0;
                else d[i][j] = Float.MAX_VALUE;

        for (int j = 0; j < relMatrix.size(); j++) {
            // if it's not set to combine connectivities and connectivity
            // strength equals zero, then ignore
            if ((pos != 0) && (Float.parseFloat(relMatrix.get(j).get(pos)) == 0))
                continue;

            // get from and to vertices ids
            Integer keyFrom = index.get(relMatrix.get(j).get(0));
            Integer keyTo = index.get(relMatrix.get(j).get(1));

            // if it's not set to combine connectivities, but it's to ignore
            // weights, strength equals 1
            Float strength = 1f;

            // if it's set to combine connectivities calculates strength
            if (pos == 0) {
                strength = 0f;

                // if it's set to ignore weights, counts all valid connections;
                // sums strengths otherwise
                if (!isIgnoreWeight) {
                    for (int w = 2; w < relMatrix.get(j).size(); w++)
                        strength += Float.parseFloat(relMatrix.get(j).get(w));
                }
                else {
                    for (int w = 2; w < relMatrix.get(j).size(); w++)
                        if (Float.parseFloat(relMatrix.get(j).get(w)) > 0)
                            strength++;
                }
            }
            // if it's not set to combine connectivities neither to ignore
            // weight, gets strength
            else if (!isIgnoreWeight) {
                strength = Float.parseFloat(relMatrix.get(j).get(pos));
            }

            // if connections isn't directed or if the adjacency matrix is to
            // be used as a distance matrix, d[i][j] must equals d[j][i]
            if ((!isDirectedConection) || (isMatrixAsDistance)) {
                if (d[keyFrom][keyTo] < Float.MAX_VALUE)
                    d[keyFrom][keyTo] += strength;
                else
                    d[keyFrom][keyTo] = strength;

                d[keyTo][keyFrom] = d[keyFrom][keyTo];
            }
            else {
                d[keyFrom][keyTo] = strength;
            }
        }

        /*// if the adjacency matrix is to be used as a distance matrix or it's
        // to calculate the shortest paths (it requires a distance matrix)
        // and the weights represent strength, not distance, changes values
        // inverting [min, max] scale
        if (((pdata.isMatrixAsDistance()) || (pdata.isShortestPath())) &&
        (!pdata.isWeightAsDistance())) {*/

        // if the weights represent strength, not distance, changes values
        // inverting [min, max] scale
        if (!isWeightAsDistance) {
            float max = Float.MIN_VALUE;

            // Float.MAX_VALUE's represent pairs of vertices without connection
            // so ignore them (it won't be a connection between them anyway,
            // even after inverting [min, max] scale)
            for (int i = 0; i < index.size(); i++) {
                for (int j = 0; j < index.size(); j++) {
                    if (d[i][j] < Float.MAX_VALUE && d[i][j] > max)
                        max = d[i][j];
                }
            }

            // invert scale by making (max - value)
            // as zero's represents the main diagonal, sums 1
            for (int i = 0; i < index.size(); i++) {
                for (int j = 0; j < index.size(); j++)
                    if ((i != j) && (d[i][j] < Float.MAX_VALUE))
                        d[i][j] = max - d[i][j] + 1;
            }
        }

        // if it's set to calculate the shortest paths, calculates them
        if (isShortestPath) {
//            if (view != null)
//                view.setStatus("Calculating shortest paths...", 20);

            FloydWarshallShortestPath.fw(d);
        }

        // if it's set to use matrix as distance, creates a distance matrix
        if (isMatrixAsDistance) {
            generateDistanceMatrix(d);

            // projects
//            if (view != null)
//                view.setStatus("Projecting...", 30);

//            proj = ProjectionFactory.getInstance(pdata.getProjectionType());
//            layout = proj.project(dmat, pdata, view);
        }
        // if it's set to use matrix as points, creates a points matrix
        else {
            generatePointsMatrix(d);

            // projects
//            if (view != null)
//                view.setStatus("Projecting...", 30);

//            proj = ProjectionFactory.getInstance(pdata.getProjectionType());
//            layout = proj.project(matrix, pdata, view);
        }

//        if (pdata.isKnnProjection()) {
//            Pair[][] neighbors = proj.getKnnneighbors();
//
//            Matrix knnMatrix = new SparseMatrix();
//
//            for (int i = 0; i < neighbors.length; i++) {
//                ArrayList<Pair> row = new ArrayList<Pair>();
//
//                for (int j = 0; j < neighbors[i].length; j++) {
//                    row.add(neighbors[i][j]);
//                }
//
//                knnMatrix.addRow(new SparseVector(row, ids.get(i), 0f, ids.size()));
//            }
//
//            // if it's set to use matrix as distance, creates a distance matrix
//            if (pdata.isMatrixAsDistance()) {
//                d = knnMatrix.toMatrix();
//
//                float max = Float.MIN_VALUE;
//
//                // distance matrices are always symmetric
//                for (int i = 0; i < d.length; i++) {
//                    for (int j = i+1; j < d.length; j++) {
//                        if (d[i][j] < Float.MAX_VALUE && d[i][j] > max)
//                            max = d[i][j];
//                    }
//                }
//
//                for (int i = 0; i < d.length; i++)
//                    for (int j = 0; j < d.length; j++)
//                        if (d[i][j] == 0)
//                            d[i][j] = 2*max;
//
//                generateDistanceMatrix(d);
//
//                // projects
//                /*if (view != null)
//                view.setStatus("Projecting...", 30);*/
//
//                proj = ProjectionFactory.getInstance(pdata.getProjectionType());
//                layout = proj.project(dmat, pdata, null);
//            }
//            // if it's set to use matrix as points, creates a points matrix
//            else {
//                // projects
//                /*if (view != null)
//                view.setStatus("Projecting...", 30);*/
//
//                proj = ProjectionFactory.getInstance(pdata.getProjectionType());
//                layout = proj.project(knnMatrix, pdata, null);
//            }
//        }

//        return layout;
          return dmat;
    }

    private void generateDistanceMatrix(float d[][]) {
        dmat = new DistanceMatrix(d.length);

        float max = Float.MIN_VALUE;

        // distance matrices are always symmetric
        for (int i = 0; i < d.length; i++) {
            for (int j = i+1; j < d.length; j++) {
                if (d[i][j] < Float.MAX_VALUE && d[i][j] > max)
                    max = d[i][j];
            }
        }

        for (int i = 0; i < d.length; i++) {
            for (int j = i+1; j < d.length; j++) {
                // when there isn't a connection between a pair of vertices,
                // set it to 2*max
                if (d[i][j] == Float.MAX_VALUE)
                    dmat.setDistance(i, j, 2*max);
                else
                    dmat.setDistance(i, j, d[i][j]);
            }
        }
    }

    private void generatePointsMatrix(float d[][]) {
        matrix = new DenseMatrix();

        /*
        float max = Float.MIN_VALUE;

        for (int i = 0; i < ids.size(); i++) {
            for (int j = 0; j < ids.size(); j++) {
                if (d[i][j] < Float.MAX_VALUE && d[i][j] > max)
                    max = d[i][j];
            }
        }

        for (int i = 0; i < ids.size(); i++) {
            for (int j = 0; j < ids.size(); j++) {
                if (d[i][j] == Float.MAX_VALUE)
                    d[i][j] = max*2;
            }
        }
        */

        // when there isn't a connection between a pair of vertices,
        // set it to zero
        for (int i = 0; i < d.length; i++) {
            for (int j = 0; j < d.length; j++) {
                if (d[i][j] == Float.MAX_VALUE)
                    d[i][j] = 0;
            }
        }

        for (int j = 0; j < d.length; j++)
            matrix.addRow(new DenseVector(d[j]));

        /*
        Normalization norm = NormalizationFactory.getInstance(NormalizationType.NORMALIZE_COLUMNS);
        try {
            matrix = norm.execute(matrix);
        } catch (IOException ex) {
            Logger.getLogger(ShortestPathProjectionLayout.class.getName()).log(Level.SEVERE, null, ex);
        }
        */
    }

//    @Override
//    public ProjectionView getProjectionView(ProjectionData pdata) {
//        // projection technique view must be used
//        return null;
//    }
//
//    @Override
//    public void createConnectivities(GraphData gdata, ProjectionData pdata, GraphModel graph) {
//        gdata.createConnectivities(graph);
//
//        proj.createConnectivities(graph, pdata, null);
//        proj.postProcessing(graph);
//    }

//    Projection proj = null;
    DistanceMatrix dmat = null;
    AbstractMatrix matrix = null;
}
