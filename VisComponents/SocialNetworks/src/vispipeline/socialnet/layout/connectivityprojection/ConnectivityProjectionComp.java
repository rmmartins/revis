/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.socialnet.layout.connectivityprojection;

import java.io.IOException;
import org.openide.util.Exceptions;
import org.openide.util.lookup.ServiceProvider;
import vispipeline.basics.annotations.Parameter;
import vispipeline.basics.annotations.Return;
import vispipeline.basics.annotations.VisComponent;
import vispipeline.basics.interfaces.AbstractComponent;
import vispipeline.basics.interfaces.AbstractParametersView;
import vispipeline.matrix.AbstractMatrix;
import vispipeline.socialnet.layout.LayoutComp;

/**
 *
 * @author Fernando Vieira Paulovich
 */
@VisComponent(hierarchy = "Networks.Layout",
name = "Connectivity Projection Layout",
description = "Retorna uma matriz de distâncias.")
@ServiceProvider(service=AbstractComponent.class)
public class ConnectivityProjectionComp extends LayoutComp implements AbstractComponent {

    @Override
    public void execute() {
        ConnectivityProjectionLayout layout = new ConnectivityProjectionLayout();

        layout.isNormalizeAttributes = isNormalizeAttributes;
        layout.isUseConnectionsAsAttribute = isUseConnectionsAsAttribute;
        layout.isCombineConnectivities = isCombineConnectivities;
        layout.isIgnoreWeight = isIgnoreWeight;
        layout.isShortestPath = isShortestPath;
        layout.isWeightAsDistance = isWeightAsDistance;
        layout.relationshipPosition = relationshipPosition;
        layout.isMatrixAsDistance = isMatrixAsDistance;
        layout.isDirectedConection = isDirectedConnection;

        dmat = layout.layout(gdata);
        conns = gdata.createConnectivities();
        pointMatrix = layout.matrix;

    }

    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new ConnectivityProjectionParamView(this);
        }

        return paramview;
    }

    @Return(name = "Points Matrix")
    public AbstractMatrix outputPM() {
        return pointMatrix;
    }

    private ConnectivityProjectionParamView paramview;
    private AbstractMatrix pointMatrix;

    @Parameter boolean isNormalizeAttributes = false;
    @Parameter boolean isUseConnectionsAsAttribute = false;
    @Parameter boolean isCombineConnectivities = false;
    @Parameter int relationshipPosition = 1;
    @Parameter boolean isIgnoreWeight = false;
    @Parameter boolean isWeightAsDistance = false;
    @Parameter boolean isShortestPath = false;
    @Parameter boolean isMatrixAsDistance = false;
    @Parameter boolean isDirectedConnection = false;
}
