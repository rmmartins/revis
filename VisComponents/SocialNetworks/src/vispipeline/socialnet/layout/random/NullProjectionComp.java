/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.socialnet.layout.random;

import java.io.IOException;
import org.openide.util.Exceptions;
import org.openide.util.lookup.ServiceProvider;
import vispipeline.basics.annotations.VisComponent;
import vispipeline.basics.interfaces.AbstractComponent;
import vispipeline.basics.interfaces.AbstractParametersView;
import vispipeline.socialnet.layout.LayoutComp;

/**
 *
 * @author Fernando Vieira Paulovich
 */
@VisComponent(hierarchy = "Networks.Layout",
name = "Null Projection Layout",
description = "Retorna uma matriz de distâncias zerada.")
@ServiceProvider(service=AbstractComponent.class)
public class NullProjectionComp extends LayoutComp implements AbstractComponent {

    @Override
    public void execute() {
        NullLayout layout = new NullLayout();

        dmat = layout.layout(gdata);
        conns = gdata.createConnectivities();
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        return null;
    }
    
}
