/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.socialnet.layout;

import graph.model.Connectivity;
import java.util.ArrayList;
import org.openide.util.lookup.ServiceProvider;
import vispipeline.basics.annotations.Param;
import vispipeline.basics.annotations.Return;
import vispipeline.basics.annotations.VisComponent;
import vispipeline.basics.interfaces.AbstractComponent;
import vispipeline.basics.interfaces.AbstractParametersView;
import vispipeline.distance.DistanceMatrix;
import vispipeline.socialnet.graphdata.GraphData;

/**
 *
 * @author Rafael
 */
@VisComponent(hierarchy = "Networks.Layout",
name = "Layout",
description = "Retorna uma matriz de distâncias a partir de um grafo.")
@ServiceProvider(service=AbstractComponent.class)
public class LayoutComp implements AbstractComponent {

    @Override
    public void execute() {
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        return null;
    }

    public void input(@Param(name = "GraphData") GraphData gdata) {
        this.gdata = gdata;
    }

    @Return(name = "Distance Matrix")
    public DistanceMatrix outputDM() {
        return dmat;
    }

    @Return(name = "Connectivities")
    public ArrayList<Connectivity> outputConn() {
        return conns;
    }

    public void reset() {
        gdata = null;
        dmat = null;
    }

    protected GraphData gdata;
    protected DistanceMatrix dmat;
    protected ArrayList<Connectivity> conns;

}
