/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.socialnet.layout.attributesprojection;

import java.io.IOException;
import org.openide.util.Exceptions;
import org.openide.util.lookup.ServiceProvider;
import vispipeline.basics.annotations.Parameter;
import vispipeline.basics.annotations.VisComponent;
import vispipeline.basics.interfaces.AbstractComponent;
import vispipeline.basics.interfaces.AbstractParametersView;
import vispipeline.socialnet.layout.Layout;
import vispipeline.socialnet.layout.LayoutComp;

/**
 *
 * @author Fernando Vieira Paulovich
 */
@VisComponent(hierarchy = "Networks.Layout",
name = "Attributes Projection Layout",
description = "Retorna uma matriz de distâncias.")
@ServiceProvider(service=AbstractComponent.class)
public class AttributesProjectionComp extends LayoutComp implements AbstractComponent {

    @Override
    public void execute() {
        AttributesProjectionLayout layout = new AttributesProjectionLayout();

        layout.isNormalizeAttributes = isNormalizeAttributes;
        layout.isUseConnectionsAsAttribute = isUseConnectionsAsAttribute;
        layout.isCombineConnectivities = isCombineConnectivities;
        layout.isIgnoreWeight = isIgnoreWeight;
        layout.isShortestPath = isShortestPath;
        layout.isWeightAsDistance = isWeightAsDistance;
        layout.relationshipPosition = relationshipPosition;

        dmat = layout.layout(gdata);
        conns = gdata.createConnectivities();        
       
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new AttributesProjectionParamView(this);
        }

        return paramview;
    }

    private AttributesProjectionParamView paramview;

    @Parameter boolean isNormalizeAttributes = false;
    @Parameter boolean isUseConnectionsAsAttribute = false;
    @Parameter boolean isCombineConnectivities = false;
    @Parameter int relationshipPosition = 1;
    @Parameter boolean isIgnoreWeight = false;
    @Parameter boolean isWeightAsDistance = false;
    @Parameter boolean isShortestPath = false;
}
