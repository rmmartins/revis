/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Projection Explorer (PEx).
 *
 * How to cite this work:
 *  
@inproceedings{paulovich2007pex,
author = {Fernando V. Paulovich and Maria Cristina F. Oliveira and Rosane 
Minghim},
title = {The Projection Explorer: A Flexible Tool for Projection-based 
Multidimensional Visualization},
booktitle = {SIBGRAPI '07: Proceedings of the XX Brazilian Symposium on 
Computer Graphics and Image Processing (SIBGRAPI 2007)},
year = {2007},
isbn = {0-7695-2996-8},
pages = {27--34},
doi = {http://dx.doi.org/10.1109/SIBGRAPI.2007.39},
publisher = {IEEE Computer Society},
address = {Washington, DC, USA},
}
 *  
 * PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.socialnet.graphdata;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import graph.model.xml.XMLGraphModelWriter;

/**
 *
 * @author Gabriel de Faria Andery
 */
public class VNAAdvancedGraphData extends GraphData {
    private BufferedReader file;
    private String location;

    /**
     * Constructor.
     */
    public VNAAdvancedGraphData(String location) throws InvalidFileException {
        super(location, 1);
        try {
            this.location = location;
            this.file = new BufferedReader(new FileReader(location));
            this.matrix = new ArrayList<ArrayList<String>>();
            this.attributes = new ArrayList<Attribute>();
            this.visualMatrix = new ArrayList<ArrayList<String>>();
            this.visualAttributes = new ArrayList<Attribute>();
            this.relationshipMatrix = new ArrayList<ArrayList<String>>();
            this.relationships = new ArrayList<Attribute>();
//            this.ids = new ArrayList<String>();
            
            this.run();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getLocation() {
        return location;
    }

    @Override
    public void run() {
        try {
            // Read section
            String buffer = this.file.readLine();

            if (buffer.trim().equalsIgnoreCase("*node data")) {
                buffer = this.file.readLine();
                
                // Read attributes and types
                parseAttributes(buffer);

                if (!this.attributes.get(0).getName().equalsIgnoreCase("id")) {
                    throw new InvalidFileException("First column, " + this.attributes.get(0).getName() + ", must be id.");
                }

                buffer = this.file.readLine();
                while ((buffer != null) &&
                        (!buffer.trim().equalsIgnoreCase("*node data")) &&
                        (!buffer.trim().equalsIgnoreCase("*node properties")) &&
                        (!buffer.trim().equalsIgnoreCase("*tie data"))) {
                    if (buffer.trim().equals("")) {
                        buffer = this.file.readLine();
                        continue;
                    }
                    ArrayList<String> tuple = parse(buffer);

                    if (tuple.size() != attributes.size())
                        throw new InvalidFileException("Node data: invalid number of attributes: " + tuple + ". Size is " + attributes.size() + ".");

                    // Add the attributes to the matrix
                    this.matrix.add(tuple);

//                    if (ids.contains(tuple.get(0)))
//                        throw new InvalidFileException("Ids must be unique: " + tuple.get(0));
//                    ids.add(tuple.get(0));


                    // Add each different value into a hashtable
                    for (int i = 0; i < tuple.size(); i++)
                        this.attributes.get(i).putValue(tuple.get(i));

                    // Read next line
                    buffer = this.file.readLine();
                }

                this.attributes.get(0).setId(true);
            }

            if ((buffer != null) && (buffer.trim().equalsIgnoreCase("*node properties"))) {
                buffer = this.file.readLine();
                ArrayList<String> atts = parse(buffer);
                ArrayList<String> possibleAtts = new ArrayList<String>();

                possibleAtts.add("id");
                possibleAtts.add("x");
                possibleAtts.add("y");
                possibleAtts.add("shape");
                possibleAtts.add("size");
                possibleAtts.add("color");
                possibleAtts.add("shortlabel");

                for (String att : atts) {
                    if (!possibleAtts.contains(att))
                        throw new InvalidFileException("Invalid property.");
                    possibleAtts.remove(att);
                    this.visualAttributes.add(new Attribute(att));
                }

                if (!this.visualAttributes.get(0).getName().equalsIgnoreCase("id")) {
                    throw new InvalidFileException("Node properties: first column must be id.");
                }
                
                // Read each line from file and add it to matrix
                buffer = this.file.readLine();
                while ((buffer != null) &&
                        (!buffer.trim().equalsIgnoreCase("*node data")) &&
                        (!buffer.trim().equalsIgnoreCase("*node properties")) &&
                        (!buffer.trim().equalsIgnoreCase("*tie data"))) {
                    if (buffer.trim().equals("")) {
                        buffer = this.file.readLine();
                        continue;
                    }
                    ArrayList<String> tuple = parse(buffer);

                    if (tuple.size() != visualAttributes.size())
                        throw new InvalidFileException("Invalid number of node properties.");

                    if (!this.attributes.get(0).containsKey(tuple.get(0)))
                        throw new InvalidFileException("Id " + tuple.get(0) + " not declared.");

                    for (int i = 1; i < tuple.size(); i++)
                        Integer.valueOf(tuple.get(i));

                    // Add the attributes to the visual matrix
                    this.visualMatrix.add(tuple);

                    // Add each different value into a hashtable
                    for (int i = 0; i < tuple.size(); i++)
                        this.visualAttributes.get(i).putValue(tuple.get(i));

                    // Read next line
                    buffer = this.file.readLine();
                }
            }

            if ((buffer != null) && (buffer.trim().equalsIgnoreCase("*tie data"))) {
                buffer = this.file.readLine();
                parseRelationships(buffer);

                if (!this.relationships.get(0).getName().equalsIgnoreCase("from")) {
                    throw new InvalidFileException("Tie data: first column must be from.");
                }

                if (!this.relationships.get(1).getName().equalsIgnoreCase("to")) {
                    throw new InvalidFileException("Tie data: second column must be to.");
                }

                // Read each line from file and add it to matrix
                buffer = this.file.readLine();
                while ((buffer != null) &&
                        (!buffer.trim().equalsIgnoreCase("*node data")) &&
                        (!buffer.trim().equalsIgnoreCase("*node properties")) &&
                        (!buffer.trim().equalsIgnoreCase("*tie data"))) {
                    if (buffer.trim().equals("")) {
                        buffer = this.file.readLine();
                        continue;
                    }
                    ArrayList<String> tuple = parse(buffer);

                    if (tuple.size() != relationships.size())
                        throw new InvalidFileException("Invalid number of attributes: " + tuple);

                    if (!this.attributes.get(0).containsKey(tuple.get(0))) {
                        //throw new InvalidFileException("Id " + tuple.get(0) + " not declared.");
                        System.err.println("Id " + tuple.get(0) + " not declared.");
                    }
                    else if (!this.attributes.get(0).containsKey(tuple.get(1))) {
                        //throw new InvalidFileException("Id " + tuple.get(1) + " not declared.");
                        System.err.println("Id " + tuple.get(1) + " not declared.");
                    }
                    else {
                        for (int i = 2; i < tuple.size(); i++)
                            Float.valueOf(tuple.get(i));

                        // Add the attributes to the matrix
                        this.relationshipMatrix.add(tuple);

                        // Add each different value into a hashtable
                        for (int i = 0; i < tuple.size(); i++)
                            this.relationships.get(i).putValue(tuple.get(i));
                    }
                    // Read next line
                    buffer = this.file.readLine();
                }
            }

            if (buffer != null) {
                System.out.println(buffer);
                throw new InvalidFileException("Invalid file.");
            }
        }
        catch (NumberFormatException e) {
            System.err.println("Visual attributes must be integers and relationships must be numerical (integer or float).");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private ArrayList<String> parse(String buffer) {
        // Parse the input string
        ArrayList<String> tuple = new ArrayList<String>();
        
        int i = 0;
        while (i < buffer.length()) {
            if ((buffer.charAt(i) == ',') || (buffer.charAt(i) == '\t') || (buffer.charAt(i) == ' ')) {
                if (i > 0)
                    tuple.add(XMLGraphModelWriter.encodeToValidChars(buffer.substring(0,i)));
                else
                    tuple.add("");

                i++;
                if (i < buffer.length()) {
                    buffer = buffer.substring(i).trim();
                    i = 0;
                }
                else
                    tuple.add("");
            }
            else if (buffer.charAt(i) == '"') {
                i++;
                if (i < buffer.length()) {
                    buffer = buffer.substring(i);

                    if (buffer.indexOf('"') > 0)
                        tuple.add(XMLGraphModelWriter.encodeToValidChars(buffer.substring(0, buffer.indexOf('"'))));
                    else
                        tuple.add("");

                    i = buffer.indexOf('"') + 1;
                    if (i < buffer.length()) {
                        i++;
                        if (i < buffer.length()) {
                            buffer = buffer.substring(i).trim();
                            i = 0;
                        }
                        else
                            tuple.add("");
                    }
                }
                else
                    tuple.add("");
            }
            else {
                i++;
                if (i >= buffer.length()) {
                    tuple.add(XMLGraphModelWriter.encodeToValidChars(buffer));
                }
            }
        }
        
        return tuple;
    }

    private void parseAttributes(String buffer) {
        // Parse the input string
        ArrayList<String> tuple = parse(buffer);

        for (String att : tuple) {
            String[] attWeight = att.split("#");
            String[] attType = attWeight[0].split("@");

            this.attributes.add(new Attribute(attType[0]));

            if (attWeight.length > 1) {
                try {
                    this.attributes.get(this.attributes.size() - 1).setWeight(Integer.parseInt(attWeight[1]));
                }
                catch (Exception e) {
                    e.printStackTrace();
                    this.attributes.get(this.attributes.size() - 1).setWeight(1);
                }
            }

            if (attType.length > 1) {

                try {
                    this.attributes.get(this.attributes.size() - 1).setType(Type.valueOf(attType[1].toUpperCase()));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (attType.length > 2)
                    this.attributes.get(this.attributes.size() - 1).setDateFormat(attType[2]);
            }
        }
    }

    private void parseRelationships(String buffer) {
        // Parse the input string
        ArrayList<String> tuple = parse(buffer);

        for (String att : tuple) {
            String[] attWeight = att.split("#");

            this.relationships.add(new Attribute(attWeight[0]));

            if (attWeight.length > 1) {
                try {
                    this.relationships.get(this.relationships.size() - 1).setWeight(Integer.parseInt(attWeight[1]));
                }
                catch (Exception e) {
                    e.printStackTrace();
                    this.relationships.get(this.relationships.size() - 1).setWeight(1);
                }
            }
        }
    }
}