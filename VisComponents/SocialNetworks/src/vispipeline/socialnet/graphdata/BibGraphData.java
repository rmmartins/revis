/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Projection Explorer (PEx).
 *
 * How to cite this work:
 *  
@inproceedings{paulovich2007pex,
author = {Fernando V. Paulovich and Maria Cristina F. Oliveira and Rosane 
Minghim},
title = {The Projection Explorer: A Flexible Tool for Projection-based 
Multidimensional Visualization},
booktitle = {SIBGRAPI '07: Proceedings of the XX Brazilian Symposium on 
Computer Graphics and Image Processing (SIBGRAPI 2007)},
year = {2007},
isbn = {0-7695-2996-8},
pages = {27--34},
doi = {http://dx.doi.org/10.1109/SIBGRAPI.2007.39},
publisher = {IEEE Computer Society},
address = {Washington, DC, USA},
}
 *  
 * PEx is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 *
 * PEx is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of 
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer 
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along 
 * with PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.socialnet.graphdata;


import bibtex.dom.BibtexAbstractValue;
import bibtex.dom.BibtexEntry;
import java.util.ArrayList;
import bibtex.dom.BibtexFile;
import bibtex.parser.BibtexParser;
import java.io.FileReader;
import java.util.Hashtable;
import java.util.List;

/**
 *
 * @author Gabriel de Faria Andery
 */

public class BibGraphData extends GraphData {
    private String location;
    private int nextId;

    /**
     * Constructor.
     */
    public BibGraphData(String location) throws InvalidFileException {
        super(location, 1);
        try {
            this.location = location;
            this.matrix = new ArrayList<ArrayList<String>>();
            this.attributes = new ArrayList<Attribute>();
            this.relationshipMatrix = new ArrayList<ArrayList<String>>();
            this.relationships = new ArrayList<Attribute>();
            this.ids = new ArrayList<Integer>();
            this.nextId = 0;
            this.run();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getLocation() {
        return location;
    }

    @Override
    public void run() {
        try {
            Hashtable<String,String> titles = new Hashtable<String,String>();
            int citation = 0;
            
            BibtexFile bibtexFile = new BibtexFile();
            BibtexParser parser = new BibtexParser(false);

            parser.parse(bibtexFile, new FileReader(this.location));

            this.attributes.add(new Attribute("id"));
            this.attributes.add(new Attribute("title"));
            this.attributes.add(new Attribute("type"));
            this.attributes.add(new Attribute("author"));
            this.attributes.add(new Attribute("abstract"));
            this.attributes.add(new Attribute("year"));
            this.attributes.add(new Attribute("quality"));
            this.attributes.add(new Attribute("history"));
            this.attributes.add(new Attribute("int_id"));
            this.attributes.add(new Attribute("comments"));
            this.attributes.add(new Attribute("conflict"));
//            this.attributes.add(new Attribute("topics"));
            //this.attributes.add(new Attribute("references"));

            List entries = bibtexFile.getEntries();            
            for (int i = 0; i < entries.size(); i++) {                
                if (entries.get(i) instanceof BibtexEntry) {
                    BibtexEntry entry = (BibtexEntry) entries.get(i);
                    
                    // @comment entries are to be ignored
                    if (entry.getEntryType().equals("comment")) {
                        continue;
                    }

                    BibtexAbstractValue value = entry.getFieldValue("title");
                    String title = "no-title";
                    if (value != null) {
                        title = entry.getFieldValue("title").toString();
                        title = title.replace("{", "");
                        title = title.replace("}", "");
                        title = filter(title);
                    }

                    value = entry.getFieldValue("author");
                    String author = "no-author";
                    if (value != null) {
                        author = entry.getFieldValue("author").toString();
                        author = author.replace("{", "");
                        author = author.replace("}", "");
                        author = filter(author);
                    }

                    value = entry.getFieldValue("abstract");
                    String abst = "no-abstract";
                    if (value != null) {
                        abst = entry.getFieldValue("abstract").toString();
                        abst = abst.replace("{", "");
                        abst = abst.replace("}", "");
                        abst = filter(abst);
                    }

                    value = entry.getFieldValue("year");
                    String year = "no-year";
                    if (value != null) {
                        year = entry.getFieldValue("year").toString();
                        year = year.replace("{", "");
                        year = year.replace("}", "");
                    }

                    value = entry.getFieldValue("quality");
                    String quality = "no-quality";
                    if (value != null) {
                        quality = entry.getFieldValue("quality").toString();
                        quality = quality.replace("{", "");
                        quality = quality.replace("}", "");
                    }

                    value = entry.getFieldValue("history");
                    String history = "no-history";
                    if (value != null) {
                        history = entry.getFieldValue("history").toString();
                        history = history.replace("{", "");
                        history = history.replace("}", "");
                    }

                    value = entry.getFieldValue("comments");
                    String comments = "no-comments";
                    if (value != null) {
                        comments = value.toString();
                        comments = comments.replace("{", "");
                        comments = comments.replace("}", "");
                    }

                    value = entry.getFieldValue("conflict");
                    String conflict = "no-conflict";
                    if (value != null) {
                        conflict = value.toString();
                        conflict = conflict.replace("{", "");
                        conflict = conflict.replace("}", "");
                    }

//                    value = entry.getFieldValue("topics");
//                    String topics = "no-topics";
//                    if (value != null) {
//                        topics = value.toString();
//                        topics = topics.replace("{", "");
//                        topics = topics.replace("}", "");
//                    }

                    ArrayList<String> tuple = new ArrayList<String>();
                    tuple.add(entry.getEntryKey());
                    tuple.add(title);
                    tuple.add(entry.getEntryType());
                    tuple.add(author);
                    tuple.add(abst);
                    tuple.add(year);
                    tuple.add(quality);                    
                    tuple.add(history);                    
//                    tuple.add(entry.getFieldValue("references").toString());
                    tuple.add(String.valueOf(nextId));
                    tuple.add(comments);
                    tuple.add(conflict);
//                    tuple.add(topics);

                    this.matrix.add(tuple);

                    ids.add(nextId);
                    nextId++;

                    // Add each different value into a hashtable                    
                    for (int j = 0; j < tuple.size(); j++)
                        this.attributes.get(j).putValue(tuple.get(j));

                    titles.put(title.toLowerCase(), entry.getEntryKey());
                }
            }

            this.relationships.add(new Attribute("from"));
            this.relationships.add(new Attribute("to"));
            this.relationships.add(new Attribute("reference"));

            for (int i = 0; i < entries.size(); i++) {
                if (entries.get(i) instanceof BibtexEntry) {
                    BibtexEntry entry = (BibtexEntry) entries.get(i);                    

                    // rafael
                    if (entry.getFieldValue("references") == null) {                        
                        continue;
                    }

                    String referece = entry.getFieldValue("references").toString().substring(1, entry.getFieldValue("references").toString().length()-1);
                    String[] references = referece.split("(\r\n)[ \t]*(\r\n)");                    

                    if (references.length == 1) {
                        references = referece.split("(\n)[ \t]*(\n)");
                    }

                    if (references.length == 1) {
                        references = referece.split("(\r)[ \t]*(\r)");
                    }

                    for (String s : references) {
                        if (!s.replaceAll("\\s*", "").equals("")) {
                            s = filter(s);

                            if (!titles.containsKey(s.toLowerCase())) {
                                titles.put(s.toLowerCase(), "C[" + (++citation) + "]");

                                ArrayList<String> tuple = new ArrayList<String>(9);
                                tuple.add("C[" + citation + "]");
                                tuple.add(s);
                                tuple.add("[referenced - no data]");
                                tuple.add("[referenced - no data]");
                                tuple.add("[referenced - no data]");
                                tuple.add("1900");
                                tuple.add("-1");
                                tuple.add("C");
                                tuple.add(String.valueOf(nextId));

                                this.matrix.add(tuple);

                                ids.add(nextId);
                                nextId++;

                                // Add each different value into a hashtable
                                for (int j = 0; j < tuple.size(); j++)
                                    this.attributes.get(j).putValue(tuple.get(j));
                            }

                            // Relationships
                            ArrayList<String> relationship = new ArrayList<String>();
                            relationship.add(entry.getEntryKey());
                            relationship.add(titles.get(s.toLowerCase()));
                            relationship.add("100");

                            this.relationshipMatrix.add(relationship);

                            // Add each different value into a hashtable
                            for (int j = 0; j < relationship.size(); j++)
                                this.relationships.get(j).putValue(relationship.get(j));
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String filter(String s) {
        s = s.replaceAll("\r\n", " ");
        s = s.replaceAll("\n", " ");
        s = s.replaceAll("\r", " ");
        s = s.replaceAll("\t", " ");
        s = s.replaceAll("[ ]+", " ");
        s = s.trim();
        return s;
    }
}