/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.socialnet.graphdata;

import bibtex.dom.BibtexEntry;
import bibtex.dom.BibtexFile;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import vispipeline.basics.interfaces.Persistence;

/**
 *
 * @author Rafael
 */
public class BibGraphDataPersistence extends Persistence<BibGraphData> {

    @Override
    public BibGraphData load(File file) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void save(BibGraphData object, File file) throws IOException {
        // index, so I can search by name
        Map<String, ArrayList<String>> index = new HashMap<String, ArrayList<String>>();
        for (ArrayList<String> item : object.getMatrix()) {
            index.put(item.get(0), item);
        }
        // original dataset
//        System.out.println("original: " + object.getUrl());
        GraphData gdata1 = GraphDataFactory.getInstance(object.getUrl());
        // update the original dataset
        for (ArrayList<String> item : gdata1.getMatrix()) {
            ArrayList<String> item2 = index.get(item.get(0));
            if (item2 != null && !item.get(0).startsWith("C[")) {
                // quality
                item.add(6, item2.get(6));
                // history
                item.add(7, item2.get(7));
                // comments
                item.add(9, item2.get(9));
                // conflict
                item.add(10, item2.get(10));
            }
        }
        // save .bib again (the modified original)
        BibtexFile bibtexFile = new BibtexFile();
        for (ArrayList<String> item : gdata1.getMatrix()) {
            // cited-only are not saved; they are only used in memory
            if (item.get(0).startsWith("C[")) {
                continue;
            }            
            BibtexEntry entry = bibtexFile.makeEntry(item.get(2), item.get(0));
            entry.addFieldValue("title", bibtexFile.makeString(item.get(1)));
            entry.addFieldValue("author", bibtexFile.makeString(item.get(3)));
            entry.addFieldValue("abstract", bibtexFile.makeString(item.get(4)));
            entry.addFieldValue("year", bibtexFile.makeString(item.get(5)));
            entry.addFieldValue("quality", bibtexFile.makeString(item.get(6)));
            entry.addFieldValue("history", bibtexFile.makeString(item.get(7)));
            String references = "";
            for (ArrayList<String> rel : gdata1.getRelationshipMatrix()) {
                if (rel.get(0).equals(item.get(0))) {
//                    System.out.println("ref: " + rel.get(0) + ", " + rel.get(1));
                    String refKey = rel.get(1);
                    for (ArrayList<String> item2 : gdata1.getMatrix()) {
                        if (item2.get(0).equals(refKey)) {
                            references += item2.get(1) + "\n\n";
                        }
                    }
                }
            }
            entry.addFieldValue("references", bibtexFile.makeString(references));
            entry.addFieldValue("comments", bibtexFile.makeString(item.get(9)));
            entry.addFieldValue("conflict", bibtexFile.makeString(item.get(10)));
//            entry.addFieldValue("topics", bibtexFile.makeString(item.get(11)));
            bibtexFile.addEntry(entry);
        }

        bibtexFile.printBibtex(new PrintWriter(file));
//        bibtexFile.printBibtex(new PrintWriter(System.out));
    }

}
