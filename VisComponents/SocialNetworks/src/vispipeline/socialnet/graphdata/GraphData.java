/* ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2005-2007 Universidade de Sao Paulo, Sao Carlos/SP, Brazil.
 * All Rights Reserved.
 *
 * This file is part of Projection Explorer (PEx).
 *
 * How to cite this work:
 *
@inproceedings{paulovich2007pex,
author = {Fernando V. Paulovich and Maria Cristina F. Oliveira and Rosane
Minghim},
title = {The Projection Explorer: A Flexible Tool for Projection-based
Multidimensional Visualization},
booktitle = {SIBGRAPI '07: Proceedings of the XX Brazilian Symposium on
Computer Graphics and Image Processing (SIBGRAPI 2007)},
year = {2007},
isbn = {0-7695-2996-8},
pages = {27--34},
doi = {http://dx.doi.org/10.1109/SIBGRAPI.2007.39},
publisher = {IEEE Computer Society},
address = {Washington, DC, USA},
}
 *
 * PEx is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * PEx is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * This code was developed by members of Computer Graphics and Image
 * Processing Group (http://www.lcad.icmc.usp.br) at Instituto de Ciencias
 * Matematicas e de Computacao - ICMC - (http://www.icmc.usp.br) of
 * Universidade de Sao Paulo, Sao Carlos/SP, Brazil. The initial developer
 * of the original code is Fernando Vieira Paulovich <fpaulovich@gmail.com>.
 *
 * Contributor(s): Rosane Minghim <rminghim@icmc.usp.br>
 *
 * You should have received a copy of the GNU General Public License along
 * with PEx. If not, see <http://www.gnu.org/licenses/>.
 *
 * ***** END LICENSE BLOCK ***** */

package vispipeline.socialnet.graphdata;

import graph.model.Connectivity;
import graph.model.Edge;
import graph.model.GraphModel;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import textprocessing.corpus.Corpus;
import textprocessing.processing.Ngram;

/**
 *
 * @author Gabriel de Faria Andery
 */
public abstract class GraphData extends Corpus {

    public GraphData(String url, int nrGrams) {
        super(url, nrGrams);
    }

    @Override
    public String getFullContent(int id) throws IOException {
        return getContent(id);
    }

    @Override
    public String getFilteredContent(int id) throws IOException {
        return getContent(id);
    }

    @Override
    public String getSearchContent(int id) throws IOException {
        return getContent(id);
    }

    @Override
    public String getViewContent(int id) throws IOException {
        return getContent(id);
    }

    private String getContent(int id) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);

        List<String> values = matrix.get(id);

        pw.println(values.get(0));
        pw.println();
        pw.println(values.get(1));
        if (!values.get(3).equals("[referenced - no data]")) {
            pw.println();
            pw.println(values.get(3));
        }
        if (!values.get(4).equals("[referenced - no data]")) {
            pw.println();
            pw.println(values.get(4));
        }
        pw.flush();
        pw.close();

        return sw.toString();
    }

    @Override
    public ArrayList<Ngram> getNgrams(int id) throws IOException {
        ArrayList<Ngram> ngrams = new ArrayList<Ngram>();

        ArrayList<String> data = this.matrix.get(id);

        for (String value : data) {
            for (String value_split : value.split(" ")) {
                if (ngrams.contains(value_split.toLowerCase())) {
                    ngrams.get(ngrams.indexOf(value_split.toLowerCase())).frequency++;
                }
                else {
                    ngrams.add(new Ngram(value_split.toLowerCase()));
                }
            }
        }

        return ngrams;
    }

    @Override
    public ArrayList<Ngram> getCorpusNgrams() throws IOException {
        ArrayList<Ngram> ngrams = new ArrayList<Ngram>();

        for (Attribute att : attributes) {
            for (Enumeration<String> e = att.values.keys(); e.hasMoreElements();) {
                String value = e.nextElement();

                for (String value_split : value.split(" ")) {
                    if (ngrams.contains(value_split.toLowerCase())) {
                        ngrams.get(ngrams.indexOf(value_split.toLowerCase())).frequency++;
                    }
                    else {
                        ngrams.add(new Ngram(value_split.toLowerCase()));
                    }
                }
            }
        }

        return ngrams;
    }

    @Override
    public float[] getClassData() {
        return new float[matrix.size()];
    }

    public ArrayList<ArrayList<String>> getMatrix() {
        return matrix;
    }

    public void setMatrix(ArrayList<ArrayList<String>> matrix) {
        this.matrix = matrix;
    }

    public ArrayList<ArrayList<String>> getVisualMatrix() {
        return visualMatrix;
    }

    public void setVisualMatrix(ArrayList<ArrayList<String>> visualMatrix) {
        this.visualMatrix = visualMatrix;
    }

    public ArrayList<ArrayList<String>> getRelationshipMatrix() {
        return relationshipMatrix;
    }

    public void setRelationshipMatrix(ArrayList<ArrayList<String>> relationshipMatrix) {
        this.relationshipMatrix = relationshipMatrix;
    }

    public ArrayList<Attribute> getAttributes() {
        return attributes;
    }

    public Attribute getAttributeByName(String name) {
        for (Attribute att: attributes) {
            if (att.getName().equals(name)) return att;
        }
        return null;
    }

    public ArrayList<Attribute> getVisualAttributes() {
        return visualAttributes;
    }

    public Attribute getVisualAttributeByName(String name) {
        for (Attribute att: visualAttributes) {
            if (att.getName().equals(name)) return att;
        }
        return null;
    }

    public ArrayList<Attribute> getRelationships() {
        return relationships;
    }

    public Attribute getRelationshipByName(String name) {
        for (Attribute att: relationships) {
            if (att.getName().equals(name)) return att;
        }
        return null;
    }

    public Attribute getRelationshipByPosition(int pos) {
        if (pos < 0) pos = 0;
        else if (pos >= relationships.size()) pos = relationships.size() - 1;

        return relationships.get(pos);
    }

    public ArrayList<Connectivity> createConnectivities() {

        Map<String, Integer> index = new HashMap<String, Integer>();
        int x = 0;
        for (ArrayList<String> row : matrix) {
            index.put(row.get(0), x++);
        }

        ArrayList<Connectivity> conns = new ArrayList<Connectivity>();

        for (int i = 2; i < relationships.size(); i++) {
            //Pair[][] uNeighbors = null;
            //Pair[][] dNeighbors = null;
            ArrayList<Edge> dNeighbors = new ArrayList<Edge>();
            //Hashtable<Integer, ArrayList<Pair>> undirected = new Hashtable<Integer, ArrayList<Pair>>();
            Hashtable<Integer, ArrayList<Pair>> directed = new Hashtable<Integer, ArrayList<Pair>>();

            for (int j = 0; j < matrix.size(); j++) {
                //undirected.put(j, new ArrayList<Pair>());
                directed.put(j, new ArrayList<Pair>());
            }

            for (int j = 0; j < relationshipMatrix.size(); j++) {
                if (Float.parseFloat(relationshipMatrix.get(j).get(i)) == 0)
                    continue;

//                Integer keyFrom = ids.indexOf(relationshipMatrix.get(j).get(0));
                Integer keyFrom = index.get(relationshipMatrix.get(j).get(0));
//                Integer keyTo = ids.indexOf(relationshipMatrix.get(j).get(1));
                Integer keyTo = index.get(relationshipMatrix.get(j).get(1));
                Float strength = Float.parseFloat(relationshipMatrix.get(j).get(i));

                //ArrayList<Pair> fromPairsUndirected = undirected.get(keyFrom);
                //ArrayList<Pair> toPairsUndirected = undirected.get(keyTo);
                //fromPairsUndirected.add(new Pair(keyTo, strength));
                //toPairsUndirected.add(new Pair(keyFrom, strength));

                Edge tmp = new Edge(keyFrom, keyTo, strength);
//                ArrayList<Pair> fromPairsDirected = directed.get(keyFrom);
//                fromPairsDirected.add(new Pair(keyTo, strength));
                dNeighbors.add(tmp);
            }

            //if (undirected.isEmpty()) continue;

            //uNeighbors = new Pair[ids.size()][];
            //dNeighbors = new Pair[ids.size()][];

//            for (int j = 0; j < ids.size(); j++) {
//                //uNeighbors[j] = new Pair[undirected.get(j).size()];
//                dNeighbors[j] = new Pair[directed.get(j).size()];
//
//                /*for (int k = 0; k < undirected.get(j).size(); k++) {
//                uNeighbors[j][k] = undirected.get(j).get(k);
//                }*/
//
//                for (int k = 0; k < directed.get(j).size(); k++) {
//                    dNeighbors[j][k] = directed.get(j).get(k);
//                }
//            }

            //Connectivity uCon = new Connectivity(relationships.get(i).getName()/*+ " [Undirected]"*/);
//            Connectivity dCon = new Connectivity(relationships.get(i).getName()/* + " [Directed]"*/);
            Connectivity dCon = new Connectivity(relationships.get(i).getName(), dNeighbors);
            //uCon.create(graph.getVertex(), uNeighbors);
//            dCon.create(graph.getVertex(), dNeighbors);
            //graph.addConnectivity(uCon);
//            graph.addConnectivity(dCon);
            conns.add(dCon);
        }
        return conns;
    }


    protected enum Type {
        NUMERICAL, STRING, DATE
    }


    public class Attribute {
        private String name;
        private Type type;
        private Hashtable<String,Integer> values;
        private int weight = 1;

        private SimpleDateFormat dateFormat;

        private Float min;
        private Float max;
        private int count = 0;

        private boolean useName = true;
        private boolean replaceSpaces = true;

        private boolean active;
        private boolean clazz = false;
        private boolean id = false;
        private boolean idApplicable = true;

        public Attribute() {
            this.active = true;
            this.type = Type.NUMERICAL;
            this.values = new Hashtable<String, Integer>();
        }

        public Attribute(String name) {
            this.name = name;
            this.active = true;
            this.type = Type.NUMERICAL;
            this.values = new Hashtable<String, Integer>();
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getType() {
            return type.toString().toLowerCase();
        }

        public void setType(Type type) {
            this.type = type;
        }

        public Hashtable<String, Integer> getValues() {
            return values;
        }

        public void setValues(Hashtable<String, Integer> values) {
            this.values = values;
        }

        public void setDateFormat(String format) {
            try {
                dateFormat = new SimpleDateFormat(format);
            } catch (Exception e) {
                if (type == Type.DATE)
                    type = Type.STRING;
            }
        }

        public SimpleDateFormat getDateFormat() {
            return dateFormat;
        }

        public boolean isUseName() {
            return useName;
        }

        public void setUseName(boolean useName) {
            this.useName = useName;
        }

        public boolean isReplaceSpaces() {
            return replaceSpaces;
        }

        public void setReplaceSpaces(boolean replaceSpaces) {
            this.replaceSpaces = replaceSpaces;
        }

        public boolean isActive() {
            return active;
        }

        public void setActive(boolean active) {
            this.active = active;
        }

        public boolean isClazz() {
            return clazz;
        }

        public void setClazz(boolean clazz) {
            this.clazz = clazz;
        }

        public boolean isId() {
            return id;
        }

        public void setId(boolean id) throws InvalidFileException {
            if (id == true) {
                if (idApplicable) this.id = true;
                else {
                    this.id = false;
                    throw new InvalidFileException("Ids must be unique.");
                }
            }
            else this.id = false;
        }

        public boolean containsKey(String key) {
            return (values.containsKey(key));
        }

        public void putValue(String key) {
            if (!values.containsKey(key)) {
                values.put(key, this.count);

                if (this.type == Type.NUMERICAL) {
                    Float aux;

                    try {
                        aux = Float.valueOf(key);
                        if (!aux.isNaN()){
                            if (min == null) min = aux;
                            else if (aux < min) min = aux;

                            if (max == null) max = aux;
                            else if (aux > max) max = aux;
                        }
                    } catch (NumberFormatException e) {
                        this.type = Type.STRING;
                        min = 0f;
                        max = Float.valueOf(this.count);
                    }
                }
                else if (this.type == Type.DATE) {
                    try {
                        dateFormat.parse(key);
                    }
                    catch (ParseException e) {
                        System.out.println(e.toString());
                        this.type = Type.STRING;
                    }
                    finally {
                        max = Float.valueOf(this.count);
                    }
                }
                else {
                    max = Float.valueOf(this.count);
                }

                this.count++;
            }
            else {
                idApplicable = false;
            }
        }

        public int getWeight() {
            return weight;
        }

        public void setWeight(int weight) {
            this.weight = weight;
        }
    }

    public class InvalidFileException extends IOException {
        public InvalidFileException(String message) {
            super(message);
        }
    }

    @Override
    public ArrayList<Integer> getIds() {
        ArrayList<Integer> ids = new ArrayList<Integer>();
        for (int i = 0; i < matrix.size(); i++) {
            ids.add(i);
        }
        return ids;
    }

    protected ArrayList<ArrayList<String>> matrix;
    protected ArrayList<ArrayList<String>> visualMatrix;
    protected ArrayList<ArrayList<String>> relationshipMatrix;
    protected ArrayList<Attribute> attributes;
    protected ArrayList<Attribute> visualAttributes;
    protected ArrayList<Attribute> relationships;
}
