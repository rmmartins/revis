/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.socialnet.view;

import graph.view.GraphFrameComp;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import org.openide.nodes.Node;
import org.openide.util.lookup.ServiceProvider;
import org.openide.windows.Mode;
import org.openide.windows.WindowManager;
import vispipeline.basics.annotations.Parameter;
import vispipeline.basics.annotations.VisComponent;
import vispipeline.basics.interfaces.AbstractComponent;


/**
 *
 * @author Rafael
 */
@VisComponent(hierarchy = "Networks.View",
name = "Network View TC",
description = "Displays a graph model in a TopComponent.")
@ServiceProvider(service = AbstractComponent.class)
public class GraphViewComp extends GraphFrameComp
        implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        if (model != null) {
            final NetworkModelViewer pview = new NetworkModelViewer();
            pview.setSize(600, 600);
            pview.setModel(model);           

            // TopComponent:
            try {
                SwingUtilities.invokeAndWait(new Runnable() {

                    public void run() {
                        GraphViewTC frame = new GraphViewTC();
//                        SysReviewProject.tcs.add(frame);
//                        frame.setCorpus(corpus);
                        frame.setModelViewer(pview);

                        if (coordinators != null) {
                            for (int i = 0; i < coordinators.size(); i++) {
                                frame.addCoordinator(coordinators.get(i));
                            }
                        }

                        Mode m1 = WindowManager.getDefault().findMode("editor");
                        m1.dockInto(frame);
//                        SysReviewProject.currentProject.addToLookup(frame);
//                        frame.setCookie(new SysReviewCookie((BibGraphData) corpus));
                        Node node = frame.getActivatedNodes()[0];
                        if (node != null) {
//                            JOptionPane.showMessageDialog(null, "node não nulo");
//                            SysReviewCookie cookie = node.getCookie(SysReviewCookie.class);
//                            if (cookie != null) {
//                                JOptionPane.showMessageDialog(null, "cookie não nulo");
//                            } else {
//                                JOptionPane.showMessageDialog(null, "cookie nulo");
//                            }
                        } else {
//                            JOptionPane.showMessageDialog(null, "node nulo");
                        }
                        frame.open();
                    }
                });
            } catch (InterruptedException e) {
                JOptionPane.showMessageDialog(null, e.toString());
            } catch (InvocationTargetException e) {
                JOptionPane.showMessageDialog(null, this.getClass().getName() +
                        e.toString());
            }

        } else {
            throw new IOException("A graph model should be provided.");
        }
    }

    @Parameter public int stage = 1;

}
