/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.socialnet.view;

import graph.model.Connectivity;
import graph.model.Edge;
import graph.model.GraphInstance;
import graph.model.GraphModel;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Observable;
import projection.model.ProjectionInstance;
import vispipeline.visualization.color.ColorScalePanel;
import vispipeline.visualization.color.ColorTable;
import vispipeline.visualization.model.AbstractInstance;
import vispipeline.visualization.model.AbstractModel;
import vispipeline.visualization.view.ModelViewer;

/**
 *
 * @author Rafael
 */
public class NetworkModelViewer extends ModelViewer {

    // order matters:
    // (a) drawing is from last to first;
    // (b) first elements will have precedence on selection.
    protected Map<AbstractInstance, InstanceView> instance_views =
            new LinkedHashMap<AbstractInstance, InstanceView>();

    // anti-aliasing
    private boolean highquality = true;

    public NetworkModelViewer() {
        colorscale = new ColorScalePanel(this);
        colorscale.setPreferredSize(new Dimension(180, 12));
        add(colorscale);
    }

    @Override
    public void setModel(AbstractModel model) {
        super.setModel(model);
        if (model instanceof GraphModel) {            
            instance_views = new LinkedHashMap<AbstractInstance, InstanceView>();
            for (AbstractInstance instance : model.getInstances()) {
                GraphInstance g_inst = (GraphInstance) instance;
                instance_views.put(instance, new InstanceView(g_inst));
            }
        }
        // adjust to viewer size, since model comes with initial positions        
        fitSize(getWidth(), getHeight());
    }

    @Override
    public AbstractInstance getInstanceByPosition(Point p) {
        for (Entry<AbstractInstance, InstanceView> entry : instance_views.entrySet()) {
            if (entry.getValue().contains(p)) {
                return entry.getKey();
            }
        }
        return null;
    }

    @Override
    public ArrayList<AbstractInstance> getInstancesByPosition(Polygon p) {
        ArrayList<AbstractInstance> list = new ArrayList<AbstractInstance>();
        for (Entry<AbstractInstance, InstanceView> entry : instance_views.entrySet()) {
            if (p.contains(entry.getValue().center)) {
                list.add(entry.getKey());
            }
        }
        return list;
    }

    @Override
    public ArrayList<AbstractInstance> getInstancesByPosition(Rectangle r) {
        ArrayList<AbstractInstance> list = new ArrayList<AbstractInstance>();
        for (Entry<AbstractInstance, InstanceView> entry : instance_views.entrySet()) {
            if (r.contains(entry.getValue().center)) {
                list.add(entry.getKey());
            }
        }
        return list;
    }

    @Override
    public void zoom(float rate) {
        // to be implemented
    }

    @Override
    public void draw(Graphics2D g) {
        GraphModel graph = (GraphModel) model;
        if (graph != null) {
            // reset canvas
            Dimension size = graph.getSize();
            g.setColor(this.getBackground());
            g.fillRect(0, 0, size.width + 10, size.height + 10);

            // set anti-aliasing on/off
            if (highquality) {
                g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                        RenderingHints.VALUE_ANTIALIAS_ON);
            } else {
                g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                        RenderingHints.VALUE_ANTIALIAS_OFF);
            }

            // edges are drawn first
            Connectivity conn = graph.getSelectedConnectivity();
            if (conn != null) {
                ArrayList<Edge> edges = conn.getEdges();
                ArrayList<AbstractInstance> instances = graph.getInstances();
                int esize = edges.size();
                for (int i = 0; i < esize; i++) {
                    GraphInstance source = (GraphInstance) instances.get(
                            edges.get(i).getSource());
                    GraphInstance target = (GraphInstance) instances.get(
                            edges.get(i).getTarget());

                    InstanceView sourcev = instance_views.get(source);
                    InstanceView targetv = instance_views.get(target);

                    //Combines the color of the two vertex to paint the edge
                    if (!source.isValid() && !target.isValid()) {
                        g.setColor(Color.BLACK);
                    } else {
//                float alpha = model.getAlpha();
//                if (!source.isSelected() || !target.isSelected()) {
//                    g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha));
//                }
                        g.setColor(new Color((source.getColor().getRed() +
                                target.getColor().getRed()) / 2,
                                (source.getColor().getGreen() +
                                target.getColor().getGreen()) / 2,
                                (source.getColor().getBlue() +
                                target.getColor().getBlue()) / 2));
                    }

                    g.setStroke(new BasicStroke(1.3f));
                    g.drawLine(sourcev.center.x, sourcev.center.y,
                            targetv.center.x, targetv.center.y);
                    g.setStroke(new BasicStroke(1.0f));

                    g.setComposite(java.awt.AlphaComposite.getInstance(
                            java.awt.AlphaComposite.SRC_OVER, 1.0f));

//                    if (this.showweight) {
//                        String label = Float.toString(edges.get(i).getWeight());
//
//                        float x = 5 + (float) Math.abs(source.center.getX() - target.center.getX()) / 2
//                                + Math.min(source.center.getX(), target.center.getX());
//                        float y = (float) Math.abs(source.center.getY() - target.center.getY()) / 2
//                                + Math.min(source.center.getY(), target.center.getY());
//
//                        //Getting the font information
//                        FontMetrics metrics = g2.getFontMetrics(g2.getFont());
//
//                        //Getting the label size
//                        int width = metrics.stringWidth(label);
//                        int height = metrics.getAscent();
//
//                        g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.75f));
//                        g2.setPaint(Color.WHITE);
//                        g2.fill(new Rectangle((int) x - 2, (int) y - height, width + 4, height + 4));
//                        g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0f));
//
//                        g2.setColor(Color.BLACK);
//                        g2.drawRect((int) x - 2, (int) y - height, width + 4, height + 4);
//
//                        g2.drawString(label, x, y);
//                    }
                }
            }

            // then the instances
            
            InstanceView[] iviews = instance_views.values().toArray(new InstanceView[0]);
            // draw non-selected first, in the reverse map order            
            for (int i = iviews.length - 1; i >= 0; i--) {
                if (!iviews[i].isSelected()) {
                    iviews[i].draw(g);
                }
            }
            // draw selected, in the reverse map order
            for (int i = iviews.length - 1; i >= 0; i--) {
                if (iviews[i].isSelected()) {
                    iviews[i].draw(g);
                }
            }
        }
    }

    @Override
    public String toString() {
        return "Network Viewer";
    }
    
    public void fitSize(int width, int height) {
        float maxx = Float.NEGATIVE_INFINITY;
        float minx = Float.POSITIVE_INFINITY;
        float maxy = Float.NEGATIVE_INFINITY;
        float miny = Float.POSITIVE_INFINITY;

        ArrayList<AbstractInstance> instances = model.getInstances();
        Dimension size = new Dimension(width, height);

        for (int i = 0; i < instances.size(); i++) {
            ProjectionInstance pi = (ProjectionInstance) instances.get(i);

            if (maxx < pi.getX()) {
                maxx = pi.getX();
            }

            if (minx > pi.getX()) {
                minx = pi.getX();
            }

            if (maxy < pi.getY()) {
                maxy = pi.getY();
            }

            if (miny > pi.getY()) {
                miny = pi.getY();
            }
        }

//        float begin = 30.0f;
        float begin = 10.0f;
        float endy = 0.0f;
        float endx = 0.0f;

        if (maxy > maxx) {
            endy = Math.min(size.width, size.height) - begin;

            if (maxy != miny) {
                endx = ((maxx - minx) * endy) / (maxy - miny);
            } else {
                endx = ((maxx - minx) * endy);
            }
        } else {
            endx = Math.min(size.width, size.height) - begin;

            if (maxx != minx) {
                endy = ((maxy - miny) * endx) / (maxx - minx);
            } else {
                endy = ((maxy - miny) * endx);
            }
        }

        for (int i = 0; i < instances.size(); i++) {
            ProjectionInstance pi = (ProjectionInstance) instances.get(i);

            if (maxx != minx) {
                pi.setX((((pi.getX() - minx) / (maxx - minx)) *
                        (endx - begin)) + begin);
            } else {
                pi.setX(begin);
            }

            if (maxy != miny) {
                pi.setY(((((pi.getY() - miny) / (maxy - miny)) *
                        (endy - begin)) + begin));
            } else {
                pi.setY(begin);
            }
        }

        for (AbstractInstance ai : instances) {
            GraphInstance gi = (GraphInstance) ai;
            instance_views.get(ai).setCenter(new Point((int)gi.getX(), (int)gi.getY()));
        }

        model.setChanged();
    }

    protected class InstanceView {

        GraphInstance instance;
        Point center;
        float radius;

        int alpha;

        public InstanceView(GraphInstance instance) {
            this.instance = instance;
            center = new Point((int)instance.getX(), (int)instance.getY());
            radius = instance.getRay();
            alpha = 255;
        }

        // these are dynamic; a bit of a problem to keep them updated.
        Color getColor() {
            return instance.getColor();
        }
        boolean isSelected() {
            return instance.isSelected();
        }
        boolean isShowLabel() {
            return instance.isShowLabel();
        }
        
        void draw(Graphics2D g) {
            if (highquality) {
                g.setRenderingHint(RenderingHints.KEY_RENDERING,
                        RenderingHints.VALUE_RENDER_QUALITY);
                g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                        RenderingHints.VALUE_ANTIALIAS_ON);
                g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
                        RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
                g.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS,
                        RenderingHints.VALUE_FRACTIONALMETRICS_ON);
                g.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING,
                        RenderingHints.VALUE_COLOR_RENDER_QUALITY);
                g.setRenderingHint(RenderingHints.KEY_DITHERING,
                        RenderingHints.VALUE_DITHER_ENABLE);
            }

            // the internal area of the instances
            Color color = getColor();
            Color alphaColor = new Color(color.getRed(), color.getGreen(),
                    color.getBlue(), alpha);

            g.setColor(alphaColor);
            g.fillOval((int) (center.x - radius), (int) (center.y - radius),
                    (int) radius * 2, (int) radius * 2);

            // then the borders
            float borderRadius = radius;
            Color color2 = Color.BLACK;
            if (isSelected()) {
                g.setStroke(new BasicStroke(4));
                borderRadius *= 1.5;
                color2 = Color.DARK_GRAY;
            }            
            Color alphaColor2 = new Color(color2.getRed(), color2.getGreen(),
                    color2.getBlue(), alpha);
            g.setColor(alphaColor2);
            g.drawOval((int) (center.x - borderRadius), (int) (center.y - borderRadius),
                    (int) borderRadius * 2, (int) borderRadius * 2);

            g.setStroke(new BasicStroke());

            //show the label associated to this instance
            if (isShowLabel()) {
                java.awt.FontMetrics metrics = g.getFontMetrics(g.getFont());

                int width = metrics.stringWidth(toString().trim());
                int height = metrics.getAscent();

                g.setComposite(java.awt.AlphaComposite.getInstance(
                        java.awt.AlphaComposite.SRC_OVER, 0.75f));
                g.setPaint(java.awt.Color.WHITE);
                g.fillRect(((int) center.x) + 3, ((int) center.y) - 1 - height,
                        width + 4, height + 4);

                g.setComposite(java.awt.AlphaComposite.getInstance(
                        java.awt.AlphaComposite.SRC_OVER, 1.0f));
                g.setColor(java.awt.Color.DARK_GRAY);
                g.drawRect(((int) center.x) + 3, ((int) center.y) - 1 - height,
                        width + 4, height + 4);

                g.drawString(toString().trim(), ((int) center.x) + 3, ((int) center.y));
            }
        }

        boolean contains(Point p) {
            if (p.distance(center) <= radius) {
                return true;
            }
            return false;
        }

        public void setAlpha(int alpha) {
            this.alpha = alpha;
        }

        public void setCenter(Point p) {
            center = p;
        }

    }
}
