/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.socialnet.model;


import graph.model.GraphModelComp;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.lookup.ServiceProvider;
import vispipeline.basics.annotations.VisComponent;
import vispipeline.basics.interfaces.AbstractComponent;
import vispipeline.socialnet.graphdata.GraphData;
import vispipeline.visualization.model.AbstractInstance;
import vispipeline.visualization.model.Scalar;

/**
 *
 * @author Rafael, Gabriel
 */
@VisComponent(hierarchy = "Networks.Model",
name = "Social Network Model",
description = "Create a network model to be visualized.")
@ServiceProvider(service=AbstractComponent.class)
public class SocialNetworkModelComp extends GraphModelComp 
        implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        super.execute();

        // novos escalares
        if (gdata != null) {
            ArrayList<AbstractInstance> vertex = model.getInstances();
            for (int i = 0; i < gdata.getAttributes().size(); i++) {
                GraphData.Attribute att = gdata.getAttributes().get(i);
//                int title = model.addTitle(att.getName());
                Scalar scalar = model.addScalar(att.getName());
                
                if (att.getType().equals("numerical")) {
                    for (int j = 0; j < vertex.size(); j++) {
                        if (gdata.getMatrix().size() > j) {
//                            vertex.get(j).setTitle(title, gdata.getMatrix().get(j).get(i));
                            vertex.get(j).setScalarValue(scalar,
                                    Float.parseFloat(gdata.getMatrix().get(j).get(i)));
                        }
                    }
                } else if (att.getType().equals("date")) {
                    for (int j = 0; j < vertex.size(); j++) {
                        if (gdata.getMatrix().size() > j) {
//                            vertex.get(j).setTitle(title, gdata.getMatrix().get(j).get(i));
                            try {
                                long tmp = att.getDateFormat().parse(gdata.getMatrix().get(j).get(i)).getTime();
                                vertex.get(j).setScalarValue(scalar, tmp);
                            } catch (ParseException ex) {
                                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }
                } else {
                    for (int j = 0; j < vertex.size(); j++) {
                        if (gdata.getMatrix().size() > j) {
//                            vertex.get(j).setTitle(title, gdata.getMatrix().get(j).get(i));
                            vertex.get(j).setScalarValue(scalar,
                                    att.getValues().get(gdata.getMatrix().get(j).get(i)));
                        }
                    }
                }
            }
        }
    }

    public void attach(GraphData gdata) {
        this.gdata = gdata;
    }

    private GraphData gdata;

}
