/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.socialnet.force;

import graph.forcelayout.ForceData;
import graph.forcelayout.ForceDirectLayout;
import graph.model.Connectivity;
import graph.model.GraphInstance;
import graph.model.GraphModel;
import java.io.IOException;
import org.openide.util.lookup.ServiceProvider;
import vispipeline.basics.annotations.Param;
import vispipeline.basics.annotations.Parameter;
import vispipeline.basics.annotations.Return;
import vispipeline.basics.annotations.VisComponent;
import vispipeline.basics.interfaces.AbstractComponent;
import vispipeline.basics.interfaces.AbstractParametersView;

/**
 *
 * @author Fernando Vieira Paulovich
 */
@VisComponent(hierarchy = "Networks.Projection",
name = "Force-Directed",
description = "...")
@ServiceProvider(service=AbstractComponent.class)
public class ForceDirectedLayoutComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {

        // initialize force data
        for (int i = 0; i < graph.getInstances().size(); i++) {
            GraphInstance v = (GraphInstance) graph.getInstances().get(i);
            if (v.fdata == null) {
//            Logger.getLogger("ForceDir Comp").info("v:"+v);
                v.fdata = new ForceData();
            }
        }

        // TODO temporarily works with the first connectivity
        Connectivity conn = graph.getConnectivities().get(0);
        if (conn.getName().equals("...")) {
            conn = graph.getConnectivities().get(1);
        }
        if (conn != null) {
            ForceDirectLayout fdl = new ForceDirectLayout(graph, null);
            fdl.setNumIterations(iterations);
            fdl.start(conn);
//            try {
//                Thread.sleep(5000);
//            } catch (InterruptedException ex) {
//                Exceptions.printStackTrace(ex);
//            }
            fdl.stop();
        }
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new GraphDataCorpusParamView(this);
        }
        return paramview;
    }

    public void input(@Param(name = "Graph") GraphModel graph) {
        this.graph = graph;
    }

    @Return(name = "Graph")
    public GraphModel output() {
        return graph;
    }

    @Override
    public void reset() {
        graph = null;
    }

    GraphModel graph;
    @Parameter public int iterations = 1000;

    private transient AbstractParametersView paramview;
    
}
