/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.socialnet.reader;

import java.io.IOException;
import java.util.ArrayList;
import org.openide.util.lookup.ServiceProvider;
import textprocessing.corpus.CorpusComp;
import vispipeline.basics.annotations.Parameter;
import vispipeline.basics.annotations.Return;
import vispipeline.basics.annotations.VisComponent;
import vispipeline.basics.interfaces.AbstractComponent;
import vispipeline.basics.interfaces.AbstractParametersView;
import vispipeline.socialnet.graphdata.GraphData;
import vispipeline.socialnet.graphdata.GraphDataFactory;

/**
 *
 * @author rmmartins, gfandery
 */
@VisComponent(hierarchy = "Networks.Graph",
name = "GraphData (.bib) Corpus",
description = "Represents a set of papers from a .bib file (in GraphData format).")
@ServiceProvider(service=AbstractComponent.class)
public class GraphDataComp extends CorpusComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        if (url.trim().length() > 0) {
            GraphData gdata = GraphDataFactory.getInstance(url);

//            for (int id : corpus.getIds()) {
//                System.out.println(corpus.getFullContent(id));
//            }
//           System.out.println("GraphData: " + gdata.getMatrix().size() + "," +
//                   gdata.getRelationshipMatrix().size());

            ArrayList<ArrayList<String>> toRemove = new ArrayList<ArrayList<String>>();
            if (!includeCitedOnly) {
                for (ArrayList<String> item : gdata.getMatrix()) {
                    if (item.get(0).startsWith("C[")) {
                        toRemove.add(item);
                    }
                }
                for (ArrayList<String> item : gdata.getRelationshipMatrix()) {
                    if (item.get(0).startsWith("C[") || item.get(1).startsWith("C[")) {
                        toRemove.add(item);
                    }
                }
//                Logger.getLogger(getClass().getName()).info("toRemove: " + toRemove.size());
                for (ArrayList<String> item : toRemove) {
                    gdata.getMatrix().remove(item);
                    gdata.getRelationshipMatrix().remove(item);
                }
            }
            corpus = gdata;
//            System.out.println("Corpus: " + corpus.getIds().size());
        } else {
            throw new IOException("A .bib file name must be provided.");
        }
    }    

    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new GraphDataCorpusParamView(this);
        }

        return paramview;
    }

    @Override
    @Return(name = "GraphData") public GraphData output() {
        return (GraphData) corpus;
    }
    
    private transient GraphDataCorpusParamView paramview;

    @Parameter
    public boolean includeCitedOnly = false;
    
}
