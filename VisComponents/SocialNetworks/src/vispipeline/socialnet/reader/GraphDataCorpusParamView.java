/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * ZipCorpusParamView.java
 *
 * Created on 27/05/2009, 09:41:46
 */
package vispipeline.socialnet.reader;

import java.io.File;
import java.io.IOException;
import javax.swing.JFileChooser;
import vispipeline.basics.interfaces.AbstractParametersView;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class GraphDataCorpusParamView extends AbstractParametersView {

    /** Creates new form ZipCorpusParamView */
    public GraphDataCorpusParamView(GraphDataComp comp) {
        initComponents();
        this.comp = comp;
        citedCheckBox.setSelected(comp.includeCitedOnly);

        reset();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        corpusTextField = new javax.swing.JTextField();
        corpusButton = new javax.swing.JButton();
        corpusLabel = new javax.swing.JLabel();
        citedCheckBox = new javax.swing.JCheckBox();

        setBorder(javax.swing.BorderFactory.createTitledBorder("GraphData Corpus (.bib, .vna)"));
        setLayout(new java.awt.GridBagLayout());

        corpusTextField.setColumns(35);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        add(corpusTextField, gridBagConstraints);

        corpusButton.setText("Search...");
        corpusButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                corpusButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        add(corpusButton, gridBagConstraints);

        corpusLabel.setText("File name");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        add(corpusLabel, gridBagConstraints);

        citedCheckBox.setText("Include cited-only entries");
        citedCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                citedCheckBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        add(citedCheckBox, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void corpusButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_corpusButtonActionPerformed
//        try {
            String folder = ".";
//            TopComponent top = TopComponent.getRegistry().getActivated();
//            if (top instanceof PipelineDrawTopComponent) {
//                folder = ((PipelineDrawTopComponent)top).getProjectFolder();
//            }
            //folder = SysReviewProject.currentProject.getProjectDirectory().getPath();
            JFileChooser jfc = new JFileChooser(folder);
            jfc.setFileFilter(new GraphFilter());
            jfc.setDialogTitle("Choose the graph data file:");
            int result = jfc.showOpenDialog(this);
            if (result == JFileChooser.APPROVE_OPTION) {
                File file = jfc.getSelectedFile();
                corpusTextField.setText(file.getAbsolutePath());
            }
//        } catch (IOException ex) {
//            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
//        }
}//GEN-LAST:event_corpusButtonActionPerformed

    private void citedCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_citedCheckBoxActionPerformed
        comp.includeCitedOnly = citedCheckBox.isSelected();
    }//GEN-LAST:event_citedCheckBoxActionPerformed

    @Override
    public void reset() {
        corpusTextField.setText(comp.getUrl());        
    }

    @Override
    public void finished() throws IOException {
        if (corpusTextField.getText().trim().length() > 0) {
            comp.setUrl(corpusTextField.getText());
        } else {
            throw new IOException("A corpus file name must be provided.");
        }
    }

    private GraphDataComp comp;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox citedCheckBox;
    private javax.swing.JButton corpusButton;
    private javax.swing.JLabel corpusLabel;
    private javax.swing.JTextField corpusTextField;
    // End of variables declaration//GEN-END:variables
}
