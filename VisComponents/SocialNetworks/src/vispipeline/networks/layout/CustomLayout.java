/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.networks.layout;

import java.util.HashMap;
import java.util.Map;
import prefuse.data.Graph;
import prefuse.data.Node;
import toxi.geom.Vec3D;
import vispipeline.distance.DistanceMatrix;

/**
 *
 * @author Rafael
 */
public class CustomLayout implements SphereLayout {

    public CustomLayout() {
        pointMap = new HashMap<Node, Vec3D>();
    }

    public void layout(Graph graph, DistanceMatrix dmat) {
        pointMap = new HashMap<Node, Vec3D>();
    }

    public Vec3D getPoint(Node node) {
        return pointMap.get(node);
    }

    public void setPoint(Node node, Vec3D vec) {
        pointMap.put(node, vec);
    }

    private Map<Node, Vec3D> pointMap;

}
