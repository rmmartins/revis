/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.networks.layout;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import org.openide.util.lookup.ServiceProvider;
import prefuse.data.Graph;
import prefuse.data.Node;
import prefuse.data.io.GraphMLWriter;
import toxi.geom.Vec3D;
import vispipeline.basics.annotations.Param;
import vispipeline.basics.annotations.Return;
import vispipeline.basics.annotations.VisComponent;
import vispipeline.basics.interfaces.AbstractComponent;
import vispipeline.basics.interfaces.AbstractParametersView;
import vispipeline.distance.DistanceMatrix;
import vispipeline.networks.input.GraphMLReaderComp;

/**
 *
 * @author Rafael
 */

@VisComponent(hierarchy = "Networks.Layout",
name = "SphereMDS Network Layout",
description = "MDS on the surface of a sphere.")
@ServiceProvider(service = AbstractComponent.class)

public class SphereMDSLayoutComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        layout = new SphereMDSLayout();
//        if (graph.getNodeTable().getColumn("mds_x") != null &&
//                graph.getNodeTable().getColumn("mds_y") != null &&
//                graph.getNodeTable().getColumn("mds_z") != null) {
//            Map<Node, Vec3D> pointMap = new HashMap<Node, Vec3D>();
//            for (int i = 0; i < graph.getNodeCount(); i++) {
//                Node n = graph.getNode(i);
//                float x = (float) n.getFloat("mds_x");
//                float y = (float) n.getFloat("mds_y");
//                float z = (float) n.getFloat("mds_z");
//                pointMap.put(n, new Vec3D(x, y, z));
//            }
//            layout.overrideLayout(pointMap);
//        } else {
            layout.layout(graph, dmat);
//            layout.save();
//            try {
//                String path = GraphMLReaderComp.lastGraphPath;
//                GraphMLWriter writer = new GraphMLWriter();
//                writer.writeGraph(graph, path);
//            } catch (Exception e) {
//                JOptionPane.showMessageDialog(null, e);
//            }
//        }
    }

    public void input(@Param(name="Prefuse graph") Graph graph,
            @Param(name="distances") DistanceMatrix dmat) {
        this.graph = graph;
        this.dmat = dmat;
    }

    @Return(name="layout")
    public SphereMDSLayout output() {
        return layout;
    }

    public void reset() {
        graph = null;
        layout = null;
        dmat = null;
    }

    public AbstractParametersView getParametersEditor() {
        return null;
    }

    private transient SphereMDSLayout layout;
    private transient Graph graph;
    private transient DistanceMatrix dmat;

}
