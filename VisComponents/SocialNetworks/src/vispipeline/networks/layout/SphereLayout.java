/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.networks.layout;

import prefuse.data.Graph;
import prefuse.data.Node;
import toxi.geom.Vec3D;
import vispipeline.distance.DistanceMatrix;

/**
 *
 * @author Rafael
 */
public interface SphereLayout {

    public void layout(Graph graph, DistanceMatrix dmat);

    public Vec3D getPoint(Node node);

}
