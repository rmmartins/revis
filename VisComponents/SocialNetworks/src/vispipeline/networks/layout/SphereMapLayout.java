package vispipeline.networks.layout;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import prefuse.data.Graph;
import prefuse.data.Node;
import projection.technique.idmap.IDMAPProjection;
import toxi.geom.Vec3D;
import vispipeline.distance.DistanceMatrix;

/**
 *
 * @author Rafael
 */
public class SphereMapLayout implements SphereLayout, Iterative {

    public SphereMapLayout() {
       
    }

    public void layout(Graph graph, DistanceMatrix dmat) {
        try {
            this.dmat = dmat;

//            if (knnNumberNeighbors > 0) {
//                KNN knn = new KNN(knnNumberNeighbors);
//                this.knnneighbors = knn.execute(dmat);
//            }

            //Based on the projection type, create the projection using JAVA REFLECTION
            logger.info("Projecting...");

            // this should be FastMap
            RandomLayout rm = new RandomLayout();
            rm.layout(graph, dmat);

            for (int i = 0; i < graph.getNodeCount(); i++) {
                Node n = graph.getNode(i);
                pointMap.put(n, rm.getPoint(n));
            }

            force = new ForceSchemeSphere();
            force.setFractionDelta(fractionDelta);
            force.setNumberIterations(nriterations);
            force.setInitialLayout(pointMap);
            force.doForce(dmat);

        } catch (IOException ex) {
            Logger.getLogger(IDMAPProjection.class.getName()).log(Level.SEVERE, null, ex);
        }
//
//        return null;


    }

    @Override
    public void iterate() {
        force.iteration(dmat);
    }

    public void toSpherical() {
        for (Vec3D v : pointMap.values()) {
            v.toSpherical();
            v.x = radius;
            v.toCartesian();
        }
    }

    public Vec3D getPoint(Node node) {
        return pointMap.get(node);
    }

    private float fractionDelta = 800.0f;
    private int[] index;
    private int nriterations = 0;
    private int knnNumberNeighbors = 0;
    private static final float EPSILON = 0.0000001f;

    private Map<Node, Vec3D> pointMap = new HashMap<Node, Vec3D>();
    private Graph graph;
    private DistanceMatrix dmat;
    private ForceSchemeSphere force;

    private float radius = 200;

    private Logger logger = Logger.getLogger(getClass().getName());

}
