/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.networks.layout;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import prefuse.data.Graph;
import prefuse.data.Node;
import toxi.geom.Vec3D;
import vispipeline.distance.DistanceMatrix;

/**
 *
 * @author Rafael
 */
public class RandomLayout implements SphereLayout {

    public void layout(Graph graph, DistanceMatrix dmat) {
        this.graph = graph;

        radius = 200.0f;
        pointMap = new HashMap<Node, Vec3D>();

        for (int i = 0; i < graph.getNodeCount(); i++) {
            float theta = new Random().nextFloat() * 360;
            float fi = new Random().nextFloat() * 360;

            double x = radius * Math.sin(Math.toRadians(fi)) *
                    Math.cos(Math.toRadians(theta));
            double y = radius * Math.sin(Math.toRadians(fi)) *
                    Math.sin(Math.toRadians(theta));
            double z = radius * Math.cos(Math.toRadians(fi));

            pointMap.put(graph.getNode(i), new Vec3D((float)x,(float)y,(float)z));
        }

        // distance matrix
//        for (int i = 0; i < graph.getNodeCount(); i++) {
//            System.out.printf("\t%d",i);
//        }
//        System.out.println();
//
//        for (int i = 0; i < graph.getNodeCount(); i++) {
//            System.out.printf("%d\t", i);
//            int[] dist = Dijkstra.dijkstra(graph, i);
//            for (int j = 0; j < dist.length; j++) {
//                System.out.printf("%d\t", dist[j]);
//            }
//            System.out.println();
//        }
        
    }

    public Vec3D getPoint(Node node) {
        return pointMap.get(node);
    }

    

    private Map<Node, Vec3D> pointMap;
    private Graph graph;
    private float radius = 200;

}
