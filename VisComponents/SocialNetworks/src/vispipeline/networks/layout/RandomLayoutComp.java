/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.networks.layout;

import java.io.IOException;
import org.openide.util.lookup.ServiceProvider;
import prefuse.data.Graph;
import vispipeline.basics.annotations.Param;
import vispipeline.basics.annotations.Return;
import vispipeline.basics.annotations.VisComponent;
import vispipeline.basics.interfaces.AbstractComponent;
import vispipeline.basics.interfaces.AbstractParametersView;
import vispipeline.distance.DistanceMatrix;

/**
 *
 * @author Rafael
 */

@VisComponent(hierarchy = "Networks.Layout",
name = "Random Network Layout",
description = "Random layout on the surface of a sphere.")
@ServiceProvider(service = AbstractComponent.class)

public class RandomLayoutComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        layout = new RandomLayout();
        layout.layout(graph, dmat);
    }

    public void input(@Param(name="prefuse graph") Graph graph,
            @Param(name="distance matrix") DistanceMatrix dmat) {
        this.graph = graph;
        this.dmat = dmat;
    }

    @Return(name="graph layout")
    public SphereLayout output() {
        return layout;
    }

    public void reset() {
        graph = null;
        layout = null;
        dmat = null;
    }

    public AbstractParametersView getParametersEditor() {
        return null;
    }

    private transient SphereLayout layout;
    private transient Graph graph;
    private transient DistanceMatrix dmat;

}
