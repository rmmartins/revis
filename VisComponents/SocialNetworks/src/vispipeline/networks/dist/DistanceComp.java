package vispipeline.networks.dist;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import org.openide.util.lookup.ServiceProvider;
import prefuse.data.Edge;
import prefuse.data.Graph;
import vispipeline.basics.annotations.Param;
import vispipeline.basics.annotations.Return;
import vispipeline.basics.annotations.VisComponent;
import vispipeline.basics.interfaces.AbstractComponent;
import vispipeline.basics.interfaces.AbstractParametersView;
import vispipeline.distance.DistanceMatrix;
import vispipeline.distance.dissimilarity.AbstractDissimilarity;
import vispipeline.distance.dissimilarity.CosineBased;
import vispipeline.matrix.AbstractMatrix;
import vispipeline.matrix.dense.DenseMatrix;
import vispipeline.matrix.dense.DenseVector;

/**
 *
 * @author Rafael
 */

@VisComponent(hierarchy = "Networks.Distance",
name = "Distance (Temporary)",
description = "Does a lot of things.")
@ServiceProvider(service = AbstractComponent.class)

public class DistanceComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        logger.info(graph.getNodeCount() + " nodes, " + graph.getEdgeCount() +
                " edges.");
        logger.info("Generating adjacency matrix...");
        // connectivity position in file to be used
//        int pos = 5;
//        Map<String, Integer> ids = new HashMap<String, Integer>();
//        int id = 0;
//        for (ArrayList<String> row : gdata.getMatrix()) {
//            ids.put(row.get(0), id++);
//        }
        
//        ArrayList<ArrayList<String>> relMatrix = gdata.getRelationshipMatrix();

        // creates an adjacency matrix
//        float[][] d = new float[ids.size()][ids.size()];
//        for (int i = 0; i < ids.size(); i++) {
//            for (int j = 0; j < ids.size(); j++) {
        float[][] d = new float[graph.getNodeCount()][graph.getNodeCount()];
        for (int i = 0; i < graph.getNodeCount(); i++) {
            for (int j = 0; j < graph.getNodeCount(); j++) {
                if (i == j) {
                    d[i][j] = 0;
                } else {
                    d[i][j] = Float.MAX_VALUE;
                }
            }
        }

        logger.info("populating the adjacency matrix...");
        for (int j = 0; j < graph.getEdgeCount(); j++) {
            Edge e = graph.getEdge(j);
            // if it's not set to combine connectivities and connectivity
            // strength equals zero, then ignore
//            if ((pos != 0) && (Float.parseFloat(e.getString("weight")) == 0)) {
//                continue;
//            }

            // get from and to vertices ids
//            Integer keyFrom = ids.get(relMatrix.get(j).get(0));
//            Integer keyTo = ids.get(relMatrix.get(j).get(1));
            Integer keyFrom = e.getSourceNode().getRow();
            Integer keyTo = e.getTargetNode().getRow();
//            logger.info(j + "|" + keyFrom + "," + keyTo);

            // if it's not set to combine connectivities, but it's to ignore
            // weights, strength equals 1
            Float strength = 1f;

            // if it's not set to combine connectivities neither to ignore
            // weight, gets strength
            strength = Float.parseFloat(e.getString("weight"));

            // if connections isn't directed or if the adjacency matrix is to
            // be used as a distance matrix, d[i][j] must equals d[j][i]
            if (d[keyFrom][keyTo] < Float.MAX_VALUE) {
                d[keyFrom][keyTo] += strength;
            } else {
                d[keyFrom][keyTo] = strength;
            }
            d[keyTo][keyFrom] = d[keyFrom][keyTo];
        }

        // if the weights represent strength, not distance, changes values
        // inverting [min, max] scale

        float max = Float.MIN_VALUE;

        // if the weights represent strength, not distance, changes values
        // inverting [min, max] scale

        // Float.MAX_VALUE's represent pairs of vertices without connection
        // so ignore them (it won't be a connection between them anyway,
        // even after inverting [min, max] scale)
        for (int i = 0; i < graph.getNodeCount(); i++) {
            for (int k = 0; k < graph.getNodeCount(); k++) {
                if (d[i][k] < Float.MAX_VALUE && d[i][k] > max) {
                    max = d[i][k];
                }
            }
        }

        // invert scale by making (max - value)
        // as zero's represents the main diagonal, sums 1
        for (int i = 0; i < graph.getNodeCount(); i++) {
            for (int k = 0; k < graph.getNodeCount(); k++) {
                if ((i != k) && (d[i][k] < Float.MAX_VALUE)) {
                    d[i][k] = max - d[i][k] + 1;
                }
            }
        }

        // if it's set to use matrix as points, creates a points matrix
        AbstractMatrix matrix = new DenseMatrix();
        // when there isn't a connection between a pair of vertices,
        // set it to zero
        for (int i = 0; i < d.length; i++) {
            for (int k = 0; k < d.length; k++) {
                if (d[i][k] == Float.MAX_VALUE) {
                    d[i][k] = 0;
                }
            }
        }
        for (int k = 0; k < d.length; k++) {
            matrix.addRow(new DenseVector(d[k]));
        }

        // projects
        logger.info("Projecting...");

        AbstractDissimilarity diss = new CosineBased();
        dmat = new DistanceMatrix(matrix, diss);
        dmat.setIds(matrix.getIds());
        dmat.setClassData(matrix.getClassData());
    }

    public void input(@Param(name="graph")Graph graph) {
        this.graph = graph;
    }

    @Return(name="shortest-paths")
    public DistanceMatrix output() {
        return dmat;
    }

    public void reset() {
        graph = null;
        dmat = null;
    }

    public AbstractParametersView getParametersEditor() {
        return null;
    }

    private Graph graph;
    private DistanceMatrix dmat;
    Logger logger = Logger.getLogger(getClass().getName());

}
