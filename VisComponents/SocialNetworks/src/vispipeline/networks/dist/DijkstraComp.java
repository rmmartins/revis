package vispipeline.networks.dist;

import java.io.IOException;
import org.openide.util.lookup.ServiceProvider;
import prefuse.data.Graph;
import vispipeline.basics.annotations.Param;
import vispipeline.basics.annotations.Return;
import vispipeline.basics.annotations.VisComponent;
import vispipeline.basics.interfaces.AbstractComponent;
import vispipeline.basics.interfaces.AbstractParametersView;
import vispipeline.distance.DistanceMatrix;

/**
 *
 * @author Rafael
 */

@VisComponent(hierarchy = "Networks.Distance",
name = "Dijkstra (Shortest-Path)",
description = "Compute a shortest-path distance matrix from a graph.")
@ServiceProvider(service = AbstractComponent.class)

public class DijkstraComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
//        dmat = new DistanceMatrix(graph.getNodeCount());
//        for (int i = 0; i < graph.getNodeCount(); i++) {
//            int[] dist = Dijkstra.dijkstra(graph, i);
//            for (int j = 0; j < dist.length; j++) {
//                dmat.setDistance(i, j, dist[j]);
//            }
//        }

        

    }

    public void input(@Param(name="graph")Graph graph) {
        this.graph = graph;
    }

    @Return(name="shortest-paths")
    public DistanceMatrix output() {
        return dmat;
    }

    public void reset() {
        graph = null;
        dmat = null;
    }

    public AbstractParametersView getParametersEditor() {
        return null;
    }

    private Graph graph;
    private DistanceMatrix dmat;

}
