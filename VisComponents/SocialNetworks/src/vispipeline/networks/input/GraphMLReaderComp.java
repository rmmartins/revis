/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vispipeline.networks.input;

import java.io.IOException;
import java.lang.Integer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.lookup.ServiceProvider;
import prefuse.data.Edge;
import prefuse.data.Graph;
import prefuse.data.io.DataIOException;
import prefuse.data.io.GraphMLReader;
import vispipeline.basics.annotations.Parameter;
import vispipeline.basics.annotations.Return;
import vispipeline.basics.annotations.VisComponent;
import vispipeline.basics.interfaces.AbstractComponent;
import vispipeline.basics.interfaces.AbstractParametersView;
import vispipeline.distance.DistanceMatrix;
import vispipeline.distance.dissimilarity.AbstractDissimilarity;
import vispipeline.distance.dissimilarity.CosineBased;
import vispipeline.matrix.AbstractMatrix;
import vispipeline.matrix.dense.DenseMatrix;
import vispipeline.matrix.dense.DenseVector;

/**
 *
 * @author Rafael
 */
@VisComponent(hierarchy = "Networks.Input",
name = "GraphML Reader",
description = "Reads a network from a GraphML file.")
@ServiceProvider(service = AbstractComponent.class)

public class GraphMLReaderComp implements AbstractComponent {

    public static String lastGraphPath;

    @Override
    public void execute() throws IOException {
        try {
            GraphMLReader reader = new GraphMLReader();
            graph = reader.readGraph(filename);
            lastGraphPath = filename;
        } catch (DataIOException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE,
                    e.getMessage(), e);
        }

        logger.info("Generating adjacency matrix...");
        
//        Map<String, Integer> ids = new HashMap<String, Integer>();
//        int id = 0;
//        for (ArrayList<String> row : gdata.getMatrix()) {
//            ids.put(row.get(0), id++);
//        }
//        ArrayList<ArrayList<String>> relMatrix = gdata.getRelationshipMatrix();
        // creates an adjacency matrix
        int nodeCount = graph.getNodeCount();
        float[][] d = new float[nodeCount][nodeCount];
        for (int i = 0; i < nodeCount; i++) {
            for (int j = 0; j < nodeCount; j++) {
                if (i == j) {
                    d[i][j] = 0;
                } else {
                    d[i][j] = Float.MAX_VALUE;
                }
            }
        }

        logger.info("populating the adjacency matrix...");
        int edgeCount = graph.getEdgeCount();
        for (int j = 0; j < edgeCount; j++) {
            Edge e = graph.getEdge(j);
            // get from and to vertices ids
            Integer keyFrom = e.getSourceNode().getRow();
            Integer keyTo = e.getTargetNode().getRow();

            // if it's not set to combine connectivities, but it's to ignore
            // weights, strength equals 1
            Float strength = 1f;

            // if it's not set to combine connectivities neither to ignore
            // weight, gets strength
            strength = (float) e.getInt("weight");

            // if connections isn't directed or if the adjacency matrix is to
            // be used as a distance matrix, d[i][j] must equals d[j][i]
            if (d[keyFrom][keyTo] < Float.MAX_VALUE) {
                d[keyFrom][keyTo] += strength;
            } else {
                d[keyFrom][keyTo] = strength;
            }
            d[keyTo][keyFrom] = d[keyFrom][keyTo];
        }

        // if the weights represent strength, not distance, changes values
        // inverting [min, max] scale

        float max = Float.MIN_VALUE;

        // if the weights represent strength, not distance, changes values
        // inverting [min, max] scale

        // Float.MAX_VALUE's represent pairs of vertices without connection
        // so ignore them (it won't be a connection between them anyway,
        // even after inverting [min, max] scale)
        for (int i = 0; i < nodeCount; i++) {
            for (int k = 0; k < nodeCount; k++) {
                if (d[i][k] < Float.MAX_VALUE && d[i][k] > max) {
                    max = d[i][k];
                }
            }
        }

        // invert scale by making (max - value)
        // as zero's represents the main diagonal, sums 1
        for (int i = 0; i < nodeCount; i++) {
            for (int k = 0; k < nodeCount; k++) {
                if ((i != k) && (d[i][k] < Float.MAX_VALUE)) {
                    d[i][k] = max - d[i][k] + 1;
                }
            }
        }

        // if it's set to use matrix as points, creates a points matrix
        AbstractMatrix matrix = new DenseMatrix();
        // when there isn't a connection between a pair of vertices,
        // set it to zero
        for (int i = 0; i < d.length; i++) {
            for (int k = 0; k < d.length; k++) {
                if (d[i][k] == Float.MAX_VALUE) {
                    d[i][k] = 0;
                }
            }
        }
        for (int k = 0; k < d.length; k++) {
            matrix.addRow(new DenseVector(d[k]));
        }

        // projects
        logger.info("Projecting...");

        AbstractDissimilarity diss = new CosineBased();
        DistanceMatrix dmat = new DistanceMatrix(matrix, diss);

//        dmat.save("c:/users/rafael/desktop/dmat.txt");
    }

    @Return (name="Prefuse Graph")
    public Graph output() {
        return graph;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new GraphMLReaderParamView(this);
        }

        return paramview;
    }

    @Override
    public void reset() {
    }
    @Parameter
    public String filename = "";
    private transient GraphMLReaderParamView paramview;
    private transient Graph graph;

    Logger logger = Logger.getLogger(getClass().getName());
}
