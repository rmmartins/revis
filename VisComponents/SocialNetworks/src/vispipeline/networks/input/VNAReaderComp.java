/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vispipeline.networks.input;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import org.openide.util.lookup.ServiceProvider;
import prefuse.data.Edge;
import prefuse.data.Graph;
import prefuse.data.Node;
import prefuse.data.Table;
import vispipeline.basics.annotations.Parameter;
import vispipeline.basics.annotations.Return;
import vispipeline.basics.annotations.VisComponent;
import vispipeline.basics.interfaces.AbstractComponent;
import vispipeline.basics.interfaces.AbstractParametersView;
import vispipeline.socialnet.graphdata.GraphData;
import vispipeline.socialnet.graphdata.GraphDataFactory;

/**
 *
 * @author Rafael
 */
@VisComponent(hierarchy = "Networks.Input",
name = "VNA Reader",
description = "Reads a network from a VNA file.")
@ServiceProvider(service = AbstractComponent.class)

public class VNAReaderComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        GraphData gdata = GraphDataFactory.getInstance(filename);
        
        relationship = 4; // all edges

        graph = new Graph();
        // Nodes
        Table nodeTable = graph.getNodeTable();
        nodeTable.addColumn("id", String.class);
        nodeTable.addColumn("node_type", int.class);
        nodeTable.addColumn("name", String.class);
        nodeTable.addColumn("weight", int.class);
        nodeTable.addColumn("rank", int.class);
        nodeTable.addColumn("authors", String.class);
        nodeTable.addColumn("year", int.class);
        nodeTable.addColumn("paper_type", String.class);
        Map<String, Integer> ids = new HashMap<String, Integer>();
        int id = 0;
        for (ArrayList<String> row : gdata.getMatrix()) {
            Node node = graph.addNode();
            node.setString("id", row.get(0));
            ids.put(row.get(0), id++);            
            node.setString("name", row.get(2));
            node.setString("authors", row.get(5));
            node.setString("paper_type", row.get(7));
            String type = row.get(1);
            if (type.equals("author")) {
                node.setInt("node_type", 0);
            } else {
                node.setInt("node_type", 1);
            }
            String weight = row.get(3);
            if (!weight.equals("[NA]")) {
                node.setInt("weight", Integer.parseInt(weight));
            }
            String rank = row.get(4);
            if (!rank.equals("[NA]")) {
                node.setInt("rank", Integer.parseInt(rank));
            }
            String year = row.get(6);
            if (!year.equals("[NA]")) {
                node.setInt("year", Integer.parseInt(year));
            }
        }
        // Edges
        Table edgeTable = graph.getEdgeTable();
        edgeTable.addColumn("weight", int.class);
        for (ArrayList<String> row : gdata.getRelationshipMatrix()) {
            int weight = Integer.parseInt(row.get(relationship + 1));
            if (weight > 0) {
                int source = ids.get(row.get(0));
                int target = ids.get(row.get(1));
                Edge e = graph.getEdge(graph.addEdge(source, target));
                e.setInt("weight", weight);
            }
        }
    }

    @Return (name="Prefuse Graph")
    public Graph output() {
        return graph;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new VNAReaderParamView(this);
        }

        return paramview;
    }

    @Override
    public void reset() {
    }
    
    @Parameter
    public String filename = "";
    @Parameter
    public int relationship = 1;

    private transient VNAReaderParamView paramview;
    private transient Graph graph;

    Logger logger = Logger.getLogger(getClass().getName());
}
