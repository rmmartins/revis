/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.networks.input;

import vispipeline.engine.util.AbstractFilter;

/**
 *
 * @author Rafael
 */
public class GraphMLFilter extends AbstractFilter {

    @Override
    public String getDescription() {
        return "GraphML (.graphml)";
    }

    @Override
    public String getFileExtension() {
        return "graphml";
    }

    @Override
    public String getProperty() {
        return "GRAPHML";
    }

}
