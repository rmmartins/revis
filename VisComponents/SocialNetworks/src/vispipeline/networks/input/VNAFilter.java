/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.networks.input;

import vispipeline.engine.util.AbstractFilter;

/**
 *
 * @author Rafael
 */
public class VNAFilter extends AbstractFilter {

    @Override
    public String getDescription() {
        return "VNA graph (.vna)";
    }

    @Override
    public String getFileExtension() {
        return "vna";
    }

    @Override
    public String getProperty() {
        return "VNA";
    }

}
