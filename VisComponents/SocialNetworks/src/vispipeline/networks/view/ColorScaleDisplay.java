/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.networks.view;

import java.awt.Color;
import javax.swing.JPanel;
import vispipeline.visualization.color.ColorScale;

/**
 *
 * @author Rafael
 */
public class ColorScaleDisplay extends JPanel {

    private ColorScale scale;
    private Color color;
    
    public void setScale(ColorScale scale) {
        this.color = null;
        this.scale = scale;
        repaint();
    }

    public void setColor(Color color) {
        this.color = color;
        this.scale = null;
        repaint();
    }

    @Override
    public void paintComponent(java.awt.Graphics g) {
        super.paintComponent(g);

        if (color != null) {
            //Getting the panel dimension - horizontal fill
            java.awt.Dimension size = this.getSize();
            int height = size.height;
            int width = size.width;

            g.setColor(color);
            g.drawRect(0, 0, width, height);
            g.fillRect(0, 0, width, height);
        } else if (scale != null) {
//                minLabel.setForeground(colorTable.getColor(0.0f));
//                maxLabel.setForeground(colorTable.getColor(1.0f));

            //Getting the panel dimension - horizontal fill
            java.awt.Dimension size = this.getSize();
            int height = size.height;
            int width = size.width;

            for (int i = 0; i <= width; i++) {
                float index = ((float) i) / ((float) width);
                g.setColor(scale.getColor(index));
                g.drawRect(i, 0, i, height);
                g.fillRect(i, 0, i, height);
            }
        }
    }

}
