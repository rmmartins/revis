/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.distance.similaritymatrix;

import java.awt.image.BufferedImage;
import vispipeline.visualization.color.ColorTable;
import vispipeline.visualization.model.AbstractInstance;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class SimilarityMatrixInstance extends AbstractInstance {

    public SimilarityMatrixInstance(SimilarityMatrixModel model, int id, int position, float cdata) {
        super(model, id);
        this.position = position;
        this.cdata = cdata;
    }

    public void draw(BufferedImage image, int border) {
        SimilarityMatrixModel smmodel = (SimilarityMatrixModel) model;

        for (int i = 0; i < distances.length; i++) {
            float colorvalue = 0.0f;

            if (smmodel.getMaxDistance() > smmodel.getMinDistance()) {
                colorvalue = (distances[i] - smmodel.getMinDistance()) /
                        (smmodel.getMaxDistance() - smmodel.getMinDistance());
            }

            ColorTable colortable = smmodel.getColorTable();
            ColorTable selcolortable = smmodel.getSelectionColorTable();

            if (selected) {
                image.setRGB(border + position, i, selcolortable.getColor(colorvalue).getRGB());
                image.setRGB(border + i, position, selcolortable.getColor(colorvalue).getRGB());
            } else {
                image.setRGB(border + position, i, colortable.getColor(1.0f - colorvalue).getRGB());
                image.setRGB(border + i, position, colortable.getColor(1.0f - colorvalue).getRGB());
            }
        }
    }

    public void setDistancesToInstances(float[] distances) {
        this.distances = distances;

        SimilarityMatrixModel smmodel = (SimilarityMatrixModel) model;
        float max = smmodel.getMaxDistance();
        float min = smmodel.getMinDistance();

        for (int i = 0; i < distances.length; i++) {
            if (max < distances[i]) {
                max = distances[i];
            }

            if (min > distances[i]) {
                min = distances[i];
            }
        }

        smmodel.setMaxDistance(max);
        smmodel.setMinDistance(min);
    }

    public float getClassData() {
        return cdata;
    }

    public int getPosition() {
        return position;
    }

    private float[] distances;
    private int position;
    private float cdata;
}
