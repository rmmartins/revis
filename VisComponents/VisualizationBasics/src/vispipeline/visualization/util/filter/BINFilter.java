/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.visualization.util.filter;

import vispipeline.engine.util.AbstractFilter;

/**
 *
 * @author Fernando V. Paulovic
 */
public class BINFilter extends AbstractFilter {

    public String getDescription() {
        return "Multidimensional Points File (*.bin)";
    }

    @Override
    public String getProperty() {
        return "POINTS.DIR";
    }

    @Override
    public String getFileExtension() {
        return "bin";
    }

}
