/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.visualization.util.filter;

import vispipeline.engine.util.AbstractFilter;

/**
 *
 * @author Fernando V. Paulovic
 */
public class SCALARFilter extends AbstractFilter {

    public String getDescription() {
        return "Salar file (*.scalar)";
    }

    @Override
    public String getProperty() {
        return "SCALAR.DIR";
    }

    @Override
    public String getFileExtension() {
        return "scalar";
    }

}
