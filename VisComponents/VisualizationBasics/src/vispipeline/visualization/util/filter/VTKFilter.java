/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.visualization.util.filter;

import vispipeline.engine.util.AbstractFilter;

/**
 *
 * @author Fernando V. Paulovic
 */
public class VTKFilter extends AbstractFilter {

    public String getDescription() {
        return "VTK File (*.vtk)";
    }

    @Override
    public String getProperty() {
        return "VTK.DIR";
    }

    @Override
    public String getFileExtension() {
        return "vtk";
    }

}
