/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.visualization.model;

import java.util.ArrayList;
import java.util.logging.Logger;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public abstract class AbstractInstance {

    public AbstractInstance(AbstractModel model, int id) {
        if(model == null) {
            throw new NullPointerException("A model cannot be null.");
        }

        this.model = model;
        this.model.getInstances().add(this);
        this.model.setChanged();
        this.scalars = new ArrayList<Float>();

        this.id = id;
        this.selected = false;
    }

    public int getId() {
        return id;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
//        Logger.getLogger(getClass().getName()).info(
//                "selected instance: " + toString() +"; scalar (" +
//                getModel().getSelectedScalar().getName()+") = " + getScalarValue(
//                getModel().getSelectedScalar()));
        this.selected = selected;
        model.setChanged();
    }

    public AbstractModel getModel() {
        return model;
    }

    public void setModel(AbstractModel model) {
        this.model = model;
    }

    @Override
    public String toString() {
        return Integer.toString(id);
    }

    public void setScalarValue(Scalar scalar, float value) {
        if (scalar != null) {
            int index = model.getScalars().indexOf(scalar);

            if (scalars.size() > index && index > -1) {
//                Logger.getLogger(getClass().getName()).info(
//                                    "scalar "+index+" ("+scalar.getName()+") " +
//                                    "from "+getScalarValue(scalar)+" to "+value);
                scalars.set(index, value);
            } else {
                int size = scalars.size();
                for (int i = 0; i < index - size; i++) {
                    scalars.add(0.0f);
                }
                scalars.add(value);
            }

            scalar.store(value);
        }
    }

    public float getScalarValue(Scalar scalar) {
        if (scalar != null) {
            int index = model.getScalars().indexOf(scalar);

            if (scalars.size() > index && index > -1) {
                return scalars.get(index);
            }
        }

        return 0.0f;
    }

    public float getNormalizedScalarValue(Scalar scalar) {
        if (scalar != null) {
            int index = model.getScalars().indexOf(scalar);

            if (scalars.size() > index && index > -1) {
                float value = scalars.get(index);
                return (value - scalar.getMin()) / (scalar.getMax() - scalar.getMin());
            }
        }

        return 0.0f;
    }

    protected AbstractModel model;
    protected int id;
    protected boolean selected;
    protected ArrayList<Float> scalars;
}
