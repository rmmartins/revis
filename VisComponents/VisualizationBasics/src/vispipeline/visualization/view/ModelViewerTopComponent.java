/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vispipeline.visualization.view;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JToggleButton;
import javax.swing.KeyStroke;
import javax.swing.filechooser.FileFilter;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;
import org.openide.util.ImageUtilities;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import vispipeline.visualization.coordination.AbstractCoordinator;
import vispipeline.visualization.model.Scalar;
import vispipeline.visualization.view.selection.AbstractSelection;
import vispipeline.visualization.view.selection.InstanceSelection;

/**
 * Top component which displays something.
 */
@ConvertAsProperties(dtd = "-//vispipeline.visualization.view//ModelViewerTC//EN",
autostore = false)
public class ModelViewerTopComponent extends TopComponent {

    private static ModelViewerTopComponent instance;
    /** path to the icon used by the component and its open action */
    static final String ICON_PATH = "vispipeline/visualization/view/map.png";
    private static final String PREFERRED_ID = "ModelViewerTCTopComponent";

    protected ModelViewer view;
    private boolean firstSelection = true;
    private boolean firstCoordination = true;
    protected ArrayList<AbstractSelection> selections =
            new ArrayList<AbstractSelection>();
//    private DefaultComboBoxModel scalarComboModel = new DefaultComboBoxModel();

    protected DummyNode dummyNode;
//    protected SaveCookie impl;

    public ModelViewerTopComponent() {
        initComponents();
        setName(NbBundle.getMessage(ModelViewerTopComponent.class, "CTL_ModelViewerTCTopComponent"));
        setToolTipText(NbBundle.getMessage(ModelViewerTopComponent.class, "HINT_ModelViewerTCTopComponent"));
        setIcon(ImageUtilities.loadImage(ICON_PATH, true));
        setActivatedNodes(new Node[]{dummyNode = new DummyNode()});        
    }
    
    public void setModelViewer(ModelViewer view) {        
        this.view = view;
        scrollPanel.setViewportView(view);
        setDisplayName(view.toString());
        addSelection(new InstanceSelection(view));

        // Setup the scalars
        updateScalars(view.model.getSelectedScalar());
        
//        Logger.getLogger(getClass().getName()).info("Passou setModelViewer");
        this.validate();
    }

    protected void disableSaveImage() {
        northPanel.remove(saveImageButton);
    }

    public void updateScalars(Scalar scalar) {
//        scalarComboModel.removeAllElements();
//        for (Scalar s : view.model.getScalars()) {
//            scalarComboModel.addElement(s);
//        }
//        scalarCombo.setModel(scalarComboModel);

        if (scalar != null) {
//            scalarCombo.setSelectedItem(scalar);
            view.model.setSelectedScalar(scalar);
//        } else {
//            scalarCombo.setSelectedItem(view.model.getSelectedScalar());
        }

        view.model.setChanged();
        view.model.notifyObservers();

        updateScalars();

//        Logger.getLogger(getClass().getName()).info(
//                "num scalars: " + scalarCombo.getItemCount());
    }

    protected void updateScalars() {
//        JOptionPane.showMessageDialog(null, "update Scalars" + view.model.getScalars().size());
        // preciso fazer isso pra iniciar um evento de mudança de lookup
        ScalarCookie sc = new ScalarCookie();
        dummyNode.addCookie(sc);
    }

    public JToggleButton addSelection(final AbstractSelection selection) {
       JToggleButton button = null;
        if (selection != null) {
            button = new JToggleButton();
            selectionButtonGroup.add(button);
            button.setIcon(selection.getIcon());
            button.setToolTipText(selection.toString());

            // the first selection is default
            if (firstSelection) {
                button.setSelected(true);
                view.setSelection(selection);
                firstSelection = false;
            }

            button.addActionListener(new java.awt.event.ActionListener() {

                @Override
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    if (view != null) {
                        view.setSelection(selection);
                    }
                    view.getModel().setChanged();
                    view.getModel().notifyObservers();
                }

            });

            button.setPreferredSize(new Dimension(25,25));
            northPanel.add(button);
        }
       return button;
    }

    public void addCoordination(final AbstractSelection selection) {
        if (selection != null) {
            JToggleButton button = new JToggleButton();
            coordinationButtonGroup.add(button);
            button.setIcon(selection.getIcon());
            button.setToolTipText(selection.toString());

            // first coordination is default
            if (firstCoordination) {
                firstCoordination = false;
                button.setSelected(true);
                view.setCoordination(selection);
            }

            button.addActionListener(new java.awt.event.ActionListener() {

                @Override
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    if (view != null) {
                        view.setCoordination(selection);
                    }
                }

            });

            button.setPreferredSize(new Dimension(23,23));
            northPanel.add(button);
        }
    }

    public void addCoordinator(AbstractCoordinator coordinator) {
        view.addCoordinator(coordinator);
        addCoordination(coordinator.getSelection(view));
    }

    // Everything below this was auto-generated.

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        selectionButtonGroup = new javax.swing.ButtonGroup();
        coordinationButtonGroup = new javax.swing.ButtonGroup();
        scrollPanel = new javax.swing.JScrollPane();
        northPanel = new javax.swing.JPanel();
        jSeparator1 = new javax.swing.JSeparator();
        saveImageButton = new javax.swing.JButton();

        setLayout(new java.awt.BorderLayout());
        add(scrollPanel, java.awt.BorderLayout.CENTER);

        northPanel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        jSeparator1.setForeground(new java.awt.Color(0, 0, 0));
        northPanel.add(jSeparator1);

        saveImageButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vispipeline/visualization/view/picture_save.png"))); // NOI18N
        saveImageButton.setMnemonic(KeyEvent.VK_S);
        org.openide.awt.Mnemonics.setLocalizedText(saveImageButton, org.openide.util.NbBundle.getMessage(ModelViewerTopComponent.class, "ModelViewerTopComponent.saveImageButton.text")); // NOI18N
        saveImageButton.setToolTipText(org.openide.util.NbBundle.getMessage(ModelViewerTopComponent.class, "ModelViewerTopComponent.saveImageButton.toolTipText")); // NOI18N
        saveImageButton.setPreferredSize(new java.awt.Dimension(25, 25));
        saveImageButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveImageButtonActionPerformed(evt);
            }
        });
        northPanel.add(saveImageButton);

        add(northPanel, java.awt.BorderLayout.PAGE_START);
    }// </editor-fold>//GEN-END:initComponents

    private void saveImageButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveImageButtonActionPerformed
        JFileChooser jfc = new JFileChooser();
        jfc.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                return (f.isDirectory() || f.getName().endsWith(".png"));
            }

            @Override
            public String getDescription() {
                return "PNG Image (.png)";
            }
        });
        jfc.setAcceptAllFileFilterUsed(false);
        jfc.setFileSelectionMode(jfc.FILES_ONLY);

        int res = jfc.showSaveDialog(view);
        if (res == jfc.APPROVE_OPTION) {
            File f = jfc.getSelectedFile();
            try {
                if (!f.getName().endsWith(".png")) {
                    f = new File(f.getAbsolutePath() + ".png");
                }
                view.saveToPngImageFile(f.getAbsolutePath());
            }
            catch (IOException e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(null, e);
            }
        }
    }//GEN-LAST:event_saveImageButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup coordinationButtonGroup;
    private javax.swing.JSeparator jSeparator1;
    protected javax.swing.JPanel northPanel;
    private javax.swing.JButton saveImageButton;
    private javax.swing.JScrollPane scrollPanel;
    private javax.swing.ButtonGroup selectionButtonGroup;
    // End of variables declaration//GEN-END:variables
    /**
     * Gets default instance. Do not use directly: reserved for *.settings files only,
     * i.e. deserialization routines; otherwise you could get a non-deserialized instance.
     * To obtain the singleton instance, use {@link #findInstance}.
     */
    public static synchronized ModelViewerTopComponent getDefault() {
        if (instance == null) {
            instance = new ModelViewerTopComponent();
        }
        return instance;
    }

    /**
     * Obtain the ModelViewerTCTopComponent instance. Never call {@link #getDefault} directly!
     */
    public static synchronized ModelViewerTopComponent findInstance() {
        TopComponent win = WindowManager.getDefault().findTopComponent(PREFERRED_ID);
        if (win == null) {
            Logger.getLogger(ModelViewerTopComponent.class.getName()).warning(
                    "Cannot find " + PREFERRED_ID + " component. It will not be located properly in the window system.");
            return getDefault();
        }
        if (win instanceof ModelViewerTopComponent) {
            return (ModelViewerTopComponent) win;
        }
        Logger.getLogger(ModelViewerTopComponent.class.getName()).warning(
                "There seem to be multiple components with the '" + PREFERRED_ID
                + "' ID. That is a potential source of errors and unexpected behavior.");
        return getDefault();
    }

    @Override
    public int getPersistenceType() {
        return TopComponent.PERSISTENCE_ALWAYS;
    }

    @Override
    public void componentOpened() {
        // TODO add custom code on component opening
    }

    @Override
    public void componentClosed() {
        // TODO add custom code on component closing
    }

    void writeProperties(java.util.Properties p) {
        // better to version settings since initial version as advocated at
        // http://wiki.apidesign.org/wiki/PropertyFiles
        p.setProperty("version", "1.0");
        // TODO store your settings
    }

    Object readProperties(java.util.Properties p) {
        if (instance == null) {
            instance = this;
        }
        instance.readPropertiesImpl(p);
        return instance;
    }

    private void readPropertiesImpl(java.util.Properties p) {
        String version = p.getProperty("version");
        // TODO read your settings according to their version
    }

    @Override
    protected String preferredID() {
        return PREFERRED_ID;
    }

    public void setCookie(Node.Cookie cookie) {
        dummyNode.addCookie(cookie);
    }

    protected class DummyNode extends AbstractNode {

        public DummyNode() {
            super(Children.LEAF);
        }

        //We will call this method, i.e., dummyNode.fire(),
        //from a document listener set on the text field:
        public void fire(boolean modified) {
            if (modified) {
                //If the text is modified,
                //we add the SaveCookie implementation to the cookieset:
//                if (impl != null)
//                    getCookieSet().assign(SaveCookie.class, impl);
            } else {
                //Otherwise, we make no assignment
                //and the SaveCookie is not made available:
//                getCookieSet().assign(SaveCookie.class);
            }
        }

        public void addCookie(Node.Cookie cookie) {
            getCookieSet().assign(cookie.getClass(), cookie);
        }
    }

    public class ScalarCookie implements Node.Cookie {

        public Scalar[] getScalars() {
            return view.model.getScalars().toArray(new Scalar[0]);
        }

        public int getSelected() {
            return view.model.getScalars().indexOf(view.model.getSelectedScalar());
        }

        public void update(int scalar) {
            if (scalar >= 0) {
                updateScalars(view.model.getScalars().get(scalar));
            }
        }
    }
}
