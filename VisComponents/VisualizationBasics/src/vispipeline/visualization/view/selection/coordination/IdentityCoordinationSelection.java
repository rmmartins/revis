/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.visualization.view.selection.coordination;

import vispipeline.visualization.view.selection.AbstractSelection;
import vispipeline.visualization.coordination.AbstractCoordinator;
import vispipeline.visualization.model.*;
import java.util.ArrayList;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import vispipeline.visualization.coordination.IdentityCoordinator;
import vispipeline.visualization.view.ModelViewer;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class IdentityCoordinationSelection extends AbstractSelection {

    public IdentityCoordinationSelection(ModelViewer viewer) {
        super(viewer);
    }

    @Override
    public void selected(ArrayList<AbstractInstance> selinst) {
        if (viewer.getModel() != null) {            
            for (AbstractCoordinator ac : viewer.getCoordinators()) {
                if (ac instanceof IdentityCoordinator) {
//                    Logger.getLogger(getClass().getName()).info(
//                            "ac = " + ac.toString());
                    ac.coordinate(new ArrayList<AbstractInstance>(selinst), null);
                }
            }

            viewer.getModel().setSelectedInstances(selinst);
            viewer.getModel().notifyObservers();
        }
    }

    @Override
    public ImageIcon getIcon() {
        return new ImageIcon(
                getClass().getResource("arrow_refresh.png"));
    }

    @Override
    public String toString() {
        return "Identity Coordination Selection";
    }

}
