/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.visualization.view.selection;

import java.util.ArrayList;
import javax.swing.ImageIcon;
import vispipeline.visualization.model.AbstractInstance;
import vispipeline.visualization.view.ModelViewer;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class InstanceSelection extends AbstractSelection {

    public InstanceSelection(ModelViewer viewer) {
        super(viewer);
    }

    @Override
    public void selected(ArrayList<AbstractInstance> selinst) {
        if (viewer.getModel() != null) {
            viewer.getModel().setSelectedInstances(selinst);
            viewer.getModel().notifyObservers();
        }
    }

    @Override
    public ImageIcon getIcon() {
        return new ImageIcon(getClass().getResource("shape_square_edit.png"));        
    }

    @Override
    public String toString() {
        return "Instance Selection";
    }

}
