/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vispipeline.visualization.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JPanel;
import vispipeline.visualization.color.ColorScalePanel;
import vispipeline.visualization.coordination.AbstractCoordinator;
import vispipeline.visualization.model.AbstractInstance;
import vispipeline.visualization.model.AbstractModel;
import vispipeline.visualization.model.Scalar;
import vispipeline.visualization.view.selection.AbstractSelection;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public abstract class ModelViewer extends JPanel implements Observer {

    public static final String COMPONENT = "component";
    public static final String MENU = "menu";

    public ModelViewer() {
        this.coordinators = new ArrayList<AbstractCoordinator>();
        this.selcolor = java.awt.Color.RED;
        this.setBackground(java.awt.Color.WHITE);

        this.addMouseMotionListener(new MouseMotionListener());
        this.addMouseListener(new MouseClickedListener());

        this.setLayout(new FlowLayout(FlowLayout.LEFT));
    }

    public void setModel(AbstractModel model) {
        if (this.model != null) {
            this.model.deleteObserver(this);

            for (int i = 0; i < coordinators.size(); i++) {
                coordinators.get(i).deleteModel(this.model);
            }
        }

        this.model = model;

        if (model != null) {
            model.addObserver(this);
        }

        repaint();
    }

    public AbstractModel getModel() {
        return model;
    }

    public void addCoordinator(AbstractCoordinator coordinator) {
        if (coordinator != null) {
            if (!coordinators.contains(coordinator)) {
                coordinators.add(coordinator);
                coordinator.addModel(model);
            }
        }
    }

    public ArrayList<AbstractCoordinator> getCoordinators() {
        return coordinators;
    }

    @Override
    protected void paintComponent(Graphics g) {
//        Logger.getLogger(getClass().getName()).info("paintComponent");
        super.paintComponent(g);        
        Graphics2D g2 = (Graphics2D)g;
        draw(g2);
        //Draw the rectangle to select the instances
        if (selsource != null && seltarget != null) {
            int x = selsource.x;
            int width = width = seltarget.x - selsource.x;

            int y = selsource.y;
            int height = seltarget.y - selsource.y;

            if (selsource.x > seltarget.x) {
                x = seltarget.x;
                width = selsource.x - seltarget.x;
            }

            if (selsource.y > seltarget.y) {
                y = seltarget.y;
                height = selsource.y - seltarget.y;
            }
            g2.setColor(selcolor);
            g2.drawRect(x, y, width, height);

            g2.setComposite(java.awt.AlphaComposite.getInstance(
                    java.awt.AlphaComposite.SRC_OVER, 0.45f));
            g2.fillRect(x, y, width, height);
        } else { //Draw the instance label
            if (showinstancelabel && label != null && labelpos != null) {
                g2.setFont(this.getFont());
                java.awt.FontMetrics metrics = g2.getFontMetrics(g2.getFont());

                //Getting the label size
                int width = metrics.stringWidth(label);
                int height = metrics.getAscent();

                g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 0.75f));
                g2.setPaint(java.awt.Color.WHITE);
                g2.fillRect(labelpos.x - 2, labelpos.y - height, width + 4, height + 4);
                g2.setComposite(java.awt.AlphaComposite.getInstance(java.awt.AlphaComposite.SRC_OVER, 1.0f));

                g2.setColor(java.awt.Color.DARK_GRAY);
                g2.drawRect(labelpos.x - 2, labelpos.y - height, width + 4, height + 4);

                //Drawing the label
                g2.drawString(label, labelpos.x, labelpos.y);
            }
        }

        //drawn the selection polygon
        if (selpolygon != null) {
            g2.setColor(selcolor);
            g2.drawPolygon(selpolygon);

            g2.setComposite(java.awt.AlphaComposite.getInstance(
                    java.awt.AlphaComposite.SRC_OVER, 0.45f));
            g2.fillPolygon(selpolygon);
        }
    }

    public abstract void draw(Graphics2D g);

    public void saveToPngImageFile(String filename) throws IOException {
        try {
//            Dimension size = ((ProjectionModel) model).getSize();
            Dimension size = getSize();
            BufferedImage buffer = new BufferedImage(size.width + 10, size.height + 10,
                    BufferedImage.TYPE_INT_RGB);
            paint(buffer.getGraphics());
            ImageIO.write(buffer, "png", new File(filename));
        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
    }    

    public void setSelection(AbstractSelection selection) {
        this.selection = selection;
        if (model != null)
            model.notifyObservers();
    }

    public void setCoordination(AbstractSelection selection) {
        this.coordination = selection;
    }

    public void colorAs(Scalar scalar) {
        if (model != null) {
            model.setSelectedScalar(scalar);
            model.notifyObservers();
        }
    }

    public void zoomIn() {
        if (model != null) {
            zoom(1.1f);

            //Change the size of the panel according to the projection
//            Dimension size = ((ProjectionModel) model).getSize();
//            setPreferredSize(new Dimension(size.width * 2, size.height * 2));
//            setSize(new Dimension(size.width * 2, size.height * 2));

            model.notifyObservers();
        }
    }

    public void zoomOut() {
        if (model != null) {
            zoom(0.9091f);

            //Change the size of the panel according to the projection
//            Dimension size = ((ProjectionModel) model).getSize();
//            setPreferredSize(new Dimension(size.width * 2, size.height * 2));
//            setSize(new Dimension(size.width * 2, size.height * 2));

            model.notifyObservers();
        }
    }

    public abstract void zoom(float rate);

    public void adjustPanel() {
//        float iniX = ((ProjectionInstance) model.getInstances().get(0)).getX();
//        float iniY = ((ProjectionInstance) model.getInstances().get(0)).getY();
//        float max_x = iniX, max_y = iniX;
//        float min_x = iniY, min_y = iniY;
//        int zero = 30;
//
//        for (int i = 1; i < model.getInstances().size(); i++) {
//            float x = ((ProjectionInstance) model.getInstances().get(i)).getX();
//            if (max_x < x) {
//                max_x = x;
//            } else if (min_x > x) {
//                min_x = x;
//            }
//
//            float y = ((ProjectionInstance) model.getInstances().get(i)).getY();
//            if (max_y < y) {
//                max_y = y;
//            } else if (min_y > y) {
//                min_y = y;
//            }
//        }
//
//        for (AbstractInstance ai : model.getInstances()) {
//            ProjectionInstance pi = (ProjectionInstance) ai;
//            pi.setX(pi.getX() + zero - min_x);
//            pi.setY(pi.getY() + zero - min_y);
//        }
//
//        Dimension d = this.getSize();
//        d.width = (int) max_x + zero;
//        d.height = (int) max_y + zero;
//        setSize(d);
//        setPreferredSize(d);
//
//        model.notifyObservers();
    }

    class MouseMotionListener extends MouseMotionAdapter {

        @Override
        public void mouseMoved(java.awt.event.MouseEvent evt) {
            super.mouseMoved(evt);

            if (model != null) {
                AbstractInstance instance =
                        getInstanceByPosition(evt.getPoint());

                if (instance != null) {
                    //Show the instance label
                    label = instance.toString();

                    if (label.trim().length() > 0) {
                        if (label.length() > 100) {
                            label = label.substring(0, 96) + "...";
                        }

                        labelpos = evt.getPoint();
                        repaint();
                    }
                } else {
                    //Clear the label
                    label = null;
                    labelpos = null;
                    repaint();
                }
            }
        }

        @Override
        public void mouseDragged(java.awt.event.MouseEvent evt) {
            if (selinstance != null) {
//                if (model.hasSelectedInstances()) {
//                    float x = evt.getX() - selinstance.getX();
//                    float y = evt.getY() - selinstance.getY();
//
//                    for (AbstractInstance ai : model.getSelectedInstances()) {
//                        AbstractInstance pi = (AbstractInstance) ai;
//                        pi.setX(x + pi.getX());
//                        pi.setY(y + pi.getY());
//                    }
//
//                    adjustPanel();
//                }
            } else if (selsource != null) {
                seltarget = evt.getPoint();
            } else if (selpolygon != null) {
                selpolygon.addPoint(evt.getX(), evt.getY());
            }

            repaint();
        }
    }

    class MouseClickedListener extends MouseAdapter {

        @Override
        public void mouseClicked(java.awt.event.MouseEvent evt) {
            super.mouseClicked(evt);

            if (evt.getButton() == java.awt.event.MouseEvent.BUTTON1) {
                if (model != null) {
                    AbstractInstance instance = getInstanceByPosition(evt.getPoint());                    
//                    changeStatus("Number of Instances in Selection: " + 0);
//                    if (instance != null) {
//                        changeStatus("Number of Instances in Selection: " + 1);
//                    Logger.getLogger(getClass().getName()).info("selected instance: " + instance);
                        model.setSelectedInstance(instance);
                        model.notifyObservers();
                        if (getSelection() != null) {                            
                            getSelection().selected(model.getSelectedInstances());
                        }
                        if (getCoordination() != null) {                            
                            getCoordination().selected(model.getSelectedInstances());
                        }
//                    }
                }
            } else if (evt.getButton() == java.awt.event.MouseEvent.BUTTON3) {
                cleanSelectedInstances();
                changeStatus("Number of Instances in Selection: " + 0);
            }
        }

        @Override
        public void mousePressed(java.awt.event.MouseEvent evt) {
            super.mousePressed(evt);

            if (evt.getButton() == java.awt.event.MouseEvent.BUTTON1) {
                if (model != null) {
                    AbstractInstance instance = getInstanceByPosition(evt.getPoint());

                    if (instance != null) {
                        if (moveinstances) {
                            if (model.getSelectedInstances().contains(instance)) {
                                selinstance = instance;
                            }
                        }
                    } else {
                        selsource = evt.getPoint();
                    }
                }
            } else if (evt.getButton() == java.awt.event.MouseEvent.BUTTON3) {
                selpolygon = new java.awt.Polygon();
                selpolygon.addPoint(evt.getX(), evt.getY());
            }
        }

        @Override
        public void mouseReleased(java.awt.event.MouseEvent evt) {
            super.mouseReleased(evt);

            if (model != null) {
                if ((selsource != null && seltarget != null) || selpolygon != null) {
                    ArrayList<AbstractInstance> instances = null;

                    if (selpolygon != null) {
                        instances = getSelectedInstances(selpolygon);
                    } else {
                        instances = getSelectedInstances(selsource, seltarget);
                    }

                    if (instances != null) {
                        float perc = 100.0f * (instances.size() / ((float) model.getInstances().size()));
                        DecimalFormat df = new DecimalFormat("0.##");
                        changeStatus("Number of Instances in Selection: " + instances.size() + " (" + df.format(perc) + "%)");

                        if (selection != null) {
                            selection.selected(new ArrayList<AbstractInstance>(instances));
                        }
                        if (coordination != null) {
                            coordination.selected(new ArrayList<AbstractInstance>(instances));
                        }
                    }
                }
            }

            selpolygon = null;
            selinstance = null;
            selsource = null;
            seltarget = null;
            
        }
    }

    public void cleanSelectedInstances() {
        if (model != null) {
            model.cleanSelectedInstances();
            model.notifyObservers();
        }
    }

    public void removeSelectedInstances() {
        if (model != null) {
            model.removeSelectedInstances();
            model.notifyObservers();
        }
    }

    public ArrayList<AbstractInstance> getSelectedInstances(Polygon polygon) {
        ArrayList<AbstractInstance> selected = new ArrayList<AbstractInstance>();

        if (model != null) {
            selected = getInstancesByPosition(polygon);
        }

        return selected;
    }

    public ArrayList<AbstractInstance> getSelectedInstances(Point source, Point target) {
        ArrayList<AbstractInstance> selinstances = new ArrayList<AbstractInstance>();

        if (model != null) {
            int x = Math.min(source.x, target.x);
            int width = Math.abs(source.x - target.x);

            int y = Math.min(source.y, target.y);
            int height = Math.abs(source.y - target.y);

            Rectangle rect = new Rectangle(x, y, width, height);
            selinstances = getInstancesByPosition(rect);
        }

        return selinstances;
    }

    public void changeStatus(String status) {
//        this.status_jLabel.setText(status);
//        this.status_jLabel.update(this.status_jLabel.getGraphics());
//        Rectangle r = this.status_jLabel.getGraphicsConfiguration().getBounds();
//        //this.status_jLabel.getGraphics().fillRect(r.x, r.y, r.width, r.height);
//        this.status_jLabel.getGraphics().clearRect(r.x, r.y, r.width, r.height);
//        this.status_jLabel.update(this.status_jLabel.getGraphics());
    }

    @Override
    public void setBackground(Color bg) {
        super.setBackground(bg);

        if (this.colorscale != null) {
            this.colorscale.setBackground(bg);
        }
    }

    public void removeInstancesWithScalar(float val) {
        if (model != null) {
            ArrayList<AbstractInstance> insts = new ArrayList<AbstractInstance>();
            Scalar scalar = model.addScalar("cdata");
            for (AbstractInstance ai : model.getInstances()) {
                if (ai.getScalarValue(scalar) == val) {
                    insts.add(ai);
                }
            }

            model.removeInstances(insts);
            model.notifyObservers();
        }
    }

    public abstract AbstractInstance getInstanceByPosition(Point p);

    public abstract ArrayList<AbstractInstance> getInstancesByPosition(Polygon p);

    public abstract ArrayList<AbstractInstance> getInstancesByPosition(Rectangle r);

    public boolean isMoveInstances() {
        return moveinstances;
    }

    public void setMoveInstance(boolean moveinstances) {
        this.moveinstances = moveinstances;
    }

    /**
     * This method is called when the model is changed. It can (must) be used to
     * re-create the visual representation of the model.
     */    
    public void update(Observable o, Object arg) {
        if (model != null) {
//            cleanImage();
            repaint();            
        }
    }

    public AbstractSelection getSelection() {
        return selection;
    }

    public AbstractSelection getCoordination() {
        return coordination;
    }

    protected ArrayList<AbstractCoordinator> coordinators;
    protected AbstractModel model;
    protected Color selcolor;
    protected boolean showinstancelabel = true;
    protected boolean moveinstances = true;
    protected Point selsource;
    protected Point seltarget;
    protected AbstractInstance selinstance;
    protected String label;
    protected Point labelpos;
    protected Polygon selpolygon;
    protected AbstractSelection selection;
    protected AbstractSelection coordination;
    protected ColorScalePanel colorscale;
    
}
