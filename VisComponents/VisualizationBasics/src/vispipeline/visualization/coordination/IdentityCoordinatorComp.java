/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.visualization.coordination;

import java.io.IOException;
import org.openide.util.lookup.ServiceProvider;
import vispipeline.basics.annotations.VisComponent;
import vispipeline.basics.interfaces.AbstractParametersView;
import vispipeline.basics.interfaces.AbstractComponent;

/**
 *
 * @author Fernando Vieira Paulovich
 */
@VisComponent(hierarchy = "Coordination",
name = "Identity Coordination",
description = "Create an identity coordination object.")
@ServiceProvider(service=AbstractComponent.class)
public class IdentityCoordinatorComp extends CoordinatorComp
        implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        coord = new IdentityCoordinator();
    }
    
}
