/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.visualization.coordination;

import vispipeline.visualization.model.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Logger;
import vispipeline.visualization.view.ModelViewer;
import vispipeline.visualization.view.selection.AbstractSelection;
import vispipeline.visualization.view.selection.coordination.IdentityCoordinationSelection;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class IdentityCoordinator extends AbstractCoordinator {

    @Override
    public void coordinate(ArrayList<AbstractInstance> selins, Object parameter) {
        if (selins.size() > 0) {
            //creating an index to speed-up the coordination process
            HashMap<Integer, AbstractInstance> selids = new HashMap<Integer, AbstractInstance>();
            for (AbstractInstance sel : selins) {
                selids.put(sel.getId(), sel);
            }
            
            for (AbstractModel am : models) {
                if (am != selins.get(0).getModel()) {
                    ArrayList<AbstractInstance> selcoord = new ArrayList<AbstractInstance>();
                    ArrayList ins = am.getInstances();
                    for (int i = 0; i < ins.size(); i++) {
                        AbstractInstance ai = (AbstractInstance) ins.get(i);
                        if (selids.keySet().contains(ai.getId())) {
//                            Logger.getLogger(getClass().getName()).info(
//                                    "found instance");
                            selcoord.add(ai);
                            // gambiarra!!! não deveria estar aqui!!!
                            Scalar scalar = selins.get(0).getModel().getSelectedScalar();
                            float value = selids.get(ai.getId()).getScalarValue(scalar);
//                            Logger.getLogger(getClass().getName()).info(
//                                    "value1="+value+",value2="+ai.getScalarValue(scalar));
                            if (ai.getScalarValue(scalar) != value) {
//                                Logger.getLogger(getClass().getName()).info(
//                                    "changing value");
                                ai.setScalarValue(scalar, value);
                            }
                        }
                    }
                    am.setSelectedInstances(selcoord);                    
                }
            }
        } else {
            for (AbstractModel am : models) {
                am.cleanSelectedInstances();
            }
        }
        
        for (AbstractModel am : models) {
            am.notifyObservers();
        }
    }

    @Override
    public AbstractSelection getSelection(ModelViewer view) {
        return new IdentityCoordinationSelection(view);
    }

}
