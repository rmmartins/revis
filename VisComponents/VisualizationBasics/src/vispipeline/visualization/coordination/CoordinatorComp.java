/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.visualization.coordination;

import java.io.IOException;
import org.openide.util.lookup.ServiceProvider;
import vispipeline.basics.annotations.Return;
import vispipeline.basics.annotations.VisComponent;
import vispipeline.basics.interfaces.AbstractComponent;
import vispipeline.basics.interfaces.AbstractParametersView;

/**
 *
 * @author Rafael
 */
@VisComponent(hierarchy = "Coordination",
name = "Coordination",
description = "Coordination object.")
@ServiceProvider(service=AbstractComponent.class)
public class CoordinatorComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {

    }

    @Return(name = "Coordinator") public AbstractCoordinator output() {
        return coord;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        return null;
    }

    @Override
    public void reset() {
        coord = null;
    }

    protected transient AbstractCoordinator coord;

    public static final long serialVersionUID = 1L;

}
