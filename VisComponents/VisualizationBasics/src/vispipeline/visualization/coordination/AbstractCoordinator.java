/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.visualization.coordination;

import vispipeline.visualization.model.*;
import java.util.ArrayList;
import vispipeline.visualization.view.ModelViewer;
import vispipeline.visualization.view.selection.AbstractSelection;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public abstract class AbstractCoordinator {

    public AbstractCoordinator() {
        models = new ArrayList<AbstractModel>();
    }

    public abstract void coordinate(ArrayList<AbstractInstance> selins, Object parameter);

    public synchronized void addModel(AbstractModel model) {
        if (model == null) {
            throw new NullPointerException();
        }

        if (!models.contains(model)) {
            models.add(model);
        }
    }

    public synchronized void deleteModel(AbstractModel model) {
        models.remove(model);
    }

    public abstract AbstractSelection getSelection(ModelViewer view);

    protected ArrayList<AbstractModel> models;
    
}
