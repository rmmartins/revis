/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vispipeline.visualization.color;

import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author Danilo Medeiros Eler
 */
@ServiceProvider(service=ColorScale.class)
public class DynamicScale extends ColorScale {

    public DynamicScale() {
        colors = new java.awt.Color[1];
        colors[0] = new java.awt.Color(200, 200, 200);
    }

    public int addColor(java.awt.Color color) {
        index++;
        java.awt.Color aux[] = new java.awt.Color[index+1];
        for (int i=0; i<colors.length; i++){
            aux[i] = colors[i];
        }
        colors = aux;
        colors[index] = color;
        return index;
    }

    @Override
    public String toString() {
        return "Dynamic Scale";
    }

    private int index = 0;
}
