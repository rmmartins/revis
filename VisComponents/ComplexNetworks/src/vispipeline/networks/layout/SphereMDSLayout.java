/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vispipeline.networks.layout;

import java.util.HashMap;
import java.util.Map;
import prefuse.data.Graph;
import prefuse.data.Node;
import toxi.geom.Vec3D;
import vispipeline.distance.DistanceMatrix;

/**
 *
 * @author Rafael
 */
public class SphereMDSLayout implements SphereLayout {

    public void layout(Graph graph, DistanceMatrix dmat) {
        this.graph = graph;
        this.dmat = dmat;

        pointMap = new HashMap<Node, Vec3D>();

        // initial random layout
        RandomLayout random = new RandomLayout();
        random.layout(graph, dmat);
        for (int i = 0; i < graph.getNodeCount(); i++) {
            Node node = graph.getNode(i);
            pointMap.put(node, random.getPoint(node));
        }

        // save initial stress
        currentStress = fullStress();

        counter = 0;
        // minimize stress
        for (int i = 0; i < num_iter; i++) {           
            iterate();
        }
        
    }

    void overrideLayout(Map<Node, Vec3D> pointMap) {
        this.pointMap = pointMap;
    }

    void save() {        
        if (graph.getNodeTable().getColumn("mds_x") == null) {
            graph.getNodeTable().addColumn("mds_x", float.class);
        }

        if (graph.getNodeTable().getColumn("mds_y") == null) {
            graph.getNodeTable().addColumn("mds_y", float.class);
        }

        if (graph.getNodeTable().getColumn("mds_z") == null) {
            graph.getNodeTable().addColumn("mds_z", float.class);
        }

        for (int i = 0; i < graph.getNodeCount(); i++) {
            Node n = graph.getNode(i);
            Vec3D v = pointMap.get(n);
            n.setFloat("mds_x", v.x);
            n.setFloat("mds_y", v.y);
            n.setFloat("mds_z", v.z);
        }
    }

    private double fullStress() {
        double num = 0.0;
        double den = 0.0;
        for (int i = 0; i < dmat.getElementCount() - 1; i++) {
            Node n1 = graph.getNode(i);
            for (int j = i + 1; j < dmat.getElementCount(); j++) {
                Node n2 = graph.getNode(j);
                // the max distance is the sphere diameter
                double norm_dist =
                        (pointMap.get(n1).distanceTo(pointMap.get(n2)))
                        / (2 * radius);
                den += (norm_dist * norm_dist);
                // also normalize the original distance
                double diff = norm_dist - (dmat.getDistance(i, j)
                        / (dmat.getMaxDistance() - dmat.getMinDistance()));
                num += (diff * diff);
            }
        }
        double stress = Double.MAX_VALUE;
        if (den != 0) {
            stress = Math.sqrt(num / den);
        }
        return stress;
    }

    private double partialNumFor(int index) {
        double num = 0.0;
        Node n1 = graph.getNode(index);
        for (int j = 0; j < dmat.getElementCount(); j++) {
            if (j != index) {
                Node n2 = graph.getNode(j);
                // the max distance is the sphere diameter
                double norm_dist =
                        (pointMap.get(n1).distanceTo(pointMap.get(n2)))
                        / (2 * radius);
                // also normalize the original distance
                double diff = norm_dist - (dmat.getDistance(index, j)
                        / (dmat.getMaxDistance() - dmat.getMinDistance()));
                num += (diff * diff);
            }
        }
        return num;
    }

    private double partialDenFor(int index) {
        double den = 0.0;
        Node n1 = graph.getNode(index);
        for (int j = 0; j < dmat.getElementCount(); j++) {
            if (j != index) {
                Node n2 = graph.getNode(j);
                // the max distance is the sphere diameter
                double norm_dist =
                        (pointMap.get(n1).distanceTo(pointMap.get(n2)))
                        / (2 * radius);
                den += (norm_dist * norm_dist);
            }
        }
        return den;
    }

    private double partialNumWithout(int index) {
        double num = 0.0;
        for (int i = 0; i < dmat.getElementCount() - 1; i++) {
            if (i != index) {
                Node n1 = graph.getNode(i);
                for (int j = i + 1; j < dmat.getElementCount(); j++) {
                    if (j != index) {
                        Node n2 = graph.getNode(j);
                        // the max distance is the sphere diameter
                        double norm_dist =
                                (pointMap.get(n1).distanceTo(pointMap.get(n2)))
                                / (2 * radius);
                        // also normalize the original distance
                        double diff = norm_dist - (dmat.getDistance(i, j)
                                / (dmat.getMaxDistance() - dmat.getMinDistance()));
                        num += (diff * diff);
                    }
                }
            }
        }
        return num;
    }

    private double partialDenWithout(int index) {
        double den = 0.0;
        for (int i = 0; i < dmat.getElementCount() - 1; i++) {
            if (i != index) {
                Node n1 = graph.getNode(i);
                for (int j = i + 1; j < dmat.getElementCount(); j++) {
                    if (j != index) {
                        Node n2 = graph.getNode(j);
                        // the max distance is the sphere diameter
                        double norm_dist =
                                (pointMap.get(n1).distanceTo(pointMap.get(n2)))
                                / (2 * radius);
                        den += (norm_dist * norm_dist);
                    }
                }
            }
        }
        return den;
    }

    // move points to the direction that minimizes stress
    public void iterate() {
        counter++;
        System.out.println("iteration #" + counter);
        double num = 0.0, den = 0.0;
        for (int i = 0; i < graph.getNodeCount(); i++) {
            // for each node
            Node n1 = graph.getNode(i);
            // save its current position
            Vec3D before = pointMap.get(n1);
            // and the partial stress without it
            num = partialNumWithout(i);
            den = partialDenWithout(i);
            // and consider going in the direction of each other node
            for (int j = 0; j < graph.getNodeCount(); j++) {
                if (i != j) {
                    Node n2 = graph.getNode(j);
                    Vec3D dest = pointMap.get(n2);
                    float x = before.x + step * (dest.x - before.x);
                    float y = before.y + step * (dest.y - before.y);
                    float z = before.z + step * (dest.z - before.z);
                    // project into the sphere surface
                    Vec3D after = new Vec3D(x, y, z).toSpherical();
                    after.x = radius;
                    after.toCartesian();
                    pointMap.put(n1, after);
                    // get the new stress; only i-distances change
                    double i_num = partialNumFor(i);
                    double i_den = partialDenFor(i);
                    double newStress = Math.sqrt((num + i_num) / (den + i_den));
                    if (newStress < currentStress) {
                        currentStress = newStress;
                        before = after;
                    } else {
                        // or else go back to what it was
                        pointMap.put(n1, before);
                    }
                }
            }
        }
    }

    public Vec3D getPoint(Node node) {
        return pointMap.get(node);
    }

    private Graph graph;
    private DistanceMatrix dmat;
    private Map<Node, Vec3D> pointMap;
    private float radius = 200.0f;

    private double currentStress = Double.MAX_VALUE;
    private float step = 0.01f;
    private int num_iter = 50;

    private int counter;

}
