package vispipeline.networks.layout;

import java.util.HashMap;
import java.util.Map;
import prefuse.data.Graph;
import prefuse.data.Node;
import toxi.geom.Vec3D;
import vispipeline.distance.DistanceMatrix;

/**
 *
 * @author Rafael
 */
public class SphereMapLayout implements SphereLayout {

    public SphereMapLayout() {
        this.nriterations = 0;
        this.fractionDelta = 200.0f;
    }

    public void layout(Graph graph, DistanceMatrix dmat) {
        this.graph = graph;
        this.dmat = dmat;
        
        //Create the indexes and shuffle them
//        ArrayList<Integer> index_aux = new ArrayList<Integer>();
//        for (int i = 0; i < graph.getNodeCount(); i++) {
//            index_aux.add(i);
//        }

        // não tenho a mínima idéia ????
//        this.index = new int[graph.getNodeCount()];
//        for (int ind = 0, j = 0; j < this.index.length; ind += index_aux.size() / 10, j++) {
//            if (ind >= index_aux.size()) {
//                ind = 0;
//            }
//
//            this.index[j] = index_aux.get(ind);
//            index_aux.remove(ind);
//        }

        radius = 200.0f;        

        //if an input aux_proj is not provided, create one
        FastmapSphereLayout fm = new FastmapSphereLayout();
        fm.layout(graph, dmat);
        for (int i = 0; i < graph.getNodeCount(); i++) {
            Node n = graph.getNode(i);
            Vec3D v = fm.getPoint(n);
            pointMap.put(graph.getNode(i), new Vec3D(v.x, v.y, v.z));
        }

        for (int i = 0; i < nriterations; i++) {
//            System.out.println("SphereMDS Iteration " + i);
            iterate();
        }

    }

    public void iterate() {
        //for each instance
        for (int ins1 = 0; ins1 < pointMap.size(); ins1++) {
//            System.out.println("ins1 = " + ins1);
            Node n1 = graph.getNode(ins1);
            float x1 = pointMap.get(n1).x;
            float y1 = pointMap.get(n1).y;
            float z1 = pointMap.get(n1).z;

            //for each other instance
            for (int ins2 = 0; ins2 < pointMap.size(); ins2++) {
//                System.out.println("ins2 = " + ins2);
                if (ins1 == ins2) {
                    continue;
                }

                Node n2 = graph.getNode(ins2);
                float x2 = pointMap.get(n2).x;
                float y2 = pointMap.get(n2).y;
                float z2 = pointMap.get(n2).z;

                //distance between projected instances
                double x1x2 = (x2 - x1);
                double y1y2 = (y2 - y1);
                double z1z2 = (z2 - z1);
                double dr2 = Math.sqrt(x1x2 * x1x2 + y1y2 * y1y2 + z1z2 * z1z2);

                if (dr2 < EPSILON) {
                    dr2 = EPSILON;
                }

                float drn = dmat.getDistance(ins2, ins1);
                float normdrn = drn; //(drn - dmat.getMinDistance()) /
                        //(dmat.getMaxDistance() - dmat.getMinDistance());

                //Calculating the (fraction of) delta
                double delta = Math.sqrt(normdrn) - Math.sqrt(dr2);
                delta *= Math.abs(delta);
                delta /= this.fractionDelta;

//                System.out.println("delta = " + delta);

                //moving ins2 -> ins1
                x2 += delta * (x1x2 / dr2);
                y2 += delta * (y1y2 / dr2);
                z2 += delta * (z1z2 / dr2);

                // project into sphere
                Vec3D p = new Vec3D(x2, y2, z2).toSpherical();
                p.x = radius;
                p.toCartesian();

                pointMap.get(n2).x = p.x;
                pointMap.get(n2).y = p.y;
                pointMap.get(n2).z = p.z;
            }
        }
    }

    public Vec3D getPoint(Node node) {
        return pointMap.get(node);
    }

    private float fractionDelta;
    private int[] index;
    private int nriterations;
    private static final float EPSILON = 0.0000001f;

    private Map<Node, Vec3D> pointMap = new HashMap<Node, Vec3D>();
    private Graph graph;
    private DistanceMatrix dmat;

    private float radius;

    

}
