/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vispipeline.networks.view;

import java.awt.Color;
import processing.core.*;
import toxi.geom.*;
import java.awt.event.*;
import java.util.List;
import javax.media.opengl.GL;
import prefuse.data.Edge;
import prefuse.data.Graph;
import prefuse.data.Node;
import processing.opengl.PGraphicsOpenGL;
import vispipeline.networks.layout.SphereLayout;
import vispipeline.networks.layout.SphereMDSLayout;
import vispipeline.visualization.color.ColorScale;
import vispipeline.visualization.color.PseudoRainbowScale;

/**
 *
 * @author Rafael
 */
public class SphereModelViewer extends PApplet {

    // main parameters    
    int sphereAlpha = 200;
    float sphereRadius = 200;
    boolean debug = true;
    Color backgroundColor = Color.BLACK;
    Color sphereColor = new Color(10, 10, 10);
    boolean nodesEnabled = true;
    boolean edgesEnabled = true;
    boolean moveSelect = true;
    int labelAttrib = -1;
    boolean nodeSpheres = true;
    boolean bezierEdges = false;
    boolean bezierSelect = true;
    
    ColorScale nodeScale = new PseudoRainbowScale();
    int nodeColorAttrib = -1;
    Class nodeColorAttribType = float.class;
    float nodeColorAttMin = 0, nodeColorAttMax = 1;

    ColorScale edgeScale = new PseudoRainbowScale();
    int edgeColorAttrib = -1;
    Class edgeColorAttribType = float.class;
    float edgeColorAttMin = 0, edgeColorAttMax = 1;
    boolean directed = true;
    Color targetColor = Color.WHITE;
    
    int minRadius = 5;
    int maxRadius = 20;
    int sizeAttrib = -1;
    Class sizeAttribType = float.class;
    float nodeSizeAttMin = 0, nodeSizeAttMax = 1;

    int minThick = 1;
    int maxThick = 5;
    int thickAttrib = -1;
    Class thickAttribType = float.class;
    float thickAttMin = 0, thickAttMax = 1;

    // simple global variables
    float smallRadius;
    float cx, cy, cz;
    float currmx, currmy;
    float dx, dy;
    float anglex = 0, angley = 0;
    float zoom = 0;
    float rad = 200;
    boolean first = true;
    int selected = -1;
    
    float[][] screenCoords;
    float[] baseDist;
    // needed objects
    MouseWheelEventDemo wheel;
    Graph graph;
    SphereLayout layout;
    // graph variables
    int vsize = 0;
    int esize = 0;
    float cps[][];
    // OpenGL
    PGraphicsOpenGL pgl;
    GL gl;
    PGraphics buffer;
    int bufferWidth, bufferHeight;

    public void setModel(Graph graph, SphereLayout layout) {
        this.graph = graph;
        this.layout = layout;

        if (graph.getNodeTable().getColumn("selected") == null) {
            graph.getNodeTable().addColumn("selected", boolean.class, false);
        }

        vsize = graph.getNodeCount();
        esize = graph.getEdgeCount();
        screenCoords = new float[vsize][2];
        baseDist = new float[vsize];

        sphereRadius = 200.0f;

        updateControlPoints();
//        updateSmallRadius();
        smallRadius = 10;

        directed = graph.isDirected();
    }

    public void setNodeColorScale(int colorAttrib, ColorScale colorScale) {
        this.nodeColorAttrib = colorAttrib;
        this.nodeScale = colorScale;
        if (colorAttrib == -1) {
            nodeColorAttribType = float.class;
        } else {
//            System.out.printf("index: %d, type: %s\n", colorAttrib, graph.getNodeTable().getColumnType(colorAttrib));
//            Column col = graph.getNodeTable().getColumn(colorAttrib);
//            System.out.println("canGetFloat: " + col.canGetFloat());
//            System.out.println("canGetInt: " + col.canGetInt());
//            System.out.println("canGetDouble: " + col.canGetDouble());
//            System.out.println("canGetString: " + col.canGetString());
            // min max
            nodeColorAttribType = graph.getNodeTable().getColumnType(colorAttrib);
            nodeColorAttMin = Float.MAX_VALUE;
            nodeColorAttMax = Float.MIN_VALUE;
            for (int i = 0; i < graph.getNodeCount(); i++) {
                float value = 0.0f;
                Node n = graph.getNode(i);
                if (nodeColorAttribType.equals(double.class)) {
                    value = (float) n.getDouble(colorAttrib);
                } else {
                    value = n.getFloat(colorAttrib);
                }
                if (value < nodeColorAttMin) {
                    nodeColorAttMin = value;
                }
                if (value > nodeColorAttMax) {
                    nodeColorAttMax = value;
                }
            }
//            System.out.printf("min: %f, max: %f\n", min, max);    
        }
    }

    public void setEdgeColorScale(int colorAttrib, ColorScale colorScale) {
        this.edgeColorAttrib = colorAttrib;
        this.edgeScale = colorScale;
        if (colorAttrib == -1) {
            edgeColorAttribType = float.class;
        } else {
//            System.out.printf("index: %d, type: %s\n", colorAttrib, graph.getNodeTable().getColumnType(colorAttrib));
//            Column col = graph.getNodeTable().getColumn(colorAttrib);
//            System.out.println("canGetFloat: " + col.canGetFloat());
//            System.out.println("canGetInt: " + col.canGetInt());
//            System.out.println("canGetDouble: " + col.canGetDouble());
//            System.out.println("canGetString: " + col.canGetString());
            // min max
            edgeColorAttribType = graph.getEdgeTable().getColumnType(colorAttrib);
            edgeColorAttMin = Float.MAX_VALUE;
            edgeColorAttMax = Float.MIN_VALUE;
            for (int i = 0; i < graph.getEdgeCount(); i++) {
                float value = 0.0f;
                Edge e = graph.getEdge(i);
                if (edgeColorAttribType.equals(double.class)) {
                    value = (float) e.getDouble(colorAttrib);
                } else {
                    value = e.getFloat(colorAttrib);
                }
                if (value < edgeColorAttMin) {
                    edgeColorAttMin = value;
                }
                if (value > edgeColorAttMax) {
                    edgeColorAttMax = value;
                }
            }
//            System.out.printf("min: %f, max: %f\n", min, max);
        }
    }

    public void setSizeScalar(int sizeAttrib) {
        this.sizeAttrib = sizeAttrib;

        if (sizeAttrib == -1) {
            sizeAttribType = float.class;
        } else {
            // min max
            sizeAttribType = graph.getNodeTable().getColumnType(sizeAttrib);
            nodeSizeAttMin = Float.MAX_VALUE;
            nodeSizeAttMax = Float.MIN_VALUE;
            for (int i = 0; i < graph.getNodeCount(); i++) {
                float value = 0.0f;
                Node n = graph.getNode(i);
                if (sizeAttribType.equals(double.class)) {
                    value = (float) n.getDouble(sizeAttrib);
                } else {
                    value = n.getFloat(sizeAttrib);
                }
                if (value < nodeSizeAttMin) {
                    nodeSizeAttMin = value;
                }
                if (value > nodeSizeAttMax) {
                    nodeSizeAttMax = value;
                }
            }
//            System.out.printf("min: %f, max: %f\n", min, max);
        }
    }

    public void setThicknessAttrib(int thickAttrib) {
        this.thickAttrib = thickAttrib;

        if (thickAttrib == -1) {
            thickAttribType = float.class;
        } else {
            // min max
            thickAttribType = graph.getEdgeTable().getColumnType(thickAttrib);
            thickAttMin = Float.MAX_VALUE;
            thickAttMax = Float.MIN_VALUE;
            for (int i = 0; i < graph.getEdgeCount(); i++) {
                float value = 0.0f;
                Edge e = graph.getEdge(i);
                if (thickAttribType.equals(double.class)) {
                    value = (float) e.getDouble(thickAttrib);
                } else {
                    value = e.getFloat(thickAttrib);
                }
                if (value < thickAttMin) {
                    thickAttMin = value;
                }
                if (value > thickAttMax) {
                    thickAttMax = value;
                }
            }
//            System.out.printf("min: %f, max: %f\n", min, max);
        }
    }

//    private void updateSmallRadius() {
//        if (vsize > 0) {
//            smallRadius = 500 / vsize;
//        }
//        if (smallRadius < 1) {
//            smallRadius = 1;
//        }
//        if (smallRadius > 10) {
//            smallRadius = 10;
//        }
//    }
    private void updateControlPoints() {
        cps = new float[esize][6];
        for (int i = 0; i < esize; i++) {
            Edge e = graph.getEdge(i);
            int v1 = e.getSourceNode().getRow();
            int v2 = e.getTargetNode().getRow();
            Node n1 = graph.getNode(v1);
            Node n2 = graph.getNode(v2);

            Vec3D vec1 = layout.getPoint(n1);
            float x1 = vec1.x;
            float y1 = vec1.y;
            float z1 = vec1.z;

            Vec3D vec2 = layout.getPoint(n2);
            float x2 = vec2.x;
            float y2 = vec2.y;
            float z2 = vec2.z;

            float u = x2 - x1;
            float v = y2 - y1;
            float w = z2 - z1;
            float dist3 = dist(x1, y1, z1, x2, y2, z2);

            float t = 0.5f;
            float ax = (x1 + (t * u));
            float ay = (y1 + (t * v));
            float az = (z1 + (t * w));
            float dist1 = dist(0, 0, 0, ax, ay, az);
            float dist2 = sphereRadius - dist1;

            //t = (dist2 / dist1) * 1.5f;
            t = (dist3 / dist1) / 1.5f;

            float cx1 = (x1 + (t * ax));
            float cy1 = (y1 + (t * ay));
            float cz1 = (z1 + (t * az));
            cps[i][0] = cx1;
            cps[i][1] = cy1;
            cps[i][2] = cz1;

            float cx2 = (x2 + (t * ax));
            float cy2 = (y2 + (t * ay));
            float cz2 = (z2 + (t * az));
            cps[i][3] = cx2;
            cps[i][4] = cy2;
            cps[i][5] = cz2;
        }
    }

    @Override
    public void setup() {
        size(600, 600, OPENGL);
        buffer = createGraphics(width, height, P3D);
        bufferWidth = width;
        bufferHeight = height;

        cx = width / 2;
        cy = height / 2;
        cz = -100.0f;

        wheel = new MouseWheelEventDemo();
    }

    @Override
    public void draw() {
        background(backgroundColor.getRGB());
        lights();

        translate(cx, cy, cz + zoom);
        rotateY(radians(anglex));
        rotateX(radians(angley));

        if (selected > -1 && labelAttrib > -1) {
            pushMatrix();
            Node n = graph.getNode(selected);
            Vec3D v = layout.getPoint(n);
            float selx = v.x;
            float sely = v.y;
            float selz = v.z;
            translate(selx, sely, selz);
            rotateX(radians(-angley));
            rotateY(radians(-anglex));
            fill(255);
            stroke(255);
            float z = 100, x, y;
            float sx = screenX(0, 0, 0);
            float sy = screenY(0, 0, 0);
            if (sx < (width / 2)) {
                x = -50;
            } else {
                x = 50;
            }
            if (sy < (height / 2)) {
                y = -50;
            } else {
                y = 50;
            }
            stroke(100);
            line(0, 0, 0, x, y, z);
            translate(x, y, z);
            fill(0);

            Object label = n.get(labelAttrib);
            String txt = String.valueOf(label);

            float w = textWidth(txt);
            float h = 15;
            box(w + 10, h, 1);
            fill(200, 200, 0);
            textAlign(CENTER, CENTER);
            text(txt, 0, 0, 1);
            popMatrix();
        }

        // draw nodes
        noStroke();
        int c;
        Node n;
        float x, y, z;
        if (nodesEnabled) {
            for (int i = 0; i < vsize; i++) {
                pushMatrix();
                n = graph.getNode(i);
                Vec3D v = layout.getPoint(n);
                x = v.x;
                y = v.y;
                z = v.z;

                // set node color
                float colorValue = nodeColorAttMin;
                if (nodeColorAttrib >= 0) {
                    if (nodeColorAttribType.equals(double.class)) {
                        colorValue = (float) n.getDouble(nodeColorAttrib);
                    } else {
                        colorValue = n.getFloat(nodeColorAttrib);
                    }
                }
                colorValue = (colorValue - nodeColorAttMin) / (nodeColorAttMax - nodeColorAttMin);
                Color nodeColor = nodeScale.getColor(colorValue);
                c = color(nodeColor.getRed(), nodeColor.getGreen(), nodeColor.getBlue());
                fill(c, 255);

                // set node size
                float sizeValue = nodeSizeAttMin;
                if (sizeAttrib >= 0) {
                    if (sizeAttribType.equals(double.class)) {
                        sizeValue = (float) n.getDouble(sizeAttrib);
                    } else {
                        sizeValue = n.getFloat(sizeAttrib);
                    }
                }
                smallRadius = (sizeValue - nodeSizeAttMin) / (nodeSizeAttMax - nodeSizeAttMin)
                        * (maxRadius - minRadius) + minRadius;


                if (nodeSpheres) {
                    // draw sphere:
                    translate(x, y, z);
                    sphereDetail(10);
                    sphere(smallRadius);

                    // save base distance
                    pushMatrix();
                    rotateX(radians(-angley));
                    rotateY(radians(-anglex));
                    float sx = screenX(0, 0, 0);
                    float sy = screenY(0, 0, 0);
                    float bsx = screenX(smallRadius, 0, 0);
                    baseDist[i] = dist(sx, sy, bsx, sy);
                    popMatrix();

                    // glow
                    if (i == selected) {
                        // change blend mode
                        if (getSketchRenderer().equals("OPENGL")) {
                            pgl = (PGraphicsOpenGL) g; // g may change
                            gl = pgl.beginGL(); // ho ho fancy
                            pgl.beginGL();
//                // This fixes the overlap issue
//                gl.glDisable(GL.GL_DEPTH_TEST);
//                // Turn on the blend mode
                            gl.glEnable(GL.GL_BLEND);
//                // Define the blend mode
                            gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE);
//                gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
                            pgl.endGL();
                        }

                        c = color(255, 255, 255);
                        for (int j = 0; j < 10; j++) {
                            int alph = 40 - (int) (j * (20.0f / 10.0f));
                            fill(c, alph);
                            sphere(smallRadius + j * (5.0f / 8.0f));
                        }

                        if (getSketchRenderer().equals("OPENGL")) {
//                // change everything back
                            pgl.beginGL();
//                // This fixes the overlap issue
                            gl.glEnable(GL.GL_DEPTH_TEST);
//                // Turn on the blend mode
                            gl.glEnable(GL.GL_BLEND);
//                // Define the blend mode
                            gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
                            pgl.endGL();
                        }
                    }

                } else {
                    //draw box:
                    Vec3D s = new Vec3D(x, y, z).toSpherical();
                    float azimuth = s.y;
                    float theta = s.z;
                    rotateY(-azimuth);
                    rotateZ(theta);
                    translate(200 + (smallRadius/3.0f), 0, 0);
                    box(smallRadius, minRadius, minRadius);
                    if (i == selected) {
                        c = color(255, 255, 255);
                        for (int j = 0; j < 10; j++) {
                            int alph = 75 - (int) (j * (30.0f / 10.0f));
                            fill(c, alph);
                            float d = j * (5.0f / 8.0f);
                            box(smallRadius + d, minRadius + d, minRadius + d);
                        }
//                        selx = values[i][0];
//                        sely = values[i][1];
//                        selz = values[i][2];
                    }
                }
                popMatrix();
            }
        }

        if (edgesEnabled) {
            int v1, v2;
//        float x1, y1, z1, x2, y2, z2;
//        float cx1, cy1, cz1, cx2, cy2, cz2;
            Edge e;
            for (int i = 0; i < esize; i++) {
                pushMatrix();

                e = graph.getEdge(i);
                v1 = (int) e.getSourceNode().getRow();
                v2 = (int) e.getTargetNode().getRow();

                if (selected > -1 && v1 != selected && v2 != selected) {
                    popMatrix();
                    continue;
                }

                noFill();
                stroke(255, 255, 255);

                // set edge color
                float colorValue = edgeColorAttMin;
                if (edgeColorAttrib >= 0) {
                    if (edgeColorAttribType.equals(double.class)) {
                        colorValue = (float) e.getDouble(edgeColorAttrib);
                    } else {
                        colorValue = e.getFloat(edgeColorAttrib);
                    }
                }
                colorValue = (colorValue - edgeColorAttMin) / (edgeColorAttMax - edgeColorAttMin);
                Color edgeColor = edgeScale.getColor(colorValue);
                c = color(edgeColor.getRed(), edgeColor.getGreen(), edgeColor.getBlue());

                int from = c;
                int to = from;
                if (directed) {
                    to = color(targetColor.getRed(), targetColor.getGreen(),
                            targetColor.getBlue());
                }

                // set edge thickness
                float thickValue = thickAttMin;
                if (thickAttrib >= 0) {
                    if (thickAttribType.equals(double.class)) {
                        thickValue = (float) e.getDouble(thickAttrib);
                    } else {
                        thickValue = e.getFloat(thickAttrib);
                    }
                }
                thickValue = (thickValue - thickAttMin) / (thickAttMax - thickAttMin);
                float thickness = thickValue * (maxThick - minThick) + minThick;
                strokeWeight(thickness);

                if (bezierEdges || (bezierSelect && selected > -1)) {
                    gradBezier(i, from, to);
                } else {
                    gradArc(i, from, to);
                }

//    if (debug) {
//      stroke(255, 0, 0);
//      line(x1, y1, z1, x2, y2, z2);
//      float t = 0.5;
//      float u = x2 - x1;
//      float v = y2 - y1;
//      float w = z2 - z1;
//      float ax = (x1 + (t * u));
//      float ay = (y1 + (t * v));
//      float az = (z1 + (t * w));
//      stroke(0, 255, 0);
//      line(0, 0, 0, ax, ay, az);
//      line(x1, y1, z1, cx1, cy1, cz1);
//      line(x2, y2, z2, cx2, cy2, cz2);
//    }

                popMatrix();
            }
        }

        noStroke();
        int color = color(sphereColor.getRed(), sphereColor.getGreen(), sphereColor.getBlue());
        fill(color, sphereAlpha);

        sphereDetail(25);
        sphere(sphereRadius * 0.98f);

//        for (int i = 0; i < vsize; i++) {
//            Vec3D point = layout.getPoint(graph.getNode(i));
//            screenCoords[i][0] = screenX(point.x, point.y, point.z);
//            screenCoords[i][1] = screenY(point.x, point.y, point.z);
//            float sz = screenZ(point.x, point.y, point.z);
//            float bsx = screenX(point.x + smallRadius, point.y, point.z);
//            baseDist[i] = dist(screenCoords[i][0], screenCoords[i][1], sz, bsx,
//                    screenCoords[i][1], sz);
//        }

        drawPickingBuffer();
    }

    @Override
    public void mousePressed() {
        currmx = mouseX;
        currmy = mouseY;
    }

    @Override
    public void mouseDragged() {
        dx = (mouseX - currmx) / width * 360;
        dy = (mouseY - currmy) / height * 360;
        anglex = (anglex + dx) % 360;
        angley = (angley - dy) % 360;
        currmx = mouseX;
        currmy = mouseY;
    }

    @Override
    public void mouseClicked() {
        if (!moveSelect) {
            doSelection();
        }
    }

    @Override
    public void mouseMoved() {
        if (moveSelect) {
            doSelection();
        }
    }

    void doSelection2() {
        for (int i = 0; i < vsize; i++) {
            Node n = graph.getNode(i);
            Vec3D v = layout.getPoint(n);
            float sx = screenX(v.x, v.y, v.z);
            float sy = screenY(v.x, v.y, v.z);
            float rdist = dist(sx, sy, mouseX, mouseY);
            if (rdist <= baseDist[i]) {
                selected = i;
                graph.getNode(i).setBoolean("selected", true);
                return;
            } else {
                graph.getNode(i).setBoolean("selected", false);
            }
        }
        selected = -1;
    }

    void doSelection() {
        //float ajuste = ((float)mouseY / (float)height) * (float)20;
        //System.out.printf("ajuste %f\n", ajuste);
        // get the pixel color under the mouse
        int pick = buffer.get(mouseX, (int) (mouseY));
        // get object id
        int id = getId(pick);
        // if id > 0 (background id = -1)
        if (id >= 0) {
            // change the cube color
            selected = id;
        } else {
            selected = -1;
        }
    }

    void drawPickingBuffer() {
//        System.out.printf("width %d height %d\n", getWidth(), getHeight());
//        System.out.flush();
//        if (bufferWidth != width || bufferHeight != height) {
//            buffer = createGraphics(width, height, P3D);
//            bufferWidth = width;
//            bufferHeight = height;
//        }
        // draw the scene in the buffer
        buffer.beginDraw();
        buffer.background(getColor(-1)); // since background is not an object, its id is -1
        buffer.noStroke();

        buffer.translate(cx, cy, cz + zoom);
        buffer.rotateY(radians(anglex));
        buffer.rotateX(radians(angley));
        buffer.pushMatrix();

        for (int i = 0; i < vsize; i++) {
            Node n = graph.getNode(i);
            Vec3D point = layout.getPoint(n);
            buffer.pushMatrix();
            int idColor = getColor(i);
            buffer.fill(idColor);
            buffer.translate(point.x, point.y, point.z);
            buffer.sphereDetail(10);
            // set size
            float sizeValue = 0;
            if (sizeAttrib >= 0) {
                if (sizeAttribType.equals(double.class)) {
                    sizeValue = (float) n.getDouble(sizeAttrib);
                } else {
                    sizeValue = n.getFloat(sizeAttrib);
                }
            }
            smallRadius = (sizeValue - nodeSizeAttMin) / (nodeSizeAttMax - nodeSizeAttMin)
                    * (maxRadius - minRadius) + minRadius;
            buffer.sphere(smallRadius);
            buffer.popMatrix();
        }

        buffer.popMatrix();
        buffer.endDraw();
    }

    // id 0 gives color -2, etc.
    int getColor(int id) {
        return -(id + 2);
    }

// color -2 gives 0, etc.
    int getId(int c) {
        return -(c + 2);
    }

    public class MouseWheelEventDemo implements MouseWheelListener {

        public MouseWheelEventDemo() {
            addMouseWheelListener(this);
        }

        public void mouseWheelMoved(MouseWheelEvent e) {
            int notches = e.getWheelRotation();
            zoom += (notches * 20);
        }
    }

    @Override
    public void keyPressed() {
        if (key == 'e' || key == 'E') {
            bezierEdges = !bezierEdges;
        }
        if (key == 'i' || key == 'I') {
            ((SphereMDSLayout) layout).iterate();
            updateControlPoints();
        }
        if (key == 'r' || key == 'R') {
            edgesEnabled = !edgesEnabled;
        }
    }

    void gradBezier(int e, int from, int to) {
        Edge edge = graph.getEdge(e);
        int v1 = (int) edge.getSourceNode().getRow();
        int v2 = (int) edge.getTargetNode().getRow();
        Node n1 = graph.getNode(v1);
        Node n2 = graph.getNode(v2);

        Vec3D vec1 = layout.getPoint(n1);
        float x1 = vec1.x;
        float y1 = vec1.y;
        float z1 = vec1.z;

        Vec3D vec2 = layout.getPoint(n2);
        float x2 = vec2.x;
        float y2 = vec2.y;
        float z2 = vec2.z;

        float cx1 = cps[e][0];
        float cy1 = cps[e][1];
        float cz1 = cps[e][2];

        float cx2 = cps[e][3];
        float cy2 = cps[e][4];
        float cz2 = cps[e][5];

        float d = dist(x1, y1, z1, x2, y2, z2) / sphereRadius * 2;
        float steps = 10 + (d * 10);

        beginShape();
        for (int j = 0; j <= steps; j++) {
            float t = (float) (j) / steps;
//            if (graph.isDirected()) {
                stroke(lerpColor(from, to, t));
//            } else {
//                stroke(from);
//            }
            float x = bezierPoint(x1, cx1, cx2, x2, t);
            float y = bezierPoint(y1, cy1, cy2, y2, t);
            float z = bezierPoint(z1, cz1, cz2, z2, t);
            vertex(x, y, z);
        }
        endShape();
    }

    void gradArc(int e, int from, int to) {
        Edge edge = graph.getEdge(e);
        int v1 = (int) edge.getSourceNode().getRow();
        int v2 = (int) edge.getTargetNode().getRow();
        Node n1 = graph.getNode(v1);
        Node n2 = graph.getNode(v2);

        Vec3D vec1 = layout.getPoint(n1);
        float x1 = vec1.x;
        float y1 = vec1.y;
        float z1 = vec1.z;

        Vec3D vec2 = layout.getPoint(n2);
        float x2 = vec2.x;
        float y2 = vec2.y;
        float z2 = vec2.z;

        Vec3D ac = new Vec3D(x1, y1, z1);
        Vec3D bc = new Vec3D(x2, y2, z2);
        float d = (ac.distanceTo(bc) / (sphereRadius * 2)) * 80;
        //println(d);
        List points = new Line3D(ac, bc).splitIntoSegments(
                null, ac.distanceTo(bc) / d, true);
        beginShape();
        for (int j = points.size() - 1; j >= 0; j--) {
            Vec3D p = ((Vec3D) points.get(j)).toSpherical();
            p.x = sphereRadius;
            p.toCartesian();
            float t = j / (float) (points.size());
//            if (graph.isDirected()) {
                stroke(lerpColor(from, to, t));
//            } else {
//                stroke(from);
//            }
            vertex(p.x, p.y, p.z);
        }
        endShape();
    }
}
