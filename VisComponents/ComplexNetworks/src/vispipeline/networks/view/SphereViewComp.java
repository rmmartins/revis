/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vispipeline.networks.view;

import java.awt.Frame;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import org.openide.util.lookup.ServiceProvider;
import prefuse.data.Graph;
import processing.core.PApplet;
import vispipeline.basics.annotations.Param;
import vispipeline.basics.annotations.VisComponent;
import vispipeline.basics.interfaces.AbstractComponent;
import vispipeline.basics.interfaces.AbstractParametersView;
import vispipeline.networks.layout.SphereLayout;

/**
 *
 * @author Rafael
 */
@VisComponent(hierarchy = "Networks.View",
name = "Network Sphere View",
description = "Visualize a network in a sphere.")
@ServiceProvider(service = AbstractComponent.class)

public class SphereViewComp implements AbstractComponent {

    @Override
    public void execute() throws IOException {
        // (Alt.1) Uma JFrame comum:

//        final SphereModelViewer applet = new SphereModelViewer();
//        applet.setModel(graph, layout);
//        applet.init();
//        SphereViewerFrame frame = new SphereViewerFrame();
//        frame.setModelViewer(applet);
//        frame.setLocationRelativeTo(null);
//        frame.setVisible(true);

//        PApplet.main(new String[]{"vispipeline.networks.view.SphereModelViewer"});

        // (Alt. 2) Imitando o main do PApplet:

        GraphicsEnvironment environment =
                GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice displayDevice = environment.getDefaultScreenDevice();
        final Frame frame = new Frame(displayDevice.getDefaultConfiguration());
        frame.setResizable(false);
        // Set the trimmings around the image
        Image image = Toolkit.getDefaultToolkit().createImage(PApplet.ICON_IMAGE);
        frame.setIconImage(image);
        frame.setTitle("Network Sphere View");

        final SphereModelViewer applet = new SphereModelViewer();

        applet.frame = frame;
        frame.setLayout(null);
        frame.add(applet);

        applet.init();

        // Wait until the applet has figured out its width.
        // In a static mode app, this will be after setup() has completed,
        // and the empty draw() has set "finished" to true.
        // TODO make sure this won't hang if the applet has an exception.
        while (applet.defaultSize && !applet.finished) {
            try {
                Thread.sleep(5);

            } catch (InterruptedException e) {
                //System.out.println("interrupt");
            }
        }

        // can't do pack earlier cuz present mode don't like it
      // (can't go full screen with a frame after calling pack)
      //        frame.pack();  // get insets. get more.
      Insets insets = frame.getInsets();

      int windowW = Math.max(applet.width, PApplet.MIN_WINDOW_WIDTH) +
        insets.left + insets.right;
      int windowH = Math.max(applet.height, PApplet.MIN_WINDOW_HEIGHT) +
        insets.top + insets.bottom;

      frame.setSize(windowW, windowH);

      frame.setLocation((applet.screenWidth - applet.width) / 2,
                          (applet.screenHeight - applet.height) / 2);

      int usableWindowH = windowH - insets.top - insets.bottom;
      applet.setBounds((windowW - applet.width)/2,
                       insets.top + (usableWindowH - applet.height)/2,
                       applet.width, applet.height);
            
      frame.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {                
                frame.dispose();
            }
        });

      // handle frame resizing events
      applet.setupFrameResizeListener();

      // all set for rockin
      if (applet.displayable()) {
        frame.setVisible(true);
      }

      applet.setModel(graph, layout);

      ControlFrame controls = new ControlFrame();      
      int xpos = frame.getLocation().x + 620;
      int ypos = frame.getLocation().y;
      controls.setLocation(xpos, ypos);
      controls.setVisible(true);
      controls.setModelViewer(applet, frame);

        // (Alt.3) Dentro do TopComponent:
        
//        SwingUtilities.invokeLater(new Runnable() {
//            public void run() {
//                TopComponent tc = new TopComponent();
//                tc.add(applet);
//                tc.setName("SphereView");
//                tc.open();
//                tc.requestVisible();
//                applet.init();
//                applet.resize(tc.getSize());
//                tc.addPropertyChangeListener(new PropertyChangeListener() {
//                    public void propertyChange(PropertyChangeEvent evt) {
//                        // don't ask.
//                        if (evt.getPropertyName().equals("ancestor")) {
//                            applet.destroy();
//                        }
//                    }
//                });
//            }
//        });

        
    }

    public void input(@Param(name="Prefuse graph") Graph graph,
            @Param(name="layout") SphereLayout layout) {
        this.graph = graph;
        this.layout = layout;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        return null;
    }

    @Override
    public void reset() {
        graph = null;
    }

    private transient Graph graph;
    private transient SphereLayout layout;
}
