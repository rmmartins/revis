package vispipeline.networks.dist;

import java.util.Arrays;
import java.util.Iterator;
import prefuse.data.Graph;
import prefuse.data.Node;

public class Dijkstra {

    // Dijkstra's algorithm to find shortest path from s to all other nodes
    public static int[] dijkstra(Graph G, int s) {
//        System.out.println("Dijkstra, s = " + s);

        final int[] dist = new int[G.getNodeCount()];  // shortest known distance from "s"
        // starts with MAX_VALUE, so that unreachable nodes will have this distance
        Arrays.fill(dist, Integer.MAX_VALUE);
        dist[s] = 0;

        final int[] pred = new int[G.getNodeCount()];  // preceeding node in path

        final boolean[] visited = new boolean[G.getNodeCount()]; // all false initially
        Arrays.fill(visited, false);

        for (int i = 0; i < dist.length; i++) {
            final int next = minVertex(dist, visited);

            // grafo não-conectado:
            if (next == -1) {
                //dist[next] = Integer.MAX_VALUE;
                continue;
            }

            visited[next] = true;

            // The shortest path to next is dist[next] and via pred[next].

            //final int [] n = G.neighbors (next);
            Node next2 = G.getNode(next);
            Iterator iter = next2.neighbors();
            for (int j = 0; iter.hasNext(); j++) {
                final int v = ((Node) iter.next()).getRow();
                //final int d = dist[next] + G.getWeight(next,v);
                final int d = dist[next] + 1;
                if (dist[v] > d) {
                    dist[v] = d;
                    pred[v] = next;
                }
//                System.out.printf("v = %d, dist[v] = %d, pred[v] = %d\n", v, dist[v], pred[v]);
            }
        }
        return dist;
    }

    private static int minVertex(int[] dist, boolean[] v) {
        int x = Integer.MAX_VALUE;
        int y = -1;   // graph not connected, or no unvisited vertices
        for (int i = 0; i < dist.length; i++) {
            if (!v[i] && dist[i] < x) {
                y = i;
                x = dist[i];
            }
        }
        return y;
    }
}
