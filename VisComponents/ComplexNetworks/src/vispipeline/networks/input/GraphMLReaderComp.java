/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vispipeline.networks.input;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.lookup.ServiceProvider;
import prefuse.data.Graph;
import prefuse.data.io.DataIOException;
import prefuse.data.io.GraphMLReader;
import vispipeline.basics.annotations.Parameter;
import vispipeline.basics.annotations.Return;
import vispipeline.basics.annotations.VisComponent;
import vispipeline.basics.interfaces.AbstractComponent;
import vispipeline.basics.interfaces.AbstractParametersView;

/**
 *
 * @author Rafael
 */
@VisComponent(hierarchy = "Networks.Input",
name = "GraphML Reader",
description = "Reads a network from a GraphML file.")
@ServiceProvider(service = AbstractComponent.class)

public class GraphMLReaderComp implements AbstractComponent {

    public static String lastGraphPath;

    @Override
    public void execute() throws IOException {
        try {
            GraphMLReader reader = new GraphMLReader();
            graph = reader.readGraph(filename);
            lastGraphPath = filename;
        } catch (DataIOException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE,
                    e.getMessage(), e);
        }
    }

    @Return (name="Prefuse Graph")
    public Graph output() {
        return graph;
    }

    @Override
    public AbstractParametersView getParametersEditor() {
        if (paramview == null) {
            paramview = new GraphMLReaderParamView(this);
        }

        return paramview;
    }

    @Override
    public void reset() {
    }
    @Parameter
    public String filename = "";
    private transient GraphMLReaderParamView paramview;
    private transient Graph graph;
}
