/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * DistanceMatrixReaderParamView.java
 *
 * Created on 19/06/2009, 17:09:44
 */
package vispipeline.networks.input;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import vispipeline.basics.interfaces.AbstractParametersView;
import vispipeline.engine.util.OpenDialog;

/**
 *
 * @author Fernando Vieira Paulovich
 */
public class GraphMLReaderParamView extends AbstractParametersView {

    /** Creates new form DistanceMatrixReaderParamView */
    public GraphMLReaderParamView(GraphMLReaderComp comp) {
        initComponents();

        this.comp = comp;
        reset();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        filePanel = new javax.swing.JPanel();
        distmatrixButton = new javax.swing.JButton();
        graphmlTextField = new javax.swing.JTextField();
        distmatrixLabel = new javax.swing.JLabel();

        setBorder(javax.swing.BorderFactory.createTitledBorder("GraphML Reader"));
        setLayout(new java.awt.GridBagLayout());

        filePanel.setBorder(javax.swing.BorderFactory.createTitledBorder("GraphML"));
        filePanel.setLayout(new java.awt.GridBagLayout());

        distmatrixButton.setText("Search...");
        distmatrixButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                distmatrixButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        filePanel.add(distmatrixButton, gridBagConstraints);

        graphmlTextField.setColumns(35);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        filePanel.add(graphmlTextField, gridBagConstraints);

        distmatrixLabel.setText("File:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        filePanel.add(distmatrixLabel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        add(filePanel, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void distmatrixButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_distmatrixButtonActionPerformed
//        try {
            JFileChooser jfc = new JFileChooser();
            if (comp.filename != null && !comp.filename.isEmpty()) {
                jfc.setCurrentDirectory(new File(comp.filename));
            }
            jfc.setFileFilter(new GraphMLFilter());
            int result = jfc.showOpenDialog(null);
            if (result == JFileChooser.APPROVE_OPTION) {
                String filename = jfc.getSelectedFile().getAbsolutePath();
                graphmlTextField.setText(filename);
            }
//        } catch (IOException ex) {
//            Logger.getLogger(GraphMLReaderParamView.class.getName()).log(Level.SEVERE, null, ex);
//        }
}//GEN-LAST:event_distmatrixButtonActionPerformed

    @Override
    public void reset() {
        graphmlTextField.setText(comp.filename);
    }

    @Override
    public void finished() throws IOException {
        if (graphmlTextField.getText().trim().length() > 0) {
            comp.filename = graphmlTextField.getText();
        } else {
            throw new IOException("A distance matrix file name must be provided.");
        }
    }

    private GraphMLReaderComp comp;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton distmatrixButton;
    private javax.swing.JLabel distmatrixLabel;
    private javax.swing.JPanel filePanel;
    private javax.swing.JTextField graphmlTextField;
    // End of variables declaration//GEN-END:variables
}
